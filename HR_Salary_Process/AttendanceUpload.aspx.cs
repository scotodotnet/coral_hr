﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using System.IO;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Salary_Process_AttendanceUpload : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    string SessionUserType;
    string SessionEpay;
    string Query = "";

    string TempNFH = "0";
    string TempCL = "0";
    string TempThreeSide = "0";
    string TempFull = "0";
    string TempHome = "0";
    string TempOTNew = "";
    string TempWH_Work_Days = "0";
    string TempFixed_Days = "0";
    string TempNFH_Work_Days = "0";
    string TempNFH_Work_Manual = "0";
    string TempNFH_Work_Station = "0";
    string TempNFH_Count_Days = "0";
    string TempLBH_Count_Days = "0";

    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = "CORAL_ERP_HR";//Session["SessionEpay"].ToString();

        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            Employee_Type_Load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }
    }

    public void Load_TwoDates()
    {
        if (ddlMonths.SelectedValue != "-Select-" && ddlfinance.SelectedValue != "-Select-")
        {

            decimal Month_Total_days = 0;
            string Month_Last = "";
            string Year_Last = "0";
            string Temp_Years = "";
            string[] Years;
            string FromDate = "";
            string ToDate = "";

            Temp_Years = ddlfinance.SelectedItem.Text;
            Years = Temp_Years.Split('-');
            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(Years[0]) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }
            switch (ddlMonths.SelectedItem.Text)
            {
                case "January": Month_Last = "01";
                    break;
                case "February": Month_Last = "02";
                    break;
                case "March": Month_Last = "03";
                    break;
                case "April": Month_Last = "04";
                    break;
                case "May": Month_Last = "05";
                    break;
                case "June": Month_Last = "06";
                    break;
                case "July": Month_Last = "07";
                    break;
                case "August": Month_Last = "08";
                    break;
                case "September": Month_Last = "09";
                    break;
                case "October": Month_Last = "10";
                    break;
                case "November": Month_Last = "11";
                    break;
                case "December": Month_Last = "12";
                    break;
                default:
                    break;
            }

            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "February") || (ddlMonths.SelectedValue == "March"))
            {
                Year_Last = Years[1];
            }
            else
            {
                Year_Last = Years[0];
            }
            FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
            ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

            txtFrom.Text = FromDate.ToString();
            txtTo.Text = ToDate.ToString();

        }
        else
        {
            txtFrom.Text = "";
            txtTo.Text = "";
        }


    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        try
        {

            if (ddlMonths.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //else if ((txtdays.Text.Trim() == "") || (txtdays.Text.Trim() == null))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Working days in a Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            //else if (Convert.ToInt32(txtdays.Text) == 0)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days Properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtNfh.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To NFh Days Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlag)
            {
                int Month_Mid_Total_Days_Count = 0;
                Month_Mid_Total_Days_Count = (int)((Convert.ToDateTime(txtTo.Text) - Convert.ToDateTime(txtFrom.Text)).TotalDays);
                Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                txtdays.Text = Month_Mid_Total_Days_Count.ToString();
                decimal total = (Convert.ToDecimal(txtdays.Text.Trim()));
                int total_check = 0;
                decimal days = 0;
                string from_date_check = txtFrom.Text.ToString();
                string to_date_check = txtTo.Text.ToString();
                from_date_check = from_date_check.Replace("/", "-");
                to_date_check = to_date_check.Replace("/", "-");
                txtFrom.Text = from_date_check;
                txtTo.Text = to_date_check;

                if (FileUpload.HasFile)
                {
                    FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                }

                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                {
                    days = 31;
                }
                else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                {
                    days = 30;
                }

                else if (ddlMonths.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                    if ((yrs % 4) == 0)
                    {
                        days = 29;
                    }
                    else
                    {
                        days = 28;
                    }
                }
                if (total > days)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            SqlConnection cn = new SqlConnection(constr);

                            //Get Employee Master Details

                            

                            DataTable dt_Emp_Mst = new DataTable();
                            string MachineID = dt.Rows[j][0].ToString();
                            string Query_Emp_mst = "";

                            if (MachineID == "1015")
                            {

                            }

                            Query_Emp_mst = "Select ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm, '2' as Wagestype,";
                            Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                            Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "'";
                            Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                            Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";
                            dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                            //Select ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName,'2' as Wagestype,
                            //ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode
                            //where ED.CompCode='ESM'
                            //and ED.LocCode='UNIT I' and ED.IsActive='Yes' 
                            //and ED.MachineID='9';

                            if (dt_Emp_Mst.Rows.Count == 0)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Token No Not Found in Employee Master. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }

                            string Department = dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString();
                            string EmpNo = dt_Emp_Mst.Rows[0]["EmpNo"].ToString();

                            string ExistingCode = dt.Rows[j][1].ToString();
                            string FirstName = dt.Rows[j][2].ToString();
                            string WorkingDays = dt.Rows[j][3].ToString();
                            string H_Allowed = dt.Rows[j][4].ToString();
                            string NFH_Str = dt.Rows[j][5].ToString();
                            string WH_Work_Days = dt.Rows[j][6].ToString();
                            string LWW_Days = dt.Rows[j][7].ToString();
                            string OT_Hrs_Str = dt.Rows[j][8].ToString();
                            string Permission_Hrs = dt.Rows[j][9].ToString();
                            string OT_Days_Str = dt.Rows[j][10].ToString();
                            string Canteen_Days_Minus = dt.Rows[j][11].ToString();
                            string Fixed_W_Days = dt.Rows[j][12].ToString();
                            string Total_Month_Days = dt.Rows[j][13].ToString();
                            string NFH_Work_Days = dt.Rows[j][14].ToString();
                            string NFH_Work_Days_Statutory = dt.Rows[j][15].ToString();
                            
                            if (MachineID == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Machine ID. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (ExistingCode == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (FirstName == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (WorkingDays == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }                            
                            else if (NFH_Str == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the NFH Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (WH_Work_Days == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Week Off Work Days properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (LWW_Days == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the LWW Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (OT_Hrs_Str == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT HOURS. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Permission_Hrs == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Permission Hours. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Fixed_W_Days == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Fixed Work Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Total_Month_Days == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Total Month Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }

                            
                            cn.Open();
                            string qry_empNo = "Select EmpNo from Employee_Mst where EmpNo= '" + EmpNo + "' and ExistingCode = '" + ExistingCode + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                            SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                            if (sdr_Emp.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                        }
                        if (!ErrFlag)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                //Get Employee Master Details
                                DataTable dt_Emp_Mst = new DataTable();
                                string MachineID = dt.Rows[i][0].ToString();
                                string Query_Emp_mst = "";

                                if (MachineID == "1015")
                                {

                                }

                                Query_Emp_mst = "Select ED.Wages, ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.DeptName as DepartmentNm, '2' as Wagestype,";
                                Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID,ED.DeptCode from Employee_Mst ED";
                                Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "'";
                                Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                                Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";
                                dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);
                          
                                WagesType = "2";
                                objatt.Months = ddlMonths.Text;
                                string TempDate = "getdate()";

                                Query_Emp_mst = "Select * from AttenanceDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + MachineID + "'";
                                Query_Emp_mst = Query_Emp_mst + " And Convert(Datetime,FromDate,103) = Convert(Datetime,'" + txtFrom.Text + "',103)";
                                Query_Emp_mst = Query_Emp_mst + " And Convert(Datetime,ToDate,103) = Convert(Datetime,'" + txtTo.Text + "',103)";
                                DataTable DT_Attn_Verfiy = new DataTable();
                                DT_Attn_Verfiy = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                                if (DT_Attn_Verfiy.Rows.Count != 0)
                                {
                                    string Del = "Delete from [" + SessionEpay + "]..AttenanceDetails where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(Datetime,FromDate,105) = Convert(Datetime,'" + txtFrom.Text.Trim() + "', 105)";
                                    objdata.RptEmployeeMultipleDetails(Del);
                                    DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                    DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);

                                    Query = "";
                                    Query = Query + "Insert into AttenanceDetails (DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,";
                                    Query = Query + "TotalDays,Ccode,Lcode,NFh,WorkingDays,CL,AbsentDays,weekoff,FromDate,ToDate,Modeval,home,";
                                    Query = Query + "halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days,";
                                    Query = Query + "NFH_Work_Days_Incentive,NFH_Work_Days_Statutory,AEH,LBH,Permission_Hrs,LWW_Days) values (";
                                    Query = Query + "'" + dt_Emp_Mst.Rows[0]["Deptcode"].ToString() + "','" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString() + "',";
                                    Query = Query + "'" + dt.Rows[i][3].ToString() + "','" + ddlMonths.Text + "','" + ddlfinance.SelectedValue + "',convert(datetime," + TempDate + ",105),";
                                    Query = Query + "'" + dt.Rows[i][13].ToString() + "','" + SessionCcode + "','" + SessionLcode + "','" + dt.Rows[i][5].ToString() + "','" + dt.Rows[i][3].ToString() + "','0',";
                                    Query = Query + "'0','0',convert(datetime,'" + txtFrom.Text + "',105),convert(datetime,'" + txtTo.Text + "',105),'" + WagesType + "','0',";
                                    Query = Query + "'0','0','" + dt.Rows[i][10].ToString() + "','" + dt.Rows[i][8].ToString() + "','" + dt.Rows[i][12].ToString() + "','" + dt.Rows[i][6].ToString() + "','" + dt.Rows[i][14].ToString() + "',";
                                    Query = Query + "'" + dt.Rows[i][14].ToString() + "','" + dt.Rows[i][15].ToString() + "','0','0','" + dt.Rows[i][9].ToString() + "','" + dt.Rows[i][7].ToString() + "')";
                                    objdata.RptEmployeeMultipleDetails(Query);
                                    //objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                }
                                else
                                {
                                    DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                    DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                                    Query = "";
                                    Query = Query + "Insert into AttenanceDetails (DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,";
                                    Query = Query + "TotalDays,Ccode,Lcode,NFh,WorkingDays,CL,AbsentDays,weekoff,FromDate,ToDate,Modeval,home,";
                                    Query = Query + "halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days,";
                                    Query = Query + "NFH_Work_Days_Incentive,NFH_Work_Days_Statutory,AEH,LBH,Permission_Hrs,LWW_Days) values (";
                                    Query = Query + "'" + dt_Emp_Mst.Rows[0]["Deptcode"].ToString() + "','" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString() + "',";
                                    Query = Query + "'" + dt.Rows[i][3].ToString() + "','" + ddlMonths.Text + "','" + ddlfinance.SelectedValue + "',convert(datetime," + TempDate + ",105),";
                                    Query = Query + "'" + dt.Rows[i][13].ToString() + "','" + SessionCcode + "','" + SessionLcode + "','" + dt.Rows[i][5].ToString() + "','" + dt.Rows[i][3].ToString() + "','0',";
                                    Query = Query + "'0','0',convert(datetime,'" + txtFrom.Text + "',105),convert(datetime,'" + txtTo.Text + "',105),'" + WagesType + "','0',";
                                    Query = Query + "'0','0','" + dt.Rows[i][10].ToString() + "','" + dt.Rows[i][8].ToString() + "','" + dt.Rows[i][12].ToString() + "','" + dt.Rows[i][6].ToString() + "','" + dt.Rows[i][14].ToString() + "',";
                                    Query = Query + "'" + dt.Rows[i][14].ToString() + "','" + dt.Rows[i][15].ToString() + "','0','0','" + dt.Rows[i][9].ToString() + "','" + dt.Rows[i][7].ToString() + "')";
                                    objdata.RptEmployeeMultipleDetails(Query);

                                    //objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                }
                                
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                sSourceConnection.Close();
                            }

                            //System.Windows.Forms.MessageBox.Show("Uploaded SuccessFully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

                        }
                        if (ErrFlag == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                            //System.Windows.Forms.MessageBox.Show("Your File Not Uploaded", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }

    protected void btnAttn_Format_Download_Click(object sender, EventArgs e)
    {
        try
        {

            if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }

            else if (ddlEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else
            {
                string staff_lab = "";
                string Employee_Type = "";
                if (ddlCategory.SelectedValue == "1")
                {
                    staff_lab = "S";
                }
                else if (ddlCategory.SelectedValue == "2")
                {
                    staff_lab = "L";
                }
                Employee_Type = ddlEmployeeType.SelectedValue.ToString();
                DataTable dt = new DataTable();
                Query = "";
                Query = Query + " Select EM.EmpNo,(EM.FirstName) as EmpName,(EM.MachineID) as MachineNo,(EM.ExistingCode) as ExisistingCode";
                Query = Query + " from Employee_Mst EM";
                Query = Query + " Where EM.LocCode='" + SessionLcode + "' and EM.CompCode='" + SessionCcode + "' and EM.IsActive='Yes' and ";                
                Query = Query + " EM.Wages='" + ddlEmployeeType.SelectedItem.Text.ToString() + "'";
                Query = Query + " Order by EM.ExistingCode Asc";

                dt = objdata.RptEmployeeMultipleDetails(Query);

                gvEmp.DataSource = dt;
                gvEmp.DataBind();

                if (dt.Rows.Count > 0)
                {
                    string attachment = "attachment;filename=Attn_Upload.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvEmp.RenderControl(htextw);
                    //gvDownload.RenderControl(htextw);
                    //Response.Write("Contract Details");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    private void Employee_Type_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        query = "select EmpType from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpType";
        ddlEmployeeType.DataBind();
    }
}
