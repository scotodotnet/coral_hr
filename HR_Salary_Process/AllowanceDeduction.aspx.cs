﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Salary_Process_AllowanceDeduction : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string[] iStr3;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Allowance And Deduction";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_AllowenceAndDeductionDet();
            Fin_Year_Add();
            Load_WagesType();
            Load_Data_EmpDet();
        }
        Load_Data();
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpTypeCd";
        ddlWages.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtMachineID.Items.Clear();
        query = "Select CONVERT(varchar(10), EmpNo) as EmpNo from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtMachineID.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["EmpNo"] = "-Select-";
        dr["EmpNo"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtMachineID.DataTextField = "EmpNo";
        txtMachineID.DataValueField = "EmpNo";
        txtMachineID.DataBind();
    }

    protected void txtMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
        if (txtMachineID.SelectedValue != "-Select-")
        {
            query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And Wages='" + ddlWages.SelectedItem.Text + "' And EmpNo='" + txtMachineID.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                txtTokenNo.Text = DT.Rows[0]["ExistingCode"].ToString();
                txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();

            }
            else
            {
                txtMachineID.SelectedValue = "-Select-";
                txtEmpName.Text = "";
                txtTokenNo.Text = "";

            }
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtTokenNo.Text = "";
        }
    }

    public void Fin_Year_Add()
    {
        int CurrentYear;
        int i;
        //Financial Year Add
        CurrentYear = DateTime.Now.Year;
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        else
        {
            CurrentYear = CurrentYear;
        }

        ddlFinYear.Items.Clear();
        ddlFinYear.Items.Add("-Select-");

        for (i = 0; i < 11; i++)
        {
            ddlFinYear.Items.Add(Convert.ToString(CurrentYear) + "-" + Convert.ToString(CurrentYear + 1));
            CurrentYear = CurrentYear - 1;
        }
    }
    public void Load_TwoDates()
    {
        if (ddlMonth.SelectedValue != "-Select-" && ddlFinYear.SelectedValue != "-Select-" && ddlWages.SelectedValue != "0")
        {
            if (ddlWages.SelectedValue != "9")
            {
                decimal Month_Total_days = 0;
                string Month_Last = "";
                string Year_Last = "0";
                string Temp_Years = "";
                string[] Years;
                string FromDate = "";
                string ToDate = "";

                Temp_Years = ddlFinYear.SelectedValue;
                Years = Temp_Years.Split('-');
                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "March") || (ddlMonth.SelectedValue == "May") || (ddlMonth.SelectedValue == "July") || (ddlMonth.SelectedValue == "August") || (ddlMonth.SelectedValue == "October") || (ddlMonth.SelectedValue == "December"))
                {
                    Month_Total_days = 31;
                }
                else if ((ddlMonth.SelectedValue == "April") || (ddlMonth.SelectedValue == "June") || (ddlMonth.SelectedValue == "September") || (ddlMonth.SelectedValue == "November"))
                {
                    Month_Total_days = 30;
                }

                else if (ddlMonth.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(Years[0]) + 1);
                    if ((yrs % 4) == 0)
                    {
                        Month_Total_days = 29;
                    }
                    else
                    {
                        Month_Total_days = 28;
                    }
                }
                switch (ddlMonth.SelectedItem.Text)
                {
                    case "January": Month_Last = "01";
                        break;
                    case "February": Month_Last = "02";
                        break;
                    case "March": Month_Last = "03";
                        break;
                    case "April": Month_Last = "04";
                        break;
                    case "May": Month_Last = "05";
                        break;
                    case "June": Month_Last = "06";
                        break;
                    case "July": Month_Last = "07";
                        break;
                    case "August": Month_Last = "08";
                        break;
                    case "September": Month_Last = "09";
                        break;
                    case "October": Month_Last = "10";
                        break;
                    case "November": Month_Last = "11";
                        break;
                    case "December": Month_Last = "12";
                        break;
                    default:
                        break;
                }

                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "February") || (ddlMonth.SelectedValue == "March"))
                {
                    Year_Last = Years[1];
                }
                else
                {
                    Year_Last = Years[0];
                }
                FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
                ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

                txtFromDate.Text = FromDate.ToString();
                txtToDate.Text = ToDate.ToString();

            }
            else
            {
                //txtToDate.Text = "";
                //txtFromDate.Text = "";
            }

        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        double Allow3 = 0.0;
        double Mis2 = 0.0;
        double Canteen = 0.0;
        double DedOthers1 = 0.0;
        double DedOthers2 = 0.0;
        double Misc = 0.0;
        double Manual_Ded1 = 0.0;
        double Advance_dbl = 0.0;
        double Manual_Ded2 = 0.0;
        double Manual_Ded3 = 0.0;

        //Check Month
        string Month_Name_Str = "";
        if (ddlWages.SelectedItem.Text.ToUpper() != "MIXING")
        {
            //FromDate Check
            DateTime FrmDate = Convert.ToDateTime(txtFromDate.Text);
            //Month_Name_Str = FrmDate.Month.ToString();

            if (FrmDate.Month.ToString() == "1")
            {
                Month_Name_Str = "January";
            }
            else if (FrmDate.Month.ToString() == "2")
            {
                Month_Name_Str = "February";
            }
            else if (FrmDate.Month.ToString() == "3")
            {
                Month_Name_Str = "March";
            }
            else if (FrmDate.Month.ToString() == "4")
            {
                Month_Name_Str = "April";
            }
            else if (FrmDate.Month.ToString() == "5")
            {
                Month_Name_Str = "May";
            }
            else if (FrmDate.Month.ToString() == "6")
            {
                Month_Name_Str = "June";
            }
            else if (FrmDate.Month.ToString() == "7")
            {
                Month_Name_Str = "July";
            }
            else if (FrmDate.Month.ToString() == "8")
            {
                Month_Name_Str = "August";
            }
            else if (FrmDate.Month.ToString() == "9")
            {
                Month_Name_Str = "September";
            }
            else if (FrmDate.Month.ToString() == "10")
            {
                Month_Name_Str = "October";
            }
            else if (FrmDate.Month.ToString() == "11")
            {
                Month_Name_Str = "November";
            }
            else if (FrmDate.Month.ToString() == "12")
            {
                Month_Name_Str = "December";
            }

            if ((Month_Name_Str).ToUpper() != ddlMonth.SelectedItem.Text.ToUpper())
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with MONTH NAME AND FROM DATE.!');", true);
                txtFromDate.Focus();
            }

            //ToDate Month Name Check
            DateTime ToDate = Convert.ToDateTime(txtToDate.Text);
            //Month_Name_Str = FrmDate.Month.ToString();

            if (ToDate.Month.ToString() == "1")
            {
                Month_Name_Str = "January";
            }
            else if (ToDate.Month.ToString() == "2")
            {
                Month_Name_Str = "February";
            }
            else if (ToDate.Month.ToString() == "3")
            {
                Month_Name_Str = "March";
            }
            else if (ToDate.Month.ToString() == "4")
            {
                Month_Name_Str = "April";
            }
            else if (ToDate.Month.ToString() == "5")
            {
                Month_Name_Str = "May";
            }
            else if (ToDate.Month.ToString() == "6")
            {
                Month_Name_Str = "June";
            }
            else if (ToDate.Month.ToString() == "7")
            {
                Month_Name_Str = "July";
            }
            else if (ToDate.Month.ToString() == "8")
            {
                Month_Name_Str = "August";
            }
            else if (ToDate.Month.ToString() == "9")
            {
                Month_Name_Str = "September";
            }
            else if (ToDate.Month.ToString() == "10")
            {
                Month_Name_Str = "October";
            }
            else if (ToDate.Month.ToString() == "11")
            {
                Month_Name_Str = "November";
            }
            else if (ToDate.Month.ToString() == "12")
            {
                Month_Name_Str = "December";
            }

            if ((Month_Name_Str).ToUpper() != ddlMonth.SelectedItem.Text.ToUpper())
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with MONTH NAME AND TO DATE.!');", true);
                txtToDate.Focus();
            }

            //Date Check
            int days = DateTime.DaysInMonth(Convert.ToDateTime(txtFromDate.Text).Year, Convert.ToDateTime(txtFromDate.Text).Month);

            if (Left_Val(txtFromDate.Text.ToString(), 2) != "01")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check FROM DATE.!');", true);
                txtFromDate.Focus();
            }
            if (days.ToString() != Left_Val(txtToDate.Text.ToString(), 2))
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check TO DATE.!');", true);
                txtToDate.Focus();
            }

            //Check Financial Year
            int CurrentYear;
            string Fin_Year_Str;
            //Financial Year Add
            CurrentYear = Convert.ToDateTime(txtFromDate.Text).Year;
            if (Convert.ToDateTime(txtFromDate.Text).Month >= 1 && Convert.ToDateTime(txtFromDate.Text).Month <= 3)
            {
                CurrentYear = CurrentYear - 1;
            }
            else
            {
                CurrentYear = CurrentYear;
            }
            Fin_Year_Str = Convert.ToString(CurrentYear) + "-" + Convert.ToString(CurrentYear + 1);
            if (Fin_Year_Str != ddlFinYear.SelectedItem.Text)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check With Financial Year...!');", true);
            }

        }

        if (txtAllow_Incentive.Text == "")
        {
            Allow3 = 0;
        }
        else
        {
            Allow3 = Convert.ToDouble(txtAllow_Incentive.Text);
        }

        //Deduction
        if (txtAdvance.Text == "")
        {
            Advance_dbl = 0;
        }
        else
        {
            Advance_dbl = Convert.ToDouble(txtAdvance.Text);
        }
        if (txtMis2.Text == "")
        {
            Mis2 = 0;
        }
        else
        {
            Mis2 = Convert.ToDouble(txtMis2.Text);
        }
        if (txtCanteen.Text == "")
        {
            Canteen = 0;
        }
        else
        {
            Canteen = Convert.ToDouble(txtCanteen.Text);
        }

        if (txtMisc.Text == "")
        {
            Misc = 0;
        }
        else
        {
            Misc = Convert.ToDouble(txtMisc.Text);
        }

        if (txtManual_Ded1.Text == "")
        {
            Manual_Ded1 = 0;
        }
        else
        {
            Manual_Ded1 = Convert.ToDouble(txtManual_Ded1.Text);
        }

        if (txtManual_Ded2.Text == "")
        {
            Manual_Ded2 = 0;
        }
        else
        {
            Manual_Ded2 = Convert.ToDouble(txtManual_Ded2.Text);
        }
        if (txtManual_Ded3.Text == "")
        {
            Manual_Ded3 = 0;
        }
        else
        {
            Manual_Ded3 = Convert.ToDouble(txtManual_Ded3.Text);
        }


        if (txtDedOthers1.Text == "")
        {
            DedOthers1 = 0;
        }
        else
        {
            DedOthers1 = Convert.ToDouble(txtDedOthers1.Text);
        }
        if (txtDedOthers2.Text == "")
        {
            DedOthers2 = 0;
        }
        else
        {
            DedOthers2 = Convert.ToDouble(txtDedOthers2.Text);
        }


        if (Allow3 == 0 && Mis2 == 0 && Advance_dbl == 0 && Canteen == 0 && Misc == 0 && Manual_Ded1 == 0 && Manual_Ded2 == 0 && Manual_Ded3 == 0 && DedOthers1 == 0 && DedOthers2 == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to enter atleast one Allowence or Deduction Amount.!');", true);
        }



        if (!ErrFlag)
        {
            string[] iStr3;
            iStr3 = ddlFinYear.SelectedItem.Text.Split('-');

            SSQL = "Select * from eAlert_Deduction_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And Month='" + ddlMonth.SelectedItem.Text + "' and FinancialYear='" + iStr3[0] + "' and Wages='" + ddlWages.SelectedItem.Text + "'";
            SSQL = SSQL + " And MachineID='" + txtMachineID.SelectedItem.Text + "' And ExisistingCode='" + txtTokenNo.Text + "'";
            SSQL = SSQL + " And FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
            SSQL = SSQL + " And ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                Allow3 = Allow3 + Convert.ToDouble(DT.Rows[0]["allowances3"].ToString());
                Mis2 = Mis2 + Convert.ToDouble(DT.Rows[0]["Misc2"].ToString());
                Advance_dbl = Advance_dbl + Convert.ToDouble(DT.Rows[0]["Advance"].ToString());
                Canteen = Canteen + Convert.ToDouble(DT.Rows[0]["Canteen"].ToString());
                Misc = Misc + Convert.ToDouble(DT.Rows[0]["Misc"].ToString());
                Manual_Ded1 = Manual_Ded1 + Convert.ToDouble(DT.Rows[0]["M_Ded1"].ToString());
                Manual_Ded2 = Manual_Ded2 + Convert.ToDouble(DT.Rows[0]["M_Ded2"].ToString());
                Manual_Ded3 = Manual_Ded3 + Convert.ToDouble(DT.Rows[0]["M_Ded3"].ToString());
                DedOthers1 = DedOthers1 + Convert.ToDouble(DT.Rows[0]["DedOthers1"].ToString());
                DedOthers2 = DedOthers2 + Convert.ToDouble(DT.Rows[0]["DedOthers2"].ToString());

                //Delete OLD Record
                SSQL = "Delete from eAlert_Deduction_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And Month='" + ddlMonth.SelectedItem.Text + "' and FinancialYear='" + iStr3[0] + "' and Wages='" + ddlWages.SelectedItem.Text + "'";
                SSQL = SSQL + " And MachineID='" + txtMachineID.SelectedItem.Text + "' And ExisistingCode='" + txtTokenNo.Text + "'";
                SSQL = SSQL + " And FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
                SSQL = SSQL + " And ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

            }


            //Insert Allowence And Deduction
            SSQL = "Insert Into eAlert_Deduction_Det(Ccode,Lcode,Month,FinancialYear,Wages,MachineID,ExisistingCode,EmpName,allowances3,";
            SSQL = SSQL + "Misc2,Advance,Canteen,Misc,M_Ded1,M_Ded2,M_Ded3,DedOthers1,DedOthers2,";
            SSQL = SSQL + "FromDate_Str,FromDate,ToDate_Str,ToDate) Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlMonth.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + iStr3[0] + "','" + ddlWages.SelectedItem.Text + "','" + txtMachineID.SelectedItem.Text + "','" + txtTokenNo.Text + "',";
            SSQL = SSQL + "'" + txtEmpName.Text + "','" + Allow3 + "','" + Mis2 + "','" + Advance_dbl + "','" + Canteen + "','" + Misc + "',";
            SSQL = SSQL + "'" + Manual_Ded1 + "','" + Manual_Ded2 + "','" + Manual_Ded3 + "','" + DedOthers1 + "','" + DedOthers2 + "','" + txtFromDate.Text + "',";
            SSQL = SSQL + "'" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "','" + txtToDate.Text + "',";
            SSQL = SSQL + "'" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Allowance And Deduction Saved Successfully.!');", true);
            Load_AllowenceAndDeductionDet();
            Clear_All_Field();
        }
    }

    public string Left_Val(string Value, int Length)
    {

        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    public string Right_Val(string Value, int Length)
    {

        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field_ALL_New();
    }

    private void Clear_All_Field()
    {
        //ddlMonth.SelectedValue = "-Select-";
        //ddlFinYear.SelectedValue = "-Select-";
        //ddlWages.SelectedValue = "0";
        txtMachineID.SelectedValue = "-Select-";
        txtEmpName.Text = ""; txtTokenNo.Text = "";
        //txtFromDate.Text = ""; txtToDate.Text = "";
        txtAllow_Incentive.Text = ""; txtAdvance.Text = "";
        txtMis2.Text = ""; txtCanteen.Text = "";
        txtMisc.Text = ""; txtManual_Ded1.Text = "";
        txtDedOthers1.Text = ""; txtDedOthers2.Text = "";
        txtManual_Ded2.Text = ""; txtManual_Ded3.Text = "";
        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Load_AllowenceAndDeductionDet();
    }

    private void Clear_All_Field_ALL_New()
    {
        ddlMonth.SelectedValue = "-Select-";
        ddlFinYear.SelectedValue = "-Select-";
        ddlWages.SelectedValue = "0";
        txtMachineID.SelectedValue = "-Select-";
        txtEmpName.Text = ""; txtTokenNo.Text = "";
        txtFromDate.Text = ""; txtToDate.Text = "";
        txtAllow_Incentive.Text = ""; txtMis2.Text = "";
        txtAdvance.Text = ""; txtCanteen.Text = "";
        txtMisc.Text = ""; txtManual_Ded1.Text = "";
        txtDedOthers1.Text = ""; txtDedOthers2.Text = "";
        txtManual_Ded2.Text = ""; txtManual_Ded3.Text = "";
        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Load_AllowenceAndDeductionDet();
    }
    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_EmpDet();
        LabelChange();
        Load_AllowenceAndDeductionDet();
        Load_TwoDates();
    }

    public void LabelChange()
    {

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();
        string[] iStr3;
        iStr3 = ddlFinYear.SelectedItem.Text.Split('-');

        SSQL = "Select * from eAlert_Deduction_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And Month='" + ddlMonth.SelectedItem.Text + "' and FinancialYear='" + iStr3[0] + "' and Wages='" + ddlWages.SelectedItem.Text + "'";
        SSQL = SSQL + " And MachineID='" + e.CommandName.ToString() + "' And ExisistingCode='" + e.CommandArgument.ToString() + "'";
        SSQL = SSQL + " And FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
        SSQL = SSQL + " And ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            //Delete OLD Record
            SSQL = "Delete from eAlert_Deduction_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And Month='" + ddlMonth.SelectedItem.Text + "' and FinancialYear='" + iStr3[0] + "' and Wages='" + ddlWages.SelectedItem.Text + "'";
            SSQL = SSQL + " And MachineID='" + e.CommandName.ToString() + "' And ExisistingCode='" + e.CommandArgument.ToString() + "'";
            SSQL = SSQL + " And FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
            SSQL = SSQL + " And ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Deleted Successfully.!');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data found.!');", true);
        }

        Load_AllowenceAndDeductionDet();
    }

    private void Load_AllowenceAndDeductionDet()
    {

        if (ddlMonth.SelectedItem.Text != "-Select-" && ddlFinYear.SelectedItem.Text != "-Select-" && ddlWages.SelectedItem.Text != "-Select-")
        {
            string Year_Last = "0";
            iStr3 = ddlFinYear.SelectedItem.Text.Split('-');
            if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "February") || (ddlMonth.SelectedValue == "March"))
            {
                //Year_Last = iStr3[1];
                Year_Last = iStr3[0];
            }
            else
            {
                Year_Last = iStr3[0];
            }


            DataTable DT = new DataTable();
            if (ddlWages.SelectedItem.Text.ToString().ToUpper() == "MIXING".ToUpper().ToString())
            {
                if (txtFromDate.Text.ToString() != "" && txtToDate.Text.ToString() != "")
                {
                    SSQL = "Select * from eAlert_Deduction_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And Month='" + ddlMonth.SelectedItem.Text + "' and FinancialYear='" + Year_Last + "' and Wages='" + ddlWages.SelectedItem.Text + "'";
                    SSQL = SSQL + " And FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
                    SSQL = SSQL + " And ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                    SSQL = SSQL + " Order by ExisistingCode Asc";
                    DT = objdata.RptEmployeeMultipleDetails(SSQL);
                    Repeater1.DataSource = DT;
                    Repeater1.DataBind();

                    ViewState["ItemTable"] = Repeater1.DataSource;
                }
                else
                {
                    Initial_Data_Referesh();
                }
            }
            else
            {
                SSQL = "Select * from eAlert_Deduction_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And Month='" + ddlMonth.SelectedItem.Text + "' and FinancialYear='" + Year_Last + "' and Wages='" + ddlWages.SelectedItem.Text + "'";
                if (txtFromDate.Text.ToString() != "" && txtToDate.Text.ToString() != "")
                {
                    if (ddlWages.SelectedItem.Text.ToString().ToUpper() == "CIVIL".ToUpper().ToString())
                    {
                        SSQL = SSQL + " And FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
                        SSQL = SSQL + " And ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                    }
                }
                SSQL = SSQL + " Order by ExisistingCode Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                Repeater1.DataSource = DT;
                Repeater1.DataBind();

                ViewState["ItemTable"] = Repeater1.DataSource;
            }
        }
        else
        {
            Initial_Data_Referesh();
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("Ccode", typeof(string)));
        dt.Columns.Add(new DataColumn("Lcode", typeof(string)));
        dt.Columns.Add(new DataColumn("Month", typeof(string)));
        dt.Columns.Add(new DataColumn("FinancialYear", typeof(string)));
        dt.Columns.Add(new DataColumn("Wages", typeof(string)));
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("ExisistingCode", typeof(string)));
        dt.Columns.Add(new DataColumn("EmpName", typeof(string)));
        dt.Columns.Add(new DataColumn("allowances3", typeof(string)));
        dt.Columns.Add(new DataColumn("Misc2", typeof(string)));
        dt.Columns.Add(new DataColumn("Advance", typeof(string)));
        dt.Columns.Add(new DataColumn("Canteen", typeof(string)));
        dt.Columns.Add(new DataColumn("Misc", typeof(string)));
        dt.Columns.Add(new DataColumn("M_Ded1", typeof(string)));
        dt.Columns.Add(new DataColumn("M_Ded2", typeof(string)));
        dt.Columns.Add(new DataColumn("M_Ded3", typeof(string)));
        dt.Columns.Add(new DataColumn("DedOthers1", typeof(string)));
        dt.Columns.Add(new DataColumn("DedOthers2", typeof(string)));
        dt.Columns.Add(new DataColumn("FromDate_Str", typeof(string)));
        dt.Columns.Add(new DataColumn("FromDate", typeof(string)));
        dt.Columns.Add(new DataColumn("ToDate_Str", typeof(string)));
        dt.Columns.Add(new DataColumn("ToDate", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

    }
    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_AllowenceAndDeductionDet();
        Load_TwoDates();
    }
    protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_AllowenceAndDeductionDet();
        Load_TwoDates();
    }
    protected void txtFromDate_TextChanged(object sender, EventArgs e)
    {
        Load_AllowenceAndDeductionDet();
        Load_TwoDates();
    }
    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        Load_AllowenceAndDeductionDet();
    }
    protected void txtTokenNo_TextChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "' And ExistingCode='" + txtTokenNo.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtMachineID.SelectedValue = DT.Rows[0]["EmpNo"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();

        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            //txtTokenNo.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Token No not Data found.!');", true);
        }
    }
}
