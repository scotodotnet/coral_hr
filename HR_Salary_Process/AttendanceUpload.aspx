﻿<%@ Page Title="Attendance Upload" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AttendanceUpload.aspx.cs" Inherits="HR_Salary_Process_AttendanceUpload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>


    <!-- begin #content -->
    <div id="content" class="content-wrapper">
        <section class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">Salary Process</a></li>
                <li class="active">Attendance Upload</li>
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Attendance Upload</h1>
            <!-- end page-header -->

            <!-- begin row -->
            <div class="row">
                <!-- begin col-12 -->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-md-12">
                            <!-- begin panel -->
                            <div class="panel panel-inverse">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <!-- begin row -->
                                        <div class="row">
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Fin. Year</label>
                                                    <asp:DropDownList runat="server" ID="ddlfinance" class="form-control select2" AutoPostBack="true"
                                                        Style="width: 100%;" OnSelectedIndexChanged="ddlfinance_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Month</label>
                                                    <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" AutoPostBack="true"
                                                        Style="width: 100%;" OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged">
                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                        <asp:ListItem Value="January">January</asp:ListItem>
                                                        <asp:ListItem Value="February">February</asp:ListItem>
                                                        <asp:ListItem Value="March">March</asp:ListItem>
                                                        <asp:ListItem Value="April">April</asp:ListItem>
                                                        <asp:ListItem Value="May">May</asp:ListItem>
                                                        <asp:ListItem Value="June">June</asp:ListItem>
                                                        <asp:ListItem Value="July">July</asp:ListItem>
                                                        <asp:ListItem Value="August">August</asp:ListItem>
                                                        <asp:ListItem Value="September">September</asp:ListItem>
                                                        <asp:ListItem Value="October">October</asp:ListItem>
                                                        <asp:ListItem Value="November">November</asp:ListItem>
                                                        <asp:ListItem Value="December">December</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <br />
                                                    <asp:FileUpload ID="FileUpload" runat="server" />
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                        <!-- end row -->
                                        <!-- begin row -->
                                        <div class="row">
                                            <!-- begin col-4 -->
                                            <div id="Div1" class="col-md-4" runat="server" visible="false">
                                                <div class="form-group">
                                                    <label>Category</label>
                                                    <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" Style="width: 100%;">
                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                        <asp:ListItem Value="Staff">Staff</asp:ListItem>
                                                        <asp:ListItem Value="Labour">Labour</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->

                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div id="Div3" class="col-md-4" runat="server" visible="false">
                                                <div class="form-group">
                                                    <label>No Of Working Days</label>
                                                    <asp:TextBox runat="server" ID="txtdays" class="form-control" Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                        <!-- end row -->
                                        <!-- begin row -->
                                        <div class="row">
                                            <!-- begin col-4 -->
                                            <div id="Div4" class="col-md-4" runat="server" visible="false">
                                                <div class="form-group">
                                                    <label>NFH Days</label>
                                                    <asp:TextBox runat="server" ID="txtNfh" class="form-control" Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>From Date</label>
                                                    <asp:TextBox runat="server" ID="txtFrom" class="form-control datepicker"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>To Date</label>
                                                    <asp:TextBox runat="server" ID="txtTo" class="form-control datepicker"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Employee Type</label>
                                                    <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" Style="width: 100%;">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- end row -->

                                        <!-- begin row -->
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <br />
                                                    <asp:Button runat="server" ID="btnUpload" Text="Upload" OnClick="btnUpload_Click" OnClientClick="ProgressBarShow();" class="btn btn-success" />
                                                    <asp:Button ID="btnAttn_Format_Download" runat="server" class="btn btn-warning"
                                                        Text="Format Download" OnClick="btnAttn_Format_Download_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->

                                    <div class="form-group">
                                        <div class="row">
                                            <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Machine ID</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMachineID" runat="server" Text='<%# Eval("MachineNo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Token NO</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblToken_No" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>EmpName</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Worked Days</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblWorked_Days" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>H.Allowed</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblH_Allowed" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>N/FH</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNFH" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>WH Work Days</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblWH_Work_Days" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>LWW Days</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLWW_Days" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>OT Hours</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOT_Hours" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Permission Hrs</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPermission_Hrs" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>OT Days</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOT_Days" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Canteen Days Minus</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCanteen_Days_Minus" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Fixed W.Days</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFixed_Work_Days" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Total Month Days</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTotal_Month_Days" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>NFH Worked Days</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNFH_Worked_Days" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>NFH D.W Statutory</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNFH_D_W_Statutory" runat="server" Text="0"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Download_loader" style="display: none" />
                        </div>
                        </div>
                    <!-- end panel -->
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUpload" />
                        <asp:PostBackTrigger ControlID="btnAttn_Format_Download" />
                    </Triggers>
                </asp:UpdatePanel>

                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </section>
    </div>
    <!-- end #content -->

</asp:Content>

