﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AllowanceDeduction.aspx.cs" Inherits="HR_Salary_Process_AllowanceDeduction" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>

<!-- begin #content -->
		<div id="content" class="content-wrapper">
		    <section class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Salary Process</a></li>
				<li class="active">Allowance & Deduction</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Allowance & Deduction</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonth" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlMonth_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="January">January</asp:ListItem>
								 <asp:ListItem Value="February">February</asp:ListItem>
								 <asp:ListItem Value="March">March</asp:ListItem>
								 <asp:ListItem Value="April">April</asp:ListItem>
								 <asp:ListItem Value="May">May</asp:ListItem>
								 <asp:ListItem Value="June">June</asp:ListItem>
								 <asp:ListItem Value="July">July</asp:ListItem>
								 <asp:ListItem Value="August">August</asp:ListItem>
								 <asp:ListItem Value="September">September</asp:ListItem>
								 <asp:ListItem Value="October">October</asp:ListItem>
								 <asp:ListItem Value="November">November</asp:ListItem>
								 <asp:ListItem Value="December">December</asp:ListItem>
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlMonth" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                             <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinYear" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlFinYear_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlFinYear" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Wages Type</label>
								 <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2" 
                                        AutoPostBack="true" style="width:100%;" 
                                        onselectedindexchanged="ddlWages_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlFinYear" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->

                               <div class="col-md-3">
							   <div class="form-group">
								<label>Token No</label>
								<asp:TextBox runat="server" ID="txtTokenNo" class="form-control" 
                                       AutoPostBack="true" ontextchanged="txtTokenNo_TextChanged" ></asp:TextBox>
								</div>
                             </div>
                         </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                           
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                           <div class="col-md-3">
                           <div class="form-group">
                           <label>Machine ID</label>
                           <asp:DropDownList runat="server" ID="txtMachineID" class="form-control select2" 
                                 AutoPostBack="true" style="width:100%;" onselectedindexchanged="txtMachineID_SelectedIndexChanged">
                           </asp:DropDownList>
                           <asp:RequiredFieldValidator ControlToValidate="txtMachineID" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
                            </div>
                           </div>
                           <!-- end col-4 -->
                          
                          <!-- begin col-4 -->
                            <div class="col-md-3">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>


                              <div class="col-md-3">
									<div class="form-group">
										<label>From Date</label>
										<asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker" 
                                            placeholder="dd/MM/YYYY" AutoPostBack="true" 
                                            ontextchanged="txtFromDate_TextChanged"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtFromDate" ValidChars="0123456789/">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                           <!-- end col-4 -->
                            
                          
                              <div class="col-md-3">
									<div class="form-group">
										<label>To Date</label>
										<asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker" 
                                            placeholder="dd/MM/YYYY" AutoPostBack="true" 
                                            ontextchanged="txtToDate_TextChanged"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtToDate" ValidChars="0123456789/">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                           </div>
                        <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                         
                                 <div class="col-md-3">
									<div class="form-group">
									    <label>Incentive</label>
										<asp:TextBox runat="server" ID="txtAllow_Incentive" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtAllow_Incentive" ValidChars="0123456789.-">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                          
                         
                          
                                 <div class="col-md-3">
									<div class="form-group">
									    <label>Mis2</label>
										<asp:TextBox runat="server" ID="txtMis2" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtMis2" ValidChars="0123456789.-">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                                 <div class="col-md-3">
									<div class="form-group">
										<label>Advance</label>
										<asp:TextBox runat="server" ID="txtAdvance" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtAdvance" ValidChars="0123456789.-">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>

                                 <div class="col-md-3">
									<div class="form-group">
										<label>Canteen</label>
										<asp:TextBox runat="server" ID="txtCanteen" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtCanteen" ValidChars="0123456789.-">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                         <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                                 <div class="col-md-3">
									<div class="form-group">
										<label>Misc</label>										
										<asp:TextBox runat="server" ID="txtMisc" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtMisc" ValidChars="0123456789.-">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-3">
									<div class="form-group">
										<label>P.Tax</label>										
										<asp:TextBox runat="server" ID="txtManual_Ded1" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtManual_Ded1" ValidChars="0123456789.-">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                                  <div class="col-md-3">
									<div class="form-group">
										<label>LWF</label>										
										<asp:TextBox runat="server" ID="txtManual_Ded2" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtManual_Ded2" ValidChars="0123456789.-">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                                  <div class="col-md-3">
									<div class="form-group">
										<label>TDS Amount</label>										
										<asp:TextBox runat="server" ID="txtManual_Ded3" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtManual_Ded3" ValidChars="0123456789.-">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-3">
									<div class="form-group">
										<label>Others</label>
										<asp:TextBox runat="server" ID="txtDedOthers1" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtDedOthers1" ValidChars="0123456789.-">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                          <%--</div>--%>
                        <!-- end row -->
                        <!-- begin row -->
                          <%--<div class="row">--%>
                          <!-- begin col-4 -->
                                 <div class="col-md-3">
									<div class="form-group">
										<label>Ded.Others2</label>										
										<asp:TextBox runat="server" ID="txtDedOthers2" class="form-control"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtDedOthers2" ValidChars="0123456789.-">
                                        </cc1:FilteredTextBoxExtender>
									</div>
                                  </div>
                               <!-- end col-4 -->
                          </div>
                        <!-- end row -->                        
                      <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success"  
                                         ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                         <%--<div style ="height:30px; margin:0;padding:0">
        <table cellspacing="0" cellpadding = "0" rules="all" border="1" id="tblHeader" 
         style="font-family:Arial;font-size:10pt;color:#000;
         border-collapse:collapse;height:100%;">
         <thead  style ="background:White">
            <tr>
               <td style ="width:6.25%;text-align:center">M ID</td>
               <td style ="width:6.25%;text-align:center">Token No</td>
               <td style ="width:10%;text-align:center">E Name</td>
               <td style ="width:4.25%;text-align:center">Incent</td>
                <td style ="width:4.50%;text-align:center">Uniform</td>
                <td style ="width:4.50%;text-align:center">Advance</td>
               <td style ="width:6.25%;text-align:center">Mess</td>
               <td style ="width:6.25%;text-align:center">Stores</td>
               <td style ="width:6.25%;text-align:center">Medical</td>
               <td style ="width:6.25%;text-align:center">L1</td>
                <td style ="width:6.25%;text-align:center">L2</td>
               <td style ="width:6.25%;text-align:center">DedOthers1</td>
               <td style ="width:6.25%;text-align:center">DedOthers2</td>
                <td style ="width:6.25%;text-align:center">Mode</td>
             
            </tr>
         </thead>
        </table>
        </div>--%>
        
         <%--<div style ="height:400px; overflow:scroll;word-break:break-all;">
        <asp:GridView ID="Repeater1" runat="server"
            AutoGenerateColumns = "false" Font-Names = "Arial" ShowHeader = "false" 
            Font-Size = "11pt" AlternatingRowStyle-BackColor = "#ffffff" Width="100%">
           <Columns>
            <asp:BoundField ItemStyle-Width = "7.20%" DataField = "MachineID" />
            <asp:BoundField ItemStyle-Width = "7.20%" DataField = "ExisistingCode" />
            <asp:BoundField ItemStyle-Width = "11.20%" DataField = "EmpName" />
            <asp:BoundField ItemStyle-Width = "4.75%" DataField = "allowances3" />
            <asp:BoundField ItemStyle-Width = "5.70%" DataField = "Deduction4" />
            <asp:BoundField ItemStyle-Width = "6.25%" DataField = "Advance" />
            <asp:BoundField ItemStyle-Width = "7.25%" DataField = "Deduction5" />
            <asp:BoundField ItemStyle-Width = "7%" DataField = "Stores" />
            <asp:BoundField ItemStyle-Width = "7%" DataField = "Medical" />
            <asp:BoundField ItemStyle-Width = "7%" DataField = "L_One" />
            <asp:BoundField ItemStyle-Width = "7.25%" DataField = "L_Two" />          
              <asp:BoundField ItemStyle-Width = "8.70%" DataField = "DedOthers1" />
            <asp:BoundField ItemStyle-Width = "8.70%" DataField = "DedOthers2" />
            <asp:TemplateField >
             <ItemTemplate>
          <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                           Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%#Eval("ExisistingCode")%>' CommandName='<%# Eval("MachineID")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Delete this Employee Allowance and Deduction?');">
                                     </asp:LinkButton>
           </ItemTemplate></asp:TemplateField>
          
           </Columns> 
        </asp:GridView>
        </div>--%>
                        
                    <!-- table start -->
					<div class="col-md-12" style="font-family:Arial;font-size:10pt;color:#000;
         border-collapse:collapse;width:100%;">
					    <div class="row" class="col-md-12">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <%--<th>M ID</th>--%>
                                                <th>Token No</th>
                                                <%--<th>E Name</th>--%>
                                                <th>Incent</th>
                                                <th>Mis2</th>
                                                <th>Advance</th>
                                                <th>Canteen</th>
                                                <th>Misc</th>
                                                <th>M.D1</th>
                                                <th>M.D2</th>
                                                <th>M.D3</th>
                                                <th>Oth1</th>
                                                <th>Oth2</th>
                                               
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <%--<td><%# Eval("MachineID")%></td>--%>
                                    <td><%# Eval("ExisistingCode")%></td>
                                    <%--<td><%# Eval("EmpName")%></td>--%>
                                    <td><%# Eval("allowances3")%></td>
                                    <td><%# Eval("Misc2")%></td>
                                    <td><%# Eval("Advance")%></td>
                                    <td><%# Eval("Canteen")%></td>
                                    <td><%# Eval("Misc")%></td>
                                    <td><%# Eval("M_Ded1")%></td>
                                    <td><%# Eval("M_Ded2")%></td>
                                    <td><%# Eval("M_Ded3")%></td>
                                    <td><%# Eval("DedOthers1")%></td>
                                    <td><%# Eval("DedOthers2")%></td>
                                   
                                     <td>
                                     <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                           Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%#Eval("ExisistingCode")%>' CommandName='<%# Eval("MachineID")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Delete this Employee Allowance and Deduction?');">
                                     </asp:LinkButton>
                                     
                                    </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        </div>
                        </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
            </section>
        </div>
<!-- end #content -->


</asp:Content>

