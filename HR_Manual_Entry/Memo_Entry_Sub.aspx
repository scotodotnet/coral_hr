﻿<%@ Page Title="Memo Entry" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Memo_Entry_Sub.aspx.cs" Inherits="HR_Manual_Entry_Memo_Entry_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script>
    $(document).ready(function() {
        //alert('hi');
    $('#example').dataTable();
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
    });
</script>
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>
<!-- begin #content -->
<div id="content" class="content-wrapper">
    <section class="content">
    <ol class="breadcrumb pull-right">
	    <li>Manul</li>
	    <li class="active">Memo</li>
	</ol>
	<h1 class="page-header">Employee Memo</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                
                <div class="panel-body">
                    <asp:UpdatePanel ID="Basic" runat="server">
                    <ContentTemplate>
                    <div class="form-group">                        
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Category</label>
								    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control select2"  AutoPostBack="true"
                                        onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								       <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
								       <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                    
                                
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Employee Type</label>
								    <asp:DropDownList runat="server" ID="ddlEmployeeType" AutoPostBack="true"
                                        class="form-control select2" style="width:100%;" 
                                        onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Token No</label>
								    <asp:DropDownList runat="server" ID="txtToken_No" AutoPostBack="true"
                                        class="form-control select2" style="width:100%;" 
                                        onselectedindexchanged="txtToken_No_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
							    <div class="form-group">
								    <label>Machine ID</label>
								    <asp:Label runat="server" ID="txtMachineID" class="form-control" BackColor="LightGray"></asp:Label>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Name</label>
								    <asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="LightGray"></asp:Label>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Department</label>
								    <asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="LightGray"></asp:Label>
								</div>
                            </div>
                            <div class="col-md-2">
							    <div class="form-group">
								    <label>Date</label>
								    <asp:TextBox runat="server" ID="txtDate" class="form-control datepicker"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Action Taken</label>
								    <asp:DropDownList ID="txtMemo_Action" runat="server" class="form-control select2">
								       <asp:ListItem Text="-Select-" Value="-Select-"></asp:ListItem>
								       <asp:ListItem Text="Verbal Warning" Value="Verbal Warning"></asp:ListItem>
								       <asp:ListItem Text="Written Warning" Value="Written Warning"></asp:ListItem>
								       <asp:ListItem Text="Termination" Value="Termination"></asp:ListItem>
								       <asp:ListItem Text="Suspension" Value="Suspension"></asp:ListItem>
								       <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                    </asp:DropDownList>
								</div>
                            </div>
                            <div class="col-md-2">
							    <div class="form-group">
								    <label>Susp.Days</label>
								    <asp:TextBox runat="server" ID="txtSuspension_Days" class="form-control"></asp:TextBox>
								    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtSuspension_Days" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Description of Issue</label>
								    <asp:DropDownList ID="txtEmp_Memo_Issue" runat="server" class="form-control select2">
								       <asp:ListItem Text="-Select-" Value="-Select-"></asp:ListItem>
								       <asp:ListItem Text="Absence" Value="Absence"></asp:ListItem>
								       <asp:ListItem Text="Conduct" Value="Conduct"></asp:ListItem>
								       <asp:ListItem Text="Safety Violation" Value="Safety Violation"></asp:ListItem>
								       <asp:ListItem Text="Tardiness" Value="Tardiness"></asp:ListItem>
								       <asp:ListItem Text="Policy Violation" Value="Policy Violation"></asp:ListItem>
								       <asp:ListItem Text="Performance Issue" Value="Performance Issue"></asp:ListItem>
								       <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                    </asp:DropDownList>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Memo Reason</label>
								    <asp:TextBox runat="server" ID="txtMemo_Reason" class="form-control"></asp:TextBox>
								</div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Remarks</label>
								    <asp:TextBox runat="server" ID="txtRemarks" class="form-control"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                        onclick="btnClear_Click" />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <br />
                        <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Token No</th>
                                                <th>Emp.Name</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                                <th>Issue</th>
                                                <th>Status</th>
                                                 <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("Token_No")%></td>
                                        <td><%# Eval("EmpName")%></td>
                                        <td><%# Eval("Memo_Date")%></td>
                                        <td><%# Eval("Memo_Action")%></td>
                                        <td><%# Eval("Memo_Issue")%></td>
                                        <td><%# Eval("Memo_Status")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditEnquiryClick" CommandArgument='<%# Eval("Token_No")%>' CommandName='<%# Eval("Memo_Date")%>'>
                                            </asp:LinkButton>
                                        </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                    </div>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>

</asp:Content>

