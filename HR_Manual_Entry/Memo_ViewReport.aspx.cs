﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;

public partial class HR_Manual_Entry_Memo_ViewReport : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; 
    string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string EmployeeType;
    string Emp_Category;
    string Emp_Token_No;
    string Memo_Action;
    string Memo_Issue;
    string Get_Report_Type = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        string date_1 = "";
        date_1 = ("01".ToString() + "-" + "12".ToString() + "-" + "2018".ToString()).ToString();
        SqlConnection con = new SqlConnection(constr);

        SessionAdmin = Session["Isadmin"].ToString();
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();

        Emp_Category = Request.QueryString["Category"].ToString();
        EmployeeType = Request.QueryString["Employee_Type"].ToString();
        Emp_Token_No = Request.QueryString["Token_No"].ToString();
        Memo_Action = Request.QueryString["Memo_Action"].ToString();
        Memo_Issue = Request.QueryString["Memo_Issue"].ToString();
        fromdate = Request.QueryString["FromDate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();        
        Get_Report_Type = Request.QueryString["Report_Type"].ToString();


        
        DataTable DT_Q_OUT = new DataTable();
        DataTable dt = new DataTable();
        query = "Select Cname,Location,Address1,Address2,Location,Pincode from AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }
        if (Get_Report_Type == "Memo_Details_Report" || Get_Report_Type == "Memo_Slip_Report")
        {
            query = "Select * from Emp_Memo_Sub where CompCode='" + SessionCcode + "' AND LocCode='" + SessionLcode + "'";
            if (Emp_Category != "")
            {
                query = query + " And Category='" + Emp_Category + "'";
            }
            if (EmployeeType != "")
            {
                query = query + " And Employee_Type='" + EmployeeType + "'";
            }
            if (Emp_Token_No != "")
            {
                query = query + " And Token_No='" + Emp_Token_No + "'";
            }
            if (Memo_Action != "")
            {
                query = query + " And Memo_Action='" + Memo_Action + "'";
            }
            if (Memo_Issue != "")
            {
                query = query + " And Memo_Issue='" + Memo_Issue + "'";
            }
            if (fromdate != "" && ToDate !="")
            {
                query = query + " And Convert(Datetime,Memo_Date,103) >= Convert(Datetime,'" + fromdate + "',103)";
                query = query + " And Convert(Datetime,Memo_Date,103) <= Convert(Datetime,'" + ToDate + "',103)";
            }
            query = query + " Order by Convert(Datetime,Memo_Date,103) Asc";            
            DT_Q_OUT = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Q_OUT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(DT_Q_OUT);
                if (Get_Report_Type =="Memo_Slip_Report")
                {
                    rd.Load(Server.MapPath("crystal_HR/Memo_Slip_Rpt.rpt"));
                }
                else
                {
                    rd.Load(Server.MapPath("crystal_HR/Memo_Det_Rpt.rpt"));
                }


                rd.SetDataSource(ds1.Tables[0]);
                string Company_Full = CmpName.ToString().ToUpper() + " - " + SessionLcode.ToString().ToUpper();
                rd.DataDefinition.FormulaFields["CompName"].Text = "'" + Company_Full + "'";
                
                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
        }
        
    }
}
