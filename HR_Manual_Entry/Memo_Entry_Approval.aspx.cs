﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Manual_Entry_Memo_Entry_Approval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Memo Approval";
        }
        Load_Data();
    }
    
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Employee_Type,Token_No,EmpName,Memo_Date,Memo_Reason,(CASE WHEN Memo_Status = '1' THEN 'Approved' ELSE 'Pending' END) AS Memo_Status";
        query = query + " from Emp_Memo_Sub where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        query = query + " And Memo_Status <> '1'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }

    protected void GridApproveEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();
        DataTable DT_Check = new DataTable();

        SSQL = "Update Emp_Memo_Sub set Memo_Status='1',ApprovedBy='" + SessionUserName + "' Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And Memo_Date='" + e.CommandName.ToString() + "' And Token_No='" + e.CommandArgument.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Approved Successfully..!');", true);
        Load_Data();
    }

}
