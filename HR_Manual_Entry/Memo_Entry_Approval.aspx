﻿<%@ Page Title="Employee Memo Approval" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Memo_Entry_Approval.aspx.cs" Inherits="HR_Manual_Entry_Memo_Entry_Approval" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<!-- begin #content -->
		<div id="content" class="content-wrapper">
		    <section class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Manual</a></li>
				<li class="active">Memo Approval</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Employee Memo Approval </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">       
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                         <ContentTemplate>                                     
                        <div class="panel-body">
                        
                         <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Emp.Type</th>
                                                <th>Token No</th>
                                                <th>Emp.Name</th>
                                                <th>Date</th>
                                                <th>Memo Reason</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Container.ItemIndex + 1 %></td>
                                    <td><%# Eval("Employee_Type")%></td>
                                    <td><%# Eval("Token_No")%></td>
                                    <td><%# Eval("EmpName")%></td>
                                    <td><%# Eval("Memo_Date")%></td>
                                    <td><%# Eval("Memo_Reason")%></td>
                                    <td>
                                     <asp:LinkButton ID="btnApprvEnquiry_Grid" class="btn btn-success btn-sm fa fa-check"  runat="server" 
                                           Text="" OnCommand="GridApproveEnquiryClick" CommandArgument='<%# Eval("Token_No")%>' CommandName='<%# Eval("Memo_Date")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this Employee Memo details?');">
                                     </asp:LinkButton>
                                    </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                      
                        </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
            </section>
        </div>
<!-- end #content -->

</asp:Content>

