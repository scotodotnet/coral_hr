﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Manual_Entry_Memo_Entry_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Query = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            ddlCategory_SelectedIndexChanged(sender,e);
            ddlEmployeeType_SelectedIndexChanged(sender, e);
        }
        Load_Data();
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select cast(EmpTypeCd as varchar(20)) as EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpTypeCd"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        
    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        txtToken_No.Items.Clear();
        Query = "Select ExistingCode from Employee_Mst where Wages='" + ddlEmployeeType.SelectedItem.Text + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' Order by ExistingCode Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        txtToken_No.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtToken_No.DataTextField = "ExistingCode";
        txtToken_No.DataValueField = "ExistingCode";
        txtToken_No.DataBind();

    }

    protected void txtToken_No_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        Query = "Select * from Employee_Mst where Wages='" + ddlEmployeeType.SelectedItem.Text + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ExistingCode='" + txtToken_No.SelectedItem.Text + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        if (dtdsupp.Rows.Count != 0)
        {
            txtMachineID.Text = dtdsupp.Rows[0]["MachineID"].ToString();
            txtEmpName.Text = dtdsupp.Rows[0]["FirstName"].ToString();
            txtDepartment.Text = dtdsupp.Rows[0]["DeptName"].ToString();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            String MsgFlag = "Insert";
            DataTable dt = new DataTable();
            bool ErrFlag = false;
            if (ddlCategory.SelectedValue == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Category..!');", true);
            }
            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type..!');", true);
            }
            if (txtToken_No.SelectedValue == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Token No..!');", true);
            }
            if (txtDate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Memo Date..!');", true);
            }
            if (txtMemo_Reason.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Memo Reason..!');", true);
            }

            //Check WIth Already Enter Date And Approve Status
            if (!ErrFlag)
            {
                Query = "select * from Emp_Memo_Sub where Category='" + ddlCategory.SelectedValue + "' and Employee_Type='" + ddlEmployeeType.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                Query = Query + " And Token_No='" + txtToken_No.SelectedValue + "' And Memo_Date='" + txtDate.Text + "' And Memo_Status='1'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee Memo Already Entered and Approved..you can not Save...');", true);
                }
            }

            if (!ErrFlag)
            {
                Query = "select * from Emp_Memo_Sub where Category='" + ddlCategory.SelectedValue + "' and Employee_Type='" + ddlEmployeeType.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                Query = Query + " And Token_No='" + txtToken_No.SelectedValue + "' And Memo_Date='" + txtDate.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count != 0)
                {
                    MsgFlag = "Update";
                    Query = "Delete from Emp_Memo_Sub where Category='" + ddlCategory.SelectedValue + "' and Employee_Type='" + ddlEmployeeType.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                    Query = Query + " And Token_No='" + txtToken_No.SelectedValue + "' And Memo_Date='" + txtDate.Text + "'";
                    objdata.RptEmployeeMultipleDetails(Query);
                }

                //Inser Data
                Query = "";
                Query = Query + "Insert into Emp_Memo_Sub(CompCode,LocCode,Category,Employee_Type_ID,Employee_Type,Token_No,MachineID,EmpName,DeptName,";
                Query = Query + "Memo_Date,Memo_Action,Memo_Issue,Suspen_Days,Memo_Reason,Remarks,Memo_Status,ApprovedBy,UserID,UserName)Values('" + SessionCcode + "','" + SessionLcode + "',";
                Query = Query + "'" + ddlCategory.SelectedValue + "','" + ddlEmployeeType.SelectedValue + "','" + ddlEmployeeType.SelectedItem.Text + "','" + txtToken_No.SelectedValue + "','" + txtMachineID.Text + "','" + txtEmpName.Text + "','" + txtDepartment.Text + "',";
                Query = Query + "'" + txtDate.Text + "','" + txtMemo_Action.SelectedValue + "','" + txtEmp_Memo_Issue.SelectedValue + "','" + txtSuspension_Days.Text + "','" + txtMemo_Reason.Text + "','" + txtRemarks.Text + "','0','','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(Query);
                Clear();
                Load_Data();
                if (MsgFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "SaveMsgAlert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "SaveMsgAlert('Update Successfully');", true);
                }
                
            }

            
        }
        catch (Exception Ex)
        {

        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Clear()
    {
        ddlCategory.SelectedIndex = 0;
        ddlEmployeeType.SelectedIndex = 0;
        txtToken_No.SelectedIndex = 0;
        txtMemo_Action.SelectedIndex = 0;
        txtSuspension_Days.Text = "";
        txtEmp_Memo_Issue.SelectedIndex = 0;
        txtMachineID.Text = "";
        txtEmpName.Text = "";
        txtDepartment.Text = "";
        txtDate.Text = "";
        txtMemo_Reason.Text = "";
        txtRemarks.Text = "";
        btnSave.Text = "Save";
        
    }

    public void Load_Data()
    {
        DataTable DT = new DataTable();
        Query = "Select Token_No,EmpName,Memo_Date,Memo_Action,Memo_Issue,(CASE WHEN Memo_Status = '1' THEN 'Approved' ELSE 'Pending' END) AS Memo_Status";
        Query = Query + " from Emp_Memo_Sub where Category='" + ddlCategory.SelectedValue + "' and Employee_Type='" + ddlEmployeeType.SelectedItem.Text + "'";
        Query = Query + " and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(Query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Emp_Memo_Sub where Memo_Date='" + e.CommandName.ToString() + "' And Token_No='" + e.CommandArgument.ToString() + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Memo_Status <> '1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {

            ddlCategory.SelectedValue = DT.Rows[0]["Category"].ToString();
            ddlEmployeeType.SelectedValue = DT.Rows[0]["Employee_Type_ID"].ToString();
            txtToken_No.SelectedValue = DT.Rows[0]["Token_No"].ToString();
            txtMachineID.Text = DT.Rows[0]["MachineID"].ToString();
            txtEmpName.Text = DT.Rows[0]["EmpName"].ToString();
            txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
            txtDate.Text = DT.Rows[0]["Memo_Date"].ToString();
            txtMemo_Action.SelectedValue = DT.Rows[0]["Memo_Action"].ToString();
            txtSuspension_Days.Text = DT.Rows[0]["Suspen_Days"].ToString();
            txtEmp_Memo_Issue.SelectedValue = DT.Rows[0]["Memo_Issue"].ToString();
            txtMemo_Reason.Text = DT.Rows[0]["Memo_Reason"].ToString();
            txtRemarks.Text = DT.Rows[0]["Remarks"].ToString();
            
            btnSave.Text = "Update";
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "SaveMsgAlert('This Memo Already Approved...You can not edit...');", true);
            Clear();
        }
    }


}
