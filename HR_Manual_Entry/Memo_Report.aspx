﻿<%@ Page Title="Employee Memo Reports" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Memo_Report.aspx.cs" Inherits="HR_Manual_Entry_Memo_Report" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>
<script>
    $(document).ready(function() {        
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
    });
 </script>
 <script type="text/javascript">
     //On UpdatePanel Refresh
     var prm = Sys.WebForms.PageRequestManager.getInstance();
     if (prm != null) {
         prm.add_endRequest(function(sender, e) {
             if (sender._postBackSettings.panelsToUpdate != null) {
                 $('#example').dataTable();
                 $('.select2').select2();
                 $('.datepicker').datepicker({
                     format: "dd/mm/yyyy",
                     autoclose: true
                 });
             }
         });
     };
</script>
<!-- begin #content -->
<div id="content" class="content-wrapper">
    <section class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Manual</a></li>
				<li class="active">Memo Report</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Employee Memo Report</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        
                        <div class="panel-body">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                        <div class="form-group">
                    
                     <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
								 <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" style="width:100%;" 
								    AutoPostBack="true" onselectedindexchanged="txtEmployeeType_SelectedIndexChanged">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Token No</label>
								 <asp:DropDownList runat="server" ID="txtTokenNo" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                      
                       
                            <!-- begin row -->
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Action Taken</label>
								    <asp:DropDownList ID="txtMemo_Action" runat="server" class="form-control select2">
								       <asp:ListItem Text="-Select-" Value="-Select-"></asp:ListItem>
								       <asp:ListItem Text="Verbal Warning" Value="Verbal Warning"></asp:ListItem>
								       <asp:ListItem Text="Written Warning" Value="Written Warning"></asp:ListItem>
								       <asp:ListItem Text="Termination" Value="Termination"></asp:ListItem>
								       <asp:ListItem Text="Suspension" Value="Suspension"></asp:ListItem>
								       <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                    </asp:DropDownList>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Description of Issue</label>
								    <asp:DropDownList ID="txtEmp_Memo_Issue" runat="server" class="form-control select2">
								       <asp:ListItem Text="-Select-" Value="-Select-"></asp:ListItem>
								       <asp:ListItem Text="Absence" Value="Absence"></asp:ListItem>
								       <asp:ListItem Text="Conduct" Value="Conduct"></asp:ListItem>
								       <asp:ListItem Text="Safety Violation" Value="Safety Violation"></asp:ListItem>
								       <asp:ListItem Text="Tardiness" Value="Tardiness"></asp:ListItem>
								       <asp:ListItem Text="Policy Violation" Value="Policy Violation"></asp:ListItem>
								       <asp:ListItem Text="Performance Issue" Value="Performance Issue"></asp:ListItem>
								       <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                    </asp:DropDownList>
								</div>
                            </div>
                            <div class="col-md-2">
								<div class="form-group">
								    <label>From Date</label>
								    <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker" style="width:100%;"></asp:TextBox>
								    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtFromDate" ValidChars="0123456789/">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                            <div class="col-md-2">
							    <div class="form-group">
								    <label>To Date</label>
								    <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker" style="width:100%;"></asp:TextBox>
								    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                          TargetControlID="txtToDate" ValidChars="0123456789/">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                        </div>
                         <!-- end row -->
                         <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnMemoSlipReport" Text="Memo Slip" class="btn btn-success" onclick="btnMemoSlipReport_Click"/>
									<asp:Button runat="server" id="btnMemoDetReport" Text="Report" class="btn btn-success" onclick="btnMemoDetReport_Click"/>
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
    </section>
</div>
<!-- end #content -->

</asp:Content>

