﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Manual_Entry_Memo_Report : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    Int32 Staff_Strength_Tot;
    decimal Staff_Salary_Tot;
    Int32 Labour_Strength_Tot;
    decimal Labour_Salary_Tot;
    Int32 Total_Strength_Tot;
    decimal Total_Salary_Tot;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = "";// Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Load_WagesType();
            Load_Token_No();
            
        }
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        string Category_Str = "0";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            Category_Str = "1";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            Category_Str = "2";
        }

        query = "Select * from MstEmployeeType where EmpCategory='" + Category_Str + "'";
        if (SessionUserType == "2")
        {
            query = query + " And EmpType <> 'SUB-STAFF' And EmpType <> 'CIVIL' And EmpType <> 'OTHERS'";
        }

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    private void Load_Token_No()
    {
        string query = "";
        DataTable DIV_DT = new DataTable();
        txtTokenNo.Items.Clear();
        query = "Select ExistingCode From Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And Wages='" + txtEmployeeType.SelectedItem.Text + "'";
        query = query + " Order by ExistingCode Asc";
        DIV_DT = objdata.RptEmployeeMultipleDetails(query);
        txtTokenNo.DataSource = DIV_DT;
        DataRow dr = DIV_DT.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        DIV_DT.Rows.InsertAt(dr, 0);
        
        txtTokenNo.DataTextField = "ExistingCode";
        txtTokenNo.DataValueField = "ExistingCode";
        txtTokenNo.DataBind();

    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
        Load_Token_No();
    }
    protected void txtEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Token_No();
    }

    protected void btnMemoSlipReport_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;



            //if (ddlcategory.SelectedValue == "-Select-")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
            //    ErrFlag = true;
            //}
            //else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
            //    ErrFlag = true;
            //}


            if (!ErrFlag)            
            {                
                if (!ErrFlag)
                {
                    string Category = "";
                    string Employee_Type = "";
                    string Token_No = "";
                    string Memo_Action = "";
                    string Memo_Issue = "";

                    if (ddlcategory.SelectedValue != "-Select-")
                    {
                        Category = ddlcategory.SelectedValue;
                    }
                    if (txtEmployeeType.SelectedItem.Text != "-Select-")
                    {
                        Employee_Type = txtEmployeeType.SelectedItem.Text;
                    }
                    if (txtTokenNo.SelectedItem.Text != "-Select-")
                    {
                        Token_No = txtTokenNo.SelectedItem.Text;
                    }
                    if (txtMemo_Action.SelectedItem.Text != "-Select-")
                    {
                        Memo_Action = txtMemo_Action.SelectedItem.Text;
                    }
                    if (txtEmp_Memo_Issue.SelectedItem.Text != "-Select-")
                    {
                        Memo_Issue = txtEmp_Memo_Issue.SelectedItem.Text;
                    }


                    ResponseHelper.Redirect("Memo_ViewReport.aspx?Category=" + Category + "&Employee_Type=" + Employee_Type + "&Token_No=" + Token_No + "&Memo_Action=" + Memo_Action + "&Memo_Issue=" + Memo_Issue + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&Report_Type=Memo_Slip_Report", "_blank", "");
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnMemoDetReport_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;

            if (!ErrFlag)
            {
                if (!ErrFlag)
                {
                    string Category = "";
                    string Employee_Type = "";
                    string Token_No = "";
                    string Memo_Action = "";
                    string Memo_Issue = "";

                    if (ddlcategory.SelectedValue != "-Select-")
                    {
                        Category = ddlcategory.SelectedValue;
                    }
                    if (txtEmployeeType.SelectedItem.Text != "-Select-")
                    {
                        Employee_Type = txtEmployeeType.SelectedItem.Text;
                    }
                    if (txtTokenNo.SelectedItem.Text != "-Select-")
                    {
                        Token_No = txtTokenNo.SelectedItem.Text;
                    }
                    if (txtMemo_Action.SelectedItem.Text != "-Select-")
                    {
                        Memo_Action = txtMemo_Action.SelectedItem.Text;
                    }
                    if (txtEmp_Memo_Issue.SelectedItem.Text != "-Select-")
                    {
                        Memo_Issue = txtEmp_Memo_Issue.SelectedItem.Text;
                    }


                    ResponseHelper.Redirect("Memo_ViewReport.aspx?Category=" + Category + "&Employee_Type=" + Employee_Type + "&Token_No=" + Token_No + "&Memo_Action=" + Memo_Action + "&Memo_Issue=" + Memo_Issue + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&Report_Type=Memo_Details_Report", "_blank", "");
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
}
