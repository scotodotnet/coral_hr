<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Report_Purchase_Request.aspx.cs" Inherits="Reports_Report_Purchase_Request" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });
                $('.select2').select2();
            }
        });
    };
</script>

<div class="content-wrapper">
        <!-- Content Header (Page header) -->
    <asp:UpdatePanel ID="upRptPurReq" runat="server">
        <ContentTemplate>
            <section class="content-header">
            <h1><i class=" text-primary"></i>Purchase Request Report</h1>
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">                          
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputName">Department</label>
					                <asp:DropDownList ID="txtDepartmentName" runat="server" class="form-control select2">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputName">Pur. Request No</label>
					                <asp:DropDownList ID="txtPurReqNo" runat="server" class="form-control select2">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
					            <div class="form-group">
                                    <label for="exampleInputName">Material Name</label>
                                    <asp:DropDownList ID="txtItemNameSelect" runat="server" class="form-control select2" AutoPostBack="true" 
                                        onselectedindexchanged="txtItemNameSelect_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtItemName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtItemCode" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                    <asp:HiddenField ID="txtItemCodeHide" runat="server" />
                                </div>
					        </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputName">From Date</label>
					                <asp:TextBox ID="txtFromDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputName">To Date</label>
					                <asp:TextBox ID="txtToDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">                            
                            <asp:Button ID="btnReports" class="btn btn-primary"  runat="server" 
                                Text="Report" onclick="btnReports_Click" />
                            <asp:Button ID="BtnPending_Request_With_PO" class="btn btn-primary"  runat="server" 
                                Text="Pending Request With PO" onclick="BtnPending_Request_With_PO_Click" />
                            <asp:Button ID="btnApp_Cancel" class="btn btn-primary" runat="server"    
                                Text="Approval_Cancel" onclick="btnApp_Cancel_Click" />
                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" 
                                onclick="btnClear_Click" />
                        </div>
                    </div>
                                <!-- /.box-footer-->
                </div>
                            <!-- /.box -->
            </div>
            
        </div>
                   
        </section>
        </ContentTemplate>
    </asp:UpdatePanel>
        
        <!-- /.content -->
    </div>



</asp:Content>

