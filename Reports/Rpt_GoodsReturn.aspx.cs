﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Rpt_GoodsReturn : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Report Goods Return";
        }
        
        Load_Data_Customer();
        Load_Data_Item();
    }

    private void Load_Data_Customer()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " and Status='Add' And LedgerGrpName='Supplier'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlSupplier.DataSource = DT;

        dr = DT.NewRow();
        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlSupplier.DataValueField = "LedgerCode";
        ddlSupplier.DataTextField = "LedgerName";
        ddlSupplier.DataBind();
    }

    private void Load_Data_Item()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataRow dr;

        SSQL = "select Mat_No,Raw_Mat_Name from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Status = 'Add'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlItem.DataSource = DT;

        dr = DT.NewRow();
        dr["Mat_No"] = "-Select-";
        dr["Raw_Mat_Name"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItem.DataValueField = "Mat_No";
        ddlItem.DataTextField = "Raw_Mat_Name";
        ddlItem.DataBind();
    }

    protected void btnRetList_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        DataGrid GvExcel = new DataGrid();
        string daycolspan = "";

        SSQL = "Select GRetNo[Return No],GRetDate[Return Date],GReceiveNo[Receipt No],GReceiveDate[Receipt Date],SuppName[Supplier],";
        SSQL = SSQL + " TransPortName[Transport],VehicleNo[Vehicle No],ReasonForRet[Reason],Notes[Notes],Remarks[Remarks],TotalQty[Qty], ";
        SSQL = SSQL + " TotalAmount[NetAmount] From Trans_GoodsReturn_Main where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "'";


        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            SSQL = SSQL + " And Convert (DateTime,GRetDate, 103)>=Convert (DateTime,'" + txtFromDate.Text.ToString() + "',103) ";
            SSQL = SSQL + " And Convert (DateTime,GRetDate, 103)<=Convert (DateTime,'" + txtToDate.Text.ToString() + "',103) ";
        }
        //else
        //{
        //    SSQL = SSQL + " And Convert (DateTime,GRetDate, 103)>=Convert (DateTime,Getdate(),103) ";
        //    SSQL = SSQL + " And Convert (DateTime,GRetDate, 103)<=Convert (DateTime,Getdate(),103) ";
        //}

        if (ddlSupplier.SelectedItem.Text != "-Select-")
        {
            SSQL = SSQL + " And SuppName='" + ddlSupplier.SelectedItem.Text + "'";
        }

        if (ddlItem.SelectedItem.Text != "-Select-")
        {
            //SSQL = SSQL + " And SuppName='" + ddlCust.SelectedItem.Text + "'";
        }

        SSQL = SSQL + " And Status =''";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        string attachment = "attachment;filename=GoodsReutrn.xls";
        Response.Clear();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        GvExcel.DataSource = DT;
        GvExcel.DataBind();

        GvExcel.HeaderStyle.Font.Bold = true;

        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        GvExcel.RenderControl(htextw);

        Response.Write("<table>");

        Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
        Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 12) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> Coral Manufacturing Work </a>");
        Response.Write("--");
        Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 12) + "'>");
        Response.Write("<a style=\"font-weight:bold\">Goods Return Report</a>");
        Response.Write("  ");

        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr Font-Bold='true' align='center'>");

        Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 12) + "'>");

        if (txtFromDate.Text != "")
        {
            Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtFromDate.Text + "</a>");
        }
        //else
        //{
        //    Response.Write("<a style=\"font-weight:bold\"> FROM  -" + DateTime.Now.ToShortDateString() + "</a>");
        //}

        Response.Write("&nbsp;&nbsp;&nbsp;");

        if (txtToDate.Text != "")
        {
            Response.Write("<a style=\"font-weight:bold\"> TO -" + txtToDate.Text + "</a>");
        }
        //else
        //{
        //    Response.Write("<a style=\"font-weight:bold\"> TO  -" + DateTime.Now.ToShortDateString() + "</a>");
        //}

        Response.Write("  ");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();
    }

    protected void btnSamplereport_Click(object sender, EventArgs e)
    {
        string RptName = "";

        RptName = "Goods Return Report";

        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?SuppName=" + ddlSupplier.SelectedItem.Text + "&ItemName=" + ddlItem.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blanck", "");
    }
}