﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Report_StdPurchase_Order_Det.aspx.cs" Inherits="Reports_Report_StdPurchase_Order_Det" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });
                $('.select2').select2();
            }
        });
    };
</script>

<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class=" text-primary"></i>Purchase Order Report</h1>
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">                          
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputName">Department</label>
					                <asp:DropDownList ID="txtDepartmentName" runat="server" class="form-control select2">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier</label>
					                <asp:DropDownList ID="txtSupplierNameSelect" runat="server" class="form-control select2" AutoPostBack="true" 
                                        onselectedindexchanged="txtSupplierNameSelect_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                    <asp:HiddenField ID="txtSuppCodehide" runat="server" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputName">Std.Pur_Order.No</label>
					                <asp:DropDownList ID="txtStdPurOrdNo" runat="server" class="form-control select2">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Sup.Qut.No</label>
					                <asp:DropDownList ID="txtSPONo" runat="server" class="form-control select2">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
					            <div class="form-group">
                                    <label for="exampleInputName">Material Name</label>
                                    <asp:DropDownList ID="txtItemNameSelect" runat="server" class="form-control select2" AutoPostBack="true" 
                                        onselectedindexchanged="txtItemNameSelect_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtItemName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtItemCode" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                    <asp:HiddenField ID="txtItemCodeHide" runat="server" />
                                </div>
					        </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputName">From Date</label>
					                <asp:TextBox ID="txtFromDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputName">To Date</label>
					                <asp:TextBox ID="txtToDate" runat="server" class="form-control datepicker"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">                            
                            <asp:Button ID="btnReport" class="btn btn-primary"  runat="server" 
                                Text="Details Report" onclick="btnReport_Click" />
                            <asp:Button ID="btnInvcFormat" class="btn btn-primary" runat="server" Text="Invoice Format" 
                                onclick="btnInvcFormat_Click" />
                            <asp:Button ID="btnPendingPO_With_Receipt" class="btn btn-primary" runat="server" Text="PendingPO With Receipt" 
                                onclick="btnPendingPO_With_Receipt_Click" />
                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Text="Clear" 
                                onclick="btnClear_Click" />
                        </div>
                    </div>
                                <!-- /.box-footer-->
                </div>
                            <!-- /.box -->
            </div>
            
        </div>
                   
        </section>
        <!-- /.content -->
    </div>

</asp:Content>

