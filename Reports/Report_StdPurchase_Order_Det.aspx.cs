﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Reports_Report_StdPurchase_Order_Det : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string DeptName = ""; string SupQtnNo = ""; string StdPurOrdNo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "CORAL ERP :: Standard Purchase Order Details";

        if (!IsPostBack)
        {
            Load_Data_Empty_Dept();
            Load_Data_Empty_SupQtnNo();
            Load_Data_Empty_StdPurOrdNo();
            Load_Data_Empty_ItemCode();
            Load_Data_Empty_Supp1();
        }

        
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDepartmentName.SelectedValue = "-Select-";
        txtSupplierNameSelect.SelectedIndex = 0;
        txtItemNameSelect.SelectedIndex = 0;
        // txtDeptCodeHide.Value = "";
        txtSupplierName.Text = "";
        txtSuppCodehide.Value = "";
        txtStdPurOrdNo.SelectedValue = "-Select-";
        //txtStdPurOrdDateHide.Value = "";
        txtSPONo.SelectedValue = "-Select-";
        //txtSPODateHide.Value = "";
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        RptName = "Standard Purchase Order Details Report";

        if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
        {
            StdPurOrdNo = txtStdPurOrdNo.SelectedItem.Text;
        }
        else
        {
            StdPurOrdNo = "";
        }
        if (txtSPONo.SelectedItem.Text != "-Select-")
        {
            SupQtnNo = txtSPONo.SelectedItem.Text;
        }
        else
        {
            SupQtnNo = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }



        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + txtSupplierName.Text + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnInvcFormat_Click(object sender, EventArgs e)
    {
        RptName = "Standard Purchase Order Details Invoice Format";


        if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
        {
            StdPurOrdNo = txtStdPurOrdNo.SelectedItem.Text;
        }
        else
        {
            StdPurOrdNo = "";
        }
        if (txtSPONo.SelectedItem.Text != "-Select-")
        {
            SupQtnNo = txtSPONo.SelectedItem.Text;
        }
        else
        {
            SupQtnNo = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + txtSupplierName.Text + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }
    

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();


    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select Mat_No as ItemCode,Raw_Mat_Name as ItemName from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        txtItemNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemNameSelect.DataTextField = "ItemName";
        txtItemNameSelect.DataValueField = "ItemCode";
        txtItemNameSelect.DataBind();

    }

    protected void txtItemNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtItemNameSelect.SelectedValue != "-Select-")
        {
            txtItemCode.Text = txtItemNameSelect.SelectedValue;
            txtItemName.Text = txtItemNameSelect.SelectedItem.Text;
            txtItemCodeHide.Value = txtItemNameSelect.SelectedValue;
        }
        else
        {
            txtItemCode.Text = ""; txtItemName.Text = ""; txtItemCodeHide.Value = "";
        }
    }

    
    protected void btnSupQtnNo_Click(object sender, EventArgs e)
    {
        //modalPop_SupQtnNo.Show();
    }
    private void Load_Data_Empty_SupQtnNo()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtSPONo.Items.Clear();
        query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";

        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtSPONo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["QuotNo"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtSPONo.DataTextField = "QuotNo";

        txtSPONo.DataBind();



    }

    protected void GridViewClick_SupQtnNo(object sender, CommandEventArgs e)
    {
        txtSPONo.Text = Convert.ToString(e.CommandArgument);
        //txtSPODateHide.Value = Convert.ToString(e.CommandName);


    }

    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        //query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        query = "Select (LedgerName) as SuppName,LedgerCode as SuppCode from Acc_Mst_Ledger where LedgerGrpName='Supplier' ";
        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and Status='Add' ";

        DT = objdata.RptEmployeeMultipleDetails(query);

        txtSupplierNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtSupplierNameSelect.DataTextField = "SuppName";
        txtSupplierNameSelect.DataValueField = "SuppCode";
        txtSupplierNameSelect.DataBind();

    }

    protected void txtSupplierNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtSupplierNameSelect.SelectedValue != "-Select-")
        {
            txtSupplierName.Text = txtSupplierNameSelect.SelectedItem.Text;
            txtSuppCodehide.Value = txtSupplierNameSelect.SelectedValue;
        }
        else
        {
            txtSupplierName.Text = ""; txtSuppCodehide.Value = "";
        }
    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);


    }


    protected void btnStdPurOrdNo_Click(object sender, EventArgs e)
    {
        //modalPop_StdPurOrdNo.Show();
    }
    private void Load_Data_Empty_StdPurOrdNo()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtStdPurOrdNo.Items.Clear();
        query = "Select Std_PO_No from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And PO_Status='1'"; 
        query = query + " Order by Std_PO_No Desc";

        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtStdPurOrdNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Std_PO_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtStdPurOrdNo.DataTextField = "Std_PO_No";

        txtStdPurOrdNo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Std_PO_No,Std_PO_Date from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And PO_Status='1'"; 
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_StdPurOrdNo.DataSource = DT;
        //Repeater_StdPurOrdNo.DataBind();
        //Repeater_StdPurOrdNo.Visible = true;

    }

    protected void GridViewClick_StdPurOrdNo(object sender, CommandEventArgs e)
    {
        txtStdPurOrdNo.Text = Convert.ToString(e.CommandArgument);
        //txtStdPurOrdDateHide.Value = Convert.ToString(e.CommandName);

    }

    protected void btnPendingPO_With_Receipt_Click(object sender, EventArgs e)
    {
        RptName = "Pending Standard Purchase Order With Receipt Details";


        if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
        {
            StdPurOrdNo = txtStdPurOrdNo.SelectedItem.Text;
        }
        else
        {
            StdPurOrdNo = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?SupplierName=" + txtSupplierName.Text + "&StdPurOrdNo=" + StdPurOrdNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }
}

