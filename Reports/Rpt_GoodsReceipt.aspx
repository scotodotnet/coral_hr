﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Rpt_GoodsReceipt.aspx.cs" Inherits="Rpt_GoodsReceipts" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
     <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.js-states').select2();
                }
            });
        };
    </script>

    <div class="content-wrapper">
        <asp:UpdatePanel ID="upGoodRecp" runat="server">
            <ContentTemplate>
                <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Report Goods Received</span></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding" >             
                            <div class="col-md-12" style="padding-bottom:15px;padding-top:15px">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="exampleInputName">Supplier Name</label>
                                        <asp:DropDownList ID="ddlSupplier" runat="server" class="form-control select2" ></asp:DropDownList>
                                        
                                    </div>

                                    <div class="col-md-3">
                                        <label for="exampleInputName">Item Name</label>
                                        <asp:DropDownList ID="ddlItem" runat="server" class="form-control select2" ></asp:DropDownList>
                                    </div>

                                     <div class="col-md-3">
                                        <label for="exampleInputName">From Date</label>
                                         <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtFromDate" ValidChars="0123456789./">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>

                                     <div class="col-md-3">
                                        <label for="exampleInputName">To Date</label>
                                         <asp:TextBox ID="txtToDate" MaxLength="20" class="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtToDate" ValidChars="0123456789./">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </div>
                        <!-- /.mail-box-messages -->
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <div class="form-group">
                                <asp:Button ID="btnReceiptList" class="btn btn-primary" runat="server" Text="Received List" OnClick="btnRecList_Click"/>
                                <asp:Button ID="btnSamplereport" class="btn btn-primary" runat="server" Text="Sample Report" OnClick="btnSamplereport_Click"/>
                            </div>
                        </div>

                        <asp:UpdatePanel ID="Panel1" runat="server">
                            <ContentTemplate>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnReceiptList" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
</asp:Content>

