﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Purchase_Request : System.Web.UI.Page
{
   
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string DeptName = ""; string PurReqNo = ""; string RequestBy = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

         Page.Title = "CORAL ERP :: Purchase Request Report";
         if (!IsPostBack)
         {
             Load_Data_Empty_Dept();
             Load_Data_Empty_PurReqNo();
            Load_Data_Empty_ItemCode();
        }

        
        
        
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDepartmentName.SelectedValue = "-Select-";
        txtItemNameSelect.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        txtPurReqNo.SelectedValue = "-Select-";
        //txtPurReqDateHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtItemCodeHide.Value = "";
        txtItemName.Text = "";

    }

    protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "Purchase Request Report";

        if (txtPurReqNo.SelectedItem.Text != "-Select-")
        {
            PurReqNo = txtPurReqNo.SelectedItem.Text;
        }
        else
        {
            PurReqNo = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&PurReqNo=" + PurReqNo + "&Requestby=" + RequestBy + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select Mat_No as ItemCode,Raw_Mat_Name as ItemName from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);        
        txtItemNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemNameSelect.DataTextField = "ItemName";
        txtItemNameSelect.DataValueField = "ItemCode";
        txtItemNameSelect.DataBind();

    }

    protected void txtItemNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtItemNameSelect.SelectedValue != "-Select-")
        {
            txtItemCode.Text = txtItemNameSelect.SelectedValue;
            txtItemName.Text = txtItemNameSelect.SelectedItem.Text;
            txtItemCodeHide.Value = txtItemNameSelect.SelectedValue;
        }
        else
        {
            txtItemCode.Text = "";txtItemName.Text = ""; txtItemCodeHide.Value = "";
        }
    }

    private void Load_Data_Empty_PurReqNo()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtPurReqNo.Items.Clear();
        query = "Select Pur_Request_No,Pur_Request_Date from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
      
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtPurReqNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Pur_Request_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtPurReqNo.DataTextField = "Pur_Request_No";

        txtPurReqNo.DataBind();





    }


    protected void btnApp_Cancel_Click(object sender, EventArgs e)
    {
        bool Errflag = false;
        if (txtPurReqNo.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Purchase Request No Not Select!........');", true);
            Errflag = true;
        }
        if (!Errflag)
        {
            DataTable da_MT_Val = new DataTable();
            string SSQL = "";
            DataTable da_Update = new DataTable();
            SSQL = "update dbo.Pur_Request_Main set Status='0' where Pur_Request_No='" + txtPurReqNo.SelectedItem.Text + "' ";
            SSQL = SSQL + " and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            da_Update = objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "update dbo.Pur_Request_Approval set Status='0' where Transaction_No='" + txtPurReqNo.SelectedItem.Text + "' ";
            SSQL = SSQL + " and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            da_Update = objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert(' Updated Successfully......');", true);
        }

    }

    protected void BtnPending_Request_With_PO_Click(object sender, EventArgs e)
    {
        RptName = "Pending Purchase Request With PO Details";

        if (txtPurReqNo.SelectedItem.Text != "-Select-")
        {
            PurReqNo = txtPurReqNo.SelectedItem.Text;
        }
        else
        {
            PurReqNo = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text.ToString();
        }
        else
        {
            DeptName = "";
        }
        


        ResponseHelper.Redirect("ReportDisplay.aspx?ItemName=" + txtItemName.Text + "&PurReqNo=" + PurReqNo + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&DeptName=" + DeptName + "&RptName=" + RptName, "_blank", "");
    }
}
