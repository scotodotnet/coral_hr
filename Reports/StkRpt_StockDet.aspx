﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StkRpt_StockDet.aspx.cs" Inherits="StkRpt_StockDet" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
     <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.js-states').select2();
                }
            });
        };
    </script>

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-stack-exchange text-primary"></i> <span> Stock Details</span></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding" >             
                            <div class="col-md-12" style="padding-bottom:15px;padding-top:15px">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="exampleInputName">Supplier</label>
                                        <asp:DropDownList ID="ddlSupplier" runat="server" class="form-control select2" ></asp:DropDownList>
                                    </div>

                                    <div class="col-md-3">
                                        <label for="exampleInputName">WareHouse</label>
                                        <asp:DropDownList ID="ddlWarehouse" runat="server" class="form-control select2" ></asp:DropDownList>
                                    </div>

                                    <div class="col-md-3">
                                        <label for="exampleInputName">Department</label>
                                        <asp:DropDownList ID="ddlDept" runat="server" class="form-control select2" ></asp:DropDownList>
                                    </div>

                                    <div class="col-md-3">
                                        <label for="exampleInputName">Item Name</label>
                                        <asp:DropDownList ID="ddlItem" runat="server" class="form-control select2" ></asp:DropDownList>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="exampleInputName">From Date</label>
                                         <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtFromDate" ValidChars="0123456789./">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>

                                    <div class="col-md-3">
                                        <label for="exampleInputName">To Date</label>
                                         <asp:TextBox ID="txtToDate" MaxLength="20" class="form-control datepicker" runat="server" autocomplete="off"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtToDate" ValidChars="0123456789./">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </div>
                        <!-- /.mail-box-messages -->
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <div class="form-group">
                                <asp:Button ID="btnStockDet" class="btn btn-primary" runat="server" Text="Stock Details" OnClick="btnStockDet_Click"/>
                                <asp:Button ID="btnStkConsRet" class="btn btn-primary" runat="server" Text="Stock Consolidate" OnClick="btnStkConsRet_Click"/>
                                <asp:Button ID="btnDeptWise" class="btn btn-primary" runat="server" Text="Department Wise" OnClick="btnDeptWise_Click"/>
                                <asp:Button ID="btnItemRackWise" class="btn btn-primary" runat="server" Text="Rack Wise" OnClick="btnItemRackWise_Click"/>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
</asp:Content>

