﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

public partial class HR_Training_History : System.Web.UI.Page
{
    public static BALDataAccess objdata = new BALDataAccess();

    public static String CurrentYear1;
    public static int CurrentYear;

    public static string SessionCcode;
    public static string SessionLcode;
    public static string SessionUserID;
    public static string SessionUserName;
    public static string SessionRights;

    string MonthID;
    string Month;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Training History";
            Load_EmpNo();

        }
        Load_Data();
    }

    private void Load_EmpNo()
    {
        SSQL = "";
        SSQL = "Select * from Employee_mst where CompCode='" + SessionCcode + "' and  LocCode='" + SessionLcode + "'";
        DataTable dt_ = new DataTable();
        dt_ = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMachineID.DataSource = dt_;
        ddlMachineID.DataTextField = "MachineID";
        ddlMachineID.DataValueField = "MachineID";
        ddlMachineID.DataBind();
        ddlMachineID.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

        ddlTokenNo.DataSource = dt_;
        ddlTokenNo.DataTextField = "ExistingCode";
        ddlTokenNo.DataValueField = "ExistingCode";
        ddlTokenNo.DataBind();
        ddlTokenNo.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlMachineID.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the MachineID');", true);
            ErrFlag = true;
            return;
        }
        if (ddlTokenNo.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Token No');", true);
            ErrFlag = true;
            return;
        }
        if (txtStartDate.Text == "" || txtEndDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Start Date and End Date Properly');", true);
            ErrFlag = true;
            return;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Delete from Training_History where Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' and machineID='" + ddlMachineID.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert into Training_History(Ccode,Lcode,MachineID,ExistingCode,Name,DeptName,TrainingDetails,Location,TrainingAgency,Start_Date_Str,Start_Date,End_Date_Str,End_Date,Days,Status,Certification,Valid_Date_Str,Valid_Date,CreatedOn)";
            SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlMachineID.SelectedValue + "','" + ddlTokenNo.SelectedValue + "','" + txtName.Text + "','" + txtDeptName.Text + "','" + txtTrainingDetails.Text + "'";
            SSQL = SSQL + ", '" + txtLocation.Text + "','" + txtTrainingAgency.Text + "','" + txtStartDate.Text + "',Convert(date,'" + txtStartDate.Text + "',103),'" + txtEndDate.Text + "',Convert(date,'" + txtEndDate.Text + "',103),'" + txtDays.Text + "'";
            SSQL = SSQL + ",'" + txtStaus.Text + "','" + rbtCertificate.SelectedValue + "','" + txtValidity.Text + "',Convert(date,'" + txtValidity.Text + "',103),'" + DateTime.Now.ToString("dd/MM/yyyy") + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            if (btnSave.Text == "Save")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Saved Successfully!!!');", true);

            }
            if (btnSave.Text == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Updated Successfully!!!');", true);
            }
            btnClear_Click(sender, e);
            Load_Data();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlMachineID.ClearSelection();
        ddlTokenNo.ClearSelection();
        txtName.Text = "";
        txtDeptName.Text = "";
        txtTrainingDetails.Text = "";
        txtLocation.Text = "";
        txtTrainingAgency.Text = "";
        txtStartDate.Text = "";
        txtEndDate.Text = "";
        txtDays.Text = "";
        txtStaus.Text = "";
        rbtCertificate.SelectedValue = "Yes";
        txtValidity.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Training_History where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ddlMachineID.SelectedValue = dt.Rows[0]["MachineID"].ToString();
            ddlTokenNo.SelectedValue = dt.Rows[0]["ExistingCode"].ToString();
            txtName.Text = dt.Rows[0]["Name"].ToString();
            txtDeptName.Text = dt.Rows[0]["DeptName"].ToString();
            txtTrainingDetails.Text= dt.Rows[0]["TrainingDetails"].ToString();
            txtLocation.Text= dt.Rows[0]["Location"].ToString();
            txtTrainingAgency.Text= dt.Rows[0]["TrainingAgency"].ToString();
            txtStartDate.Text= dt.Rows[0]["Start_Date_Str"].ToString();
            txtEndDate.Text= dt.Rows[0]["End_Date_Str"].ToString();
            txtDays.Text= dt.Rows[0]["Days"].ToString();
            txtStaus.Text= dt.Rows[0]["Status"].ToString();
            txtValidity.Text= dt.Rows[0]["Valid_Date_Str"].ToString();
            rbtCertificate.SelectedValue= dt.Rows[0]["Certification"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from Training_History where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from Training_History where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void ddlMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_EmpData("MachineID");
    }

    private void Load_EmpData(string Base)
    {
        SSQL = "";
        SSQL = "Select * from Employee_mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        if (Base == "MachineID")
        {
            SSQL = SSQL + " and MachineID='" + ddlMachineID.SelectedValue + "'";
            ddlTokenNo.SelectedValue = ddlMachineID.SelectedItem.Text;
        }
        if (Base == "TokenNo")
        {
            SSQL = SSQL + " and ExistingCode='" + ddlTokenNo.SelectedValue + "'";
            ddlMachineID.SelectedValue = ddlTokenNo.SelectedItem.Text;
        }

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtName.Text = dt.Rows[0]["FirstName"].ToString();
            txtDeptName.Text = dt.Rows[0]["DeptName"].ToString();
        }
    }

    protected void ddlTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_EmpData("TokenNo");
    }

    protected void txtStartDate_TextChanged(object sender, EventArgs e)
    {
        Calculate_days();
    }

    private void Calculate_days()
    {
        if (txtStartDate.Text != "" && txtEndDate.Text != "")
        {
            txtDays.Text = (1 + (Convert.ToDateTime(txtEndDate.Text) - Convert.ToDateTime(txtStartDate.Text)).Days).ToString();
        }
    }

    protected void txtEndDate_TextChanged(object sender, EventArgs e)
    {
        Calculate_days();
    }
}