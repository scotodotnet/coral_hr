﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HR_Training_History.aspx.cs" Inherits="HR_Training_History" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <!-- begin #content -->
    <div id="content" class="content-wrapper">
        <section class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">HR Training</a></li>
                <li class="active">HR Training</li>
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">HR Training </h1>
            <!-- end page-header -->

            <!-- begin row -->
            <div class="row">
                <!-- begin col-12 -->
                <div class="col-md-12">
                    <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <div class="panel-body">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>MachineID</label>
                                                <asp:DropDownList ID="ddlMachineID" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlMachineID_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlMachineID" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Token No</label>
                                                <asp:DropDownList ID="ddlTokenNo" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlTokenNo_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlTokenNo" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <asp:Label ID="txtName" runat="server" Text="" CssClass="form-control"></asp:Label>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Department</label>
                                                <asp:Label ID="txtDeptName" runat="server" Text="" CssClass="form-control"></asp:Label>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Training Details</label>
                                                <asp:TextBox ID="txtTrainingDetails" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlTokenNo" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Location</label>
                                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtLocation" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Training Agency</label>
                                                <asp:TextBox ID="txtTrainingAgency" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtTrainingAgency" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Start Date</label>
                                                <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control datepicker startdate" AutoPostBack="true" OnTextChanged="txtStartDate_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtStartDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>End Date</label>
                                                <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control datepicker enddate" AutoPostBack="true" OnTextChanged="txtEndDate_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtEndDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Days</label>
                                                <asp:Label ID="txtDays" runat="server" CssClass="form-control totalDays"></asp:Label>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <asp:TextBox ID="txtStaus" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtStaus" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Certificate</label>
                                                <asp:RadioButtonList ID="rbtCertificate" runat="server" CssClass="form-control" RepeatColumns="3" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Yes" Value="Yes" style="padding-right: 40px" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Validity</label>
                                                <asp:TextBox ID="txtValidity" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtTrainingAgency" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                    ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger"
                                                    OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->

                                    <!-- table start -->
                                    <asp:Panel runat="server" ID="Panel1" ScrollBars="Both">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table class="table table-responsive table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>MachineID</th>
                                                                    <th>Token No</th>
                                                                    <th>Name</th>
                                                                    <th>Dept Name</th>
                                                                    <th>TrainingDetails</th>
                                                                    <th>Location</th>
                                                                    <th>TrainingAgency</th>
                                                                    <th>Start Date</th>
                                                                    <th>End Date</th>
                                                                    <th>Days</th>
                                                                    <th>Status</th>
                                                                    <th>Certification</th>
                                                                    <th>Valid To</th>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1%></td>
                                                            <td><%# Eval("MachineID")%></td>
                                                            <td><%# Eval("ExistingCode")%></td>
                                                            <td><%# Eval("Name")%></td>
                                                            <td><%# Eval("DeptName")%></td>
                                                            <td><%# Eval("TrainingDetails")%></td>
                                                            <td><%# Eval("Location")%></td>
                                                            <td><%# Eval("TrainingAgency")%></td>
                                                            <td><%# Eval("Start_Date_Str")%></td>
                                                            <td><%# Eval("End_Date_Str")%></td>
                                                            <td><%# Eval("Days")%></td>
                                                            <td><%# Eval("Status")%></td>
                                                            <td><%# Eval("Certification")%></td>
                                                            <td><%# Eval("Valid_Date")%></td>

                                                            <td>
                                                                <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                    Text="" OnCommand="btnEditEnquiry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("Auto_ID")%>'>
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                    Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("Auto_ID")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Training details?');">
                                                                </asp:LinkButton>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <!-- table End -->
                                    </asp:Panel>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </section>
    </div>
    <!-- end #content -->


    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        });
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('#example').dataTable();
                    $('.datepicker').datepicker({
                        autoclose: true,
                        format: 'dd/mm/yyyy'
                    });

                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
</asp:Content>

