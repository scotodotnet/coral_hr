﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MainDashboard.aspx.cs" Inherits="MainDashboard" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="@entityIcon text-primary"></i><span>Dashboard</span></h3>
                            <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div id="main-wrapper" class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="box-header with-border">
                                        <asp:Label ID="lblTit1" runat="server" Visible="false" class="box-title">
                                        <i class="@entityIcon text-primary"></i><span>Monthly Strength</span></asp:Label>
                                    </div>
                                    <asp:Chart ID="chSuppWisePurDet" runat="server" Compression="100" EnableViewState="true" Width="490px">
                                        <Series>
                                            <asp:Series Name="sriSuppPurDet" ChartArea="caPurDet" ChartType="Bar" Label="#VALX: #VALY"
                                                IsXValueIndexed="true">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="caPurDet"></asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>

                                    <asp:Chart ID="Chart1" runat="server" Height="300px" Width="400px" Visible="false">
                                        <Titles>
                                            <asp:Title ShadowOffset="3" Name="Items" />
                                        </Titles>
                                        <Legends>
                                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                                LegendStyle="Row" />
                                        </Legends>
                                        <Series>
                                            <asp:Series Name="Default" />
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                                        </ChartAreas>
                                    </asp:Chart>
                                    <asp:Chart ID="Chart2" runat="server" Width="488px">
                                        <Series>
                                            <asp:Series Name="Series1" XValueMember="0" YValueMembers="2">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>

                                <div class="col-sm-6">
                                    <div class="box-header with-border">
                                        <asp:Label runat="server" ID="lblTit2" class="box-title" Visible="false">
                                        <i class="@entityIcon text-primary"></i><span>Percentage</span></asp:Label>
                                    </div>
                                    <asp:Chart ID="chItemWisePurDet" runat="server" Compression="100" EnableViewState="true" Width="490px">
                                        <Series>
                                            <asp:Series Name="sriItemPurDet" ChartArea="caPurDet" ChartType="Pie" Label="#VALX: #VALY"
                                                IsXValueIndexed="true">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="caPurDet"></asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>
                            </div>
                        </div>

                        <div class="box-body no-padding">
                            <%-- @*<div class="mailbox-controls">
                                    <div class="text-center">
                                        <button type="button" id="btn-export-excel" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i></button>
                                        <button type="button" id="btn-export-pdf" class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o"></i></button>
                                        <button type="button" id="btn-export-csv" class="btn btn-primary btn-sm"><i class="fa fa-file-text-o"></i></button>
                                    </div>
                                </div>*@--%>
                            <div class="table-responsive mailbox-messages">

                                <!-- /.table -->
                            </div>
                            <!-- /.mail-box-messages -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer no-padding">
                        </div>
                    </div>
                    <div hidden class="callout callout-info" style="margin-bottom: 0!important;">
                        <h4><i class="fa fa-info"></i>Info:</h4>
                        <p>
                        </p>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</asp:Content>

