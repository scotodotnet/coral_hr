﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Employee_Profile_Employee_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionAdmin;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "HR | Employee Details";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Menu-EmployeeProfile"));
            //li.Attributes.Add("class", "has-sub active open");

            Session.Remove("MachineID_Apprv");
        }
        CreateEmployeeDisplay();
    }

    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        Session.Remove("MachineID_Apprv");
        Session["MachineID"] = e.CommandName.ToString();
        Response.Redirect("EmployeeDetails.aspx");
    }

    protected void lbtnAdd_Click(object sender, EventArgs e)
    {
        Session.Remove("MachineID");
        Response.Redirect("EmployeeDetails.aspx");
    }
    public void CreateEmployeeDisplay()
    {
        DataTable dtDisplay = new DataTable();
        string IsActive = "";
        string EmpStatus = "";
        string Query = "";
        if (rbtnIsActive.SelectedValue == "1" || rbtnIsActive.SelectedValue == "2")
        {
            if (rbtnIsActive.SelectedValue == "1")
            {
                IsActive = "Yes";
            }
            else if (rbtnIsActive.SelectedValue == "2")
            {
                IsActive = "No";
            }


            Query = "select MachineID,ExistingCode,FirstName,CatName,Gender,DeptName,Designation,CONVERT(varchar(10),BirthDate,103) as DOB from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='" + IsActive + "'";
            if (SessionAdmin == "2")
            {
                Query = Query + " And Eligible_PF='1'";
            }
            Query = Query + " Order by ExistingCode Asc";

        }
        if (rbtnIsActive.SelectedValue == "3" || rbtnIsActive.SelectedValue == "4")
        {
            if (rbtnIsActive.SelectedValue == "3")
            {
                EmpStatus = "Pending";
            }
            else if (rbtnIsActive.SelectedValue == "4")
            {
                EmpStatus = "Cancel";
            }
            Query = "Select  ES.MachineID,ENJ.ExistingCode,ENJ.FirstName,ENJ.CatName,ENJ.Gender,ENJ.DeptName,ENJ.Designation,CONVERT(varchar(10),ENJ.BirthDate,103) as DOB";
            Query = Query + " from Employee_Mst_New_Emp ENJ inner join Employee_Mst_Status ES on ES.CompCode=ENJ.CompCode";
            Query = Query + " And ES.LocCode=ENJ.LocCode And ES.Token_No=ENJ.ExistingCode And ES.MachineID=ENJ.MachineID where ENJ.CompCode='" + SessionCcode + "'";
            Query = Query + " And ENJ.LocCode='" + SessionLcode + "' And ES.CompCode='" + SessionCcode + "' AND ES.LocCode='" + SessionLcode + "' And ENJ.IsActive='Yes'";
            Query = Query + " And ES.Emp_Status='" + EmpStatus + "' ";
            Query = Query + " Order by ES.Token_No Asc";
        }


        dtDisplay = objdata.RptEmployeeMultipleDetails(Query);

        //dtdDisplay = objdata.EmployeeDisplay(SessionCcode,SessionLcode);
        if (dtDisplay.Rows.Count > 0)
        {
            Repeater1.DataSource = dtDisplay;
            Repeater1.DataBind();
        }


    }

    protected void rbtnIsActive_SelectedIndexChanged(object sender, EventArgs e)
    {
        CreateEmployeeDisplay();

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable Doc_dt = new DataTable();

        if (txtAdharNo.Text != "")
        {
            string SSQL = "";//"Select EM.ExistingCode,EM.LocCode from Employee_Doc_Mst ED inner join Employee_Mst EM on ED.EmpNo=EM.EmpNo";
            //SSQL = SSQL + " Where ED.CompCode='" + SessionCcode + "' and EM.CompCode='" + SessionCcode + "' and DocNo='" + txtAdharNo.Text + "'";
            //SSQL = SSQL + " And DocType='Adhar Card'";

            SSQL = "Select ExistingCode,LocCode from Employee_Doc_Mst where CompCode='" + SessionCcode + "' And DocType ='Adhar Card' and DocNo='" + txtAdharNo.Text + "'";

            Doc_dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Doc_dt.Rows.Count != 0)
            {
                txtTokenNo.Text = Doc_dt.Rows[0]["ExistingCode"].ToString();
                txtLocation.Text = Doc_dt.Rows[0]["LocCode"].ToString();
            }
            else
            {
                txtTokenNo.Text = "";
                txtLocation.Text = "";
            }
        }
        else
        {
            txtTokenNo.Text = "";
            txtLocation.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the ADHAR NO...');", true);
        }
    }
}
