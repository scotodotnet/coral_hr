﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HR_Employee_Expenses.aspx.cs" Inherits="HR_Employee_Profile_HR_Employee_Expenses" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- begin #content -->
    <div id="content" class="content-wrapper">
        <section class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">Employees</a></li>
                <li class="active">Expenses Details</li>
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Expenses Details </h1>
            <!-- end page-header -->

            <!-- begin row -->
            <div class="row">
                <!-- begin col-12 -->
                <div class="col-md-12">
                    <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <div class="panel-body">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Particulars</label>
                                                <asp:TextBox ID="txtParticulars" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtParticulars" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <asp:HiddenField runat="server" ID="HdnAuto_ID" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Expenses Type</label>
                                                <asp:DropDownList ID="ddlExpensesType" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                    <asp:ListItem Text="-Select-" Value="-Select-"></asp:ListItem>
                                                    <asp:ListItem Text="Safety" Value="Safety"></asp:ListItem>
                                                    <asp:ListItem Text="Welfare" Value="Welfare"></asp:ListItem>
                                                    <asp:ListItem Text="Statutory" Value="Statutory"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Qty</label>
                                                <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtQty" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtQty" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Cost</label>
                                                <asp:TextBox ID="txtCost" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtCost" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtCost" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- begin row -->
                                    <div class="row">

                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Start Date</label>
                                                <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtStartDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtStartDate" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Next Due Date</label>
                                                <asp:TextBox ID="txtNextDueDate" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtNextDueDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtNextDueDate" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Life Spam</label>
                                                <asp:TextBox ID="txtLifeSpam" runat="server" CssClass="form-control" Text="0" AutoPostBack="true" OnTextChanged="txtLifeSpam_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtLifeSpam" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtLifeSpam" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Monthly Cost</label>
                                                <asp:TextBox ID="txtmonthlyCost" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtmonthlyCost" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtmonthlyCost" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="2" Columns="2" Style="resize: none"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                    ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger"
                                                    OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->

                                    <!-- table start -->
                                    <asp:Panel runat="server" ID="Panel1" ScrollBars="Both">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table class="table table-responsive table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>Particulars</th>
                                                                    <th>Expenses Type</th>
                                                                    <th>Qty</th>
                                                                    <th>Cost</th>
                                                                    <th>Start Date</th>
                                                                    <th>Next Due Date</th>
                                                                    <th>Life Spam</th>
                                                                    <th>Monthly Cost</th>
                                                                    <th>Remarks</th>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1%></td>
                                                            <td><%# Eval("Particular")%></td>
                                                            <td><%# Eval("ExpensesType")%></td>
                                                            <td><%# Eval("Qty")%></td>
                                                            <td><%# Eval("Cost")%></td>
                                                            <td><%# Eval("Start_Date_Str")%></td>
                                                            <td><%# Eval("Next_Due_Date_Str")%></td>
                                                            <td><%# Eval("Life_Spam")%></td>
                                                            <td><%# Eval("Monthly_Cost")%></td>
                                                            <td><%# Eval("Remarks")%></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                    Text="" OnCommand="btnEditEnquiry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("Auto_ID")%>'>
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                    Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("Auto_ID")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Expenses details?');">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <!-- table End -->
                                    </asp:Panel>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </section>
    </div>
    <!-- end #content -->


    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        });
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('#example').dataTable();
                    $('.datepicker').datepicker({
                        autoclose: true,
                        format: 'dd/mm/yyyy'
                    });

                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
</asp:Content>
