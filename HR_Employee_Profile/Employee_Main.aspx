﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Employee_Main.aspx.cs" Inherits="HR_Employee_Profile_Employee_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>
<!-- begin #content -->
<div id="content" class="content-wrapper">
    <section class="content">
 <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Master</a></li>
        <li><a href="javascript:;">EmployeeProfile</a></li>
        <li class="active">EmployeeProfile</li>
    </ol>
    <!-- end breadcrumb -->
     <!-- begin page-header -->
    <h1 class="page-header">Employee Details</h1>
    <!-- end page-header -->
    
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <div>
                <!-- begin panel -->
                <div class="panel panel-inverse">                    
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-3">
                                <asp:LinkButton ID="lbtnAdd" runat="server" class="btn btn-success" 
                                    onclick="lbtnAdd_Click" >Add New Employee</asp:LinkButton>
                                
                            </div>
                             <!-- begin col-2 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Adhar No</label>
                                        <asp:TextBox ID="txtAdharNo" runat="server" class="form-control" MaxLength="12" ></asp:TextBox>
                                       
                                   </div>
                                </div>
                             <!-- end col-2 -->
                             <div class="col-md-2">
                             <div class="form-group">
                            
                             <asp:Button runat="server" id="btnSearch" Text="Search" style="margin-top: 16%;" class="btn btn-primary" onclick="btnSearch_Click" />
                              </div>
                             </div>
                             <!-- begin col-2 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Token No</label>
                                        <asp:TextBox ID="txtTokenNo" runat="server" class="form-control" Enabled="false" ></asp:TextBox>
                                   </div>
                                </div>
                             <!-- end col-2 -->
                              <!-- begin col-2 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Location</label>
                                        <asp:TextBox ID="txtLocation" runat="server" class="form-control" Enabled="false" ></asp:TextBox>
                                   </div>
                                </div>
                             <!-- end col-2 -->
                        </div>
                          
                  <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <label>Employee ActiveMode</label>
                        <asp:RadioButtonList ID="rbtnIsActive" runat="server" AutoPostBack="true"
                          RepeatDirection="Horizontal" 
                              onselectedindexchanged="rbtnIsActive_SelectedIndexChanged">
                        <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Pending" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Cancel" Value="4"></asp:ListItem>
                        </asp:RadioButtonList>
                   </div>
                </div>
                        <div class="row">
                               <div class="form-group">
                               <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                                       <th>Token ID</th>
                                                        <th>Name</th>
                                                        <th>Category</th>
                                                        <th>Gender</th>
                                                        <th>DeptName</th>
                                                        <th>Designation</th>
                                                        <th>DateOfBirth</th>
                                                        <th>Edit</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("ExistingCode")%></td>
                                                <td><%# Eval("FirstName")%></td>
                                                <td><%# Eval("CatName")%></td>
                                                 <td><%# Eval("Gender")%></td>
                                                <td><%# Eval("DeptName")%></td>
                                                <td><%# Eval("Designation")%></td>
                                                <td><%# Eval("DOB")%></td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument="Edit" CommandName='<%# Eval("MachineID")%>'>
                                                    </asp:LinkButton>
                                                    </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    </section>
</div>


</asp:Content>

