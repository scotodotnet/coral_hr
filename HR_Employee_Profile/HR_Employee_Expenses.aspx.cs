﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Employee_Profile_HR_Employee_Expenses : System.Web.UI.Page
{
    public static BALDataAccess objdata = new BALDataAccess();

    public static String CurrentYear1;
    public static int CurrentYear;

    public static string SessionCcode;
    public static string SessionLcode;
    public static string SessionUserID;
    public static string SessionUserName;
    public static string SessionRights;

    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Expenses";

        }
        Load_Data();
    }
    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from Employee_Expenses where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlExpensesType.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Expenses Type');", true);
            ErrFlag = true;
            return;
        }
        if (txtParticulars.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Particular');", true);
            ErrFlag = true;
            return;
        }
        if (txtQty.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Qty');", true);
            ErrFlag = true;
            return;
        }
        if (txtCost.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Cost');", true);
            ErrFlag = true;
            return;
        }
        if (txtStartDate.Text == "" || txtNextDueDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Start Date and Next Due Date Properly');", true);
            ErrFlag = true;
            return;
        }
        if (txtLifeSpam.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Life Spam');", true);
            ErrFlag = true;
            return;
        }
        if (txtmonthlyCost.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Monthly Cost');", true);
            ErrFlag = true;
            return;
        }

        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Delete from Employee_Expenses where Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' and ExpensesType='" + ddlExpensesType.SelectedValue + "' and Particular='" + txtParticulars.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert into Employee_Expenses(Ccode,Lcode,Particular,ExpensesType,Qty,Cost,Start_Date,Start_Date_Str,Next_Due_Date,Next_Due_Date_Str,Life_Spam,Monthly_Cost,Remarks,Created_On)";
            SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtParticulars.Text + "','" +ddlExpensesType.SelectedValue + "','" + txtQty.Text + "','" + txtCost.Text + "',Convert(date,'" + txtStartDate.Text + "',103),'"+ txtStartDate.Text + "',Convert(date,'" + txtNextDueDate.Text + "',103),'" + txtNextDueDate.Text + "'";
            SSQL = SSQL + ", '" + txtLifeSpam.Text + "','" + txtmonthlyCost.Text + "','"+txtRemarks.Text+"','"+ DateTime.Now.ToString("dd/MM/yyyy") + "')";
            
            objdata.RptEmployeeMultipleDetails(SSQL);

            if (btnSave.Text == "Save")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Saved Successfully!!!');", true);

            }
            if (btnSave.Text == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Updated Successfully!!!');", true);
            }
            btnClear_Click(sender, e);
            Load_Data();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtParticulars.Text = "";
        ddlExpensesType.ClearSelection();
        txtQty.Text = "0";
        txtCost.Text = "0";
        txtStartDate.Text = "";
        txtNextDueDate.Text = "";
        txtLifeSpam.Text = "";
        txtmonthlyCost.Text = "";
        txtRemarks.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Employee_Expenses where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ddlExpensesType.SelectedValue = dt.Rows[0]["ExpensesType"].ToString();
            txtParticulars.Text = dt.Rows[0]["Particular"].ToString();
            txtQty.Text = dt.Rows[0]["Qty"].ToString();
            txtCost.Text = dt.Rows[0]["Cost"].ToString();
            txtStartDate.Text = dt.Rows[0]["Start_Date_Str"].ToString();
            txtNextDueDate.Text = dt.Rows[0]["Next_Due_Date_Str"].ToString();
            txtLifeSpam.Text = dt.Rows[0]["Life_Spam"].ToString();
            txtmonthlyCost.Text = dt.Rows[0]["Monthly_Cost"].ToString();
            txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from Employee_Expenses where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }

    protected void txtLifeSpam_TextChanged(object sender, EventArgs e)
    {
        try
        {
            if (txtCost.Text != "" && txtLifeSpam.Text != "")
            {
                txtmonthlyCost.Text = Math.Round((Convert.ToDecimal(txtCost.Text) / Convert.ToDecimal(txtLifeSpam.Text)), 0, MidpointRounding.AwayFromZero).ToString();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Error:" + ex.Message + "');", true);
        }
    }
}