﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EmployeeDetails.aspx.cs" Inherits="HR_Employee_Profile_EmployeeDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .well1 {
            padding: 15px;
            background: #eeeeee;
            box-shadow: none;
            -webkit-box-shadow: none
        }

        .LabelColor {
            color: #116dca
        }

        .BorderStyle {
            border: 1px solid #5f656b;
            color: #020202;
            font-weight: bold;
        }

        .select2 {
            border: 1px solid #5f656b;
            color: #020202;
            font-weight: bold;
        }
    </style>

    <div class="content-wrapper">
        <%--<asp:UpdatePanel ID="upEmpPortal" runat="server">
            <ContentTemplate>--%>
        <section class="content-header">
            <h1 class="page-header">Employee History</h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div id="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab">Basic</a></li>
                                    <li><a href="#tab_2" data-toggle="tab">Salary</a></li>
                                    <li><a href="#tab_3" data-toggle="tab">Personal</a></li>
                                    <li><a href="#tab_9" data-toggle="tab">Family Details</a></li>
                                    <li><a href="#tab_4" data-toggle="tab">General</a></li>
                                    <li><a href="#tab_5" data-toggle="tab">Adolescent</a></li>
                                    <li><a href="#tab_6" data-toggle="tab">Documents</a></li>
                                    <li><a href="#tab_7" data-toggle="tab">Experience</a></li>
                                    <li><a href="#tab_8" data-toggle="tab">Qualifications</a></li>
                                </ul>
                                <!-- begin wizard step-1 -->
                                <div class="tab-content">
                                    <div class="tab-pane active well1" id="tab_1">
                                        <asp:UpdatePanel ID="upBasic" runat="server">
                                            <ContentTemplate>
                                                <fieldset class="box box-primary">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">Basic</label></h3>
                                                    </div>
                                                    <%--<legend class="pull-left width-full">Basic</legend>--%>

                                                    <div class="col-md-12">
                                                        <div class="row" style="padding-top: 1%">
                                                            <div class="col-md-9">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Machine ID</label>
                                                                            <span class="mandatory">*</span>
                                                                            <asp:TextBox ID="txtMachineID" runat="server" AutoComplete="off"
                                                                                class="form-control  BorderStyle" AutoPostBack="true"
                                                                                OnTextChanged="txtMachineID_TextChanged"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Existing Number</label>
                                                                            <span class="mandatory">*</span>
                                                                            <asp:TextBox ID="txtExistingCode" runat="server" AutoComplete="off"
                                                                                class="form-control  BorderStyle"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Division</label>
                                                                            <asp:DropDownList ID="ddlDivision" runat="server"
                                                                                class="form-control  BorderStyle select2" Style="width: 100%">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4" runat="server" id="divTicketNo" visible="false">
                                                                        <div class="form-group block1">
                                                                            <label class="LabelColor">Ticket Number</label>
                                                                            <span class="mandatory">*</span>
                                                                            <asp:TextBox ID="txtTokenID" runat="server" AutoComplete="off"
                                                                                class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Category</label>
                                                                            <span class="mandatory">*</span>
                                                                            <asp:DropDownList ID="ddlCategory" runat="server"
                                                                                class="form-control BorderStyle select2" Style="width: 100%;">
                                                                                <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                                <asp:ListItem Value="STAFF" Text="STAFF"></asp:ListItem>
                                                                                <asp:ListItem Value="LABOUR" Text="LABOUR"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Sub Category</label>
                                                                            <span class="mandatory">*</span>
                                                                            <asp:DropDownList ID="ddlSubCategory" runat="server"
                                                                                class="form-control BorderStyle select2" Style="width: 100%;">
                                                                                <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                                <asp:ListItem Value="INSIDER" Text="INSIDER"></asp:ListItem>
                                                                                <asp:ListItem Value="OUTSIDER" Text="OUTSIDER"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Shift</label>
                                                                            <span class="mandatory">*</span>
                                                                            <asp:DropDownList ID="ddlShift" runat="server"
                                                                                class="form-control BorderStyle select2" Style="width: 100%;">
                                                                                <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                                <asp:ListItem Value="GENERAL" Text="GENERAL"></asp:ListItem>
                                                                                <asp:ListItem Value="SHIFT" Text="SHIFT"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="media">
                                                                    <%--style="margin-top: -19px;">--%>
                                                                    <a class="media-right" href="javascript:;" style="float: right;">
                                                                        <asp:Image ID="Image3" runat="server" class="media-object"
                                                                            Style="width: 158px; height: 161px;" Visible="false"
                                                                            ImageUrl="~/assets/img/login-bg/man-user-50.png" />
                                                                        <img id="img1" alt="" height="100%" width="100%" />
                                                                    </a>
                                                                    <asp:FileUpload ID="FileUpload1" runat="server" onchange="showimagepreview(this)" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">First Name</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtFirstName" runat="server" AutoComplete="off"
                                                                        class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Initial /Last Name</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtLastName" runat="server" AutoComplete="off"
                                                                        class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Date of Birth</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtDOB" runat="server" class="form-control BorderStyle datepicker"
                                                                        placeholder="dd/MM/YYYY" AutoPostBack="true"
                                                                        OnTextChanged="txtDOB_TextChanged"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtDOB" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Age</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtAge" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group block1">
                                                                    <label class="LabelColor">Gender</label><span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlGender" runat="server" class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                        <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                                        <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                                                                        <asp:ListItem Value="Female" Text="Female"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="row">

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Department</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="true"
                                                                        class="form-control  BorderStyle select2" Style="width: 100%;"
                                                                        OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Designation</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlDesignation" runat="server"
                                                                        class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                               <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Sub Section</label>
                                                                    <asp:DropDownList ID="ddlSubSection" runat="server"
                                                                        class="form-control BorderStyle select2" Style="width: 100%;">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <div class="form-group">
                                                                        <label class="LabelColor">Job Description</label>
                                                                        <asp:TextBox ID="txtDescription" runat="server" AutoComplete="off"
                                                                            class="form-control  BorderStyle" Style="width: 100%;">
                                                                        </asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                              <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <div class="form-group">
                                                                        <label class="LabelColor">Qualification</label>
                                                                        <span class="mandatory">*</span>
                                                                        <asp:DropDownList ID="txtQulification" runat="server"
                                                                            class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Date of Join</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtDOJ" runat="server" AutoComplete="off"
                                                                        class="form-control  BorderStyle datepicker"
                                                                        placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2"
                                                                        runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtDOJ" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Emp MobileNo</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtEmpMobileNo" runat="server" AutoComplete="off"
                                                                        class="form-control  BorderStyle" MaxLength="10"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                                                        FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtEmpMobileNo" ValidChars="0123456789">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">OT Eligible</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlOTEligible" runat="server" class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                        <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                                        <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
                                                                        <asp:ListItem Value="No" Text="No"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Wages Type</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlWagesType" runat="server"
                                                                        class="form-control  BorderStyle select2"
                                                                        Style="width: 100%;" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="ddlWagesType_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ControlToValidate="ddlWagesType"
                                                                        InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field"
                                                                        class="form_error" ID="RequiredFieldValidator15" runat="server"
                                                                        EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-3" runat="server" id="IF_PF_Eligible">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">PF Eligible</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:RadioButtonList ID="RdbPFEligible" runat="server" class="form-control  BorderStyle"
                                                                        RepeatColumns="2" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="RdbPFEligible_SelectedIndexChanged">
                                                                        <asp:ListItem Value="1" Text="Yes" style="padding-right: 40px"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">PF No</label>
                                                                    <asp:TextBox ID="txtPFNo" runat="server" class="form-control  BorderStyle" Enabled="false"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtPFNo" ValidChars="0123456789">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group block1">
                                                                    <label class="LabelColor">PF Date</label>
                                                                    <asp:TextBox ID="txtPFDate" runat="server" class="form-control  BorderStyle datepicker" Enabled="false" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtPFDate" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2" runat="server" id="Div7">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">PF ABRY</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:RadioButtonList ID="Rdp_PF_Goverment" runat="server" class="form-control  BorderStyle"
                                                                        RepeatColumns="2">
                                                                        <asp:ListItem Value="1" Text="Yes" style="padding-right: 10px"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">PF Code</label>
                                                                    <asp:DropDownList ID="ddlPFCode" runat="server" class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-3" runat="server" id="IF_ESI_Eligible">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">ESI Eligible</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:RadioButtonList ID="RdbESIEligible" runat="server"
                                                                        class="form-control  BorderStyle" RepeatColumns="2" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="RdbESIEligible_SelectedIndexChanged">
                                                                        <asp:ListItem Value="1" Text="Yes" style="padding-right: 40px"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                                    </asp:RadioButtonList>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">ESI No</label>
                                                                    <asp:TextBox ID="txtESINo" runat="server" AutoComplete="off"
                                                                        class="form-control  BorderStyle" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">ESI Date</label>
                                                                    <asp:TextBox ID="txtESIDate" runat="server" AutoComplete="off"
                                                                        class="form-control  BorderStyle datepicker" Enabled="false"
                                                                        placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtESIDate" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">ESI Code</label>
                                                                    <asp:DropDownList ID="ddlESICode" runat="server"
                                                                        class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">UAN</label>
                                                                    <asp:TextBox ID="txtUAN" runat="server" AutoComplete="off"
                                                                        class="form-control  BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Hostel Room No</label>
                                                                    <asp:TextBox ID="txtHostelRoom" runat="server" AutoComplete="off"
                                                                        class="form-control  BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Vehicle Type</label>
                                                                    <asp:DropDownList ID="ddlVehicleType" runat="server"
                                                                        class="form-control  BorderStyle select2"
                                                                        Style="width: 100%" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="ddlVehicleType_SelectedIndexChanged">
                                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                        <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                        <asp:ListItem Value="OWN">OWN</asp:ListItem>
                                                                        <asp:ListItem Value="OWN Bus">OWN Bus</asp:ListItem>
                                                                        <asp:ListItem Value="Private">Private</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Bus No</label>
                                                                    <asp:DropDownList ID="txtBusNo" runat="server" class="form-control  BorderStyle select2"
                                                                        Style="width: 100%" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="txtBusNo_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Route</label>
                                                                    <asp:DropDownList ID="txtVillage" runat="server" class="form-control  BorderStyle select2"
                                                                        Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                             <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Vehicle No</label>
                                                                    <asp:TextBox ID="txtVehicleNo" runat="server" 
                                                                        class="form-control BorderStyle" ></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Active Mode</label>
                                                                    <asp:RadioButtonList ID="dbtnActive" runat="server"
                                                                        class="form-control  BorderStyle"
                                                                        RepeatDirection="Horizontal" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="dbtnActive_SelectedIndexChanged">
                                                                        <asp:ListItem Value="1" Text="Yes" style="padding-right: 40px" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Relieved Date</label>
                                                                    <asp:TextBox ID="txtReliveDate" runat="server" AutoComplete="off"
                                                                        class="form-control  BorderStyle datepicker"
                                                                        placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtReliveDate" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Reason for Relieving</label>
                                                                    <asp:TextBox ID="txtReason" runat="server" Style="resize: none"
                                                                        class="form-control BorderStyle" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">480 Days Completed</label>
                                                                    <asp:TextBox ID="txt480Days" runat="server" AutoComplete="off"
                                                                        class="form-control BorderStyle datepicker"
                                                                        placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Week - Off</label><span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlWeekOff" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                        <asp:ListItem Value="Sunday" Text="Sunday"></asp:ListItem>
                                                                        <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                                                                        <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                                                                        <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                                                                        <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                                                                        <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                                                                        <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                                                                        <asp:ListItem Value="NONE" Text="NONE"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Form I Obtained Date  </label>
                                                                    <asp:CheckBox ID="chkExment" runat="server"
                                                                        AutoPostBack="true" OnCheckedChanged="chkExment_CheckedChanged" />
                                                                    <span class="LabelColor">Exempted Staff </span>
                                                                    <asp:TextBox ID="txtFormIDate" runat="server" class="form-control BorderStyle datepicker" placeholder="dd/MM/YYYY" Enabled="false"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtFormIDate" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>

                                                             <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Employee Level</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlEmpLevel" runat="server"
                                                                        class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                        <asp:ListItem Value="Trainee">Trainee</asp:ListItem>
                                                                        <asp:ListItem Value="SemiSkilled">SemiSkilled</asp:ListItem>
                                                                        <asp:ListItem Value="Skilled">Skilled</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Level</label>
                                                                    <asp:DropDownList ID="ddlHostelExp" runat="server"
                                                                        class="form-control BorderStyle select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Classification</label>
                                                                    <span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlWorkType" runat="server" AutoPostBack="true"
                                                                        class="form-control BorderStyle select2" Style="width: 100%;"
                                                                        OnSelectedIndexChanged="ddlWorkType_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                        <asp:ListItem Value="1" Text="Probation"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="Confirmation"></asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Confirmation Date</label>
                                                                    <asp:TextBox ID="txtConfirmation_Date" runat="server" Style="resize: none"
                                                                        class="form-control BorderStyle datepicker" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Notes(Experience)</label>
                                                                    <asp:TextBox ID="txtExpNote" runat="server" Style="resize: none"
                                                                        class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Inssurance Join Date </label>
                                                                    <asp:CheckBox ID="ChkInsurance_Elgbl" runat="server"
                                                                        AutoPostBack="true" OnCheckedChanged="ChkInsurance_Elgbl_CheckedChanged" />
                                                                    <span class="LabelColor">Eligible Insurance</span>
                                                                    <asp:TextBox ID="txtInssurance_Joining_Date" runat="server" class="form-control BorderStyle datepicker" placeholder="dd/MM/YYYY" Enabled="false"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtInssurance_Joining_Date" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Group Data of Joining</label>
                                                                    <asp:TextBox ID="txtGroup_DOJ" runat="server" Style="resize: none" placeholder="DD/MM/YYYY"
                                                                        class="form-control BorderStyle datepicker"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Reporting Person</label>
                                                                    <asp:DropDownList ID="ddlRptingPerson" runat="server"
                                                                        class="form-control BorderStyle select2" Style="width: 100%;">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Resignation Date</label>
                                                                    <asp:TextBox ID="txtResignationdate" runat="server" Style="resize: none"
                                                                        class="form-control BorderStyle datepicker"></asp:TextBox>
                                                                </div>
                                                            </div> 
                                                            <%--<div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Grade</label>
                                                                    <asp:TextBox ID="txtGrade" runat="server" Style="resize: none"
                                                                        class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>--%>
                                                          
                                                             <!-- begin col-4 -->
                                                            <div id="Div4" class="col-md-2" runat="server" >
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Grade</label>
                                                                   <%-- <asp:DropDownList ID="ddlGrade" runat="server" class="form-control BorderStyle select2" Style="width: 100%">
                                                                    </asp:DropDownList>--%>
                                                                     <asp:TextBox ID="txtGrade" runat="server" Style="resize: none"
                                                                        class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>

                                                    </div>

                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="tab-pane well1" id="tab_2">
                                        <asp:UpdatePanel ID="upSalary" runat="server">
                                            <ContentTemplate>
                                                <fieldset class="box box-primary">

                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">Bank/Cash</label></h3>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="row" style="padding-top: 1%">
                                                            <!-- begin col-3 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Salary Through</label><span class="mandatory">*</span>
                                                                    <asp:RadioButtonList ID="rbtnSalaryThrough" runat="server"
                                                                        RepeatDirection="Horizontal" class="form-control BorderStyle" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="rbtnSalaryThrough_SelectedIndexChanged">
                                                                        <asp:ListItem Value="1" Text="Cash" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="Bank"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                            </div>
                                                            <!-- end col-3 -->
                                                            <!-- begin col-3 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Bank Name</label>
                                                                    <asp:DropDownList ID="ddlBankName" Enabled="false" runat="server" class="form-control BorderStyle select2" Style="width: 100%;" AutoPostBack="true" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <!-- end col-3 -->
                                                            <!-- begin col-3 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">IFSC Code</label>
                                                                    <asp:TextBox ID="txtIFSC" runat="server" class="form-control BorderStyle" Enabled="true"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <!-- end col-3 -->
                                                            <!-- begin col-3 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Branch</label>
                                                                    <asp:TextBox ID="txtBranch" runat="server" class="form-control BorderStyle" Enabled="true"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-3 -->
                                                        </div>

                                                        <div class="row">
                                                            <!-- begin col-3 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Account Number</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtAccNo" runat="server" Enabled="false" class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Account Holder Name</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtAccount_Holder_Name" runat="server" Enabled="true" class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div runat="server" visible="false">
                                                                <div class="col-md-2" runat="server" id="Div6">
                                                                    <div class="form-group">
                                                                        <label class="LabelColor">TDS Eligible</label><span class="mandatory">*</span>
                                                                        <asp:RadioButtonList ID="RdbTDSEligible" runat="server" class="form-control  BorderStyle"
                                                                            RepeatColumns="2" AutoPostBack="true" OnSelectedIndexChanged="RdbTDSEligible_SelectedIndexChanged">
                                                                            <asp:ListItem Value="1" Text="Yes" style="padding-right: 40px"></asp:ListItem>
                                                                            <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label class="LabelColor">TDS %</label>
                                                                        <asp:TextBox ID="txtTDSPre" runat="server" class="form-control  BorderStyle"
                                                                            Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end col-3 -->
                                                        </div>

                                                        <legend class="pull-left width-full">Fixed Salary</legend>

                                                        <div class="row">
                                                            <!-- begin col-2 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Gross Salary</label>
                                                                    <asp:TextBox ID="txtBasic" runat="server" class="form-control BorderStyle" Text="0.0" Enabled="true"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtBasic" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Basic</label>
                                                                    <asp:TextBox ID="txtBasic_Sal_Part" runat="server" class="form-control BorderStyle" Text="0.0" Enabled="true"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtBasic_Sal_Part" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">DA</label>
                                                                    <asp:TextBox ID="txtDA_Sal_Part" runat="server" class="form-control BorderStyle" Text="0.0" Enabled="true"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtDA_Sal_Part" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">HRA</label>
                                                                    <asp:TextBox ID="txtHRA_Sal_Part" runat="server" class="form-control BorderStyle" Text="0.0" Enabled="true"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtHRA_Sal_Part" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">WA</label>
                                                                    <asp:TextBox ID="txtWA_Sal_Part" runat="server" class="form-control BorderStyle" Text="0.0" Enabled="true"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtWA_Sal_Part" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Conveyance Allow.</label>
                                                                    <asp:TextBox ID="txtSPL_Allow_Sal_Part" runat="server" class="form-control BorderStyle" Text="0.0" Enabled="true"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtSPL_Allow_Sal_Part" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Other Allow.</label>
                                                                    <asp:TextBox ID="txtOther_Allow_Sal_Part" runat="server" class="form-control BorderStyle" Text="0.0" Enabled="true"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtOther_Allow_Sal_Part" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- begin col-2 -->
                                                            <div id="Div1" class="col-md-2" runat="server" visible="true">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">VPF</label>
                                                                    <asp:TextBox ID="txtVPF" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtVPF" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-2 -->
                                                            <!-- begin col-2 -->
                                                            <div class="col-md-2" runat="server" id="divAll1" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Allowance 1</label>
                                                                    <asp:TextBox ID="txtAllowance1" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtAllowance1" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-2 -->
                                                            <!-- begin col-2 -->
                                                            <div class="col-md-2" runat="server" id="divAll2" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Other Allowance</label>
                                                                    <asp:TextBox ID="txtAllowance2" runat="server" class="form-control BorderStyle" Text="0.0" Enabled="false"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtAllowance2" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-2 -->
                                                            <!-- begin col-2 -->
                                                            <div class="col-md-2" runat="server" id="divDed1" visible="true">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Deduction 1</label>
                                                                    <asp:TextBox ID="txtDeduction1" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtDeduction1" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3" runat="server">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Mail ID</label>
                                                                    <asp:TextBox ID="txtMail_ID_Emp" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <!-- end col-2 -->
                                                            <!-- begin col-2 -->
                                                            <div class="col-md-2" runat="server" id="divDed2" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Deduction 2</label>
                                                                    <asp:TextBox ID="txtDeduction2" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtDeduction2" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2" runat="server" id="DivDayscholar_Vech_ID" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Dayscholar Vehicle</label>
                                                                    <asp:RadioButtonList ID="RdpDayscholar_Vehicle" runat="server"
                                                                        RepeatDirection="Horizontal" class="form-control BorderStyle">
                                                                        <asp:ListItem Value="1" Text="Yes" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                            </div>
                                                            <!-- end col-2 -->
                                                        </div>

                                                        <div class="row">
                                                            <!-- begin col-2 -->
                                                            <div class="col-md-2" runat="server" id="divOTSal" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">OT Salary</label>
                                                                    <asp:TextBox ID="txtOTSal" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtOTSal" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-2 -->
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="tab-pane well1" id="tab_3">
                                        <asp:UpdatePanel ID="upPersonal" runat="server">
                                            <ContentTemplate>
                                                <fieldset class="box box-primary">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">Personal</label></h3>
                                                    </div>


                                                    <%--<legend class="pull-left width-full">Personal</legend>--%>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-9">

                                                                <!-- begin row -->
                                                                <div class="row">
                                                                    <!-- begin col-4 -->
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Martial Status</label><span class="mandatory">*</span>
                                                                            <asp:DropDownList ID="ddlMartialStatus" runat="server" class="form-control BorderStyle select2" Style="width: 100%;">
                                                                                <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                                <asp:ListItem Value="Single" Text="Single"></asp:ListItem>
                                                                                <asp:ListItem Value="Married" Text="Married"></asp:ListItem>
                                                                                <asp:ListItem Value="Divorced" Text="Divorced"></asp:ListItem>
                                                                                <asp:ListItem Value="Widowed" Text="Widowed"></asp:ListItem>
                                                                                <asp:ListItem Value="Separated" Text="Separated"></asp:ListItem>
                                                                                <asp:ListItem Value="None" Text="None"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <%-- <asp:RequiredFieldValidator ControlToValidate="ddlMartialStatus" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                       </asp:RequiredFieldValidator>--%>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end col-4 -->
                                                                    <!-- begin col-4 -->
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Nationality</label>
                                                                            <asp:TextBox ID="txtNationality" runat="server" Text="INDIAN" class="form-control BorderStyle">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end col-4 -->
                                                                    <!-- begin col-4 -->
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Religion</label>
                                                                            <asp:TextBox ID="txtReligion" runat="server" class="form-control BorderStyle">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end col-4 -->
                                                                    <!-- begin col-2 -->
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Height</label>
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">Cms</span>
                                                                                <asp:TextBox ID="txtHeight" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                                    TargetControlID="txtHeight" ValidChars="0123456789.">
                                                                                </cc1:FilteredTextBoxExtender>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end col-2 -->
                                                                    <!-- begin col-2 -->
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Weight</label>
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">Kg</span>
                                                                                <asp:TextBox ID="txtWeight" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                                    TargetControlID="txtWeight" ValidChars="0123456789.">
                                                                                </cc1:FilteredTextBoxExtender>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <!-- end row -->
                                                                <!-- begin row -->
                                                                <div class="row">
                                                                    <!-- begin col-3 -->
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Community</label>
                                                                            <asp:DropDownList ID="ddlCommunity" runat="server" class="form-control BorderStyle select2" Style="width: 100%;">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end col-3 -->
                                                                    <!-- begin col-2 -->
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Std Working Hrs</label><span class="mandatory">*</span>
                                                                            <asp:TextBox ID="txtStdWorkingHrs" runat="server" Text="0" class="form-control BorderStyle"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                                TargetControlID="txtStdWorkingHrs" ValidChars="0123456789">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end col-2 -->
                                                                    <!-- begin col-2 -->
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Physically Challenged</label>
                                                                            <asp:RadioButtonList ID="rbtnPhysically" runat="server" class="form-control BorderStyle"
                                                                                RepeatDirection="Horizontal" AutoPostBack="true"
                                                                                OnSelectedIndexChanged="rbtnPhysically_SelectedIndexChanged">
                                                                                <asp:ListItem Value="1" Text="Yes" style="padding-right: 40px"></asp:ListItem>
                                                                                <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                                            </asp:RadioButtonList>

                                                                        </div>
                                                                    </div>
                                                                    <!-- end col-2 -->
                                                                    <!-- begin col-2 -->
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="LabelColor">Physically Reason</label>
                                                                            <asp:TextBox ID="txtPhyReason" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end col-2 -->


                                                                </div>
                                                                <!-- end row -->
                                                            </div>
                                                            <div class="col-md-3" id="PersonImg" runat="server">
                                                                <div class="media" style="margin-top: -19px;">
                                                                    <a class="media-right" href="javascript:;" style="float: right;">
                                                                        <asp:Image ID="Image1" runat="server" class="media-object" Style="width: 158px; height: 161px;" Visible="false" ImageUrl="~/assets/img/login-bg/man-user-50.png" />
                                                                        <img id="img2" alt="" height="100%" width="100%" />
                                                                    </a>
                                                                    <asp:FileUpload ID="FileUpload2" runat="server" onchange="showimagepreview2(this)" />
                                                                </div>
                                                            </div>
                                                            <!-- begin row -->

                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Blood Group</label><br />
                                                                    <asp:DropDownList ID="ddlBloodGrp" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                                        <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                                        <asp:ListItem Value="A+" Text="A+"></asp:ListItem>
                                                                        <asp:ListItem Value="A-" Text="A-"></asp:ListItem>
                                                                        <asp:ListItem Value="A1+" Text="A1+"></asp:ListItem>
                                                                        <asp:ListItem Value="B+" Text="B+"></asp:ListItem>
                                                                        <asp:ListItem Value="B-" Text="B-"></asp:ListItem>
                                                                        <asp:ListItem Value="AB+" Text="AB+"></asp:ListItem>
                                                                        <asp:ListItem Value="AB-" Text="AB-"></asp:ListItem>
                                                                        <asp:ListItem Value="O+" Text="O+"></asp:ListItem>
                                                                        <asp:ListItem Value="O-" Text="O-"></asp:ListItem>
                                                                        <asp:ListItem Value="NONE" Text="NONE"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Recruitment Through</label>
                                                                    <asp:DropDownList ID="txtRecruitThrg" runat="server"
                                                                        class="form-control BorderStyle select2" Style="width: 100%" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="txtRecruitThrg_SelectedIndexChanged">
                                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                        <asp:ListItem Value="Recruitment Officer">Recruitment Officer</asp:ListItem>
                                                                        <asp:ListItem Value="Walk in">Walk in</asp:ListItem>
                                                                        <asp:ListItem Value="Job portals">Job portals</asp:ListItem>
                                                                        <asp:ListItem Value="Reference">Reference</asp:ListItem>
                                                                        <asp:ListItem Value="Transfer">Transfer</asp:ListItem>
                                                                        <asp:ListItem Value="ReJoin">ReJoin</asp:ListItem>
                                                                        <asp:ListItem Value="Placement">Placement</asp:ListItem>
                                                                        <asp:ListItem Value="Existing Employee">Existing Employee</asp:ListItem>
                                                                        <asp:ListItem Value="Direct">Direct</asp:ListItem>
                                                                        <asp:ListItem Value="Others ">Others</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label runat="server" id="lblRecruit">Recruiter Name</label>
                                                                    <label runat="server" id="lblAgent" visible="false">Agent Name</label>
                                                                    <asp:DropDownList ID="txtRecruitmentName" runat="server"
                                                                        class="form-control BorderStyle select2" AutoPostBack="true" Style="width: 100%"
                                                                        OnSelectedIndexChanged="txtRecruitmentName_SelectedIndexChanged">
                                                                    </asp:DropDownList>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Recruiter Mobile</label>
                                                                    <asp:TextBox ID="txtRecruitMobile" runat="server" class="form-control BorderStyle" MaxLength="10"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Unit</label>
                                                                    <asp:DropDownList ID="ddlUnit" runat="server" class="form-control BorderStyle select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Existing Emp.No</label>
                                                                    <asp:TextBox ID="txtExistingEmpNo" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-3" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Existing Employee Name</label>
                                                                    <asp:TextBox ID="txtExistingEmpName" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-3" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Referal Type</label>
                                                                    <asp:DropDownList ID="txtRefType" runat="server" class="form-control BorderStyle select2"
                                                                        Style="width: 100%" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="txtRefType_SelectedIndexChanged">
                                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                        <asp:ListItem Value="Agent">Agent</asp:ListItem>
                                                                        <asp:ListItem Value="Parent">Parent</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row" runat="server" visible="false">


                                                            <!-- begin col-3 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Agent Name</label>
                                                                    <asp:DropDownList ID="txtAgentName" runat="server" class="form-control BorderStyle select2"
                                                                        Style="width: 100%" Enabled="false" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="txtAgentName_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <!-- end col-3 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Parents Name</label>
                                                                    <asp:TextBox ID="txtRefParentsName" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Mobile No</label>
                                                                    <asp:TextBox ID="txtRefMobileNo" runat="server" class="form-control BorderStyle" MaxLength="10" Enabled="false"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtRefMobileNo" ValidChars="0123456789">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Commission Amount</label>
                                                                    <asp:TextBox ID="txtCommissionAmt" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtCommissionAmt" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div id="Div2" class="col-md-2" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Working Unit</label>
                                                                    <asp:DropDownList ID="ddlWorkingUnit" runat="server" class="form-control BorderStyle select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div id="Div3" class="col-md-2" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Salary Unit</label>
                                                                    <asp:DropDownList ID="ddlSalaryUnit" runat="server" class="form-control BorderStyle select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                           
                                                        </div>

                                                        <div class="row" runat="server" visible="false">


                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Leave From1</label>
                                                                    <asp:TextBox ID="txtLeaveFrom" runat="server" class="form-control BorderStyle datepicker"
                                                                        placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtLeaveFrom" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Leave To1</label>
                                                                    <asp:TextBox ID="txtLeaveTo" runat="server" class="form-control BorderStyle datepicker"
                                                                        placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtLeaveTo" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Festival</label>
                                                                    <asp:TextBox ID="txtFestival1" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">LeaveDays</label>
                                                                    <asp:TextBox ID="txtLeaveDays1" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>

                                                        <div class="row" runat="server" visible="false">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Leave From2</label>
                                                                    <asp:TextBox ID="txtLeaveFrom2" runat="server" class="form-control BorderStyle datepicker"
                                                                        placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtLeaveFrom2" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Leave To2</label>
                                                                    <asp:TextBox ID="txtLeaveTo2" runat="server" class="form-control BorderStyle datepicker"
                                                                        placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtLeaveTo2" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Festival</label>
                                                                    <asp:TextBox ID="txtFestival2" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">LeaveDays</label>
                                                                    <asp:TextBox ID="txtLeaveDays2" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Recruitment Remarks</label>
                                                                    <asp:TextBox ID="txtRecruitment_Remarks" runat="server" Style="resize: none"
                                                                        class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Certificate</label>
                                                                    <asp:TextBox ID="txtCertificate" runat="server" Style="resize: none"
                                                                        class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="tab-pane well1" id="tab_9">
                                        <asp:UpdatePanel ID="upFamily_Members" runat="server">
                                            <ContentTemplate>
                                                <fieldset class="box box-primary">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">Family Member Details</label></h3>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Name</label>
                                                                    <asp:TextBox ID="txtFam_Name" runat="server" Style="width: 100%" AutoComplete="off"
                                                                        class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Date of Birth</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtFam_DOB" runat="server" class="form-control BorderStyle datepicker"
                                                                        placeholder="dd/MM/YYYY" AutoPostBack="true"
                                                                        OnTextChanged="txtFam_DOB_TextChanged"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtFam_DOB" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Age</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtFam_Age" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Relationship</label>
                                                                    <asp:TextBox ID="txtFam_Relationship" runat="server" Style="width: 100%" AutoComplete="off"
                                                                        class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Mobile No</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtFam_Mobile_No" runat="server" class="form-control BorderStyle">
                                                                    </asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtFam_Mobile_No" ValidChars="0123456789">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Nominee</label>
                                                                    <asp:TextBox ID="txtFam_Nominee" runat="server" Style="width: 100%" AutoComplete="off"
                                                                        class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Nominee %</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtFam_Nominee_Percent" runat="server" class="form-control BorderStyle">
                                                                    </asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtFam_Nominee_Percent" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <asp:Button ID="BtnAdd_Family" runat="server" class="btn btn-success" Style="margin-top: 16%;"
                                                                        ValidationGroup="ValidateAdl_Field" Text="ADD" OnClick="BtnAdd_Family_Click" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <asp:Repeater ID="Repeater_Family" runat="server" EnableViewState="false">
                                                                    <HeaderTemplate>
                                                                        <table id="example" class="table table-hover table-striped RowTest">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>S.No</th>
                                                                                    <th>Name</th>
                                                                                    <th>DOB / Age</th>
                                                                                    <th>Relationship</th>
                                                                                    <th>Mobile No</th>
                                                                                    <th>Nominee</th>
                                                                                    <th>Nominee %</th>
                                                                                    <th>Mode</th>
                                                                                </tr>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                                            <td><%# Eval("Member_Name")%></td>
                                                                            <td><%# Eval("Member_DOB")%> / <%# Eval("Member_Age")%></td>
                                                                            <td><%# Eval("Relationship")%></td>
                                                                            <td><%# Eval("Member_Mobile_No")%></td>
                                                                            <td><%# Eval("Member_Nominee")%></td>
                                                                            <td><%# Eval("Member_Nominee_Per")%></td>

                                                                            <td>
                                                                                <asp:LinkButton ID="btnDeleteGrid_Family" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                    Text="" OnCommand="GridDelete_Family_Click" CommandArgument='Delete' CommandName='<%# Container.ItemIndex + 1 %>'
                                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Family Member details?');">
                                                                                </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate></table></FooterTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="tab-pane well1" id="tab_4">
                                        <asp:UpdatePanel ID="upGeneral" runat="server">
                                            <ContentTemplate>
                                                <fieldset class="box box-primary">
                                                    <%--<legend class="pull-left width-full">General</legend>--%>

                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">General</label></h3>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="row">

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Nominee</label>
                                                                    <asp:TextBox ID="txtNominee" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Nominee Relationship</label>
                                                                    <asp:TextBox ID="txtNomineeRelation" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Father Name/Spouse Name</label>
                                                                    <asp:TextBox ID="txtFatherName" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Parents Mobile1</label>
                                                                    <asp:TextBox ID="txtParentMob1" runat="server" class="form-control BorderStyle" MaxLength="10"></asp:TextBox>
                                                                    <%--  <asp:RequiredFieldValidator ControlToValidate="txtParentMob1" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator20" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtParentMob1" ValidChars="0123456789">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>

                                                        <div class="row">

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Mother Name</label>
                                                                    <asp:TextBox ID="txtMotherName" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Parents Mobile2</label>
                                                                    <asp:TextBox ID="txtParentMob2" runat="server" class="form-control BorderStyle" MaxLength="10"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtParentMob2" ValidChars="0123456789">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Guardian Name</label>
                                                                    <asp:TextBox ID="txtGuardianName" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Guardian Mobile</label>
                                                                    <asp:TextBox ID="txtGuardianMobile" runat="server" class="form-control BorderStyle" MaxLength="10"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtGuardianMobile" ValidChars="0123456789">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Permanent Address</label>
                                                                    <asp:TextBox ID="txtPermAddr" runat="server" class="form-control BorderStyle" Style="resize: none"
                                                                        TextMode="MultiLine"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Permanent Taluk</label>
                                                                    <asp:DropDownList ID="txtPermTaluk" runat="server"
                                                                        class="form-control BorderStyle select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Permanent District</label>
                                                                    <asp:DropDownList ID="txtPermDist" runat="server" class="form-control BorderStyle select2"
                                                                        Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">State </label>
                                                                    <asp:CheckBox ID="chkOtherState" runat="server" />OtherState
                                                                        <asp:DropDownList ID="ddlState" runat="server"
                                                                            class="form-control BorderStyle select2" Width="100%">
                                                                        </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Temp Taluk</label>
                                                                    <asp:DropDownList ID="txtTempTaluk" runat="server" Style="width: 100%"
                                                                        class="form-control BorderStyle select2">
                                                                    </asp:DropDownList>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Temp District</label>
                                                                    <asp:DropDownList ID="txtTempDist" runat="server" Style="width: 100%"
                                                                        class="form-control BorderStyle select2">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Temp Address</label>
                                                                    <asp:CheckBox ID="chkSame" runat="server"
                                                                        AutoPostBack="true" OnCheckedChanged="chkSame_CheckedChanged" />Same as Permanent
                                                                        <asp:TextBox ID="txtTempAddr" runat="server" Style="resize: none"
                                                                            class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Identification Mark1</label>
                                                                    <asp:TextBox ID="txtIdenMark1" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Identification Mark2</label>
                                                                    <asp:TextBox ID="txtIdenMark2" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row"></div>
                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="tab-pane well1" id="tab_5">
                                        <asp:UpdatePanel ID="upAdole" runat="server">
                                            <ContentTemplate>
                                                <fieldset class="box box-primary">
                                                    <%--<legend class="pull-left width-full">Adolescent</legend>--%>

                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">Adolescent</label></h3>
                                                    </div>

                                                    <div class="col-md-12">

                                                        <div class="row">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <asp:CheckBox ID="chkAdolescent" runat="server" AutoPostBack="true"
                                                                        OnCheckedChanged="chkAdolescent_CheckedChanged" />
                                                                    Adolescent 
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div id="Div5" class="col-md-4" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Status</label>
                                                                    <asp:DropDownList ID="txtStatus" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <asp:CheckBox ID="chkAge18Complete" runat="server" AutoPostBack="true"
                                                                        OnCheckedChanged="chkAge18Complete_CheckedChanged" />
                                                                    Adolescent Completed 
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:TextBox ID="txtAge18Comp_Date" runat="server" class="form-control BorderStyle datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtAge18Comp_Date" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>

                                                        <div class="row">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Certificate No</label>
                                                                    <asp:TextBox ID="txtCertificate_No" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                                    <%--   <asp:RequiredFieldValidator ControlToValidate="txtCertificate_No"  Display="Dynamic" ValidationGroup="ValidateAdl_Field" class="form_error" ID="RequiredFieldValidator33" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>--%>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Certificate Date</label>
                                                                    <asp:TextBox ID="txtCertificate_Date" runat="server" class="form-control BorderStyle datepicker"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtCertificate_Date" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Next Due Date</label>
                                                                    <asp:TextBox ID="txtAdoles_Due_Date" runat="server" class="form-control BorderStyle datepicker"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtAdoles_Due_Date" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>

                                                        <div class="row">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Type of certificate</label>
                                                                    <asp:DropDownList ID="txtAdols_Type" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                        <asp:ListItem Value="Adolescent">Adolescent</asp:ListItem>
                                                                        <asp:ListItem Value="Noise Testing">Noise Testing</asp:ListItem>
                                                                        <asp:ListItem Value="Canteen">Canteen</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <%--<asp:RequiredFieldValidator ControlToValidate="txtAdols_Type" InitialValue="-Select-"  Display="Dynamic" ValidationGroup="ValidateAdl_Field" class="form_error" ID="RequiredFieldValidator36" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>--%>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Remarks</label>
                                                                    <asp:TextBox ID="txtAdols_Remarks" runat="server" class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <asp:Button ID="BtnAdolcent_Add" runat="server" class="btn btn-success" Style="margin-top: 16%;" ValidationGroup="ValidateAdl_Field" Text="ADD" OnClick="BtnAdolcent_Add_Click" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                                                    <HeaderTemplate>
                                                                        <table id="example" class="display table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>S.No</th>
                                                                                    <th>CertificateNo</th>
                                                                                    <th>CertificateDate</th>
                                                                                    <th>Due Date</th>
                                                                                    <th>Type of certificate</th>
                                                                                    <th>Remarks</th>
                                                                                    <th>Mode</th>
                                                                                </tr>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                                            <td><%# Eval("Certificate_No")%></td>
                                                                            <td><%# Eval("Certificate_Date_Str")%></td>
                                                                            <td><%# Eval("Next_Due_Date_Str")%></td>
                                                                            <td><%# Eval("Certificate_Type")%></td>
                                                                            <td><%# Eval("Remarks")%></td>


                                                                            <td>
                                                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                    Text="" OnCommand="GridDeleteClick_Certificate" CommandArgument='Delete' CommandName='<%# Eval("Certificate_No")%>'
                                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Certificate No details?');">
                                                                                </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate></table></FooterTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="tab-pane well1" id="tab_6">
                                        <asp:UpdatePanel ID="upDoc" runat="server">
                                            <ContentTemplate>
                                                <fieldset class="box box-primary">
                                                    <%--<legend class="pull-left width-full">Documents</legend>--%>

                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">Documents</label></h3>
                                                    </div>

                                                    <div class="col-md-12">

                                                        <div class="row">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Document Type</label>
                                                                    <asp:DropDownList ID="ddlDocType" runat="server" class="form-control BorderStyle select2"
                                                                        Width="100%" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                        <asp:ListItem Value="Adhar Card" Text="Adhar Card"></asp:ListItem>
                                                                        <asp:ListItem Value="Voter Card" Text="Voter Card"></asp:ListItem>
                                                                        <asp:ListItem Value="Ration Card" Text="Ration Card"></asp:ListItem>
                                                                        <asp:ListItem Value="Pan Card" Text="Pan Card"></asp:ListItem>
                                                                        <asp:ListItem Value="Driving Licence" Text="Driving Licence"></asp:ListItem>
                                                                        <asp:ListItem Value="Smart Card" Text="Smart Card"></asp:ListItem>
                                                                        <asp:ListItem Value="Bank Pass Book" Text="Bank Pass Book"></asp:ListItem>
                                                                        <asp:ListItem Value="Passport" Text="Passport"></asp:ListItem>
                                                                        <asp:ListItem Value="Others" Text="Others"></asp:ListItem>

                                                                    </asp:DropDownList>

                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Document No</label>
                                                                    <asp:TextBox ID="txtDocNo" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->

                                                            <!-- begin col-4 -->
                                                            <%--<div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="LabelColor">Document Description</label>
                                                        <asp:TextBox ID="txtDocDesc" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                    
                                                    </div>
                                                </div>--%>
                                                            <!-- end col-4 -->

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <asp:LinkButton ID="btnDocAdd" runat="server" class="btn btn-success" Style="margin-top: 16%;" Text="ADD" OnClick="btnDocAdd_Click" />
                                                                </div>
                                                            </div>
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label runat="server" id="txtDigit" style="margin-top: 30px; margin-left: -78px;"></label>
                                                                    <%--<asp:Label ID="txtDigit" runat="server"></asp:Label>--%>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-2" id="docImg" runat="server" visible="false">
                                                                <div class="media" style="margin-top: -19px;">
                                                                    <a class="media-right" href="javascript:;" style="float: right;">
                                                                        <asp:Image ID="Image2" runat="server" class="media-object rounded-corner" Style="width: 158px; height: 120px;" ImageUrl="~/assets/img/login-bg/man-user-50.png" />

                                                                    </a>

                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2">

                                                                <cc1:AsyncFileUpload runat="server" ID="filUpload" CssClass="hideupload"
                                                                    UploaderStyle="Modern" CompleteBackColor="White" UploadingBackColor="#CCFFFF"
                                                                    ThrobberID="imgLoader" OnUploadedComplete="FileUploadComplete" />
                                                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/2.gif" /><br />
                                                                <br />
                                                                <img id="imgDisplay" alt="" src="" style="display: none" />
                                                            </div>
                                                        </div>

                                                        <div class="row"></div>
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                                    <HeaderTemplate>
                                                                        <table id="example1" class="display table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>S.No</th>
                                                                                    <th>DocType</th>
                                                                                    <th>DocNo</th>
                                                                                    <th>Images</th>
                                                                                    <th>Mode</th>
                                                                                </tr>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                                            <td><%# Eval("DocType")%></td>
                                                                            <td><%# Eval("DocNo")%></td>
                                                                            <td>
                                                                                <asp:ImageButton ID="Image1" ImageUrl='<%# Eval("imgurl") %>' runat="server" Height="50px"
                                                                                    Width="50px" Style="cursor: pointer" OnClientClick="Javascript:return loadm(this.src);" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:LinkButton ID="LinkButton1" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                                    Text="" OnCommand="GridEditClick" CommandArgument='<%# Eval("DocType")%>' CommandName='<%# Eval("DocNo")%>'>
                                                                                </asp:LinkButton>
                                                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                    Text="" OnCommand="GridDeleteClick" CommandArgument='Delete' CommandName='<%# Eval("DocNo")%>'
                                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this DocNo details?');">
                                                                                </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate></table></FooterTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>

                                                        <div id="divBackground" class="modal">
                                                        </div>
                                                        <div id="divImage">
                                                            <table style="height: 100%; width: 100%">
                                                                <tr>
                                                                    <td valign="middle" align="center">
                                                                        <img id="imgLoader" alt="" src="images/loader.gif" />
                                                                        <img id="imgFull" alt="" src="" style="display: none; height: 500px; width: 590px" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" valign="bottom">
                                                                        <input id="btnClose" type="button" value="close" onclick="HideDiv()" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>


                                                        <!-- table End -->
                                                        <style>
                                                            .hideupload #ctl00_ContentPlaceHolder1_filUpload_ctl04 {
                                                                display: none
                                                            }
                                                        </style>
                                                        <%-- <!-- begin row -->
                                            <div class="row">
                                                <label class="control-label col-md-5 col-sm-5"></label>
                                                <div class="col-md-3 col-sm-3">
                                                   <asp:Button runat="server" id="btnBack" Text="Back" class="btn btn-primary" 
                                                        onclick="btnBack_Click" Visible="false"/>
                                                    <asp:Button runat="server" id="btnEmpSave" Text="Save" ValidationGroup="Validate_Field" class="btn btn-success" 
                                                        onclick="btnEmpSave_Click"/>
									               <asp:Button runat="server" id="btnEmpClear" Text="Clear" class="btn btn-danger" 
                                                        onclick="btnEmpClear_Click" />
                                                </div>
                                                <div class="col-md-3 col-sm-3">

                                                </div>
                                            </div>
                                            <!-- end row -->--%>
                                                    </div>
                                                </fieldset>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="tab-pane well1" id="tab_7">
                                        <asp:UpdatePanel ID="upEmpExpDet" runat="server">
                                            <ContentTemplate>
                                                <fieldset class="box box-primary">
                                                    <%--<legend class="pull-left width-full">Experience</legend>--%>

                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">Experience</label></h3>
                                                    </div>

                                                    <div class="col-md-12">

                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Company Name</label>
                                                                    <asp:TextBox ID="txtPrvCompName" runat="server" Style="width: 100%" AutoComplete="off"
                                                                        class="form-control  BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Designation</label>
                                                                    <asp:TextBox ID="txtPrvDesign" runat="server" class="form-control BorderStyle"
                                                                        AutoComplete="off" Style="width: 100%"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">From Date</label>
                                                                    <asp:TextBox ID="txtPrvFDate" runat="server" class="form-control  BorderStyle datepicker"
                                                                        AutoComplete="off" Style="width: 100%" AutoPostBack="true"
                                                                        OnTextChanged="txtPrvFDate_TextChanged"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">To Date</label>
                                                                    <asp:TextBox ID="txtPrvTDate" runat="server" class="form-control  BorderStyle datepicker"
                                                                        AutoComplete="off" Style="width: 100%" AutoPostBack="true"
                                                                        OnTextChanged="txtPrvTDate_TextChanged"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">No Of Year</label>
                                                                    <asp:TextBox ID="txtPrvNoYears" runat="server" class="form-control  BorderStyle"
                                                                        AutoComplete="off" Style="width: 100%"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Remarks</label>
                                                                    <asp:TextBox ID="txtExpRemarks" runat="server" class="form-control  BorderStyle"
                                                                        AutoComplete="off" TextMode="MultiLine"
                                                                        Style="width: 100%; resize: none"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <asp:Button ID="btnAddExp" runat="server" class="btn btn-success" Style="margin-top: 16%;"
                                                                        ValidationGroup="ValidateAdl_Field" Text="ADD" OnClick="btnAddExp_Click" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <asp:Repeater ID="rptExpDetEmp" runat="server" EnableViewState="false">
                                                                    <HeaderTemplate>
                                                                        <table id="example" class="table table-hover table-striped RowTest">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>S.No</th>
                                                                                    <th>Company Name</th>
                                                                                    <th>Designation</th>
                                                                                    <th>From Date</th>
                                                                                    <th>To Date</th>
                                                                                    <th>Work Year</th>
                                                                                    <th>Remarks</th>
                                                                                    <%--<th>Mode</th>--%>
                                                                                </tr>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                                            <td><%# Eval("CompanyName")%></td>
                                                                            <td><%# Eval("Designation")%></td>
                                                                            <td><%# Eval("FDate")%></td>
                                                                            <td><%# Eval("TDate")%></td>
                                                                            <td><%# Eval("NoOfYear")%></td>
                                                                            <td><%# Eval("Remarks")%></td>

                                                                            <%-- <td>
                                                                                    <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                        Text="" OnCommand="GridDeleteClick_Certificate" CommandArgument='Delete' CommandName='<%# Eval("CompanyName")%>'
                                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Certificate No details?');">
                                                                                    </asp:LinkButton>
                                                                                </td>--%>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate></table></FooterTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                            <%-- <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnAddExp" />
                                                    </Triggers>--%>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="tab-pane well1" id="tab_8">
                                        <asp:UpdatePanel ID="upQualifications" runat="server">
                                            <ContentTemplate>
                                                <fieldset class="box box-primary">
                                                    <%--<legend class="pull-left width-full">Experience</legend>--%>

                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">Qualifications</label></h3>
                                                    </div>

                                                    <div class="col-md-12">

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Institute Name</label>
                                                                    <asp:TextBox ID="txtInstiName" runat="server" Style="width: 100%" AutoComplete="off"
                                                                        class="form-control  BorderStyle"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Course</label>
                                                                    <asp:TextBox ID="txtCourse" runat="server" class="form-control BorderStyle"
                                                                        AutoComplete="off" Style="width: 100%"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Year Of Passing</label>
                                                                    <asp:TextBox ID="txtPassYear" runat="server" class="form-control  BorderStyle"
                                                                        AutoComplete="off" Style="width: 100%"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Precentage(%)</label>
                                                                    <asp:TextBox ID="txtPrecent" runat="server" class="form-control  BorderStyle"
                                                                        AutoComplete="off" Style="width: 100%"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Remarks</label>
                                                                    <asp:TextBox ID="txtEduRemark" runat="server" class="form-control  BorderStyle"
                                                                        AutoComplete="off" TextMode="MultiLine"
                                                                        Style="width: 100%; resize: none"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <asp:Button ID="btnAddEdu" runat="server" class="btn btn-success" Style="margin-top: 16%;"
                                                                        ValidationGroup="ValidateAdl_Field" Text="ADD" OnClick="btnAddEdu_Click" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <asp:Repeater ID="rptEduDetEmp" runat="server" EnableViewState="false">
                                                                    <HeaderTemplate>
                                                                        <table id="example" class="display table table-striped table-border table-hover">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>S.No</th>
                                                                                    <th>Institute Name</th>
                                                                                    <th>Course</th>
                                                                                    <th>Year Of Passing</th>
                                                                                    <th>Precentage</th>
                                                                                    <th>Remarks</th>
                                                                                    <%--<th>Mode</th>--%>
                                                                                </tr>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                                            <td><%# Eval("InstituteName")%></td>
                                                                            <td><%# Eval("CourseName")%></td>
                                                                            <td><%# Eval("PassingYear")%></td>
                                                                            <td><%# Eval("Precentage")%></td>
                                                                            <td><%# Eval("Remarks")%></td>

                                                                            <%-- <td>
                                                                                    <asp:LinkButton ID="grdbtnEmpEdu" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                        Text="" OnCommand="grdbtnEmpEdu_Cilck" CommandArgument='Delete' CommandName='<%# Container.ItemIndex %>'
                                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Certificate No details?');">
                                                                                    </asp:LinkButton>
                                                                                </td>--%>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate></table></FooterTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                            <%--  <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnAddEdu" />
                                                    </Triggers>--%>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            <br />

                            <asp:Panel ID="Approve_Cancel_panel" runat="server" Visible="false">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="LabelColor">Employee Cancel Reason</label>
                                            <asp:TextBox ID="txtCanecel_Reason_Approve" runat="server" TextMode="MultiLine" class="form-control BorderStyle"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div class="row">
                                <label class="control-label col-md-5 col-sm-5"></label>
                                <div class="col-md-3 col-sm-3">
                                    <asp:Button runat="server" ID="btnBack" Text="Back" class="btn btn-primary"
                                        OnClick="btnBack_Click" Visible="false" />
                                    <asp:Button runat="server" ID="btnApprove" Text="Approve" class="btn btn-success"
                                        OnClick="btnApprove_Click" Visible="false" />
                                    <asp:Button runat="server" ID="btnCancel_Approve" Text="Cancel" class="btn btn-danger"
                                        OnClick="btnCancel_Approve_Click" Visible="false" />
                                    <asp:Button runat="server" ID="btnEmpSave" Text="Save" ValidationGroup="Validate_Field" class="btn btn-success"
                                        OnClick="btnEmpSave_Click" />
                                    <asp:Button runat="server" ID="btnEmpClear" Text="Clear" class="btn btn-danger"
                                        OnClick="btnEmpClear_Click" />
                                </div>
                                <div class="col-md-3 col-sm-3">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>


    <script src="../assets/js/master_list_jquery.min.js"></script>
    <script src="../assets/js/master_list_jquery-ui.min.js"></script>
    <link href="../assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
    <script type="text/javascript">
        function showimagepreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script type="text/javascript">
        function showimagepreview2(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img2').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('#example1').dataTable();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>

    <script type="text/javascript">
        function loadm(url) {
            var img = new Image();
            var bcgDiv = document.getElementById("divBackground");
            var imgDiv = document.getElementById("divImage");
            var imgFull = document.getElementById("imgFull");
            var imgLoader = document.getElementById("imgLoader");
            imgLoader.style.display = "block";
            img.onload = function () {
                imgFull.src = img.src;
                imgFull.style.display = "block";
                imgLoader.style.display = "none";
            };
            img.src = url;
            var width = document.body.clientWidth;
            if (document.body.clientHeight > document.body.scrollHeight) {
                bcgDiv.style.height = document.body.clientHeight + "px";
            }
            else {
                bcgDiv.style.height = document.body.scrollHeight + "px";
            }
            imgDiv.style.left = (width - 650) / 2 + "px";
            imgDiv.style.top = "100px";
            bcgDiv.style.width = "100%";

            bcgDiv.style.display = "block";
            imgDiv.style.display = "block";
            return false;
        }
        function HideDiv() {
            var bcgDiv = document.getElementById("divBackground");
            var imgDiv = document.getElementById("divImage");
            var imgFull = document.getElementById("imgFull");
            if (bcgDiv != null) {
                bcgDiv.style.display = "none";
                imgDiv.style.display = "none";
                imgFull.style.display = "none";
            }
        }
    </script>

</asp:Content>

