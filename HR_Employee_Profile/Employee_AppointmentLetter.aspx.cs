﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Employee_Profile_Employee_AppointmentLetter : System.Web.UI.Page
{
    public static BALDataAccess objdata = new BALDataAccess();

    public static String CurrentYear1;
    public static int CurrentYear;

    public static string SessionCcode;
    public static string SessionLcode;
    public static string SessionUserID;
    public static string SessionUserName;
    public static string SessionRights;

    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Expenses";
            //AutoGenerateAppointmentNo();

        }
        Load_Data();
    }
    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from Employee_Appointment where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
    private void AutoGenerateAppointmentNo()
    {
        SSQL = "";
        SSQL = "Select * from Employee_Appointment where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
      
        if (txtemployee.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the EmployeeName');", true);
            ErrFlag = true;
            return;
        }
        if (txtDesig.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Designation');", true);
            ErrFlag = true;
            return;
        }
        if (txtPrep.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the PreparedBy');", true);
            ErrFlag = true;
            return;
        }
        if (txtPrepDesig.Text == "" )
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the PreparedByDesignation');", true);
            ErrFlag = true;
            return;
        }
        if (txtAddres.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Address');", true);
            ErrFlag = true;
            return;
        }
       

        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Delete from Employee_Appointment where Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' and AppNo='" + txtAppNo.Text+ "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert into Employee_Appointment(Ccode,Lcode,EmpName,Date_Str,Designation,AnnualSalary,PreparedBy,PreparedByDesig,Address,DOJ_Str,Location,BaseSalary,DA,HRA,SpecAllow,OtherAllow,EmpPFCont,TotEmpCont,CostCTC,TotEmpDed,TakeHomePay,Created_On)";
            SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtemployee.Text + "','" + txtAppDate.Text + "','" + txtDesig.Text + "','" + txtAnnual.Text + "','" + txtPrep.Text + "','" + txtPrepDesig.Text + "','" + txtAddres.Text + "'";
            SSQL = SSQL + ", '" + txtdoj.Text + "','" + txtloc.Text + "','" + txtBaseSal.Text + "','" + txtDA.Text + "','" + txtHRallow.Text + "','" + txtspAllow.Text + "','" + txtOtherAllow.Text + "','" + txtPF.Text + "','" + txtEmpCont.Text + "','" + txtCOCC.Text + "','" + txtEmpDed.Text + "','" + txtthp.Text + "','" + DateTime.Now.ToString("dd/MM/yyyy") + "')";

            objdata.RptEmployeeMultipleDetails(SSQL);

            if (btnSave.Text == "Save")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Saved Successfully!!!');", true);

            }
            if (btnSave.Text == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Updated Successfully!!!');", true);
            }
            btnClear_Click(sender, e);
            Load_Data();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtAppNo.Text = "";
        txtemployee.Text = "";
        txtAppDate.Text = "";
        txtDesig.Text = "";
        txtAnnual.Text = "0";
        txtPrep.Text = "";
        txtPrepDesig.Text = "";
        txtAddres.Text = "";
        txtdoj.Text = "";
        txtAppNo.Text = "";
        txtloc.Text = "0";
        txtBaseSal.Text = "0";
        txtDA.Text = "0";
        txtHRallow.Text = "0";
        txtspAllow.Text = "0";
        txtOtherAllow.Text = "0";
        txtPF.Text = "0";
        txtEmpCont.Text = "0";
        txtCOCC.Text = "0";
        txtEmpDed.Text = "0";
        txtthp.Text = "0";
        btnSave.Text = "Save";
    }
    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Employee_Appointment where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and AppNo='" + e.CommandName.ToString() + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtAppNo.Text = dt.Rows[0]["AppNo"].ToString();
            txtemployee.Text = dt.Rows[0]["EmpName"].ToString();
            txtAppDate.Text = dt.Rows[0]["Date_Str"].ToString();
            txtDesig.Text = dt.Rows[0]["Designation"].ToString();
            txtAnnual.Text = dt.Rows[0]["AnnualSalary"].ToString();
            txtPrep.Text = dt.Rows[0]["PreparedBy"].ToString();
            txtPrepDesig.Text = dt.Rows[0]["PreparedBYDesig"].ToString();
            txtAddres.Text = dt.Rows[0]["Address"].ToString();
            txtdoj.Text = dt.Rows[0]["DOJ_Str"].ToString();
            txtloc.Text = dt.Rows[0]["Location"].ToString();
            txtBaseSal.Text = dt.Rows[0]["BaseSalary"].ToString();
            txtDA.Text = dt.Rows[0]["DA"].ToString();
            txtHRallow.Text = dt.Rows[0]["HRA"].ToString();
            txtspAllow.Text = dt.Rows[0]["SpecAllow"].ToString();
            txtOtherAllow.Text = dt.Rows[0]["OtherAllow"].ToString();
            txtPF.Text = dt.Rows[0]["EmpPFCont"].ToString();
            txtEmpCont.Text = dt.Rows[0]["TotEmpCont"].ToString();
            txtCOCC.Text = dt.Rows[0]["CostCTC"].ToString();
            txtEmpDed.Text = dt.Rows[0]["TotEmpDed"].ToString();
            txtthp.Text = dt.Rows[0]["TakeHomePay"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from Employee_Appointment where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and AppNo='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }
}