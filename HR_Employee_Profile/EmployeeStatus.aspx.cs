﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Employee_Profile_EmployeeStatus : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Approval";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

        }
        Load_Data();
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select ENJ.EmpNo,ES.MachineID,ES.Token_No,ENJ.FirstName,ENJ.DeptName,ENJ.Designation,ES.Emp_Status,ES.Cancel_Reson";
        query = query + " from Employee_Mst_New_Emp ENJ inner join Employee_Mst_Status ES on ES.CompCode=ENJ.CompCode";
        query = query + " And ES.LocCode=ENJ.LocCode And ES.Token_No=ENJ.ExistingCode And ES.MachineID=ENJ.MachineID where ENJ.CompCode='" + SessionCcode + "'";
        query = query + " And ENJ.LocCode='" + SessionLcode + "' And ES.CompCode='" + SessionCcode + "' AND ES.LocCode='" + SessionLcode + "' And ENJ.IsActive='Yes'";
        query = query + " And (ES.Emp_Status='Pending' Or ES.Emp_Status='Cancel')";

        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }
}
