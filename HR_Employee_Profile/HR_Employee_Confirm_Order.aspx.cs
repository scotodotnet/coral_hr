﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class HR_Employee_Profile_HR_Employee_Confirm_Order : System.Web.UI.Page
{
    public static BALDataAccess objdata = new BALDataAccess();

    public static String CurrentYear1;
    public static int CurrentYear;

    public static string SessionCcode;
    public static string SessionLcode;
    public static string SessionUserID;
    public static string SessionUserName;
    public static string SessionRights;

    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Confirm Order";
            Load_EmpNo();

        }
        Load_Data();
    }
    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from Employee_Confirm_Details where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
    private void Load_EmpNo()
    {
        SSQL = "";
        SSQL = "Select * from Employee_mst where CompCode='" + SessionCcode + "' and  LocCode='" + SessionLcode + "'";
        DataTable dt_ = new DataTable();
        dt_ = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMachineID.DataSource = dt_;
        ddlMachineID.DataTextField = "MachineID";
        ddlMachineID.DataValueField = "MachineID";
        ddlMachineID.DataBind();
        ddlMachineID.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

        ddlTokenNo.DataSource = dt_;
        ddlTokenNo.DataTextField = "ExistingCode";
        ddlTokenNo.DataValueField = "ExistingCode";
        ddlTokenNo.DataBind();
        ddlTokenNo.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlMachineID.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the MachineID to Confirm');", true);
            ErrFlag = true;
            return;
        }
        if (ddlTokenNo.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Token No to Confirm');", true);
            ErrFlag = true;
            return;
        }
        if (txtRefID.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Reference Number');", true);
            ErrFlag = true;
            return;
        }
        if (txtDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Date');", true);
            ErrFlag = true;
            return;
        }
        if (txtProbationFrom.Text == "" || txtProbationTo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Probation Date From and To Properly');", true);
            ErrFlag = true;
            return;
        }
        if (txtConfirmName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Confirm by Name');", true);
            ErrFlag = true;
            return;
        }if (txtConfirmedFrom.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Confirm Date');", true);
            ErrFlag = true;
            return;
        }
        if (txtConfirmDesignation.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Confirm by Designation');", true);
            ErrFlag = true;
            return;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Delete from Employee_Confirm_Details where Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' and machineID='" + ddlMachineID.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert into Employee_Confirm_Details(Ccode,Lcode,RfID,Date,MachineID,ExistingCode,Name,DeptName,Designation,Probation_Period_From,Probation_Period_From_Str,Probation_Period_To,Probation_Period_To_Str,ConfirmFrom,ConfirmFrom_Str,Confirmed_By,Confirm_By_Designation,Created_On)";
            SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtRefID.Text + "','" + txtDate.Text + "','" + ddlMachineID.SelectedValue + "','" + ddlTokenNo.SelectedValue + "','" + txtName.Text + "','" + txtDeptName.Text + "','" + txtDesignation.Text + "'";
            SSQL = SSQL + ", Convert(date,'" + txtProbationFrom.Text + "',103),'" + txtProbationFrom.Text + "',Convert(date,'" + txtProbationTo.Text + "',103),'" + txtProbationTo.Text + "',Convert(date,'" + txtConfirmedFrom.Text + "',103),'"+ txtConfirmedFrom.Text + "','" + txtConfirmName.Text + "'";
            SSQL = SSQL + ",'" + txtConfirmDesignation.Text + "','" + DateTime.Now.ToString("dd/MM/yyyy") + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            if (btnSave.Text == "Save")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Saved Successfully!!!');", true);

            }
            if (btnSave.Text == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Updated Successfully!!!');", true);
            }
            btnClear_Click(sender, e);
            Load_Data();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlMachineID.ClearSelection();
        ddlTokenNo.ClearSelection();
        txtName.Text = "";
        txtDeptName.Text = "";
        txtRefID.Text = "";
        txtDate.Text = "";
        txtDesignation.Text = "";
        txtProbationFrom.Text = "";
        txtProbationTo.Text = "";
        txtConfirmedFrom.Text = "";
        txtConfirmName.Text = "";
        txtConfirmDesignation.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from Employee_Confirm_Details where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }
    protected void ddlTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_EmpData("TokenNo");
    }

    protected void ddlMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_EmpData("MachineID");
    }
    private void Load_EmpData(string Base)
    {
        SSQL = "";
        SSQL = "Select * from Employee_mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        if (Base == "MachineID")
        {
            SSQL = SSQL + " and MachineID='" + ddlMachineID.SelectedValue + "'";
            ddlTokenNo.SelectedValue = ddlMachineID.SelectedItem.Text;
        }
        if (Base == "TokenNo")
        {
            SSQL = SSQL + " and ExistingCode='" + ddlTokenNo.SelectedValue + "'";
            ddlMachineID.SelectedValue = ddlTokenNo.SelectedItem.Text;
        }

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtName.Text = dt.Rows[0]["FirstName"].ToString();
            txtDeptName.Text = dt.Rows[0]["DeptName"].ToString();
            txtDesignation.Text = dt.Rows[0]["Designation"].ToString();
        }
    }
    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Employee_Confirm_Details where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ddlMachineID.SelectedValue = dt.Rows[0]["MachineID"].ToString();
            ddlTokenNo.SelectedValue = dt.Rows[0]["ExistingCode"].ToString();
            txtName.Text = dt.Rows[0]["Name"].ToString();
            txtDeptName.Text = dt.Rows[0]["DeptName"].ToString();
            txtRefID.Text = dt.Rows[0]["RfID"].ToString();
            txtDate.Text = dt.Rows[0]["date"].ToString();
            txtDesignation.Text = dt.Rows[0]["Designation"].ToString();
            txtProbationFrom.Text = dt.Rows[0]["Probation_Period_From_Str"].ToString();
            txtProbationTo.Text = dt.Rows[0]["Probation_Period_To_Str"].ToString();
            txtConfirmedFrom.Text = dt.Rows[0]["ConfirmFrom_Str"].ToString();
            txtConfirmName.Text = dt.Rows[0]["Confirmed_By"].ToString();
            txtConfirmDesignation.Text = dt.Rows[0]["Confirm_By_Designation"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
    }
}