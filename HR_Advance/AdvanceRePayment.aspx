﻿<%@ Page Title="Advance RePayment Entry" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdvanceRePayment.aspx.cs" Inherits="HR_Advance_AdvanceRePayment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     });
 </script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                 $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };

</script>

<!-- begin #content -->
<div id="content" class="content-wrapper">
    <section class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Advance</a></li>
				<li class="active">Advance RePayment</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Advance RePayment</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								<label>Wages Type</label>
								 <asp:DropDownList runat="server" ID="ddlWagesType" class="form-control select2" style="width:100%;" 
								  AutoPostBack="true" OnSelectedIndexChanged="ddlWagesType_SelectedIndexChanged" >
							 	 </asp:DropDownList>
							 	 </div>
								</div>
                               
                           <!-- end col-4 -->
                             <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Existing Code</label>
								 <asp:DropDownList runat="server" ID="ddlExistingCode" class="form-control select2" style="width:100%;"
								  AutoPostBack="true" OnSelectedIndexChanged="ddlExistingCode_SelectedIndexChanged" >
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                         </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                           <div class="col-md-4">
							   <div class="form-group">
								<label>Machine ID</label>
								<asp:Label runat="server" ID="txtTokenNo" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                         <div class="col-md-4">
							   <div class="form-group">
								<label>Designation</label>
								<asp:Label runat="server" ID="txtDesignation" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           
                       
                         
                          <!-- begin col-4 -->
                             <div class="col-md-4">
							   <div class="form-group">
								<label>Balance Amount</label>
								<asp:Label runat="server" ID="txtBalanceAmt" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                             </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                           <div class="col-md-4">
									<div class="form-group">
										<label>Amount</label>
										<asp:TextBox runat="server" ID="txtAmount" class="form-control"></asp:TextBox>
									</div>
                                  </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                             <div class="row">
                           <div class="col-md-4">
									<div class="form-group">
										<label>Date</label>
										<asp:TextBox runat="server" ID="txtdate" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                           <!-- end col-4 -->
                          
                           </div>
                        <!-- end row -->
                        
                                                 <!-- end col-4 -->
                              
                      
                          
                      <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        
                        </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnSave" />
                        </Triggers>
                        </asp:UpdatePanel>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

  
</asp:Content>

