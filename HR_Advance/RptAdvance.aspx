﻿<%@ Page Title="Advance Report" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RptAdvance.aspx.cs" Inherits="HR_Advance_RptAdvance" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     });
 </script>
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                 $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };

</script>

<!-- begin #content -->
<div id="content" class="content-wrapper">
    <section class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Advance</a></li>
				<li class="active">Salary Advance Report</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Salary Advance Report</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">                        
                        <div class="panel-body">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								<label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="ddlWagesType" class="form-control select2" style="width:100%;" 
								  AutoPostBack="true" OnSelectedIndexChanged="ddlWagesType_SelectedIndexChanged" >
							 	 </asp:DropDownList>
							 	 </div>
								</div>
                               
                           <!-- end col-4 -->
                             <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Employee No</label>
								 <asp:DropDownList runat="server" ID="ddlMacineId" class="form-control select2" style="width:100%;"
								  AutoPostBack="true" OnSelectedIndexChanged="ddlMacineId_SelectedIndexChanged" >
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <div class="col-md-3">
							   <div class="form-group">
								<label>Existing No</label>
								
								<asp:TextBox runat="server" ID="txtExist" class="form-control"></asp:TextBox>
								</div>
                             </div>
                     
                     
                       <!-- begin row -->
                        
                           <!-- begin col-4 -->
                           <div class="col-md-3">
							   <div class="form-group">
							   </br>
                          <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" 
                                        Width="75" Height="28" class="btn btn-primary" />
                                        </div>
                             </div>
                                 </div>
                                    <!-- end row -->
                          <!-- begin col-4 -->
                        
                             <div class="row">
                           <div class="col-md-3">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-3">
							   <div class="form-group">
								<label>Department</label>
								<asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                         
                   
                       
                          
                           <!-- begin col-4 -->
                         <div class="col-md-3">
							   <div class="form-group">
								<label>Designation</label>
								<asp:Label runat="server" ID="txtDesignation" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                          
                          <!-- begin col-4 -->
                             <div class="col-md-3">
							   <div class="form-group">
								<label>Date Of Joining</label>
								<asp:Label runat="server" ID="txtDOJ" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           
                           </div>
                        <!-- end row -->
                          <!-- begin row -->  
                         <div class="row">
                       
                               <div class="form-group">
                               <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                                       <th>SL.No</th>
                                                       
                                                        <th>AVAILED DATE</th>
                                                        <th>ADVANCE AMOUNT</th>
                                                        <th>SETTLED DATE</th>
                                                        <th>DURATION</th>
                                                        <th>BALANCE AMOUNT</th>
                                                        <th>COMPLETED</th>
                                                        <th>Edit</th>
                                                      
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                            <td><%# Container.ItemIndex+1 %></td>                     
                                             
                                                <td><%# Eval("CreateDate")%></td>
                                                 <td><%# Eval("Amount")%></td>
                                                <td><%# Eval("Modifieddate")%></td>
                                                <td><%# Eval("ReductionMonth")%></td>
                                                  <td><%# Eval("BalanceAmount")%></td>
                                                   <td><%# Eval("Completed")%></td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument="Edit" CommandName='<%# Eval("Id")%>'>
                                                    </asp:LinkButton>
                                                   
                                                    </td>
                                                    
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
                               </div>
                         
                        </div>
                       
                            <!-- end row -->
                      <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="All Employee" class="btn btn-success" 
                                         onclick="btnAll_Emp_Advance_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        
                        </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
    </section>
</div>
<!-- end #content -->

  
</asp:Content>

