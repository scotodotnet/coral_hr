﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DownLoadClear.aspx.cs" Inherits="HR_Biometric_DownLoadClear" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
</script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
</script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
</script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
</script>

    <!-- begin #content -->
    <div id="content" class="content-wrapper">
        <section class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">Bio Metric</a></li>
                <li class="active">DownLoad/Clear</li>
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">DownLoad/Clear </h1>
            <!-- end page-header -->

            <!-- begin row -->
            <div class="row">
                <!-- begin col-12 -->
                <div class="col-md-12">
                    <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <contenttemplate>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>IP Address</label>
								  <asp:DropDownList runat="server" ID="ddlIPAddress" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" onselectedindexchanged="ddlIPAddress_SelectedIndexChanged">
								  </asp:DropDownList>
								  <asp:RequiredFieldValidator ControlToValidate="ddlIPAddress" Display="Dynamic" InitialValue="-Select-"  ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                          <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btndownload" Text="Download" class="btn btn-success" onclick="btndownload_Click" OnClientClick="ProgressBarShow();"/>
									<asp:Button runat="server" id="btndownloadClear" Text="Download/Clear" class="btn btn-danger" onclick="btndownloadClear_Click" OnClientClick="ProgressBarShow();"/>
								<asp:Button runat="server" id="btnDocumnet" Text="Document" class="btn btn-danger" onclick="btnDocumnet_Click" Visible="false"/>
							
								 </div>
                               </div>
                              <!-- end col-4 -->
                             
                             
                             
                             
                              </div>
                        <!-- end row -->
                        
                        
                          <!-- begin row -->
                          <div class="row">
                          <asp:Label ID="lblDwnCmpltd" runat="server" Font-Bold="True" Font-Italic="True" 
                                    Font-Size="Medium" ForeColor="Red"></asp:Label>
                          </div>
                          <!-- end row -->
                           <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>Token Number</label>
								 <asp:TextBox runat="server" ID="txtTokenNumber" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4" runat="server" visible="false" id="Div_Wages">
							    <div class="form-group">
								    <label>Wages Type</label>
								    <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2" style="width:100%;">
							 	    </asp:DropDownList>
								</div>
                               </div>
                           </div>
                          <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>From Date</label>
								 <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                               <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>To Date</label>
								 <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button ID="btnConversion" class="btn btn-success"  runat="server" 
                         Text="Convert" ValidationGroup="Validate_Field" OnClick="btnConversion_Click"
                          OnClientClick="ProgressBarShow();"/>
                         <asp:Button ID="Button1" class="btn btn-success"  runat="server" 
                         Text="IDConvert" onclick="Button1_Click" Visible="true" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                         <div class="row">
                         <div class="col-md-4"></div>
                           
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                         <div id="Download_loader" style="display:none"/></div>
                        </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-12 -->
        </section>
    </div>
    <!-- end #content -->



</asp:Content>

