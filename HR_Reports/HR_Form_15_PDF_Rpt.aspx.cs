﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class HR_Reports_HR_Form_15_PDF_Rpt : System.Web.UI.Page
{
    string fromdate;
    string todate;
    string Wages;
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SSQL = "";
    string Year;
    string Year1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-FORM 15";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            fromdate = Request.QueryString["FromDate"].ToString();
            todate = Request.QueryString["ToDate"].ToString();
            string TempWages = Request.QueryString["WagesType"].ToString();
            Wages = TempWages.Replace("_", "&");
            //Wages = Request.QueryString["WagesType"].ToString();
            Year1 = Request.QueryString["Year"].ToString();

            Form_15();
        }
    }

    public void Form_15()
    {
        DataTable DT_OUT = new DataTable();
        SSQL = "Select EM.EmpNo,EM.ExistingCode,EM.FirstName as EmpName,EM.FamilyDetails as FatherName,EM.DeptName as DepartmentNm,convert(varchar,EM.DOJ,103) as DOJ,";
        SSQL = SSQL + " Year(SD.FromDate) As Year_Str,left(SD.Month,3) + '-' + Right(Year(SD.FromDate),2) as Month_Str,Round(SD.GrossEarnings,0) as Basic_Wages,SD.WorkedDays,SD.FromDate,AD.LWW_Days,";
        SSQL = SSQL + " Convert(Varchar(20),SD.FromDate,103) as FromDate_S,Convert(Varchar(20),SD.ToDate,103) as ToDate_S from Employee_Mst EM";
        SSQL = SSQL + " inner Join [CORAL_ERP_HR]..SalaryDetails SD on EM.EmpNo=SD.EmpNo inner Join [CORAL_ERP_HR]..AttenanceDetails AD on EM.EmpNo=AD.EmpNo And SD.FromDate=AD.FromDate";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And Year(SD.FromDate)='" + Year1.ToString() + "' And Year(AD.FromDate)='" + Year1.ToString() + "'";
        if (Wages != "")
        {
            SSQL = SSQL + " And EM.Wages='" + Wages + "'";
        }
        SSQL = SSQL + " Order by EM.ExistingCode,SD.FromDate Asc";
        DT_OUT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_OUT.Rows.Count > 0)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(DT_OUT);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("../crystal_HR/Form_15_Det.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }

}
