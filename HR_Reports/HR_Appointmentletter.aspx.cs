﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using CrystalDecisions.CrystalReports.Engine;

public partial class HR_Reports_HR_Appointmentletter : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;

    string Tokeno = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Appointment Letter";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            //Status = Request.QueryString["Status"].ToString();
            //Tokeno = Request.QueryString["TokenNo"].ToString();
            Datestr = Request.QueryString["FromDate"].ToString();
            Get_Report();
        }
    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    private void Get_Report()
    {
        string TableName = "";

        SSQL = "";
        SSQL = "Select AppNo,EmpName,Date_Str,Designation,AnnualSalary,PreparedBy,PreparedBYDesig,Address,DOJ_Str,Location,BaseSalary,DA,HRA,SpecAllow,OtherAllow,EmpPFCont,TotEmpCont,CostCTC,TotEmpDed,TakeHomePay ";
        SSQL = SSQL + " from Employee_Appointment where Date_Str='" + Datestr + "'";
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

        if (AutoDTable.Rows.Count != 0)
        {

            ds.Tables.Add(AutoDTable);

            report.Load(Server.MapPath("../crystal_HR/AppointmentLetter.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }
}