﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class HR_Reports_HR_FormI_WorkMenPermanent_PDF : System.Web.UI.Page
{
    string fromdate;
    string todate;
    string Wages;
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    string SessionUserType;
    string SSQL = "";
    string mIpAddress_IN;
    string mIpAddress_OUT;
    DataTable dsEmployee = new DataTable();
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    string RegNo;
    DataTable dtdlocation = new DataTable();
    string Address1;
    string Address2;
    string city;
    string Pincode;
    string PF_DOJ_Get_str;
    DateTime PF_DOJ_Get;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-FORM I WORK MEN PERMANENT REPORT ";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            fromdate = Request.QueryString["FromDate"].ToString();
            todate = Request.QueryString["ToDate"].ToString();
            string TempWages = Request.QueryString["WagesType"].ToString();
            Wages = TempWages.Replace("_", "&");

            
            DataTable dt = new DataTable();
            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count != 0)
            {
                SessionCompanyName = dt.Rows[0]["CompName"].ToString();
            }


            FormIworkMenPermanentReport();
        }
    }

    public void FormIworkMenPermanentReport()
    {
        DataTable dtdlocation = new DataTable();
        

        SSQL = "Select * from Location_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
        dtdlocation = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dtdlocation.Rows.Count > 0)
        {
            RegNo = dtdlocation.Rows[0]["Register_No"].ToString();
            Address1 = dtdlocation.Rows[0]["Add1"].ToString();
            Address2 = dtdlocation.Rows[0]["Add2"].ToString();
            city = dtdlocation.Rows[0]["City"].ToString();
            Pincode = dtdlocation.Rows[0]["Pincode"].ToString();

        }
        else
        {
            RegNo = "";
        }

        SSQL = "";
        SSQL = "Select MachineID,ExistingCode,FirstName,LastName,Address1,DeptName,Designation,Eligible_PF,PFDOJ,PFNo,Convert(varchar(20),DOJ,103) as DOJ,Emp_Permn_Date from Employee_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And IsActive='Yes'";
        if (fromdate != "" && todate != "")
        {
            SSQL = SSQL + " And CONVERT(DateTime,DOJ, 103) >= CONVERT(DateTime,'" + fromdate + "', 103)";
            SSQL = SSQL + " and CONVERT(DateTime,DOJ, 103) <= CONVERT(DateTime,'" + todate + "', 103)";
        }
        if (Wages != "")
        {
            SSQL = SSQL + " And Wages='" + Wages + "'";
        }
        //Check User Type
        if (SessionUserType == "2")
        {
            SSQL = SSQL + " And Eligible_PF='1'";
        }
        SSQL = SSQL + " Order by ExistingCode Asc";


        dsEmployee = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dsEmployee.Rows.Count > 0)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dsEmployee);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("../crystal_HR/Form_I_Det.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }
}
