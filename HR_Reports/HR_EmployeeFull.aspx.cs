﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class HR_Reports_HR_EmployeeFull : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = ""; string status = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Full Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();

            Empcode = Request.QueryString["Empcode"].ToString();
            Division = Request.QueryString["Division"].ToString();
            status = Request.QueryString["status"].ToString();


            GetEmpFullProfileTable();

            ds.Tables.Add(AutoDataTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("/crystal_HR/single_emp_profile1.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
    }
    public void GetEmpFullProfileTable()
    {

        AutoDataTable.Columns.Add("CompCode");
        AutoDataTable.Columns.Add("LocCode");
        AutoDataTable.Columns.Add("EmpPrefix");
        AutoDataTable.Columns.Add("EmpNo");
        AutoDataTable.Columns.Add("ExistingCode");
        AutoDataTable.Columns.Add("MachineID");
        AutoDataTable.Columns.Add("FirstName");
        AutoDataTable.Columns.Add("LastName");
        AutoDataTable.Columns.Add("MiddleInitial");
        AutoDataTable.Columns.Add("ShiftType");



        AutoDataTable.Columns.Add("Gender");
        AutoDataTable.Columns.Add("BirthDate");
        AutoDataTable.Columns.Add("Age");
        AutoDataTable.Columns.Add("DOJ");
        AutoDataTable.Columns.Add("MaritalStatus");
        AutoDataTable.Columns.Add("CatName");
        AutoDataTable.Columns.Add("SubCatName");
        AutoDataTable.Columns.Add("TypeName");
        AutoDataTable.Columns.Add("EmpStatus");
        AutoDataTable.Columns.Add("IsActive");

        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Designation");
        AutoDataTable.Columns.Add("PayPeriod_Desc");
        AutoDataTable.Columns.Add("BaseSalary");
        AutoDataTable.Columns.Add("PFNo");
        AutoDataTable.Columns.Add("Nominee");
        AutoDataTable.Columns.Add("ESINo");
        AutoDataTable.Columns.Add("StdWrkHrs");
        AutoDataTable.Columns.Add("OTEligible");
        AutoDataTable.Columns.Add("Nationality");

        AutoDataTable.Columns.Add("Qualification");
        AutoDataTable.Columns.Add("Certificate");
        AutoDataTable.Columns.Add("FamilyDetails");
        AutoDataTable.Columns.Add("Address1");
        AutoDataTable.Columns.Add("Address2");
        AutoDataTable.Columns.Add("RecuritmentThro");
        AutoDataTable.Columns.Add("IDMark1");
        AutoDataTable.Columns.Add("IDMark2");
        AutoDataTable.Columns.Add("BloodGroup");
        AutoDataTable.Columns.Add("Handicapped");

        AutoDataTable.Columns.Add("Height");
        AutoDataTable.Columns.Add("Weight");
        AutoDataTable.Columns.Add("BankName");
        AutoDataTable.Columns.Add("BranchCode");
        AutoDataTable.Columns.Add("AccountNo");


        AutoDataTable.Columns.Add("BusNo");
        AutoDataTable.Columns.Add("Refrence");
        AutoDataTable.Columns.Add("Permanent_Dist");
        AutoDataTable.Columns.Add("Present_Dist");
        AutoDataTable.Columns.Add("WeekOff");
        AutoDataTable.Columns.Add("parentsMobile");
        AutoDataTable.Columns.Add("EmployeeMobile");
        AutoDataTable.Columns.Add("Wages");
        AutoDataTable.Columns.Add("Adhar_No");

        AutoDataTable.Columns.Add("img", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("Add_Proof", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_3", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_4", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_5", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_6", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_7", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_8", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_9", System.Type.GetType("System.Byte[]"));


        string s = Empcode;
        string[] delimiters = new string[] { "->" };
        string[] items = s.Split(delimiters, StringSplitOptions.None);
        string ss = items[0];
        iEmpDet = items[1];



        //string Emp_No = null;
        //string Add_Where_Cont = null;


        //if (iEmpDet[0] != "ALL" & iEmpDet[0] == "")
        //{
        //    Emp_No = iEmpDet;
        //    iEmpDet = Emp_No.Split(delimiters, StringSplitOptions.None);

        //    Add_Where_Cont = "";
        //    Add_Where_Cont += " Where CompCode='" + CompanyCode + "'";
        //    Add_Where_Cont += " and LocCode='" + LocationCode + "' and EmpNo='" + iEmpDet[2] + "'";
        //}
        //else
        //{
        //    Add_Where_Cont = "";
        //    Add_Where_Cont += " Where CompCode='" + CompanyCode + "'";
        //    Add_Where_Cont += " LocCode='" + LocationCode + "'";
        //}


        SSQL = "";
        SSQL = "select isnull(CompCode,'') as [CompCode]";
        SSQL += ",isnull(LocCode,'') as [LocCode]";
        SSQL += ",isnull(EmpPrefix,'') as [EmpPrefix]";
        SSQL += ",isnull(EmpNo,'') as [EmpNo]";
        SSQL += ",isnull(ExistingCode,'') as [ExistingCode]";
        SSQL += ",isnull(MachineID,'') as [MachineID]";
        SSQL += ",isnull(FirstName,'') as [FirstName]";
        SSQL += ",isnull(LastName,'') as [LastName]";
        SSQL += ",isnull(MiddleInitial,'') as [MiddleInitial]";
        SSQL += ",isnull(ShiftType,'') as [ShiftType]";
        SSQL += ",isnull(Gender,'') as [Gender]";
        SSQL += ",isnull(BirthDate,'') as [BirthDate]";
        SSQL += ",isnull(Age,'') as [Age]";
        SSQL += ",isnull(DOJ,'') as [DOJ]";
        SSQL += ",isnull(MaritalStatus,'') as [MaritalStatus]";
        SSQL += ",isnull(CatName,'') as [CatName]";
        SSQL += ",isnull(SubCatName,'') as [SubCatName]";
        SSQL += ",isnull(TypeName,'') as [TypeName]";
        SSQL += ",isnull(EmpStatus,'') as [EmpStatus]";
        SSQL += ",isnull(IsActive,'') as [IsActive]";
        SSQL += ",isnull(DeptName,'') as [DeptName]";
        SSQL += ",isnull(Designation,'') as [Designation]";
        SSQL += ",isnull(PayPeriod_Desc,'') as [PayPeriod_Desc]";
        SSQL += ",BaseSalary";
        SSQL += ",isnull(PFNo,'') as [PFNo]";
        SSQL += ",isnull(Nominee,'') as [Nominee]";
        SSQL += ",isnull(ESINo,'') as [ESINo]";
        SSQL += ",StdWrkHrs";
        SSQL += ",isnull(OTEligible,'') as [OTEligible]";
        SSQL += ",isnull(Nationality,'') as [Nationality]";
        SSQL += ",isnull(Qualification,'') as [Qualification]";
        SSQL += ",isnull(Certificate,'') as [Certificate]";
        SSQL += ",isnull(FamilyDetails,'') as [FamilyDetails]";
        SSQL += ",isnull(Address1,'') as [Address1]";
        SSQL += ",isnull(Address2,'') as [Address2]";
        SSQL += ",isnull(RecuritmentThro,'') as [RecuritmentThro]";
        SSQL += ",isnull(IDMark1,'') as [IDMark1]";
        SSQL += ",isnull(IDMark2,'') as [IDMark2]";
        SSQL += ",isnull(BloodGroup,'') as [BloodGroup]";
        SSQL += ",isnull(Handicapped,'') as [Handicapped]";
        SSQL += ",isnull(Height,'') as [Height]";
        SSQL += ",isnull(Weight,'') as [Weight]";
        SSQL += ",isnull(BankName,'') as [BankName]";
        SSQL += ",isnull(BranchCode,'') as [BranchCode]";
        SSQL += ",isnull(AccountNo,'') as [AccountNo]";
        SSQL += ",isnull(BusNo,'') as [BusNo]";
        SSQL += ",isnull(Refrence,'') as [Refrence]";
        SSQL += ",isnull(Permanent_Dist,'') as [Permanent_Dist]";
        SSQL += ",isnull(Present_Dist,'') as [Present_Dist]";
        SSQL += ",isnull(WeekOff,'') as [WeekOff]";
        SSQL += ",isnull(parentsMobile,'') as [parentsMobile]";

        SSQL += ",isnull(EmployeeMobile,'') as [EmployeeMobile]";
        SSQL += ",isnull(Wages,'') as [Wages]";
        SSQL += ",isnull(Adhar_No,'') as [Adhar_No]";

        if (status == "Approval")
        {
            SSQL = SSQL + " from Employee_Mst";
        }
        else
        {
            SSQL = SSQL + " from Employee_Mst_New_Emp";
        }


        //SSQL += " from employee_mst";
        SSQL += " WHERE  EmpNo = '" + iEmpDet + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";

        if (Division != "-Select-")
        {
            SSQL += " and Division='" + Division + "'";
        }

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);


        if (mDataSet.Rows.Count == 0)
        {
            SSQL = "";
            SSQL = "select isnull(CompCode,'') as [CompCode]";
            SSQL += ",isnull(LocCode,'') as [LocCode]";
            SSQL += ",isnull(EmpPrefix,'') as [EmpPrefix]";
            SSQL += ",isnull(EmpNo,'') as [EmpNo]";
            SSQL += ",isnull(ExistingCode,'') as [ExistingCode]";
            SSQL += ",isnull(MachineID,'') as [MachineID]";
            SSQL += ",isnull(FirstName,'') as [FirstName]";
            SSQL += ",isnull(LastName,'') as [LastName]";
            SSQL += ",isnull(MiddleInitial,'') as [MiddleInitial]";
            SSQL += ",isnull(ShiftType,'') as [ShiftType]";
            SSQL += ",isnull(Gender,'') as [Gender]";
            SSQL += ",isnull(BirthDate,'') as [BirthDate]";
            SSQL += ",isnull(Age,'') as [Age]";
            SSQL += ",isnull(DOJ,'') as [DOJ]";
            SSQL += ",isnull(MaritalStatus,'') as [MaritalStatus]";
            SSQL += ",isnull(CatName,'') as [CatName]";
            SSQL += ",isnull(SubCatName,'') as [SubCatName]";
            SSQL += ",isnull(TypeName,'') as [TypeName]";
            SSQL += ",isnull(EmpStatus,'') as [EmpStatus]";
            SSQL += ",isnull(IsActive,'') as [IsActive]";
            SSQL += ",isnull(DeptName,'') as [DeptName]";
            SSQL += ",isnull(Designation,'') as [Designation]";
            SSQL += ",isnull(PayPeriod_Desc,'') as [PayPeriod_Desc]";
            SSQL += ",BaseSalary";
            SSQL += ",isnull(PFNo,'') as [PFNo]";
            SSQL += ",isnull(Nominee,'') as [Nominee]";
            SSQL += ",isnull(ESINo,'') as [ESINo]";
            SSQL += ",StdWrkHrs";
            SSQL += ",isnull(OTEligible,'') as [OTEligible]";
            SSQL += ",isnull(Nationality,'') as [Nationality]";
            SSQL += ",isnull(Qualification,'') as [Qualification]";
            SSQL += ",isnull(Certificate,'') as [Certificate]";
            SSQL += ",isnull(FamilyDetails,'') as [FamilyDetails]";
            SSQL += ",isnull(Address1,'') as [Address1]";
            SSQL += ",isnull(Address2,'') as [Address2]";
            SSQL += ",isnull(RecuritmentThro,'') as [RecuritmentThro]";
            SSQL += ",isnull(IDMark1,'') as [IDMark1]";
            SSQL += ",isnull(IDMark2,'') as [IDMark2]";
            SSQL += ",isnull(BloodGroup,'') as [BloodGroup]";
            SSQL += ",isnull(Handicapped,'') as [Handicapped]";
            SSQL += ",isnull(Height,'') as [Height]";
            SSQL += ",isnull(Weight,'') as [Weight]";
            SSQL += ",isnull(BankName,'') as [BankName]";
            SSQL += ",isnull(BranchCode,'') as [BranchCode]";
            SSQL += ",isnull(AccountNo,'') as [AccountNo]";
            SSQL += ",isnull(BusNo,'') as [BusNo]";
            SSQL += ",isnull(Refrence,'') as [Refrence]";
            SSQL += ",isnull(Permanent_Dist,'') as [Permanent_Dist]";
            SSQL += ",isnull(Present_Dist,'') as [Present_Dist]";
            SSQL += ",isnull(WeekOff,'') as [WeekOff]";
            SSQL += ",isnull(parentsMobile,'') as [parentsMobile]";

            SSQL += ",isnull(EmployeeMobile,'') as [EmployeeMobile]";
            SSQL += ",isnull(Wages,'') as [Wages]";
            SSQL += ",isnull(Adhar_No,'') as [Adhar_No]";




            SSQL += " from Employee_Mst_New_Emp";
            SSQL += " WHERE  EmpNo = '" + iEmpDet + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";

            if (Division != "-Select-")
            {
                SSQL += " and Division='" + Division + "'";
            }

            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        }

        if (mDataSet.Rows.Count > 0)
        {

            string mUnitName = "";
            string strPhotoPath = "";

            if (SessionLcode == "UNIT I")
            {
                mUnitName = "Unit1-Photos";
            }
            else if (SessionLcode == "UNIT II")
            {
                mUnitName = "Unit2-Photos";
            }
            else
            {
                mUnitName = "mUnitName";
            }


            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {
                DataTable DT_Photo = new DataTable();
                string SS = "Select *from Photo_Path_Det";
                DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

                string PhotoDet = "";
                string UNIT_Folder = "";
                if (DT_Photo.Rows.Count != 0)
                {
                    PhotoDet = DT_Photo.Rows[0]["Photo_Path"].ToString();
                }
                //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/"; }
                //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/"; }
                //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/"; }
                //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "//SABA-PC/To Share/Narmatha/Emp_Scan/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }

                if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
                if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet + "/UNIT_II/"; }
                if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet + "/UNIT_III/"; }
                if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }



                byte[] imageByteData_Photo = new byte[0];
                byte[] imageByteData_ID_1 = new byte[0];
                byte[] imageByteData_ID_2 = new byte[0];
                byte[] imageByteData_ID_3 = new byte[0];
                byte[] imageByteData_ID_4 = new byte[0];
                byte[] imageByteData_ID_5 = new byte[0];
                byte[] imageByteData_ID_6 = new byte[0];
                byte[] imageByteData_ID_7 = new byte[0];
                byte[] imageByteData_ID_8 = new byte[0];
                byte[] imageByteData_ID_9 = new byte[0];

                string mid = mDataSet.Rows[iRow]["MachineID"].ToString();
                // string path = "D:/Photo/" + mid + ".jpg";

                //Employee_Phto
                string path_1 = UNIT_Folder + "/Photos/" + mid + ".jpg";
                string Photo_Path = "";
                Photo_Path = path_1;
                if (!System.IO.File.Exists(Photo_Path))
                {
                    //imageByteData_Photo = System.IO.File.ReadAllBytes(path);
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_Photo = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 1
                //path_1 = "~/" + UNIT_Folder + "/ID_Proof/A_Copy/A_" + mid + ".jpg";
                path_1 = UNIT_Folder + "/ID_Proof/A_Copy/A_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                //Photo_Path = HttpContext.Current.Request.MapPath(path_1);
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_1 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 2
                //path_1 = "~/" + UNIT_Folder + "/ID_Proof/P_Copy/P_" + mid + ".jpg";
                path_1 = UNIT_Folder + "/ID_Proof/P_Copy/P_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_2 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 3
                path_1 = UNIT_Folder + "/ID_Proof/V_Copy/V_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_3 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 4
                path_1 = UNIT_Folder + "/ID_Proof/R_Copy/R_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_4 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 5
                path_1 = UNIT_Folder + "/ID_Proof/DL_Copy/DL_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_5 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 6
                path_1 = UNIT_Folder + "/ID_Proof/SC_Copy/SC_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_6 = System.IO.File.ReadAllBytes(Photo_Path);


                //ID Proof 7
                path_1 = UNIT_Folder + "/ID_Proof/B_Copy/B_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_7 = System.IO.File.ReadAllBytes(Photo_Path);


                //ID Proof 8
                path_1 = UNIT_Folder + "/ID_Proof/pass_Copy/Pass_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_8 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 9
                path_1 = UNIT_Folder + "/ID_Proof/O_Copy/O_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_9 = System.IO.File.ReadAllBytes(Photo_Path);


                string datee = mDataSet.Rows[iRow]["DOJ"].ToString();
                string[] date1 = datee.Split(' ');


                string birthh = mDataSet.Rows[iRow]["BirthDate"].ToString();
                string[] birthdatee = birthh.Split(' ');
                SSQL = "Select * from Company_Mst";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();



                AutoDataTable.Rows[iRow]["CompCode"] = dt.Rows[0]["CompName"]; ;
                AutoDataTable.Rows[iRow]["LocCode"] = SessionLcode.ToString();
                AutoDataTable.Rows[iRow]["EmpPrefix"] = mDataSet.Rows[iRow]["EmpPrefix"].ToString();
                AutoDataTable.Rows[iRow]["EmpNo"] = mDataSet.Rows[iRow]["EmpNo"];
                AutoDataTable.Rows[iRow]["ExistingCode"] = mDataSet.Rows[iRow]["ExistingCode"];
                AutoDataTable.Rows[iRow]["MachineID"] = mDataSet.Rows[iRow]["MachineID"];
                AutoDataTable.Rows[iRow]["ExistingCode"] = mDataSet.Rows[iRow]["ExistingCode"];
                AutoDataTable.Rows[iRow]["MachineID"] = mDataSet.Rows[iRow]["MachineID"];
                AutoDataTable.Rows[iRow]["FirstName"] = mDataSet.Rows[iRow]["FirstName"];
                AutoDataTable.Rows[iRow]["LastName"] = mDataSet.Rows[iRow]["LastName"];
                AutoDataTable.Rows[iRow]["MiddleInitial"] = mDataSet.Rows[iRow]["MiddleInitial"];
                AutoDataTable.Rows[iRow]["ShiftType"] = mDataSet.Rows[iRow]["ShiftType"];


                AutoDataTable.Rows[iRow]["Gender"] = mDataSet.Rows[iRow]["Gender"].ToString();
                AutoDataTable.Rows[iRow]["BirthDate"] = birthdatee[0];
                AutoDataTable.Rows[iRow]["Age"] = mDataSet.Rows[iRow]["Age"];
                AutoDataTable.Rows[iRow]["DOJ"] = date1[0];
                AutoDataTable.Rows[iRow]["MaritalStatus"] = mDataSet.Rows[iRow]["MaritalStatus"];
                AutoDataTable.Rows[iRow]["CatName"] = mDataSet.Rows[iRow]["CatName"];
                AutoDataTable.Rows[iRow]["SubCatName"] = mDataSet.Rows[iRow]["SubCatName"];
                AutoDataTable.Rows[iRow]["TypeName"] = mDataSet.Rows[iRow]["TypeName"];
                AutoDataTable.Rows[iRow]["EmpStatus"] = mDataSet.Rows[iRow]["EmpStatus"];
                AutoDataTable.Rows[iRow]["IsActive"] = mDataSet.Rows[iRow]["IsActive"];



                AutoDataTable.Rows[iRow]["DeptName"] = mDataSet.Rows[iRow]["DeptName"].ToString();
                AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[iRow]["Designation"];
                AutoDataTable.Rows[iRow]["PayPeriod_Desc"] = mDataSet.Rows[iRow]["PayPeriod_Desc"];
                AutoDataTable.Rows[iRow]["BaseSalary"] = mDataSet.Rows[iRow]["BaseSalary"];
                AutoDataTable.Rows[iRow]["PFNo"] = mDataSet.Rows[iRow]["PFNo"];
                AutoDataTable.Rows[iRow]["Nominee"] = mDataSet.Rows[iRow]["Nominee"];
                AutoDataTable.Rows[iRow]["ESINo"] = mDataSet.Rows[iRow]["ESINo"];
                AutoDataTable.Rows[iRow]["StdWrkHrs"] = mDataSet.Rows[iRow]["StdWrkHrs"];
                AutoDataTable.Rows[iRow]["OTEligible"] = mDataSet.Rows[iRow]["OTEligible"];
                AutoDataTable.Rows[iRow]["Nationality"] = mDataSet.Rows[iRow]["Nationality"];

                AutoDataTable.Rows[iRow]["Qualification"] = mDataSet.Rows[iRow]["Qualification"].ToString();
                AutoDataTable.Rows[iRow]["Certificate"] = mDataSet.Rows[iRow]["Certificate"];
                AutoDataTable.Rows[iRow]["FamilyDetails"] = mDataSet.Rows[iRow]["FamilyDetails"];
                AutoDataTable.Rows[iRow]["Address1"] = mDataSet.Rows[iRow]["Address1"];
                AutoDataTable.Rows[iRow]["Address2"] = mDataSet.Rows[iRow]["Address2"];
                AutoDataTable.Rows[iRow]["RecuritmentThro"] = mDataSet.Rows[iRow]["RecuritmentThro"];
                AutoDataTable.Rows[iRow]["IDMark1"] = mDataSet.Rows[iRow]["IDMark1"];
                AutoDataTable.Rows[iRow]["IDMark2"] = mDataSet.Rows[iRow]["IDMark2"];
                AutoDataTable.Rows[iRow]["BloodGroup"] = mDataSet.Rows[iRow]["BloodGroup"];
                AutoDataTable.Rows[iRow]["Handicapped"] = mDataSet.Rows[iRow]["Handicapped"];

                AutoDataTable.Rows[iRow]["Height"] = mDataSet.Rows[iRow]["Height"].ToString();
                AutoDataTable.Rows[iRow]["Weight"] = mDataSet.Rows[iRow]["Weight"];
                AutoDataTable.Rows[iRow]["BankName"] = mDataSet.Rows[iRow]["BankName"];
                AutoDataTable.Rows[iRow]["BranchCode"] = mDataSet.Rows[iRow]["BranchCode"];
                AutoDataTable.Rows[iRow]["AccountNo"] = mDataSet.Rows[iRow]["AccountNo"];




                AutoDataTable.Rows[iRow]["BusNo"] = mDataSet.Rows[iRow]["BusNo"].ToString();
                AutoDataTable.Rows[iRow]["Refrence"] = mDataSet.Rows[iRow]["Refrence"];
                AutoDataTable.Rows[iRow]["Permanent_Dist"] = mDataSet.Rows[iRow]["Permanent_Dist"];
                AutoDataTable.Rows[iRow]["Present_Dist"] = mDataSet.Rows[iRow]["Present_Dist"];
                AutoDataTable.Rows[iRow]["WeekOff"] = mDataSet.Rows[iRow]["WeekOff"];

                AutoDataTable.Rows[iRow]["parentsMobile"] = mDataSet.Rows[iRow]["parentsMobile"].ToString();
                AutoDataTable.Rows[iRow]["EmployeeMobile"] = mDataSet.Rows[iRow]["EmployeeMobile"];
                AutoDataTable.Rows[iRow]["Wages"] = mDataSet.Rows[iRow]["Wages"];
                AutoDataTable.Rows[iRow]["Adhar_No"] = mDataSet.Rows[iRow]["Adhar_No"];

                //byte[] imgbyte = new byte[fs.Length + 1];
                //imgbyte = br.ReadBytes(Convert.ToInt32((fs.Length)));

                AutoDataTable.Rows[iRow]["img"] = imageByteData_Photo;
                AutoDataTable.Rows[iRow]["Add_Proof"] = imageByteData_ID_1;
                AutoDataTable.Rows[iRow]["ID_Proof"] = imageByteData_ID_2;
                AutoDataTable.Rows[iRow]["ID_Proof_3"] = imageByteData_ID_3;
                AutoDataTable.Rows[iRow]["ID_Proof_4"] = imageByteData_ID_4;
                AutoDataTable.Rows[iRow]["ID_Proof_5"] = imageByteData_ID_5;
                AutoDataTable.Rows[iRow]["ID_Proof_6"] = imageByteData_ID_6;

                AutoDataTable.Rows[iRow]["ID_Proof_7"] = imageByteData_ID_7;
                AutoDataTable.Rows[iRow]["ID_Proof_8"] = imageByteData_ID_8;
                AutoDataTable.Rows[iRow]["ID_Proof_9"] = imageByteData_ID_9;





                //br.Close();
                //fs.Close();

                //br = null;
                //fs = null;

            }

        }

    }

    protected void Page_Unload(object sender, EventArgs e)
    {

    }
}
