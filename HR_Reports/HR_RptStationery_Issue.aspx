﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HR_RptStationery_Issue.aspx.cs" Inherits="HR_Reports_HR_RptStationery_Issue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- begin #content -->
    <div id="content" class="content-wrapper">
        <section class="content">
            <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                <ContentTemplate>
                    <!-- begin breadcrumb -->
                    <ol class="breadcrumb pull-right">
                        <li><a href="javascript:;">Report</a></li>
                        <li class="active">Stationery Issued Details</li>
                    </ol>
                    <!-- end breadcrumb -->
                    <!-- begin page-header -->
                    <h1 class="page-header">Stationery Issued Details</h1>
                    <!-- end page-header -->

                    <!-- begin row -->
                    <div class="row">
                        <!-- begin col-12 -->
                        <div class="col-md-12">
                            <!-- begin panel -->
                            <div class="panel panel-inverse">

                                <div class="panel-body">
                                    <div class="form-group">

                                        <!-- begin row -->
                                        <div class="row">
                                            <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Issued To Machine ID</label>
                                                    <asp:DropDownList runat="server" ID="ddlMachineID" class="form-control select2" Style="width: 100%;" AutoPostBack="true" OnSelectedIndexChanged="ddlMachineID_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Token No</label>
                                                    <asp:DropDownList runat="server" ID="ddlTkno" class="form-control select2" Style="width: 100%;" AutoPostBack="true" OnSelectedIndexChanged="ddlTkno_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Emp Name</label>
                                                    <asp:Label runat="server" Enabled="false" ID="txtEmpName" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>From Date</label>
                                                    <asp:TextBox ID="txtFromdate" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>To Date</label>
                                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                        <!-- begin row -->
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Issued By</label>
                                                    <asp:TextBox ID="txtIssued_By" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <br />
                                                    <asp:Button runat="server" ID="btnReport" Text="Report" class="btn btn-success" OnClick="btnReport_Click" />
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <div class="col-md-4"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- end panel -->
                        </div>
                        <!-- end col-12 -->
                    </div>
                    <!-- end row -->
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnReport" />
                </Triggers>
            </asp:UpdatePanel>
        </section>
    </div>
    <!-- end #content -->

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });

                    $('.select2').select2();
                }
            });
        };
    </script>
</asp:Content>

