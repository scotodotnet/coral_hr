﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class HR_Reports_HR_improper : System.Web.UI.Page
{
    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;

    BALDataAccess objdata = new BALDataAccess();
    //string SessionAdmin = "admin";
    //string cc = "Naveen Cotton Mill (P) Ltd";
    //string SessionCcode = "ESM";
    //string SessionLcode = "UNIT I";
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    DateTime ShiftdateStartIN;
    DateTime ShiftdateEndIN;
    DateTime InTime_Check;
    DateTime InToTime_Check;
    DateTime EmpdateIN;

    DataTable mEmployeeDS = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable DS_InTime = new DataTable();
    DataTable DS_Time = new DataTable();
    string Final_InTime;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Time_IN_Str;
    string Time_OUT_Str;
    string Date_Value_Str;
    string Shift_Start_Time;
    string Shift_End_Time;
    string Final_Shift;
    string Total_Time_get;
    string[] Time_Minus_Value_Check;
    DataSet ds = new DataSet();
    int time_Check_dbl;
    DataTable mdatatable = new DataTable();
    DataTable LocalDT = new DataTable();
    DataTable Shift_DS = new DataTable();
    TimeSpan InTime_TimeSpan;
    string From_Time_Str = "";
    string To_Time_Str = "";
    DateTime date1;
    DataTable AutoDTable = new DataTable();
    DataColumn auto = new DataColumn();
    DataTable dtIPaddress = new DataTable();
    Boolean Shift_Check_blb = false;
    DataTable DataCells = new DataTable();

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Division = "";
    string status = "";

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Improper Report";

            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            DataSet ds = new DataSet();






            DataCells.Columns.Add("CompanyName");
            DataCells.Columns.Add("LocationName");
            DataCells.Columns.Add("ShiftDate");

            DataCells.Columns.Add("SNo");
            DataCells.Columns.Add("Dept");
            DataCells.Columns.Add("Type");
            DataCells.Columns.Add("Shift");
            DataCells.Columns.Add("Category");
            DataCells.Columns.Add("SubCategory");
            DataCells.Columns.Add("EmpCode");
            DataCells.Columns.Add("ExCode");
            DataCells.Columns.Add("Name");
            DataCells.Columns.Add("TimeIN");
            DataCells.Columns.Add("TimeOUT");
            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("PrepBy");
            DataCells.Columns.Add("PrepDate");
            DataCells.Columns.Add("TotalMIN");
            DataCells.Columns.Add("GrandTOT");







            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
            status = Request.QueryString["status"].ToString();
            if (SessionUserType == "2")
            {
                NonAdminGetImproperPunch();

                ds.Tables.Add(DataCells);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/PresentAbstract.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                string SSQL = "";
                DataTable dt2 = new DataTable();

                SSQL = "";
                SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
                SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
                SSQL = SSQL + " inner join ";
                if (status == "Approval")
                {
                    SSQL = SSQL + "Employee_Mst";
                }
                else
                {
                    SSQL = SSQL + "Employee_Mst_New_Emp";
                }
                SSQL = SSQL + " EM on EM.MachineID = LD.MachineID";
                SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
                SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";

                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And EM.Division = '" + Division + "'";
                }
                if (ShiftType1 != "ALL")
                {
                    SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
                }
                if (Date != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
                }

                SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);






                report.DataDefinition.FormulaFields["New_Formula"].Text = "'" + dt2.Rows.Count.ToString() + "'";


                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {


                GetImproperPunch();


                ds.Tables.Add(DataCells);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("/crystal_HR/PresentAbstract.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                string SSQL = "";
                DataTable dt2 = new DataTable();

                SSQL = "";
                SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
                SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
                SSQL = SSQL + " inner join ";
                if (status == "Approval")
                {
                    SSQL = SSQL + " Employee_Mst";
                }
                else
                {
                    SSQL = SSQL + " Employee_Mst_New_Emp";
                }
                SSQL = SSQL + " EM on EM.MachineID = LD.MachineID";

                SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
                SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";

                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And EM.Division = '" + Division + "'";
                }
                if (ShiftType1 != "ALL")
                {
                    SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
                }
                if (Date != "")
                {
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
                }

                SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);






                report.DataDefinition.FormulaFields["New_Formula"].Text = "'" + dt2.Rows.Count.ToString() + "'";


                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
        }
    }


    public void NonAdminGetImproperPunch()
    {

        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt3 = new DataTable();
        double Count = 0;
        double Count1;

        DateTime dayy = Convert.ToDateTime(Date);



        string SSQL = "";

        DataTable dt2 = new DataTable();
        SSQL = "select MachineID,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
        SSQL = SSQL + "Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";

        if (Date != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }

        SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";
        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);



        SSQL = "";

        SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT";
        SSQL = SSQL + " from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";



        if (Date != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }


        SSQL = SSQL + " And (TypeName='Proper' or TypeName='Absent') And Shift!='No Shift' And TimeIN!=''";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') as FirstName,TypeName,TimeIN,TimeOUT";
        SSQL = SSQL + " from LogTime_Days   ";
        SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='" + SessionLcode.ToString() + "'";

        if (Date != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }


        SSQL = SSQL + " And TypeName='Improper'";

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst";
        dt3 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt2.Rows.Count != 0)
        {
            if (dt1.Rows.Count != 0)
            {
                int sno = 1;
                DataTable Emp_dt = new DataTable();
                string category;
                string subCat;
                for (int i = 0; i < dt1.Rows.Count; i++)
                {

                    SSQL = "Select * from ";
                    if (status == "Approval")
                    {
                        SSQL = SSQL + " Employee_Mst";
                    }
                    else
                    {
                        SSQL = SSQL + " Employee_Mst_New_Emp";
                    }
                    SSQL = SSQL + " where MachineID='" + dt1.Rows[i]["MachineID"].ToString() + "' ";
                    Emp_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    category = "";
                    subCat = "";
                    if (Emp_dt.Rows.Count != 0)
                    {
                        category = Emp_dt.Rows[0]["CatName"].ToString();
                        subCat = Emp_dt.Rows[0]["SubCatName"].ToString();
                    }


                    DataCells.NewRow();
                    DataCells.Rows.Add();

                    DataCells.Rows[i]["CompanyName"] = dt3.Rows[0]["CompName"].ToString();
                    DataCells.Rows[i]["LocationName"] = SessionLcode;
                    DataCells.Rows[i]["ShiftDate"] = Date;
                    DataCells.Rows[i]["SNo"] = sno;


                    DataCells.Rows[i]["Dept"] = dt1.Rows[i]["DeptName"].ToString();
                    DataCells.Rows[i]["Type"] = dt1.Rows[i]["TypeName"].ToString();
                    DataCells.Rows[i]["Shift"] = dt1.Rows[i]["Shift"].ToString();
                    DataCells.Rows[i]["Category"] = category;

                    DataCells.Rows[i]["SubCategory"] = subCat;
                    DataCells.Rows[i]["EmpCode"] = dt1.Rows[i]["MachineID"].ToString();
                    DataCells.Rows[i]["ExCode"] = dt1.Rows[i]["ExistingCode"].ToString();
                    DataCells.Rows[i]["Name"] = dt1.Rows[i]["FirstName"].ToString();

                    DataCells.Rows[i]["TimeIN"] = dt1.Rows[i]["TimeIN"].ToString();
                    DataCells.Rows[i]["TimeOUT"] = dt1.Rows[i]["TimeOUT"].ToString();
                    DataCells.Rows[i]["MachineID"] = dt1.Rows[i]["MachineID"].ToString();

                    DataCells.Rows[i]["PrepBy"] = dt2.Rows.Count;
                    DataCells.Rows[i]["PrepDate"] = dt.Rows.Count;
                    DataCells.Rows[i]["TotalMIN"] = "";
                    DataCells.Rows[i]["TotalMIN"] = "";


                    sno = sno + 1;

                }


            }
        }
    }

    public void GetImproperPunch()
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt3 = new DataTable();
        double Count = 0;
        double Count1;

        DateTime dayy = Convert.ToDateTime(Date);



        string SSQL = "";

        DataTable dt2 = new DataTable();
        SSQL = "select MachineID,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
        SSQL = SSQL + "Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";

        if (Date != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }

        SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";
        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);



        SSQL = "";

        SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT";
        SSQL = SSQL + " from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";



        if (Date != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }


        SSQL = SSQL + " And (TypeName='Proper' or TypeName='Absent') And Shift!='No Shift' And TimeIN!=''";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') as FirstName,TypeName,TimeIN,TimeOUT";
        SSQL = SSQL + " from LogTime_Days   ";
        SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='" + SessionLcode.ToString() + "'";

        if (Date != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }


        SSQL = SSQL + " And TypeName='Improper'";

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst";
        dt3 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt2.Rows.Count != 0)
        {
            if (dt1.Rows.Count != 0)
            {
                int sno = 1;
                DataTable Emp_dt = new DataTable();
                string category;
                string subCat;
                for (int i = 0; i < dt1.Rows.Count; i++)
                {

                    SSQL = "Select * from ";
                    if (status == "Approval")
                    {
                        SSQL = SSQL + " Employee_Mst";
                    }
                    else
                    {
                        SSQL = SSQL + " Employee_Mst_New_Emp";
                    }
                    SSQL = SSQL + " where MachineID='" + dt1.Rows[i]["MachineID"].ToString() + "' ";
                    Emp_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    category = "";
                    subCat = "";
                    if (Emp_dt.Rows.Count != 0)
                    {
                        category = Emp_dt.Rows[0]["CatName"].ToString();
                        subCat = Emp_dt.Rows[0]["SubCatName"].ToString();
                    }


                    DataCells.NewRow();
                    DataCells.Rows.Add();

                    DataCells.Rows[i]["CompanyName"] = dt3.Rows[0]["CompName"].ToString();
                    DataCells.Rows[i]["LocationName"] = SessionLcode;
                    DataCells.Rows[i]["ShiftDate"] = Date;
                    DataCells.Rows[i]["SNo"] = sno;


                    DataCells.Rows[i]["Dept"] = dt1.Rows[i]["DeptName"].ToString();
                    DataCells.Rows[i]["Type"] = dt1.Rows[i]["TypeName"].ToString();
                    DataCells.Rows[i]["Shift"] = dt1.Rows[i]["Shift"].ToString();
                    DataCells.Rows[i]["Category"] = category;

                    DataCells.Rows[i]["SubCategory"] = subCat;
                    DataCells.Rows[i]["EmpCode"] = dt1.Rows[i]["MachineID"].ToString();
                    DataCells.Rows[i]["ExCode"] = dt1.Rows[i]["ExistingCode"].ToString();
                    DataCells.Rows[i]["Name"] = dt1.Rows[i]["FirstName"].ToString();

                    DataCells.Rows[i]["TimeIN"] = dt1.Rows[i]["TimeIN"].ToString();
                    DataCells.Rows[i]["TimeOUT"] = dt1.Rows[i]["TimeOUT"].ToString();
                    DataCells.Rows[i]["MachineID"] = dt1.Rows[i]["MachineID"].ToString();

                    DataCells.Rows[i]["PrepBy"] = dt2.Rows.Count;
                    DataCells.Rows[i]["PrepDate"] = dt.Rows.Count;
                    DataCells.Rows[i]["TotalMIN"] = "";
                    DataCells.Rows[i]["TotalMIN"] = "";


                    sno = sno + 1;

                }


            }
        }


    }

    protected void Page_Unload(object sender, EventArgs e)
    {

    }
}
