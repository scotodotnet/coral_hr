﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class HR_Reports_HR_EmployeeHistory : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable sDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = ""; string status = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Full Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();

            Empcode = Request.QueryString["Empcode"].ToString();
            Division = Request.QueryString["Division"].ToString();
            status = Request.QueryString["status"].ToString();


            GetEmpFullProfileTable();

            ds.Tables.Add(AutoDataTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("/crystal_HR/single_Emp_History.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }
    public void GetEmpFullProfileTable()
    {

        AutoDataTable.Columns.Add("CompCode");
        AutoDataTable.Columns.Add("LocCode");

        AutoDataTable.Columns.Add("EmpNo");
        AutoDataTable.Columns.Add("ExistingCode");
        AutoDataTable.Columns.Add("MachineID");
        AutoDataTable.Columns.Add("FirstName");
        AutoDataTable.Columns.Add("LastName");
        AutoDataTable.Columns.Add("MiddleInitial");
        AutoDataTable.Columns.Add("DOB");
        AutoDataTable.Columns.Add("Age");
        AutoDataTable.Columns.Add("DOJ");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Designation");
        AutoDataTable.Columns.Add("Qualification");

        AutoDataTable.Columns.Add("Wages");
        AutoDataTable.Columns.Add("Reason");
        AutoDataTable.Columns.Add("TransDate");
        AutoDataTable.Columns.Add("OLD_Salary");
        AutoDataTable.Columns.Add("BaseSalary");
        AutoDataTable.Columns.Add("Rem1");
        AutoDataTable.Columns.Add("Rem2");

        string s = Empcode;
        string[] delimiters = new string[] { "->" };
        string[] items = s.Split(delimiters, StringSplitOptions.None);
        string ss = items[0];
        iEmpDet = items[1];



        //string Emp_No = null;
        //string Add_Where_Cont = null;


        //if (iEmpDet[0] != "ALL" & iEmpDet[0] == "")
        //{
        //    Emp_No = iEmpDet;
        //    iEmpDet = Emp_No.Split(delimiters, StringSplitOptions.None);

        //    Add_Where_Cont = "";
        //    Add_Where_Cont += " Where CompCode='" + CompanyCode + "'";
        //    Add_Where_Cont += " and LocCode='" + LocationCode + "' and EmpNo='" + iEmpDet[2] + "'";
        //}
        //else
        //{
        //    Add_Where_Cont = "";
        //    Add_Where_Cont += " Where CompCode='" + CompanyCode + "'";
        //    Add_Where_Cont += " LocCode='" + LocationCode + "'";
        //}


        SSQL = " Select isnull(Emp.CompCode,'') as [CompCode],isnull(Emp.LocCode,'') as [LocCode],isnull(Emp.EmpNo,'') as [EmpNo],";
        SSQL = SSQL + " isnull(Emp.MachineID, '') as [MachineID],isnull(Emp.FirstName, '') as [FirstName],";
        SSQL = SSQL + " isnull(Emp.LastName, '') as [LastName],isnull(Emp.MiddleInitial, '') as [MiddleInitial],";
        SSQL = SSQL + " isnull(Emp.BirthDate, '') as [DOB],isnull(Emp.Age, '') as [Age],isnull(Emp.DOJ, '') as [DOJ],";
        SSQL = SSQL + " isnull(Emp.DeptName, '') as [DeptName],isnull(Emp.Designation, '') as [Designation],";
        SSQL = SSQL + " isnull(Emp.Qualification, '') as [Qualification],isnull(Emp.Wages, '') as [Wages],Emp.BaseSalary [Salary] ";
        SSQL = SSQL + " From Employee_Mst Emp  WHERE Emp.EmpNo = '" + iEmpDet + "' and Emp.CompCode = '" + SessionCcode + "' and ";
        SSQL = SSQL + " Emp.LocCode = '" + SessionLcode + "'";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = " Select * From (";
        SSQL = SSQL + " Select 'Salary Increment'[Reason],SALDET.Salary_Update_Date[TransDate],SALDET.OLD_Salary,Emp.BaseSalary,";
        SSQL = SSQL + " ''[Rem1],''[Rem2] from Employee_Mst Emp";
        SSQL = SSQL + " Inner Join Salary_Update_Det SALDET on SALDET.MachineID = Emp.MachineID and SALDET.CompCode = Emp.CompCode";
        SSQL = SSQL + " WHERE Emp.EmpNo = '" + iEmpDet + "' and Emp.CompCode = '" + SessionCcode + "' and ";
        SSQL = SSQL + " Emp.LocCode = '" + SessionLcode + "'";

        SSQL = SSQL + " Union All ";

        SSQL = SSQL + " Select 'Memo Entry'[Reason],MEMO.Memo_Date[TransDate],'0.00'OLD_Salary,";
        SSQL = SSQL + " '0.00'BaseSalary,Memo_Action[Rem1],Memo_Issue[Rem2] From Employee_Mst Emp";
        SSQL = SSQL + " Inner Join Emp_Memo_Sub MEMO on MEMO.MachineID = Emp.MachineID and MEMO.CompCode = Emp.CompCode";
        SSQL = SSQL + " WHERE Emp.EmpNo = '" + iEmpDet + "' and Emp.CompCode = '" + SessionCcode + "' and Emp.LocCode = '" + SessionLcode + "' )A";

        sDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mDataSet.Rows.Count > 0)
        {

            string mUnitName = "";
            string strPhotoPath = "";

            if (SessionLcode == "UNIT I")
            {
                mUnitName = "Unit1-Photos";
            }
            else if (SessionLcode == "UNIT II")
            {
                mUnitName = "Unit2-Photos";
            }
            else
            {
                mUnitName = "mUnitName";
            }

            if (sDataSet.Rows.Count > 0)
            {

                for (int iRow = 0; iRow < sDataSet.Rows.Count; iRow++)
                {
                    string datee = mDataSet.Rows[0]["DOJ"].ToString();
                    string[] date1 = datee.Split(' ');


                    string birthh = mDataSet.Rows[0]["DOB"].ToString();
                    string[] birthdatee = birthh.Split(' ');

                    string TransDate = sDataSet.Rows[iRow]["TransDate"].ToString();
                    string[] TransDt = TransDate.Split(' ');

                    SSQL = "Select * from Company_Mst";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[iRow]["CompCode"] = dt.Rows[0]["CompName"]; ;
                    AutoDataTable.Rows[iRow]["LocCode"] = SessionLcode.ToString();
                    AutoDataTable.Rows[iRow]["EmpNo"] = mDataSet.Rows[0]["EmpNo"];
                    AutoDataTable.Rows[iRow]["MachineID"] = mDataSet.Rows[0]["MachineID"];
                    AutoDataTable.Rows[iRow]["FirstName"] = mDataSet.Rows[0]["FirstName"];
                    AutoDataTable.Rows[iRow]["LastName"] = mDataSet.Rows[0]["LastName"];
                    AutoDataTable.Rows[iRow]["MiddleInitial"] = mDataSet.Rows[0]["MiddleInitial"];

                    AutoDataTable.Rows[iRow]["DOB"] = birthdatee[0];
                    AutoDataTable.Rows[iRow]["Age"] = mDataSet.Rows[0]["Age"];
                    AutoDataTable.Rows[iRow]["DOJ"] = date1[0];
                    AutoDataTable.Rows[iRow]["DeptName"] = mDataSet.Rows[0]["DeptName"];
                    AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[0]["Designation"];
                    AutoDataTable.Rows[iRow]["Qualification"] = mDataSet.Rows[0]["Qualification"];
                    AutoDataTable.Rows[iRow]["Wages"] = mDataSet.Rows[0]["Wages"];

                    AutoDataTable.Rows[iRow]["TransDate"] = TransDt[0];

                    AutoDataTable.Rows[iRow]["OLD_Salary"] = sDataSet.Rows[iRow]["OLD_Salary"].ToString();
                    AutoDataTable.Rows[iRow]["BaseSalary"] = sDataSet.Rows[iRow]["BaseSalary"];

                    if (sDataSet.Rows[iRow]["Reason"].ToString() == "Salary Increment")
                    {
                        string Rem1 = "Sanctioned an Anl Inc. Basic Pay Rs." + sDataSet.Rows[iRow]["BaseSalary"] + "/-pm";
                        AutoDataTable.Rows[iRow]["Reason"] = Rem1;
                    }
                    else if (sDataSet.Rows[iRow]["Reason"].ToString() == "Memo Entry")
                    {
                        string Rem1 = "Issue MEMO For " + sDataSet.Rows[iRow]["Rem2"] + "";
                        AutoDataTable.Rows[iRow]["Reason"] = Rem1;
                    }
                    AutoDataTable.Rows[iRow]["Rem1"] = sDataSet.Rows[iRow]["Rem1"];
                    AutoDataTable.Rows[iRow]["Rem2"] = sDataSet.Rows[iRow]["Rem2"];
                }
            }
            else
            {
                string datee = mDataSet.Rows[0]["DOJ"].ToString();
                string[] date1 = datee.Split(' ');


                string birthh = mDataSet.Rows[0]["DOB"].ToString();
                string[] birthdatee = birthh.Split(' ');
                SSQL = "Select * from Company_Mst";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[0]["CompCode"] = dt.Rows[0]["CompName"]; ;
                AutoDataTable.Rows[0]["LocCode"] = SessionLcode.ToString();
                AutoDataTable.Rows[0]["EmpNo"] = mDataSet.Rows[0]["EmpNo"];
                AutoDataTable.Rows[0]["MachineID"] = mDataSet.Rows[0]["MachineID"];
                AutoDataTable.Rows[0]["FirstName"] = mDataSet.Rows[0]["FirstName"];
                AutoDataTable.Rows[0]["LastName"] = mDataSet.Rows[0]["LastName"];
                AutoDataTable.Rows[0]["MiddleInitial"] = mDataSet.Rows[0]["MiddleInitial"];

                AutoDataTable.Rows[0]["DOB"] = birthdatee[0];
                AutoDataTable.Rows[0]["Age"] = mDataSet.Rows[0]["Age"];
                AutoDataTable.Rows[0]["DOJ"] = date1[0];
                AutoDataTable.Rows[0]["DeptName"] = mDataSet.Rows[0]["DeptName"];
                AutoDataTable.Rows[0]["Designation"] = mDataSet.Rows[0]["Designation"];
                AutoDataTable.Rows[0]["Qualification"] = mDataSet.Rows[0]["Qualification"];
                AutoDataTable.Rows[0]["Wages"] = mDataSet.Rows[0]["Wages"];

                AutoDataTable.Rows[0]["OLD_Salary"] = mDataSet.Rows[0]["Salary"];
                AutoDataTable.Rows[0]["BaseSalary"] = mDataSet.Rows[0]["Salary"];
            }
        }

        if (AutoDataTable.Rows.Count < 10)
        {
            int Row_Count = AutoDataTable.Rows.Count;
            for (int i = Row_Count; i < Row_Count + 8; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["CompCode"] = AutoDataTable.Rows[0]["CompCode"]; ;
                AutoDataTable.Rows[i]["LocCode"] = SessionLcode.ToString();
                AutoDataTable.Rows[i]["EmpNo"] = AutoDataTable.Rows[0]["EmpNo"];
                AutoDataTable.Rows[i]["MachineID"] = AutoDataTable.Rows[0]["MachineID"];
                AutoDataTable.Rows[i]["FirstName"] = AutoDataTable.Rows[0]["FirstName"];
                AutoDataTable.Rows[i]["LastName"] = AutoDataTable.Rows[0]["LastName"];
                AutoDataTable.Rows[i]["MiddleInitial"] = AutoDataTable.Rows[0]["MiddleInitial"];

                AutoDataTable.Rows[i]["DOB"] = AutoDataTable.Rows[0]["DOB"];
                AutoDataTable.Rows[i]["Age"] = AutoDataTable.Rows[0]["Age"];
                AutoDataTable.Rows[i]["DOJ"] = AutoDataTable.Rows[0]["DOJ"];
                AutoDataTable.Rows[i]["DeptName"] = AutoDataTable.Rows[0]["DeptName"];
                AutoDataTable.Rows[i]["Designation"] = AutoDataTable.Rows[0]["Designation"];
                AutoDataTable.Rows[i]["Qualification"] = AutoDataTable.Rows[0]["Qualification"];
                AutoDataTable.Rows[i]["Wages"] = AutoDataTable.Rows[0]["Wages"];
                AutoDataTable.Rows[i]["TransDate"] = "";

                AutoDataTable.Rows[i]["OLD_Salary"] = AutoDataTable.Rows[0]["OLD_Salary"].ToString();
                AutoDataTable.Rows[i]["BaseSalary"] = AutoDataTable.Rows[0]["BaseSalary"];

                AutoDataTable.Rows[i]["Reason"] = "";

                AutoDataTable.Rows[i]["Rem1"] = AutoDataTable.Rows[0]["Rem1"];
                AutoDataTable.Rows[i]["Rem2"] = AutoDataTable.Rows[0]["Rem2"];
            }
        }
    }

}
