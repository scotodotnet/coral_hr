﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Collections;

public partial class HR_Reports_HR_Form25B : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    BALDataAccess objdata = new BALDataAccess();
    string cc = "CORAL MANUFACTURING INDIA WORKS PVT LTD";
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();

    string SSQL = "";
    DataTable dsEmployee = new DataTable();
    DataTable mDataSet = new DataTable();
    string[] Time_Minus_Value_Check;
    int shiftCount;
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string SessionCcode;
    string SessionLcode;
    string fromdate;
    string todate;
    string cmbWages;
    DataTable mLocalDS = new DataTable();
    DateTime dtime;
    DateTime dtime1;
    double OT_Time = 0;
    DateTime date2 = new DateTime();
    string Encry_MachineID;

    bool isPresent = false;
    string Time_IN_Str;
    string Time_Out_Str;
    int j = 0;

    string Date_Value_Str1;
    DateTime InTime_Check;
    DateTime InTime_Check1;
    DateTime InToTime_Check;
    TimeSpan InTime_TimeSpan;
    string From_Time_Str;




    string Machine_ID_Str = "";
    string OT_Week_OFF_Machine_No = null;
    string Date_Value_Str = "";
    string Total_Time_get = "";
    string Final_OT_Work_Time_1 = "00:00";
    // Decimal Month_Mid_Join_Total_Days_Count;
    DateTime NewDate1;


    string To_Time_Str = "";
    DataTable DS_Time = new DataTable();
    DataTable DS_InTime = new DataTable();
    string Final_InTime = "";
    string Final_OutTime = "";
    string Final_Shift = "";
    DataTable Shift_DS_Change = new DataTable();
    Int32 K = 0;
    bool Shift_Check_blb = false;
    string Shift_Start_Time_Change = null;
    string Shift_End_Time_Change = null;
    string Employee_Time_Change = "";
    DateTime ShiftdateStartIN_Change = default(DateTime);
    DateTime ShiftdateEndIN_Change = default(DateTime);
    DateTime EmpdateIN_Change = default(DateTime);

    string Fin_Year;
    string Months_Full_Str;
    //string Months_Full_Str;
    string Date_Col_Str;
    string Month_Int;
    string Spin_Wages;
    string Wages;
    string Spin_Machine_ID_Str;
    string[] Att_Date_Check;
    string Att_Already_Date_Check;
    string Att_Year_Check;
    string Month_Name_Change;
    string halfPresent;

    //string Att_Year_Check = "";
    string Token_No_Get;
    TimeSpan ts4 = new TimeSpan();

    Decimal Total_Days_Count;
    double Final_Count;

    double Present_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    double FullNight_Shift_Count;
    double NFH_Week_Off_Minus_Days;

    Boolean NFH_Day_Check = false;
    Boolean Check_Week_Off_DOJ = false;
    int aintI = 1;
    int aintK = 1;
    int aintCol;
    int Appsent_Count;
    string SessionAdmin;
    string SessionUserType;
    string SessionCompanyName;
    string SessionLocationName;
    int dayCount;


    string mIpAddress_IN;
    string mIpAddress_OUT;
    string year;

    string Emp_WH_Day;
    string TokenNo;

    DateTime WH_DOJ_Date_Format;
    DateTime Week_Off_Date;
    string RegNo;
    DataTable Datacells = new DataTable();
    DataTable dt = new DataTable();
    DataTable mEmployeeDS = new DataTable();
    string Location_Full_Address_Join;
    Double NFH_Final_Disp_Days;
    System.Web.UI.WebControls.DataGrid grid =
                                     new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        if (!IsPostBack)
        {
            Page.Title = "Spay Module | Report Form25B";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
            //li.Attributes.Add("class", "droplink active open");
        }
        //Wages = Request.QueryString["Wages"].ToString();
        //year = Request.QueryString["Year"].ToString();

        fromdate = Request.QueryString["FromDate"].ToString();
        todate = Request.QueryString["ToDate"].ToString();
        TokenNo = Request.QueryString["TokenNo"].ToString();

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        //SessionCompanyName = Session["CompanyName"].ToString();
        //SessionLocationName = Session["LocationName"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        string TempWages = Request.QueryString["Wages"].ToString();
        Wages = TempWages.Replace("_", "&");


        if (SessionUserType == "2")
        {
            FORM25B_IF();
        }
        else
        {
            //Between_Date_Query_Output();
            FORM25B();
        }
    }

    //FORM25B code start
    public void FORM25B()
    {
        DataTable NFH_DS = new DataTable();
        DataTable TeamCount = new DataTable();
        DataTable YarnType = new DataTable();
        DataTable ShiftDT = new DataTable();
        DataTable KG_DT = new DataTable();
        DataTable Total_DT = new DataTable();

        DateTime date1;
        date1 = Convert.ToDateTime(fromdate);
        ArrayList OT_Array_Value = new ArrayList();
        string dat = todate;
        DateTime Date2 = Convert.ToDateTime(dat);
        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        int daycount1 = (int)((Date2 - date1).TotalDays);
        int daysAdded1 = 0;


        string Grand_sum = "0";



        //Get the Location
        SSQL = "Select * from Location_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        NFH_DS = objdata.RptEmployeeMultipleDetails(SSQL);


        if (NFH_DS.Rows.Count > 0)
        {
            RegNo = NFH_DS.Rows[0]["Register_No"].ToString();
            Location_Full_Address_Join = "" + NFH_DS.Rows[0]["Add1"].ToString() + "," + NFH_DS.Rows[0]["City"].ToString() + "";
            //Address2 = NFH_DS.Rows[0]["Add2"].ToString();
        }
        else
        {
            RegNo = "";
        }

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        string name = dt.Rows[0]["CompName"].ToString();


        grid.DataSource = NFH_DS;
        grid.DataBind();
        string attachment = "attachment;filename=FORM25B.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);


        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='4'></td>");

        Response.Write("<td colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> FORM 25 </a>");
        Response.Write("</td>");

        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> Name and Address of the factory: </a>");
        Response.Write("</td>");
        Response.Write("<td align='center' colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> (Prescribed under rule 103) </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> " + name + " </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='" + Convert.ToInt16(daycount + 3) + "'>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> " + Location_Full_Address_Join + " </a>");
        Response.Write("</td>");
        Response.Write("<td align='center' colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> MUSTER ROLL </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> Registration Number :" + RegNo + " </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("</td>");
        Response.Write("</tr>");



        Response.Write("<tr>");

        Response.Write("<td colspan='4'>");
        Response.Write("</td>");

        Response.Write("<td align='center' colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\">For the Month of " + date1.ToString("MMMM") + " " + date1.Year + " </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> SNo </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> TokenNo </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Name </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Father's Name </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Date of Birth </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Dept. & Designation </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Week Off </a>");
        Response.Write("</td>");
        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> " + Convert.ToString(dayy.DayOfWeek).Substring(0, 3) + " </a>");
            Response.Write("</td>");

            daycount -= 1;
            daysAdded += 1;
        }
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> NFH </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> WH </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Days Worked </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> OT Days </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> NFH W.Days </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Total Work Days </a>");
        Response.Write("</td>");

        Response.Write("</tr>");
        Response.Write("<tr>");

        while (daycount1 >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1).ToShortDateString());
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> " + dayy.Day.ToString() + " </a>");
            Response.Write("</td>");

            daycount1 -= 1;
            daysAdded1 += 1;
        }

        Response.Write("</tr>");


        DataTable Emp_DS = new DataTable();
        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='" + Wages + "' And IsActive='Yes'";
        SSQL = SSQL + " And CONVERT(DATETIME,DOJ, 103)<=CONVERT(DATETIME,'" + Date2.ToString("dd/MM/yyyy") + "',103)";
        if (TokenNo != "")
        {
            SSQL = SSQL + " And ExistingCode='" + TokenNo + "'";
        }
        SSQL = SSQL + " Order by ExistingCode Asc";

        Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Emp_DS.Rows.Count != 0)
        {

            for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
            {
                int colIndex;
                Boolean isPresent = false;
                Double Worked_Days_Count = 0;

                string Emp_Dept_Name = Emp_DS.Rows[intRow]["DeptName"].ToString();
                string Emp_Desig_Name = Emp_DS.Rows[intRow]["Designation"].ToString();


                Response.Write("<tr align='left'>");

                Response.Write("<td>");
                Response.Write("<a>" + Convert.ToInt16(intRow + 1) + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a>" + Emp_DS.Rows[intRow]["ExistingCode"].ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a>" + Emp_DS.Rows[intRow]["FirstName"].ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a>" + Emp_DS.Rows[intRow]["LastName"].ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a>" + Convert.ToDateTime(Emp_DS.Rows[intRow]["BirthDate"].ToString()).ToString("dd/MM/yyyy") + "</a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a>" + Emp_Dept_Name + "-" + Emp_Desig_Name + "</a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a>" + Emp_DS.Rows[intRow]["WeekOff"].ToString() + "</a>");
                Response.Write("</td>");


                double Without_NFH_WH_Worked_Days_Count = 0;
                double Total_Days_Cal = 0;
                double NFH_Days_Count = 0;
                double NFH_Work_Days_Count = 0;
                double WH_Days_Count = 0;
                double WH_Work_Days_Count = 0;


                for (int intCol = 0; intCol <= daysAdded - 1; intCol++)
                {

                    //string Employee_Punch = "";
                    isPresent = false;
                    string Machine_ID_Str = "";
                    string OT_Week_OFF_Machine_No = "";
                    string Date_Value_Str = "";
                    string Date_value_str1 = "";
                    DataTable mLocalDS = new DataTable();
                    //string Time_IN_Str = "";
                    //string Time_Out_Str = "";
                    //string Total_Time_get = "";
                    Int32 j = 0;
                    //double time_Check_dbl = 0;
                    //Dim Emp_Total_Work_Time_1 As Double
                    //string Emp_Total_Work_Time_1 = "00:00";
                    isPresent = false;
                    //For i = 0 To shiftCount - 1 Step 2 'take account only IN Column(leave OUT Column)
                    //MsgBox(.Cells(intI, 2).value)
                    Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
                    OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();


                    NewDate1 = Convert.ToDateTime(fromdate);

                    dtime = NewDate1.AddDays(intCol);
                    dtime1 = dtime.AddDays(1);
                    //Date_Value_Str = String.Format(Convert.ToString(dtime), "yyyy/MM/dd");
                    //Date_Value_Str1 = String.Format(Convert.ToString(dtime1), "yyyy/MM/dd");



                    Date_Value_Str = (Convert.ToDateTime(dtime)).ToString("yyyy/MM/dd");
                    Date_value_str1 = (Convert.ToDateTime(dtime1)).ToString("yyyy/MM/dd");



                    //Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day = "";
                    SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    DS_WH = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DS_WH.Rows.Count != 0)
                    {
                        Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    }
                    else
                    {
                        Emp_WH_Day = "";
                    }

                    //TimeIN Get
                    DataTable mLocalDS1 = new DataTable();


                    //Shift Change Check Start
                    //DateTime InTime_Check = default(DateTime);
                    //DateTime InToTime_Check = default(DateTime);
                    //TimeSpan InTime_TimeSpan = default(TimeSpan);
                    //string From_Time_Str = "";
                    //string To_Time_Str = "";
                    DataTable DS_Time = new DataTable();
                    DataTable DS_InTime = new DataTable();
                    //string Final_InTime = "";
                    //string Final_OutTime = "";
                    //string Final_Shift = "";
                    DataTable Shift_DS_Change = new DataTable();
                    Int32 K = 0;
                    //bool Shift_Check_blb = false;
                    //string Shift_Start_Time_Change = null;
                    //string Shift_End_Time_Change = null;
                    //string Employee_Time_Change = "";
                    //DateTime ShiftdateStartIN_Change = default(DateTime);
                    //DateTime ShiftdateEndIN_Change = default(DateTime);
                    //DateTime EmpdateIN_Change = default(DateTime);


                    string Shift_Name_Store = "";
                    //double Calculate_Attd_Work_Time = 0;
                    //double Calculate_Attd_Work_Time_1 = 4.0;
                    bool Half_Day_Count_Check = false;
                    DataTable Log_DT = new DataTable();
                    SSQL = "Select * from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).ToString("dd/MM/yyyy") + "'";
                    SSQL = SSQL + " And MachineID='" + OT_Week_OFF_Machine_No + "'";
                    Log_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Log_DT.Rows.Count != 0)
                    {
                        Shift_Name_Store = Log_DT.Rows[0]["Shift"].ToString();
                        if (Log_DT.Rows[0]["Present"].ToString() == "1.0")
                        {
                            isPresent = true;
                            halfPresent = "0";
                        }
                        else if (Log_DT.Rows[0]["Present"].ToString() == "0.5")
                        {
                            isPresent = true;
                            halfPresent = "1";
                            Half_Day_Count_Check = true;
                        }
                    }



                    string qry_nfhcheck = "Select NFHDate from NFH_Mst where CONVERT(DATETIME,DateStr, 103)=CONVERT(DATETIME,'" + Convert.ToDateTime(Date_Value_Str).ToString("dd/MM/yyyy") + "',103)";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(qry_nfhcheck);

                    bool NFH_CHeck_Bol = false;

                    //bool NFH_Day_Check = false;
                    System.DateTime NFH_Date = default(System.DateTime);
                    System.DateTime DOJ_Date_Format = default(System.DateTime);
                    string DOJ_Date_Str = Emp_DS.Rows[intRow]["DOJ"].ToString();
                    NFH_CHeck_Bol = false;
                    if (mLocalDS.Rows.Count > 0)
                    {
                        //Date OF Joining Check Start
                        if (!string.IsNullOrEmpty(DOJ_Date_Str))
                        {
                            NFH_Date = Convert.ToDateTime(mLocalDS.Rows[0]["NFHDate"]);
                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                NFH_CHeck_Bol = true;
                            }
                            else
                            {
                                //Skip
                            }
                        }
                        else
                        {
                            NFH_CHeck_Bol = true;
                        }

                    }


                    if (isPresent == true)
                    {
                        if ((Shift_Name_Store).ToString().ToUpper() == "GENERAL")
                        {
                            Shift_Name_Store = "G";
                        }
                        else if ((Shift_Name_Store).ToString().ToUpper() == "SHIFT1")
                        {
                            Shift_Name_Store = "1";
                        }
                        else if ((Shift_Name_Store).ToString().ToUpper() == "SHIFT2")
                        {
                            Shift_Name_Store = "2";
                        }
                        else if ((Shift_Name_Store).ToString().ToUpper() == "SHIFT3")
                        {
                            Shift_Name_Store = "3";
                        }
                        else
                        {
                            Shift_Name_Store = "N";
                        }
                    }

                    if (NFH_CHeck_Bol == true)
                    {
                        NFH_Days_Count = NFH_Days_Count + 1;
                        //.Cells(intI, intK).value = "NH"
                        if (isPresent == true)
                        {
                            if (halfPresent == "0")
                            {
                                NFH_Work_Days_Count = NFH_Work_Days_Count + 1;

                                Response.Write("<td>");
                                Response.Write("<span style=color:green><a>NH / " + Shift_Name_Store.ToString() + "</a></span>");
                                Response.Write("</td>");


                                //_with1.Cells(intI, intK).value = "NH / " + Shift_Name_Store;
                                //_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
                            }
                            else if (Half_Day_Count_Check == true)
                            {
                                NFH_Work_Days_Count = NFH_Work_Days_Count + 0.5;

                                Response.Write("<td>");
                                Response.Write("<span style=color:green><a>NH / " + Shift_Name_Store.ToString() + "</a></span>");
                                Response.Write("</td>");


                                //_with1.Cells(intI, intK).value = "NH / " + Shift_Name_Store + "/H";
                                //_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
                            }
                        }
                        else
                        {

                            Response.Write("<td>");
                            Response.Write("<span style=color:red><a>" + "NH" + "</a></span>");
                            Response.Write("</td>");

                            //_with1.Cells(intI, intK).value = "NH";
                            //_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                        }
                        //_with1.Cells(intI, intK).Font.Size = 10;
                        //_with1.Cells(intI, intK).Font.Bold = true;
                        //_with1.cells(intI, intK).HorizontalAlignment = -4108;
                        //_with1.cells(intI, intK).VerticalAlignment = -4108;

                    }
                    else
                    {


                        string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();


                        if ((Emp_WH_Day).ToString().ToUpper() == (Attn_Date_Day).ToString().ToUpper())
                        {
                            WH_Days_Count = WH_Days_Count + 1;
                            //.Cells(intI, intK).value = "WH"
                            if (isPresent == true)
                            {
                                if (halfPresent == "0")
                                {
                                    WH_Work_Days_Count = WH_Work_Days_Count + 1;

                                    Response.Write("<td>");
                                    Response.Write("<span style=color:green><a>WH / " + Shift_Name_Store.ToString() + "</a></span>");
                                    Response.Write("</td>");

                                    //_with1.Cells(intI, intK).value = "WH / " + Shift_Name_Store;
                                    //_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
                                }
                                else if (Half_Day_Count_Check == true)
                                {
                                    WH_Work_Days_Count = WH_Work_Days_Count + 0.5;

                                    Response.Write("<td>");
                                    Response.Write("<span style=color:green><a>WH / " + Shift_Name_Store.ToString() + "</a></span>");
                                    Response.Write("</td>");

                                    //_with1.Cells(intI, intK).value = "WH / " + Shift_Name_Store + "/H";
                                    //_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
                                }
                            }
                            else
                            {

                                Response.Write("<td>");
                                Response.Write("<span style=color:red><a>" + "WH" + "</a></span>");
                                Response.Write("</td>");


                                //_with1.Cells(intI, intK).value = "WH";
                                //_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                            }
                            //_with1.Cells(intI, intK).Font.Size = 10;
                            //_with1.Cells(intI, intK).Font.Bold = true;
                            //_with1.cells(intI, intK).HorizontalAlignment = -4108;
                            //_with1.cells(intI, intK).VerticalAlignment = -4108;
                        }
                        else
                        {
                            if (isPresent == true)
                            {
                                if (halfPresent == "0")
                                {
                                    Without_NFH_WH_Worked_Days_Count = Without_NFH_WH_Worked_Days_Count + 1;
                                }
                                else if (Half_Day_Count_Check == true)
                                {
                                    Without_NFH_WH_Worked_Days_Count = Without_NFH_WH_Worked_Days_Count + 0.5;
                                }
                                if (halfPresent == "0")
                                {

                                    Response.Write("<td>");
                                    Response.Write("<a>" + Shift_Name_Store.ToString() + "</a>");
                                    Response.Write("</td>");

                                    //_with1.Cells(intI, intK).value = Shift_Name_Store;
                                    //.Cells(intI, intK).Font.Name = "Webdings"
                                    //_with1.Cells(intI, intK).Font.Bold = true;
                                    //_with1.Cells(intI, intK).Font.Size = 10;
                                    //ElseIf halfPresent = "1" Then
                                }
                                else if (Half_Day_Count_Check == true)
                                {

                                    Response.Write("<td>");
                                    Response.Write("<a>" + Shift_Name_Store.ToString() + "/H</a>");
                                    Response.Write("</td>");


                                    //_with1.Cells(intI, intK).value = Shift_Name_Store + "/H";
                                    ////.Cells(intI, intK).Font.Name = "Webdings"
                                    //_with1.Cells(intI, intK).Font.Bold = true;
                                    //_with1.Cells(intI, intK).Font.Size = 10;
                                }
                                //_with1.Cells(intI, intK).Font.Bold = true;
                                //_with1.Cells(intI, intK).Font.Size = 10;
                                //_with1.cells(intI, intK).HorizontalAlignment = -4108;
                                //_with1.cells(intI, intK).VerticalAlignment = -4108;
                            }
                            else
                            {
                                //Check Leave Master
                                DataTable Leave_DS = new DataTable();
                                string Leave_Short_Name = "A";
                                SSQL = "Select * from Leave_Register_Mst where Machine_No='" + OT_Week_OFF_Machine_No + "'";
                                SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And From_Date_Dt >='" + Date_Value_Str + "'";
                                SSQL = SSQL + " And To_Date_Dt <='" + Date_Value_Str + "'";
                                Leave_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (Leave_DS.Rows.Count != 0)
                                {
                                    //Get Leave Short Name
                                    SSQL = "Select * from LeaveType_Mst Where LeaveType='" + Leave_DS.Rows[0]["LeaveType"].ToString() + "'";
                                    Leave_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (Leave_DS.Rows.Count != 0)
                                        Leave_Short_Name = Leave_DS.Rows[0]["Short_Name"].ToString();

                                    Response.Write("<td>");
                                    Response.Write("<a>" + Leave_Short_Name.ToString() + "/H</a>");
                                    Response.Write("</td>");

                                    //_with1.Cells(intI, intK).value = Leave_Short_Name;
                                }
                                else
                                {

                                    Response.Write("<td>");
                                    Response.Write("<span style=color:red><a>" + "A" + "</a></span>");
                                    Response.Write("</td>");


                                    //_with1.Cells(intI, intK).value = "A";
                                }

                                //.Cells(intI, intK).value = "A"
                                //_with1.Cells(intI, intK).Font.Bold = true;
                                ////.cells(intI, intK).Font.Color = RGB(255, 0, 0)
                                //_with1.cells(intI, intK).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                                //_with1.Cells(intI, intK).Font.Size = 10;
                                //_with1.cells(intI, intK).HorizontalAlignment = -4108;
                                //_with1.cells(intI, intK).VerticalAlignment = -4108;
                            }
                        }
                    }




                }


                //double Get_Absent_Days = 0;
                double Get_Total_Work_Days = 0;
                //Get_Absent_Days = NFH_Days_Count + WH_Days_Count + Without_NFH_WH_Worked_Days_Count
                //Get_Absent_Days = Total_Days_Cal - Get_Absent_Days
                Get_Total_Work_Days = Without_NFH_WH_Worked_Days_Count + NFH_Work_Days_Count + WH_Work_Days_Count;

                //Total Days Added

                Response.Write("<td>");
                Response.Write("<a>" + NFH_Days_Count.ToString() + "</a>");
                Response.Write("</td>");


                //_with1.Cells(intI, intK).value = NFH_Days_Count;
                //_with1.Cells(intI, intK).Font.Bold = true;
                //_with1.Cells(intI, intK).Font.Size = 10;
                //.cells(intI, intK).HorizontalAlignment = -4108 : .cells(intI, intK).VerticalAlignment = -4108



                Response.Write("<td>");
                Response.Write("<a>" + WH_Days_Count.ToString() + "</a>");
                Response.Write("</td>");


                //_with1.Cells(intI, intK + 1).value = WH_Days_Count;
                //_with1.Cells(intI, intK + 1).Font.Bold = true;
                //_with1.Cells(intI, intK + 1).Font.Size = 10;


                Response.Write("<td>");
                Response.Write("<a>" + Without_NFH_WH_Worked_Days_Count.ToString() + "</a>");
                Response.Write("</td>");


                //_with1.Cells(intI, intK + 2).value = Without_NFH_WH_Worked_Days_Count;
                //_with1.Cells(intI, intK + 2).Font.Bold = true;
                //_with1.Cells(intI, intK + 2).Font.Size = 10;

                Response.Write("<td>");
                Response.Write("<a>" + WH_Work_Days_Count.ToString() + "</a>");
                Response.Write("</td>");


                //_with1.Cells(intI, intK + 3).value = WH_Work_Days_Count;
                //_with1.Cells(intI, intK + 3).Font.Bold = true;
                //_with1.Cells(intI, intK + 3).Font.Size = 10;

                Response.Write("<td>");
                Response.Write("<a>" + NFH_Work_Days_Count.ToString() + "</a>");
                Response.Write("</td>");


                //_with1.Cells(intI, intK + 4).value = NFH_Work_Days_Count;
                //_with1.Cells(intI, intK + 4).Font.Bold = true;
                //_with1.Cells(intI, intK + 4).Font.Size = 10;

                Response.Write("<td>");
                Response.Write("<a>" + Get_Total_Work_Days.ToString() + "</a>");
                Response.Write("</td>");



                //_with1.Cells(intI, intK + 5).value = Get_Total_Work_Days;
                //_with1.Cells(intI, intK + 5).Font.Bold = true;
                //_with1.Cells(intI, intK + 5).Font.Size = 10;


                //intI += 1;

                Without_NFH_WH_Worked_Days_Count = 0;
                NFH_Days_Count = 0;
                NFH_Work_Days_Count = 0;
                WH_Days_Count = 0;
                WH_Work_Days_Count = 0;
                Total_Days_Cal = 0;


                Response.Write("</tr>");
            }



        }



        Response.Write("</table>");


        Response.End();
        Response.Clear();

    }
    //FORM25B code end


    //FORM25B_IF code start
    public void FORM25B_IF()
    {
        DataTable NFH_DS = new DataTable();
        DataTable TeamCount = new DataTable();
        DataTable YarnType = new DataTable();
        DataTable ShiftDT = new DataTable();
        DataTable KG_DT = new DataTable();
        DataTable Total_DT = new DataTable();

        DateTime date1;
        date1 = Convert.ToDateTime(fromdate);
        ArrayList OT_Array_Value = new ArrayList();
        string dat = todate;
        DateTime Date2 = Convert.ToDateTime(dat);
        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        int daycount1 = (int)((Date2 - date1).TotalDays);
        int daysAdded1 = 0;


        string Grand_sum = "0";



        //Get the Location
        SSQL = "Select * from Location_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        NFH_DS = objdata.RptEmployeeMultipleDetails(SSQL);


        if (NFH_DS.Rows.Count > 0)
        {
            RegNo = NFH_DS.Rows[0]["Register_No"].ToString();
            Location_Full_Address_Join = "" + NFH_DS.Rows[0]["Add1"].ToString() + "," + NFH_DS.Rows[0]["City"].ToString() + "";
            //Address2 = NFH_DS.Rows[0]["Add2"].ToString();
        }
        else
        {
            RegNo = "";
        }

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        string name = dt.Rows[0]["CompName"].ToString();


        grid.DataSource = NFH_DS;
        grid.DataBind();
        string attachment = "attachment;filename=FORM25B.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);


        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='4'></td>");

        Response.Write("<td colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> FORM 25 </a>");
        Response.Write("</td>");

        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> Name and Address of the factory: </a>");
        Response.Write("</td>");
        Response.Write("<td align='center' colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> (Prescribed under rule 103) </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> " + name + " </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='" + Convert.ToInt16(daycount + 3) + "'>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> " + Location_Full_Address_Join + " </a>");
        Response.Write("</td>");
        Response.Write("<td align='center' colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> MUSTER ROLL </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> Registration Number :" + RegNo + " </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("</td>");
        Response.Write("</tr>");



        Response.Write("<tr>");

        Response.Write("<td colspan='4'>");
        Response.Write("</td>");

        Response.Write("<td align='center' colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\">For the Month of " + date1.ToString("MMMM") + " " + date1.Year + " </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> SNo </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> TokenNo </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Name </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Father's Name </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Date of Birth </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Dept. & Designation </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Week Off </a>");
        Response.Write("</td>");
        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> " + Convert.ToString(dayy.DayOfWeek).Substring(0, 3) + " </a>");
            Response.Write("</td>");

            daycount -= 1;
            daysAdded += 1;
        }
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Days Worked </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> NFH </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> WH </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> L / A </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Total Days </a>");
        Response.Write("</td>");
        //Response.Write("<td rowspan='2'>");
        //Response.Write("<a style=\"font-weight:bold\"> Total Work Days </a>");
        //Response.Write("</td>");

        Response.Write("</tr>");
        Response.Write("<tr>");

        while (daycount1 >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1).ToShortDateString());
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> " + dayy.Day.ToString() + " </a>");
            Response.Write("</td>");

            daycount1 -= 1;
            daysAdded1 += 1;
        }

        Response.Write("</tr>");


        DataTable Emp_DS = new DataTable();
        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='" + Wages + "' And IsActive='Yes'";
        SSQL = SSQL + " And CONVERT(DATETIME,DOJ, 103)<=CONVERT(DATETIME,'" + Date2.ToString("dd/MM/yyyy") + "',103) And Eligible_PF='1' ";
        SSQL = SSQL + " Order by ExistingCode Asc";

        Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Emp_DS.Rows.Count != 0)
        {

            for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
            {
                int colIndex;
                Boolean isPresent = false;
                Double Worked_Days_Count = 0;

                string Emp_Dept_Name = Emp_DS.Rows[intRow]["DeptName"].ToString();
                string Emp_Desig_Name = Emp_DS.Rows[intRow]["Designation"].ToString();

                if (Emp_DS.Rows[intRow]["ExistingCode"].ToString() == "H02449")
                {
                    string aaaaaa = "2";
                }
                Response.Write("<tr align='left'>");

                Response.Write("<td>");
                Response.Write("<a>" + Convert.ToInt16(intRow + 1) + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a>" + Emp_DS.Rows[intRow]["ExistingCode"].ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a>" + Emp_DS.Rows[intRow]["FirstName"].ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a>" + Emp_DS.Rows[intRow]["LastName"].ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                //Response.Write("<a>" + Emp_DS.Rows[intRow]["BirthDate"].ToString() + "</a>");
                Response.Write("<a>" + Convert.ToDateTime(Emp_DS.Rows[intRow]["BirthDate"].ToString()).ToString("dd/MM/yyyy") + "</a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a>" + Emp_Dept_Name + "-" + Emp_Desig_Name + "</a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a>" + Emp_DS.Rows[intRow]["WeekOff"].ToString() + "</a>");
                Response.Write("</td>");


                double Without_NFH_WH_Worked_Days_Count = 0;
                double Total_Days_Cal = 0;
                double NFH_Days_Count = 0;
                double WH_Minus_Count = 0;
                double WH_Present_Count = 0;
                double NFH_Present_Day_Add = 0;
                double Mill_NFH_Days_Count = 0;
                double Form25_NFH_Present_Goverment_Rights = 0;
                double NFH_Work_Days_Count = 0;
                double WH_Days_Count = 0;
                double WH_Work_Days_Count = 0;


                for (int intCol = 0; intCol <= daysAdded - 1; intCol++)
                {

                    string Employee_Punch = "";
                    isPresent = false;
                    string Machine_ID_Str = "";
                    string OT_Week_OFF_Machine_No = "";
                    string Date_Value_Str = "";
                    string Date_value_str1 = "";
                    DataTable mLocalDS = new DataTable();
                    string Time_IN_Str = "";
                    string Time_Out_Str = "";
                    string Total_Time_get = "";
                    Int32 j = 0;
                    double time_Check_dbl = 0;
                    //Dim Emp_Total_Work_Time_1 As Double
                    string Emp_Total_Work_Time_1 = "00:00";
                    isPresent = false;
                    //For i = 0 To shiftCount - 1 Step 2 'take account only IN Column(leave OUT Column)
                    //MsgBox(.Cells(intI, 2).value)
                    Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
                    OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();


                    NewDate1 = Convert.ToDateTime(fromdate);

                    dtime = NewDate1.AddDays(intCol);
                    dtime1 = dtime.AddDays(1);
                    //Date_Value_Str = String.Format(Convert.ToString(dtime), "yyyy/MM/dd");
                    //Date_Value_Str1 = String.Format(Convert.ToString(dtime1), "yyyy/MM/dd");



                    Date_Value_Str = (Convert.ToDateTime(dtime)).ToString("yyyy/MM/dd");
                    Date_value_str1 = (Convert.ToDateTime(dtime1)).ToString("yyyy/MM/dd");

                    //Check Adolescent Employee
                    string Adolecesent_Date_str = (Convert.ToDateTime(dtime)).ToString("dd/MM/yyyy");
                    string query_str = "Select FLOOR((CAST ((Convert(datetime,'" + Adolecesent_Date_str + "',103)) AS INTEGER) - CAST(CONVERT(datetime,BirthDate,103)  AS INTEGER)) / 365.25) as Age_check from ";
                    query_str = query_str + " Employee_Mst where ExistingCode='" + Emp_DS.Rows[intRow]["ExistingCode"].ToString() + "' and LocCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
                    DataTable DT_Ado = new DataTable();
                    bool Adol_Comple_Shift_Check = false;
                    DT_Ado = objdata.RptEmployeeMultipleDetails(query_str);
                    if (DT_Ado.Rows.Count != 0)
                    {
                        string Age_check_str = DT_Ado.Rows[0]["Age_check"].ToString();
                        if (Convert.ToDecimal(Age_check_str) < 18)
                        {
                            Adol_Comple_Shift_Check = true;
                        }
                        else
                        {
                            Adol_Comple_Shift_Check = false;
                        }
                    }
                    else
                    {
                        Adol_Comple_Shift_Check = false;
                    }
                    //Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day = "";
                    SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'And Eligible_PF='1'";
                    DS_WH = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DS_WH.Rows.Count != 0)
                    {
                        Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    }
                    else
                    {
                        Emp_WH_Day = "";
                    }

                    //TimeIN Get
                    DataTable mLocalDS1 = new DataTable();


                    //Shift Change Check Start
                    DateTime InTime_Check = default(DateTime);
                    DateTime InToTime_Check = default(DateTime);
                    TimeSpan InTime_TimeSpan = default(TimeSpan);
                    string From_Time_Str = "";
                    string To_Time_Str = "";
                    DataTable DS_Time = new DataTable();
                    DataTable DS_InTime = new DataTable();
                    string Final_InTime = "";
                    string Final_OutTime = "";
                    string Final_Shift = "";
                    DataTable Shift_DS_Change = new DataTable();
                    Int32 K = 0;
                    bool Shift_Check_blb = false;
                    string Shift_Start_Time_Change = null;
                    string Shift_End_Time_Change = null;
                    string Employee_Time_Change = "";
                    DateTime ShiftdateStartIN_Change = default(DateTime);
                    DateTime ShiftdateEndIN_Change = default(DateTime);
                    DateTime EmpdateIN_Change = default(DateTime);


                    string Shift_Name_Store = "";
                    double Calculate_Attd_Work_Time = 0;
                    double Calculate_Attd_Work_Time_1 = 4.0;
                    bool Half_Day_Count_Check = false;
                    DataTable Log_DT = new DataTable();
                    SSQL = "Select *from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And Attn_Date_Str='" + Convert.ToDateTime(Date_Value_Str).ToString("dd/MM/yyyy") + "'";
                    SSQL = SSQL + " And MachineID='" + OT_Week_OFF_Machine_No + "'";
                    Log_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Log_DT.Rows.Count != 0)
                    {

                        if (Adol_Comple_Shift_Check == true)
                        {
                            Shift_Name_Store = "SHIFT1";//Log_DT.Rows[0]["Shift"].ToString();
                        }
                        else
                        {
                            Shift_Name_Store = Log_DT.Rows[0]["Shift"].ToString();
                        }
                        if (Log_DT.Rows[0]["Present"].ToString() == "1.0")
                        {
                            isPresent = true;
                            halfPresent = "0";
                        }
                        else if (Log_DT.Rows[0]["Present"].ToString() == "0.5")
                        {
                            isPresent = true;
                            halfPresent = "1";
                            Half_Day_Count_Check = true;
                        }
                    }



                    string qry_nfhcheck = "Select NFHDate,Form25_NFH_Present,NFH_Type,Form25DisplayText from NFH_Mst where CONVERT(DATETIME,DateStr, 103)=CONVERT(DATETIME,'" + Convert.ToDateTime(Date_Value_Str).ToString("dd/MM/yyyy") + "',103)";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(qry_nfhcheck);

                    bool NFH_CHeck_Bol = false;

                    bool NFH_Day_Check = false;
                    System.DateTime NFH_Date = default(System.DateTime);
                    System.DateTime DOJ_Date_Format = default(System.DateTime);
                    string DOJ_Date_Str = Emp_DS.Rows[intRow]["DOJ"].ToString();
                    NFH_CHeck_Bol = false;
                    string NFH_Display_Text = "";
                    bool WH_Minus_Check_bool = false;
                    bool NFH_Form25_Prescent_CHeck_Bol = false;
                    if (mLocalDS.Rows.Count > 0)
                    {
                        //Date OF Joining Check Start
                        if (!string.IsNullOrEmpty(DOJ_Date_Str))
                        {
                            NFH_Date = Convert.ToDateTime(mLocalDS.Rows[0]["NFHDate"]);
                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                NFH_CHeck_Bol = true;
                            }
                            else
                            {
                                //Skip
                            }
                        }
                        else
                        {
                            NFH_CHeck_Bol = true;
                        }
                        if (NFH_CHeck_Bol == true)
                        {
                            NFH_Display_Text = mLocalDS.Rows[0]["Form25DisplayText"].ToString().ToUpper();
                            if (mLocalDS.Rows[0]["NFH_Type"].ToString().ToUpper() == "WH Minus".ToUpper().ToString())
                            {
                                WH_Minus_Check_bool = true;
                            }
                            else
                            {
                                WH_Minus_Check_bool = false;
                            }
                            if (mLocalDS.Rows[0]["Form25_NFH_Present"].ToString().ToUpper() == "Yes".ToUpper().ToString())
                            {
                                NFH_Form25_Prescent_CHeck_Bol = true;
                            }
                            else
                            {
                                NFH_Form25_Prescent_CHeck_Bol = false;
                            }
                        }

                    }


                    if (isPresent == true)
                    {
                        if ((Shift_Name_Store).ToString().ToUpper() == "GENERAL")
                        {
                            Shift_Name_Store = "G";
                        }
                        else if ((Shift_Name_Store).ToString().ToUpper() == "SHIFT1")
                        {
                            Shift_Name_Store = "1";
                        }
                        else if ((Shift_Name_Store).ToString().ToUpper() == "SHIFT2")
                        {
                            Shift_Name_Store = "2";
                        }
                        else if ((Shift_Name_Store).ToString().ToUpper() == "SHIFT3")
                        {
                            Shift_Name_Store = "3";
                        }
                        else
                        {
                            Shift_Name_Store = "N";
                        }
                    }

                    if (NFH_CHeck_Bol == true)
                    {
                        NFH_Days_Count = NFH_Days_Count + 1;
                        if (WH_Minus_Check_bool == true)
                        {
                            WH_Minus_Count = WH_Minus_Count + 1;
                            string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                            if ((Emp_WH_Day).ToString().ToUpper() == (Attn_Date_Day).ToString().ToUpper())
                            {
                                if (isPresent == true)
                                {
                                    if (halfPresent == "0")
                                    {
                                        WH_Present_Count = WH_Present_Count + 1;
                                    }
                                    else if (Half_Day_Count_Check == true)
                                    {
                                        WH_Present_Count = WH_Present_Count + 0.5;
                                    }
                                }
                            }
                            else
                            {
                                if (isPresent == true)
                                {
                                    if (halfPresent == "0")
                                    {
                                        NFH_Present_Day_Add = NFH_Present_Day_Add + 1;
                                    }
                                    else if (Half_Day_Count_Check == true)
                                    {
                                        NFH_Present_Day_Add = NFH_Present_Day_Add + 0.5;
                                    }
                                }
                            }
                        }

                        if (NFH_Display_Text != "")
                        {
                            Mill_NFH_Days_Count = Mill_NFH_Days_Count + 1;
                            Response.Write("<td>");
                            Response.Write("<span style=color:green><a>" + NFH_Display_Text.ToString() + "</a></span>");
                            Response.Write("</td>");
                        }
                        else
                        {
                            if (NFH_Form25_Prescent_CHeck_Bol == true)
                            {
                                if (isPresent == true)
                                {
                                    if (halfPresent == "0")
                                    {
                                        Form25_NFH_Present_Goverment_Rights = Form25_NFH_Present_Goverment_Rights + 1;
                                        Response.Write("<td>");
                                        Response.Write("<span style=color:green><a>NH / " + Shift_Name_Store.ToString() + "</a></span>");
                                        Response.Write("</td>");
                                    }
                                    else if (Half_Day_Count_Check == true)
                                    {
                                        Form25_NFH_Present_Goverment_Rights = Form25_NFH_Present_Goverment_Rights + 0.5;
                                        Response.Write("<td>");
                                        Response.Write("<span style=color:green><a>NH / " + Shift_Name_Store.ToString() + "</a></span>");
                                        Response.Write("</td>");
                                    }
                                }
                                else
                                {
                                    Response.Write("<td>");
                                    Response.Write("<span style=color:green><a>" + "NH" + "</a></span>");
                                    Response.Write("</td>");
                                }
                            }
                            else
                            {
                                Mill_NFH_Days_Count = Mill_NFH_Days_Count + 1;
                                Response.Write("<td>");
                                Response.Write("<span style=color:green><a>" + "NH" + "</a></span>");
                                Response.Write("</td>");
                            }
                        }
                    }
                    else
                    {


                        string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                        if ((Emp_WH_Day).ToString().ToUpper() == (Attn_Date_Day).ToString().ToUpper())
                        {
                            WH_Days_Count = WH_Days_Count + 1;
                            //.Cells(intI, intK).value = "WH"
                            if (isPresent == true)
                            {
                                if (halfPresent == "0")
                                {
                                    WH_Present_Count = WH_Present_Count + 1;
                                }
                                else if (Half_Day_Count_Check == true)
                                {
                                    WH_Present_Count = WH_Present_Count + 0.5;
                                }
                                Response.Write("<td>");
                                Response.Write("<span style=color:green><a>" + "WH" + "</a></span>");
                                Response.Write("</td>");
                            }
                            else
                            {
                                Response.Write("<td>");
                                Response.Write("<span style=color:green><a>" + "WH" + "</a></span>");
                                Response.Write("</td>");
                            }
                        }
                        else
                        {
                            if (isPresent == true)
                            {
                                if (halfPresent == "0")
                                {
                                    Without_NFH_WH_Worked_Days_Count = Without_NFH_WH_Worked_Days_Count + 1;
                                }
                                else if (Half_Day_Count_Check == true)
                                {
                                    Without_NFH_WH_Worked_Days_Count = Without_NFH_WH_Worked_Days_Count + 0.5;
                                }
                                if (halfPresent == "0")
                                {
                                    Response.Write("<td>");
                                    Response.Write("<a>" + Shift_Name_Store.ToString() + "</a>");
                                    Response.Write("</td>");
                                }
                                else if (Half_Day_Count_Check == true)
                                {
                                    Response.Write("<td>");
                                    Response.Write("<a>" + Shift_Name_Store.ToString() + "/H</a>");
                                    Response.Write("</td>");
                                }
                            }
                            else
                            {
                                //Check Leave Master
                                DataTable Leave_DS = new DataTable();
                                string Leave_Short_Name = "A";
                                SSQL = "Select * from Leave_Register_Mst where Machine_No='" + OT_Week_OFF_Machine_No + "'";
                                SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And From_Date_Dt >='" + Date_Value_Str + "'";
                                SSQL = SSQL + " And To_Date_Dt <='" + Date_Value_Str + "'";
                                Leave_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (Leave_DS.Rows.Count != 0)
                                {
                                    //Get Leave Short Name
                                    SSQL = "Select * from LeaveType_Mst Where LeaveType='" + Leave_DS.Rows[0]["LeaveType"].ToString() + "'";
                                    Leave_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (Leave_DS.Rows.Count != 0)
                                        Leave_Short_Name = Leave_DS.Rows[0]["Short_Name"].ToString();

                                    Response.Write("<td>");
                                    Response.Write("<a>" + Leave_Short_Name.ToString() + "/H</a>");
                                    Response.Write("</td>");

                                    //_with1.Cells(intI, intK).value = Leave_Short_Name;
                                }
                                else
                                {

                                    Response.Write("<td>");
                                    Response.Write("<span style=color:red><a>" + "A" + "</a></span>");
                                    Response.Write("</td>");
                                }
                            }
                        }
                    }
                    Total_Days_Cal = Total_Days_Cal + 1;



                }


                double Get_Absent_Days = 0;
                double Get_Total_Work_Days = 0;
                //Get_Absent_Days = NFH_Days_Count + WH_Days_Count + Without_NFH_WH_Worked_Days_Count
                //Get_Absent_Days = Total_Days_Cal - Get_Absent_Days
                Get_Total_Work_Days = Without_NFH_WH_Worked_Days_Count + NFH_Present_Day_Add;
                double Final_Display_Present_Count = Get_Total_Work_Days;
                if (Get_Total_Work_Days != 0 && WH_Minus_Count != 0)
                {
                    double NFH_Week_Off_Minus_Days = WH_Minus_Count;
                    double NFH_Final_Disp_Days = 0;
                    if (NFH_Week_Off_Minus_Days <= WH_Present_Count)
                    {
                        WH_Present_Count = WH_Present_Count - NFH_Week_Off_Minus_Days;
                    }
                    else
                    {
                        //NFH Days Minus for Working Days
                        double Balance_NFH_Days_Minus_IN_WorkDays = 0;
                        Balance_NFH_Days_Minus_IN_WorkDays = NFH_Week_Off_Minus_Days - WH_Present_Count;
                        Final_Display_Present_Count = Final_Display_Present_Count - Balance_NFH_Days_Minus_IN_WorkDays;
                        WH_Present_Count = 0;
                    }
                }
                Get_Absent_Days = Mill_NFH_Days_Count + WH_Days_Count + Final_Display_Present_Count;
                Get_Absent_Days = Total_Days_Cal - Get_Absent_Days;
                double Final_Work_Days = 0;
                Final_Work_Days = Final_Display_Present_Count;

                //Final total Add
                Response.Write("<td>");
                Response.Write("<a>" + Final_Work_Days.ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a>" + NFH_Days_Count.ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a>" + WH_Days_Count.ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a>" + Get_Absent_Days.ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td>");
                Response.Write("<a>" + Total_Days_Cal.ToString() + "</a>");
                Response.Write("</td>");


                Without_NFH_WH_Worked_Days_Count = 0;
                Form25_NFH_Present_Goverment_Rights = 0;
                WH_Present_Count = 0;
                WH_Minus_Count = 0;
                NFH_Present_Day_Add = 0;
                NFH_Days_Count = 0;
                Mill_NFH_Days_Count = 0;
                WH_Days_Count = 0;
                Total_Days_Cal = 0;


                Without_NFH_WH_Worked_Days_Count = 0;
                NFH_Days_Count = 0;
                NFH_Work_Days_Count = 0;
                WH_Days_Count = 0;
                WH_Work_Days_Count = 0;
                Total_Days_Cal = 0;


                Response.Write("</tr>");
            }



        }



        Response.Write("</table>");


        //Fotter total add
        string Row_Count = Emp_DS.Rows.Count.ToString();
        Row_Count = (Convert.ToDecimal(Row_Count) + Convert.ToDecimal(7)).ToString();

        Response.Write("<table><tr><td></td></tr></table>");

        Response.Write("<table>");
        Response.Write("<tr><td></td><td></td><td></td><td></td><td></td>");

        Response.Write("<td><table border='1'><tr>");

        Response.Write("<td><a style=\"font-weight:bold\"> " + "Present" + " </a></td>");
        Response.Write("<td><a style=\"font-weight:bold\"> " + "General" + " </a></td>");
        string S1 = @"""G""";
        string S2 = @"""G/A""";
        for (int intColw = 0; intColw <= daysAdded - 1; intColw++)
        {
            Response.Write("<td><a style=\"font-weight:bold\"> " + "=--(SUM(COUNTIF(OFFSET(H1,8," + intColw + "):OFFSET(H1," + Row_Count + "," + intColw + "),{" + S1 + "," + S2 + "})))" + " </a></td>");
        }
        Response.Write("</tr><tr>");

        Response.Write("<td><a style=\"font-weight:bold\"> " + "Present" + " </a></td>");
        Response.Write("<td><a style=\"font-weight:bold\"> " + "1" + " </a></td>");
        S1 = @"""1""";
        S2 = @"""1/A""";
        for (int intColw = 0; intColw <= daysAdded - 1; intColw++)
        {
            Response.Write("<td><a style=\"font-weight:bold\"> " + "=--(SUM(COUNTIF(OFFSET(H1,8," + intColw + "):OFFSET(H1," + Row_Count + "," + intColw + "),{" + S1 + "," + S2 + "})))" + " </a></td>");
        }
        Response.Write("</tr><tr>");

        Response.Write("<td><a style=\"font-weight:bold\"> " + "Present" + " </a></td>");
        Response.Write("<td><a style=\"font-weight:bold\"> " + "2" + " </a></td>");
        S1 = @"""2""";
        S2 = @"""2/A""";
        for (int intColw = 0; intColw <= daysAdded - 1; intColw++)
        {
            Response.Write("<td><a style=\"font-weight:bold\"> " + "=--(SUM(COUNTIF(OFFSET(H1,8," + intColw + "):OFFSET(H1," + Row_Count + "," + intColw + "),{" + S1 + "," + S2 + "})))" + " </a></td>");
        }
        Response.Write("</tr><tr>");

        Response.Write("<td><a style=\"font-weight:bold\"> " + "Present" + " </a></td>");
        Response.Write("<td><a style=\"font-weight:bold\"> " + "3" + " </a></td>");
        S1 = @"""3""";
        S2 = @"""3/A""";
        for (int intColw = 0; intColw <= daysAdded - 1; intColw++)
        {
            Response.Write("<td><a style=\"font-weight:bold\"> " + "=--(SUM(COUNTIF(OFFSET(H1,8," + intColw + "):OFFSET(H1," + Row_Count + "," + intColw + "),{" + S1 + "," + S2 + "})))" + " </a></td>");
        }
        Response.Write("</tr><tr>");

        Response.Write("<td><a style=\"font-weight:bold\"> " + "No Shift" + " </a></td>");
        Response.Write("<td><a style=\"font-weight:bold\"> " + "" + " </a></td>");
        S1 = @"""N""";
        S2 = @"""N/A""";
        for (int intColw = 0; intColw <= daysAdded - 1; intColw++)
        {
            Response.Write("<td><a style=\"font-weight:bold\"> " + "=--(SUM(COUNTIF(OFFSET(H1,8," + intColw + "):OFFSET(H1," + Row_Count + "," + intColw + "),{" + S1 + "," + S2 + "})))" + " </a></td>");
        }
        Response.Write("</tr><tr>");

        Response.Write("<td><a style=\"font-weight:bold\"> " + "L / A" + " </a></td>");
        Response.Write("<td><a style=\"font-weight:bold\"> " + "" + " </a></td>");
        S1 = @"""A""";
        S2 = @"""N/A""";
        for (int intColw = 0; intColw <= daysAdded - 1; intColw++)
        {
            Response.Write("<td><a style=\"font-weight:bold\"> " + "=COUNTIF(OFFSET(H1,8," + intColw + "):OFFSET(H1," + Row_Count + "," + intColw + ")," + S1 + ")" + " </a></td>");
        }
        Response.Write("</tr><tr>");

        Response.Write("<td><a style=\"font-weight:bold\"> " + "WH" + " </a></td>");
        Response.Write("<td><a style=\"font-weight:bold\"> " + "" + " </a></td>");
        S1 = @"""WH""";
        S2 = @"""N/A""";
        for (int intColw = 0; intColw <= daysAdded - 1; intColw++)
        {
            Response.Write("<td><a style=\"font-weight:bold\"> " + "=COUNTIF(OFFSET(H1,8," + intColw + "):OFFSET(H1," + Row_Count + "," + intColw + ")," + S1 + ")" + " </a></td>");
        }
        Response.Write("</tr><tr>");

        string total_head_First = (Convert.ToDecimal(Row_Count) + Convert.ToDecimal(2)).ToString();
        string total_head_Last = (Convert.ToDecimal(Row_Count) + Convert.ToDecimal(8)).ToString();
        Response.Write("<td><a style=\"font-weight:bold\"> " + "Total" + " </a></td>");
        Response.Write("<td><a style=\"font-weight:bold\"> " + "" + " </a></td>");
        for (int intColw = 0; intColw <= daysAdded - 1; intColw++)
        {
            Response.Write("<td><a style=\"font-weight:bold\"> " + "=SUM(OFFSET(H1," + total_head_First + "," + intColw + "):OFFSET(H1," + total_head_Last + "," + intColw + "))" + " </a></td>");
        }
        Response.Write("</tr>");
        Response.Write("</table>");

        Response.Write("</td></tr>");
        Response.Write("</table>");

        Response.End();
        Response.Clear();

    }
    //FORM25B_IF code end

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }


    private void Between_Date_Query_Output()
    {
        Heading_Add_Form25B();
        string TableName = "";


        TableName = "Employee_Mst";


        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();

        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("FirstName");
        AutoDTable.Columns.Add("LastName");
        AutoDTable.Columns.Add("DOB");
        AutoDTable.Columns.Add("DeptName_Designation");
        AutoDTable.Columns.Add("Weekoff");



        date1 = Convert.ToDateTime(fromdate);
        string dat = todate;
        Date2 = Convert.ToDateTime(dat);

        DateTime Query_Date_Check = new DateTime();
        DateTime DOJ_Date_Check_Emp = new DateTime();

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        string Query_header = "";
        string Query_Pivot = "";
        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.Day.ToString()));

            if (daysAdded == 0)
            {
                Query_header = "isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = "[" + dayy.Day.ToString() + "]";
            }
            else
            {
                Query_header = Query_header + ",isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = Query_Pivot + ",[" + dayy.Day.ToString() + "]";
            }
            daycount -= 1;
            daysAdded += 1;
        }

        AutoDTable.Columns.Add("NFH");
        AutoDTable.Columns.Add("WH");
        AutoDTable.Columns.Add("Days_Worked");
        AutoDTable.Columns.Add("OT_Days");
        AutoDTable.Columns.Add("NFH_W_Days");
        AutoDTable.Columns.Add("Total_Work_Days");

        string query = "";

        query = "Select DeptName,MachineID,ExistingCode,FirstName,DOJ,LastName,DOB,Designation,WeekOff," + Query_header + " from ( ";
        query = query + " Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff,";
        query = query + " EM.LastName,CONVERT(varchar(20),EM.BirthDate,103) as DOB,EM.Designation,DATENAME(dd, Attn_Date) as Day_V, ";
        query = query + " (Case when EM.DOJ > LD.Attn_Date then '' when Datename(weekday,LD.Attn_Date)=EM.WeekOff then ";
        //Week off
        query = query + " (CASE WHEN LD.Present = 1.0 THEN ";
        query = query + " (CASE WHEN Shift='GENERAL' then 'WH/G' WHEN Shift='SHIFT1' then 'WH/1' WHEN Shift='SHIFT2' then 'WH/2' WHEN Shift='SHIFT3' then 'WH/3' else 'WH/N' end) ";
        query = query + " WHEN LD.Present = 0.5 THEN (CASE WHEN Shift='GENERAL' then 'WH/G' WHEN Shift='SHIFT1' then 'WH/1' WHEN Shift='SHIFT2' then 'WH/2' WHEN Shift='SHIFT3' then 'WH/3' else 'WH/N' end) else 'WH' End)";
        query = query + " else";
        //Normal Days
        query = query + " (CASE WHEN LD.Present = 1.0 THEN (CASE WHEN Shift='GENERAL' then 'G' WHEN Shift='SHIFT1' then '1' WHEN Shift='SHIFT2' then '2' WHEN Shift='SHIFT3' then '3' else 'N' end) ";
        query = query + " WHEN LD.Present = 0.5 THEN (CASE WHEN Shift='GENERAL' then 'G' WHEN Shift='SHIFT1' then '1' WHEN Shift='SHIFT2' then '2' WHEN Shift='SHIFT3' then '3' else 'N' end) else 'A' End)";
        query = query + " end) as Presents";

        query = query + " from LogTime_Days LD inner join " + TableName + " EM on EM.LocCode=LD.LocCode And EM.CompCode=LD.CompCode And EM.ExistingCode=LD.ExistingCode ";
        query = query + " where LD.LocCode='" + SessionLcode + "' ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + fromdate + "',103) ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + todate + "',103) ";
        if (TokenNo != "")
        {
            query = query + " And LD.ExistingCode='" + TokenNo + "' And EM.ExistingCode='" + TokenNo + "'";
        }
        if (Wages != "-Select-")
        {
            query = query + " And EM.Wages='" + Wages + "'";
            query = query + " And LD.Wages='" + Wages + "'";
        }
        //if (Division != "-Select-")
        //{
        //    query = query + " And EM.Division = '" + Division + "'";
        //}
        query = query + " And EM.LocCode='" + SessionLcode + "') as CV ";
        query = query + " pivot (max (Presents) for Day_V in (" + Query_Pivot + ")) as AvgIncomePerDay ";
        query = query + " order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);


        //NFH Check start
        query = "Select NFHDate from NFH_Mst where convert(datetime,NFHDate,103) >=convert(datetime,'" + fromdate + "',103)";
        query = query + " And convert(datetime,NFHDate,103) <=convert(datetime,'" + todate + "',103) order by NFHDate Asc";
        DataTable DT_NH = new DataTable();
        DT_NH = objdata.RptEmployeeMultipleDetails(query);
        if (DT_NH.Rows.Count != 0)
        {
            for (int ih = 0; ih < DT_NH.Rows.Count; ih++)
            {
                DateTime dayy = Convert.ToDateTime(DT_NH.Rows[ih]["NFHDate"].ToString());

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string DOJ_Date_Str = "";
                    if (dt.Rows[i]["DOJ"].ToString() != "")
                    {
                        DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
                    }
                    else
                    {
                        DOJ_Date_Str = "";
                    }
                    if (DOJ_Date_Str != "")
                    {
                        Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                        DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                        if (DOJ_Date_Check_Emp <= Query_Date_Check)
                        {
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "G") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/G" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "1") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/1" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "2") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/2" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "3") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/3" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "N") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/N" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "NH/A" + "</b></span>"; }

                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/G") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/G" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/1") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/1" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/2") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/2" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/3") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/3" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/N") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/N" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "NH" + "</b></span>"; }
                        }
                        else
                        {
                            dt.Rows[i]["" + dayy.Day.ToString() + ""] = "";
                        }
                    }
                }
            }
        }
        //NFH Check End

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            string MachineID = dt.Rows[i]["MachineID"].ToString();
            int DT_Row = AutoDTable.Rows.Count - 1;
            AutoDTable.Rows[DT_Row]["SNo"] = (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString();
            AutoDTable.Rows[DT_Row]["DeptName_Designation"] = dt.Rows[i]["DeptName"].ToString() + "-" + dt.Rows[i]["Designation"].ToString();
            //AutoDTable.Rows[DT_Row]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
            AutoDTable.Rows[DT_Row]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[DT_Row]["FirstName"] = dt.Rows[i]["FirstName"].ToString();
            AutoDTable.Rows[DT_Row]["LastName"] = dt.Rows[i]["LastName"].ToString();
            AutoDTable.Rows[DT_Row]["DOB"] = dt.Rows[i]["DOB"].ToString();
            AutoDTable.Rows[DT_Row]["Weekoff"] = dt.Rows[i]["Weekoff"].ToString();

            string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
            string DOJ_Date_Str = "";
            if (dt.Rows[i]["DOJ"].ToString() != "")
            {
                DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
            }
            else
            {
                DOJ_Date_Str = "";
            }

            daycount = (int)((Date2 - date1).TotalDays);
            daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                if (DOJ_Date_Str != "")
                {
                    Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                    DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                    if (DOJ_Date_Check_Emp <= Query_Date_Check)
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "G") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "G" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "1") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "1" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "2") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "2" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "3") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "3" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "N") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "N" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "A" + "</b></span>"; }

                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/G") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/G" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/1") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/1" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/2") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/2" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/3") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/3" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/N") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/N" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "WH" + "</b></span>"; }

                    }
                    else
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "";
                    }
                }
                else
                {
                    AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "G") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "G" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "1") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "1" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "2") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "2" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "3") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "3" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "N") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "N" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "A" + "</b></span>"; }

                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/G") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/G" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/1") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/1" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/2") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/2" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/3") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/3" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/N") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/N" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "WH" + "</b></span>"; }
                }

                daycount -= 1;
                daysAdded += 1;
            }
            //Get Total Days
            string NFH_Worked_Days = "0";
            string WH_Worked_Days = "0";
            string Normal_Worked_Days = "0";
            query = "Select isnull(sum(present),0) as Total_Days from LogTime_Days where ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            query = query + " And LocCode='" + SessionLcode + "'";
            query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + fromdate + "',103) ";
            query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + todate + "',103) ";
            dt1 = objdata.RptEmployeeMultipleDetails(query);
            if (dt1.Rows.Count != 0)
            {
                AutoDTable.Rows[DT_Row]["Total_Work_Days"] = dt1.Rows[0]["Total_Days"].ToString();
            }
            else
            {
                AutoDTable.Rows[DT_Row]["Total_Work_Days"] = "0";
            }
            //Get NFH Worked Days
            query = "Select isnull(sum(LD.Present),0) as NFH_Work_Days,Count(*) as NFH_Days from LogTime_Days LD inner join NFH_Mst NM on NM.NFHDate=LD.Attn_Date where LD.ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            query = query + " And LD.LocCode='" + SessionLcode + "'";
            query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + fromdate + "',103) ";
            query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + todate + "',103) ";
            query = query + " And CONVERT(datetime,NM.NFHDate,103) >= CONVERT(datetime,'" + fromdate + "',103) ";
            query = query + " And CONVERT(datetime,NM.NFHDate,103) <= CONVERT(datetime,'" + todate + "',103) ";
            dt1 = objdata.RptEmployeeMultipleDetails(query);
            if (dt1.Rows.Count != 0)
            {
                NFH_Worked_Days = dt1.Rows[0]["NFH_Work_Days"].ToString();
                AutoDTable.Rows[DT_Row]["NFH_W_Days"] = dt1.Rows[0]["NFH_Work_Days"].ToString();
                AutoDTable.Rows[DT_Row]["NFH"] = dt1.Rows[0]["NFH_Days"].ToString();
            }
            else
            {
                AutoDTable.Rows[DT_Row]["NFH_W_Days"] = "0";
                AutoDTable.Rows[DT_Row]["NFH"] = "0";
            }

            //Get WH Worked Days
            query = "Select isnull(sum(Present),0) as WH_Work_Days,Count(*) as WH_Days  from LogTime_Days where ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            query = query + " And LocCode='" + SessionLcode + "'";
            query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + fromdate + "',103) ";
            query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + todate + "',103) ";
            query = query + " And Datename(weekday,Attn_Date)= '" + dt.Rows[i]["WeekOff"].ToString() + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(query);
            if (dt1.Rows.Count != 0)
            {
                WH_Worked_Days = dt1.Rows[0]["WH_Work_Days"].ToString();
                AutoDTable.Rows[DT_Row]["OT_Days"] = dt1.Rows[0]["WH_Work_Days"].ToString();
                AutoDTable.Rows[DT_Row]["WH"] = dt1.Rows[0]["WH_Days"].ToString();
            }
            else
            {
                AutoDTable.Rows[DT_Row]["OT_Days"] = "0";
                AutoDTable.Rows[DT_Row]["WH"] = "0";
            }
            Normal_Worked_Days = (Convert.ToDecimal(NFH_Worked_Days) + Convert.ToDecimal(WH_Worked_Days)).ToString();
            Normal_Worked_Days = (Convert.ToDecimal(AutoDTable.Rows[DT_Row]["Total_Work_Days"].ToString()) - Convert.ToDecimal(Normal_Worked_Days)).ToString();
            AutoDTable.Rows[DT_Row]["Days_Worked"] = Normal_Worked_Days;
        }

        if (AutoDTable.Rows.Count != 0)
        {
            grid.DataSource = AutoDTable;
            grid.DataBind();
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
        }

    }

    private void Between_Date_Query_Output_IF()
    {
        Heading_Add_Form25B();
        string TableName = "";


        TableName = "Employee_Mst";


        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();

        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("FirstName");
        AutoDTable.Columns.Add("LastName");
        AutoDTable.Columns.Add("DOB");
        AutoDTable.Columns.Add("DeptName_Designation");
        AutoDTable.Columns.Add("Weekoff");



        date1 = Convert.ToDateTime(fromdate);
        string dat = todate;
        Date2 = Convert.ToDateTime(dat);

        DateTime Query_Date_Check = new DateTime();
        DateTime DOJ_Date_Check_Emp = new DateTime();

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        string Query_header = "";
        string Query_Pivot = "";
        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.Day.ToString()));

            if (daysAdded == 0)
            {
                Query_header = "isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = "[" + dayy.Day.ToString() + "]";
            }
            else
            {
                Query_header = Query_header + ",isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = Query_Pivot + ",[" + dayy.Day.ToString() + "]";
            }
            daycount -= 1;
            daysAdded += 1;
        }

        AutoDTable.Columns.Add("NFH");
        AutoDTable.Columns.Add("WH");
        AutoDTable.Columns.Add("Days_Worked");
        AutoDTable.Columns.Add("OT_Days");
        AutoDTable.Columns.Add("NFH_W_Days");
        AutoDTable.Columns.Add("Total_Work_Days");

        string query = "";

        query = "Select DeptName,MachineID,ExistingCode,FirstName,DOJ,LastName,DOB,Designation,WeekOff," + Query_header + " from ( ";
        query = query + " Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff,";
        query = query + " EM.LastName,CONVERT(varchar(20),EM.BirthDate,103) as DOB,EM.Designation,DATENAME(dd, Attn_Date) as Day_V, ";
        query = query + " (Case when EM.DOJ > LD.Attn_Date then '' when Datename(weekday,LD.Attn_Date)=EM.WeekOff then ";
        //Week off
        query = query + " (CASE WHEN LD.Present = 1.0 THEN ";
        query = query + " (CASE WHEN Shift='GENERAL' then 'WH' WHEN Shift='SHIFT1' then 'WH' WHEN Shift='SHIFT2' then 'WH' WHEN Shift='SHIFT3' then 'WH' else 'WH' end) ";
        query = query + " WHEN LD.Present = 0.5 THEN (CASE WHEN Shift='GENERAL' then 'WH' WHEN Shift='SHIFT1' then 'WH' WHEN Shift='SHIFT2' then 'WH' WHEN Shift='SHIFT3' then 'WH' else 'WH' end) else 'WH' End)";
        query = query + " else";
        //Normal Days
        // Adolescent Check
        query = query + " (CASE when (FLOOR((CAST ((Convert(datetime,LD.Attn_Date,103)) AS INTEGER) - CAST(CONVERT(datetime,BirthDate,103)  AS INTEGER)) / 365.25)) < 18 then '1'";
        // Adolescent Check End
        query = query + " else (";
        query = query + " (CASE WHEN LD.Present = 1.0 THEN (CASE WHEN Shift='GENERAL' then 'G' WHEN Shift='SHIFT1' then '1' WHEN Shift='SHIFT2' then '2' WHEN Shift='SHIFT3' then '3' else 'N' end) ";
        query = query + " WHEN LD.Present = 0.5 THEN (CASE WHEN Shift='GENERAL' then 'G' WHEN Shift='SHIFT1' then '1' WHEN Shift='SHIFT2' then '2' WHEN Shift='SHIFT3' then '3' else 'N' end) else 'A' End)";
        query = query + " ) end)";
        query = query + " end) as Presents";

        query = query + " from LogTime_Days LD inner join " + TableName + " EM on EM.LocCode=LD.LocCode And EM.CompCode=LD.CompCode And EM.ExistingCode=LD.ExistingCode ";
        query = query + " where LD.LocCode='" + SessionLcode + "' ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + fromdate + "',103) ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + todate + "',103) ";
        if (TokenNo != "")
        {
            query = query + " And LD.ExistingCode='" + TokenNo + "' And EM.ExistingCode='" + TokenNo + "'";
        }
        if (Wages != "-Select-")
        {
            query = query + " And EM.Wages='" + Wages + "'";
            query = query + " And LD.Wages='" + Wages + "'";
        }
        //if (Division != "-Select-")
        //{
        //    query = query + " And EM.Division = '" + Division + "'";
        //}
        query = query + " And EM.LocCode='" + SessionLcode + "') as CV ";
        query = query + " pivot (max (Presents) for Day_V in (" + Query_Pivot + ")) as AvgIncomePerDay ";
        query = query + " order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);


        //NFH Check start
        query = "Select NFHDate from NFH_Mst where convert(datetime,NFHDate,103) >=convert(datetime,'" + fromdate + "',103)";
        query = query + " And convert(datetime,NFHDate,103) <=convert(datetime,'" + todate + "',103) order by NFHDate Asc";
        DataTable DT_NH = new DataTable();
        DT_NH = objdata.RptEmployeeMultipleDetails(query);
        if (DT_NH.Rows.Count != 0)
        {
            for (int ih = 0; ih < DT_NH.Rows.Count; ih++)
            {
                DateTime dayy = Convert.ToDateTime(DT_NH.Rows[ih]["NFHDate"].ToString());

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string DOJ_Date_Str = "";
                    if (dt.Rows[i]["DOJ"].ToString() != "")
                    {
                        DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
                    }
                    else
                    {
                        DOJ_Date_Str = "";
                    }
                    if (DOJ_Date_Str != "")
                    {
                        Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                        DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                        if (DOJ_Date_Check_Emp <= Query_Date_Check)
                        {
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "G") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/G" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "1") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/1" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "2") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/2" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "3") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/3" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "N") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/N" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "NH/A" + "</b></span>"; }

                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/G") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/G" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/1") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/1" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/2") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/2" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/3") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/3" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/N") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/N" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "NH" + "</b></span>"; }
                        }
                        else
                        {
                            dt.Rows[i]["" + dayy.Day.ToString() + ""] = "";
                        }
                    }
                }
            }
        }
        //NFH Check End

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            string MachineID = dt.Rows[i]["MachineID"].ToString();
            int DT_Row = AutoDTable.Rows.Count - 1;
            AutoDTable.Rows[DT_Row]["SNo"] = (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString();
            AutoDTable.Rows[DT_Row]["DeptName_Designation"] = dt.Rows[i]["DeptName"].ToString() + "-" + dt.Rows[i]["Designation"].ToString();
            //AutoDTable.Rows[DT_Row]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
            AutoDTable.Rows[DT_Row]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[DT_Row]["FirstName"] = dt.Rows[i]["FirstName"].ToString();
            AutoDTable.Rows[DT_Row]["LastName"] = dt.Rows[i]["LastName"].ToString();
            AutoDTable.Rows[DT_Row]["DOB"] = dt.Rows[i]["DOB"].ToString();
            AutoDTable.Rows[DT_Row]["Weekoff"] = dt.Rows[i]["Weekoff"].ToString();

            string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
            string DOJ_Date_Str = "";
            if (dt.Rows[i]["DOJ"].ToString() != "")
            {
                DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
            }
            else
            {
                DOJ_Date_Str = "";
            }

            daycount = (int)((Date2 - date1).TotalDays);
            daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                if (DOJ_Date_Str != "")
                {
                    Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                    DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                    if (DOJ_Date_Check_Emp <= Query_Date_Check)
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "G") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "G" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "1") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "1" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "2") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "2" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "3") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "3" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "N") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "N" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "A" + "</b></span>"; }

                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/G") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/G" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/1") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/1" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/2") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/2" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/3") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/3" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/N") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/N" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "WH" + "</b></span>"; }

                    }
                    else
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "";
                    }
                }
                else
                {
                    AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "G") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "G" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "1") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "1" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "2") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "2" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "3") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "3" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "N") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "N" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "A" + "</b></span>"; }

                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/G") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/G" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/1") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/1" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/2") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/2" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/3") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/3" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/N") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/N" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "WH" + "</b></span>"; }
                }

                daycount -= 1;
                daysAdded += 1;
            }
            //Get Total Days
            string NFH_Worked_Days = "0";
            string WH_Worked_Days = "0";
            string Normal_Worked_Days = "0";
            query = "Select isnull(sum(present),0) as Total_Days from LogTime_Days where ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            query = query + " And LocCode='" + SessionLcode + "'";
            query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + fromdate + "',103) ";
            query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + todate + "',103) ";
            dt1 = objdata.RptEmployeeMultipleDetails(query);
            if (dt1.Rows.Count != 0)
            {
                AutoDTable.Rows[DT_Row]["Total_Work_Days"] = dt1.Rows[0]["Total_Days"].ToString();
            }
            else
            {
                AutoDTable.Rows[DT_Row]["Total_Work_Days"] = "0";
            }
            //Get NFH Worked Days
            query = "Select isnull(sum(LD.Present),0) as NFH_Work_Days,Count(*) as NFH_Days from LogTime_Days LD inner join NFH_Mst NM on NM.NFHDate=LD.Attn_Date where LD.ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            query = query + " And LD.LocCode='" + SessionLcode + "'";
            query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + fromdate + "',103) ";
            query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + todate + "',103) ";
            query = query + " And CONVERT(datetime,NM.NFHDate,103) >= CONVERT(datetime,'" + fromdate + "',103) ";
            query = query + " And CONVERT(datetime,NM.NFHDate,103) <= CONVERT(datetime,'" + todate + "',103) ";
            dt1 = objdata.RptEmployeeMultipleDetails(query);
            if (dt1.Rows.Count != 0)
            {
                NFH_Worked_Days = dt1.Rows[0]["NFH_Work_Days"].ToString();
                AutoDTable.Rows[DT_Row]["NFH_W_Days"] = dt1.Rows[0]["NFH_Work_Days"].ToString();
                AutoDTable.Rows[DT_Row]["NFH"] = dt1.Rows[0]["NFH_Days"].ToString();
            }
            else
            {
                AutoDTable.Rows[DT_Row]["NFH_W_Days"] = "0";
                AutoDTable.Rows[DT_Row]["NFH"] = "0";
            }

            //Get WH Worked Days
            query = "Select isnull(sum(Present),0) as WH_Work_Days,Count(*) as WH_Days  from LogTime_Days where ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            query = query + " And LocCode='" + SessionLcode + "'";
            query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + fromdate + "',103) ";
            query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + todate + "',103) ";
            query = query + " And Datename(weekday,Attn_Date)= '" + dt.Rows[i]["WeekOff"].ToString() + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(query);
            if (dt1.Rows.Count != 0)
            {
                WH_Worked_Days = dt1.Rows[0]["WH_Work_Days"].ToString();
                AutoDTable.Rows[DT_Row]["OT_Days"] = dt1.Rows[0]["WH_Work_Days"].ToString();
                AutoDTable.Rows[DT_Row]["WH"] = dt1.Rows[0]["WH_Days"].ToString();
            }
            else
            {
                AutoDTable.Rows[DT_Row]["OT_Days"] = "0";
                AutoDTable.Rows[DT_Row]["WH"] = "0";
            }
            Normal_Worked_Days = (Convert.ToDecimal(NFH_Worked_Days) + Convert.ToDecimal(WH_Worked_Days)).ToString();
            Normal_Worked_Days = (Convert.ToDecimal(AutoDTable.Rows[DT_Row]["Total_Work_Days"].ToString()) - Convert.ToDecimal(Normal_Worked_Days)).ToString();
            AutoDTable.Rows[DT_Row]["Days_Worked"] = Normal_Worked_Days;
        }

        if (AutoDTable.Rows.Count != 0)
        {
            grid.DataSource = AutoDTable;
            grid.DataBind();
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
        }

    }

    private void Heading_Add_Form25B()
    {
        DataTable NFH_DS = new DataTable();
        DataTable TeamCount = new DataTable();
        DataTable YarnType = new DataTable();
        DataTable ShiftDT = new DataTable();
        DataTable KG_DT = new DataTable();
        DataTable Total_DT = new DataTable();

        DateTime date1;
        date1 = Convert.ToDateTime(fromdate);
        ArrayList OT_Array_Value = new ArrayList();
        string dat = todate;
        DateTime Date2 = Convert.ToDateTime(dat);
        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        int daycount1 = (int)((Date2 - date1).TotalDays);
        int daysAdded1 = 0;


        string Grand_sum = "0";



        //Get the Location
        SSQL = "Select * from Location_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        NFH_DS = objdata.RptEmployeeMultipleDetails(SSQL);


        if (NFH_DS.Rows.Count > 0)
        {
            RegNo = NFH_DS.Rows[0]["Register_No"].ToString();
            Location_Full_Address_Join = "" + NFH_DS.Rows[0]["Add1"].ToString() + "," + NFH_DS.Rows[0]["City"].ToString() + "";
            //Address2 = NFH_DS.Rows[0]["Add2"].ToString();
        }
        else
        {
            RegNo = "";
        }

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        string name = dt.Rows[0]["CompName"].ToString();


        grid.DataSource = NFH_DS;
        grid.DataBind();
        string attachment = "attachment;filename=FORM25B.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);


        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='4'></td>");

        Response.Write("<td colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> FORM 25 </a>");
        Response.Write("</td>");

        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> Name and Address of the factory: </a>");
        Response.Write("</td>");
        Response.Write("<td align='center' colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> (Prescribed under rule 103) </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> " + name + " </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='" + Convert.ToInt16(daycount + 3) + "'>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> " + Location_Full_Address_Join + " </a>");
        Response.Write("</td>");
        Response.Write("<td align='center' colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\"> MUSTER ROLL </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td align='left' colspan='4'>");
        Response.Write("<a style=\"font-weight:bold\"> Registration Number :" + RegNo + " </a>");
        Response.Write("</td>");
        Response.Write("<td colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("</td>");
        Response.Write("</tr>");



        Response.Write("<tr>");

        Response.Write("<td colspan='4'>");
        Response.Write("</td>");

        Response.Write("<td align='center' colspan='" + Convert.ToInt16(daycount + 9) + "'>");
        Response.Write("<a style=\"font-weight:bold\">For the Month of " + date1.ToString("MMMM") + " " + date1.Year + " </a>");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");

        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> SNo </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> TokenNo </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Name </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Father's Name </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Date of Birth </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Dept. & Designation </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Week Off </a>");
        Response.Write("</td>");
        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> " + Convert.ToString(dayy.DayOfWeek).Substring(0, 3) + " </a>");
            Response.Write("</td>");

            daycount -= 1;
            daysAdded += 1;
        }
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> NFH </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> WH </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Days Worked </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> OT Days </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> NFH W.Days </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> Total Work Days </a>");
        Response.Write("</td>");

        Response.Write("</tr>");
        Response.Write("<tr>");

        while (daycount1 >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1).ToShortDateString());
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> " + dayy.Day.ToString() + " </a>");
            Response.Write("</td>");

            daycount1 -= 1;
            daysAdded1 += 1;
        }

        Response.Write("</tr></table>");
    }
}
