﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HR_RptPaySlip_Mail.aspx.cs" Inherits="HR_Reports_HR_RptPaySlip_Mail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('#example').dataTable();
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>

 <!-- begin #content -->
<div id="content" class="content-wrapper">
    <section class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">Pay Slip Mail</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Pay Slip Mail Details</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        
                        <div class="panel-body">
                            <asp:UpdatePanel ID="SalPay" runat="server">
                                <ContentTemplate>
                                    <div class="form-group">
                        <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
								 <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
								</asp:DropDownList>
								<asp:RequiredFieldValidator ControlToValidate="ddlcategory" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlcategory" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlcategory" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlcategory" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" style="width:100%;" 
								    AutoPostBack="true" onselectedindexchanged="txtEmployeeType_SelectedIndexChanged">
								 </asp:DropDownList>
								 <asp:RequiredFieldValidator ControlToValidate="txtEmployeeType" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="txtEmployeeType" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="txtEmployeeType" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                 <asp:RequiredFieldValidator ControlToValidate="txtEmployeeType" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                               
                           <!-- end col-4 -->
                           <div class="col-md-2" runat="server" id="IF_FromDate_Hide">
								<div class="form-group">
								 <label>FromDate</label>
								  <asp:TextBox ID="txtfrom" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtfrom" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                               <div class="col-md-2" runat="server" id="IF_ToDate_Hide">
								<div class="form-group">
								 <label>ToDate</label>
								 <asp:TextBox ID="txtTo" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtTo" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                               <div class="col-md-3" runat="server" id="Div3">
								<div class="form-group">
								 <label>Token No</label>
								 <asp:DropDownList runat="server" ID="txtToken_No" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                        </div>
                    <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                               
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								 <label>Month</label>
                                    <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" Style="width: 100%;">
                                    </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
								</div>
                               </div>
                                <div class="col-md-2">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSearch" Text="Search" class="btn btn-success" ValidationGroup="Validate_PayField" OnClick="btnSearch_Click"/>
						    	 </div>
                               </div>
                               <div class="col-md-2"></div>
                               <div class="col-md-2">
                                    <div class="form-group">
                                        <br />
                                        <asp:CheckBox runat="server" ID="ChkSelect_ALL" class="form-control" 
                                            Text=" Select ALL" oncheckedchanged="ChkSelect_ALL_CheckedChanged" AutoPostBack="true"/>							 	 
                                    </div>
                               </div>
                               <div class="col-md-2">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="BtnMail_Sent" Text="Sent Mail" class="btn btn-success" ValidationGroup="Validate_PayField" 
									    OnClientClick="ProgressBarShow();" OnClick="BtnMail_Sent_Click"/>
						    	 </div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       
                          <div class="col-md-12">
                                        <div class="row">
                                            <asp:Repeater ID="Repeater_Payslip" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-hover table-striped RowTest">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>Token No</th>
                                                                <th>Name</th>
                                                                <th>Mail ID</th>
                                                                <th>Net Amount</th>
                                                                <th>Select</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("Token_No")%></td>
                                                        <td><%# Eval("EmpName")%></td>
                                                        <td><%# Eval("Mail_ID")%></td>
                                                        <td><%# Eval("Net_Pay")%></td>
                                                        <td>
                                                            <asp:CheckBox ID="ChkPay_Select" Text="" runat="server"/>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                    <div id="Download_loader" style="display:none"/></div>        
                                </ContentTemplate>
                                
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
    </section>
</div>
<!-- end #content -->

</asp:Content>

