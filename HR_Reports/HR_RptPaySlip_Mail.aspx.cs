﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;

using System.Collections.Generic;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Net.Mail;
using System.Net;

using iTextSharp.text;
using iTextSharp.text.pdf;

public partial class HR_Reports_HR_RptPaySlip_Mail : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    
    string SessionUserName;
    string SessionUserID;
    string SessionRights;

    List<byte[]> All_Module_Report_files_Add = new List<byte[]>();
    DataTable AutoDataTable = new DataTable();
    string Attach_File_Name = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = "";//Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Load_WagesType();
            Load_Token_No();
            Months_load();
            Initial_Data_Payslip_Referesh();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }

        Load_Payslip_OLD_data();
        //IFUser Work
        //IFUserType();
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        string Category_Str = "0";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            Category_Str = "1";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            Category_Str = "2";
        }
        query = "Select *from MstEmployeeType where EmpCategory='" + Category_Str + "'";
        if (SessionUserType == "2")
        {
            query = query + " And EmpType <> 'SUB-STAFF' And EmpType <> 'CIVIL' And EmpType <> 'OTHERS'";
        }

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    private void Load_Token_No()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtToken_No.Items.Clear();
        query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And Wages='" + txtEmployeeType.SelectedItem.Text + "' Order by ExistingCode Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtToken_No.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtToken_No.DataTextField = "ExistingCode";
        txtToken_No.DataValueField = "ExistingCode";
        txtToken_No.DataBind();
    }

    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }

    protected void txtEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Token_No();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select A.ExisistingCode as Token_No,B.FirstName as EmpName,B.Mail_ID,A.RoundOffNetPay as Net_Pay,'False' as Checked from SalaryDetails A";
        query = query + " inner join Employee_Mst B on A.ExisistingCode=B.ExistingCode And A.Ccode=B.CompCode And A.Lcode=B.LocCode";
        query = query + " where A.Ccode='" + SessionCcode + "' And A.Lcode='" + SessionLcode + "' And B.CompCode='" + SessionCcode + "' And B.LocCode='" + SessionLcode + "'";
        query = query + " And B.Wages='" + txtEmployeeType.SelectedItem.Text + "' And A.Month='" + ddlMonths.SelectedItem.Text + "' And A.FinancialYear='" + ddlFinance.SelectedValue + "'";
        query = query + " And (A.Mail_Status is null or A.Mail_Status='' or A.Mail_Status='0')";
        if (txtToken_No.SelectedItem.Text.ToString() != "-Select-")
        {
            query = query + " And A.ExisistingCode='" + txtToken_No.SelectedItem.Text + "'";
        }
        query = query + " Order by A.ExisistingCode Asc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            Repeater_Payslip.DataSource = DT;
            Repeater_Payslip.DataBind();
            ViewState["Payslip_Table"] = Repeater_Payslip.DataSource;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('No Record Found...');", true);
        }
    }

    private void Initial_Data_Payslip_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("Token_No", typeof(string)));
        dt.Columns.Add(new DataColumn("EmpName", typeof(string)));
        dt.Columns.Add(new DataColumn("Mail_ID", typeof(string)));
        dt.Columns.Add(new DataColumn("Net_Pay", typeof(string)));
        dt.Columns.Add(new DataColumn("Checked", typeof(string)));

        Repeater_Payslip.DataSource = dt;
        Repeater_Payslip.DataBind();
        ViewState["Payslip_Table"] = Repeater_Payslip.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_Payslip_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["Payslip_Table"];
        Repeater_Payslip.DataSource = dt;
        Repeater_Payslip.DataBind();
    }

    protected void BtnMail_Sent_Click(object sender, EventArgs e)
    {

        bool Mail_Checking = false;
        DataTable DT_RPT = new DataTable();
        DT_RPT = (DataTable)ViewState["Payslip_Table"];
        string query = "";
        try
        {
            for (int i = 0; i < Repeater_Payslip.Items.Count; i++)
            {
                CheckBox chk = (CheckBox)Repeater_Payslip.Items[i].FindControl("ChkPay_Select");
                if (chk.Checked == true)
                {
                    //Sent Mail Function Call   
                    All_Module_Report_files_Add.Clear();
                    string Token_No_Str = DT_RPT.Rows[i]["Token_No"].ToString();
                    PaySlip_Mail_Sendting_Single_Employee(DT_RPT.Rows[i]["Token_No"].ToString());
                    //Update
                    if (DT_RPT.Rows[i]["Mail_ID"].ToString() != "")
                    {
                        query = "Update SalaryDetails Set Mail_Status='1' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        query = query + " And Month='" + ddlMonths.SelectedItem.Text + "' And FinancialYear='" + ddlFinance.SelectedValue + "'";
                        query = query + " And ExisistingCode='" + Token_No_Str + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                        Mail_Checking = true;
                    }
                }

            }
            btnSearch_Click(sender, e);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
            if (Mail_Checking == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Mail Sent Successfully...');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Mail Not Sent...');", true);
            }
        }
        catch (Exception ex)
        {
            string Error_msg = ex.Message.ToString();
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Mail Sent Faild..." + Error_msg + "');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void ChkSelect_ALL_CheckedChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < Repeater_Payslip.Items.Count; i++)
        {
            ((CheckBox)Repeater_Payslip.Items[i].FindControl("ChkPay_Select")).Checked = ChkSelect_ALL.Checked;
            
        }
    }

    private void Payslip_Generate(string Token_No_Search)
    {
        string query = "";
        DataTable DT_Out = new DataTable();

        query = "Select EmpDet.UAN,AttnDet.Fixed_Work_Days, EmpDet.BankName,EmpDet.BranchCode,EmpDet.AccountNo,EmpDet.IFSC_Code,";
        query = query + " EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName,EmpDet.LastName as FatherName,";
        query = query + " EmpDet.DeptName,EmpDet.Designation,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,";
        query = query + " EmpDet.Basic_Sal_Part [FixBaseSal],EmpDet.DA_Sal_Part[FixDA],EmpDet.HRA_Sal_Part[FixHRA],";
        query = query + " EmpDet.WA_Sal_Part[FIXWA],EmpDet.SPL_Allow_Part[FixAllow],EmpDet.Other_Allow_Part[FixOtherAllow],";
        query = query + " EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber,AttnDet.TotalDays as Total_Month_Days,";
        query = query + " SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL,Cast(SalDet.WH_Work_Days as decimal(18,2))[WH_Work_Days],";
        query = query + " SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Conveyance,";
        query = query + " SalDet.Basic_Uniform as PF_Fixed_Sal,Cast(SalDet.Basic_Spl_Allowance as Decimal(18,2)) Basic_Spl_Allowance,";
        query = query + " Cast(SalDet.BasicRAI as decimal(8,2)) BasicRAI,Cast(SalDet.WashingAllow as Decimal(18,2))[WH],SalDet.allowances1,SalDet.allowances2,";
        query = query + " SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,";
        query = query + " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Misc,SalDet.Advance,";
        //query = query + " (Cast(SalDet.ProvidentFund as Decimal(18,2))+Cast(EmpDet.VPF as Decimal(18,2)))ProvidentFund,SalDet.ESI,";
        query = query + " SalDet.ProvidentFund,SalDet.ESI,";
        query = query + " SalDet.GrossEarnings,SalDet.TotalDeductions,SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,";
        query = query + " AttnDet.Permission_Hrs,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,";
        query = query + " cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2,SalDet.Month,SalDet.Year,SalDet.SalaryDate,";
        query = query + " AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,AttnDet.AEH,AttnDet.LBH,SalDet.SalaryThrough,";
        query = query + " SalDet.RoundOffNetPay,EmpDet.Account_Holder_Name,AttnDet.LWW_Days,SalDet.EmployeerPFone[EmpPFone],SalDet.EmployeerPFTwo[EmpPFTwo], ";
        query = query + " TDSAmt[TDS],SalDet.M_Ded1,SalDet.M_Ded2 From Employee_Mst EmpDet Inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo";
        query = query + " Inner Join AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        query = query + " Where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "'";
        query = query + " And AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND ";
        query = query + " AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and SalDet.Lcode='" + SessionLcode + "' And ";
        query = query + " AttnDet.Lcode='" + SessionLcode + "' And EmpDet.CompCode='" + SessionCcode + "' ";
        query = query + " and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'";
        query = query + " and EmpDet.ExistingCode='" + Token_No_Search + "'";
        //if (EmployeeType == "MIXING")
        //{
        //    query = query + " And Convert(Datetime,SalDet.FromDate,103)=Convert(Datetime,'" + fromdate + "',103)";
        //    query = query + " And Convert(Datetime,SalDet.ToDate,103)=Convert(Datetime,'" + ToDate + "',103)";

        //    query = query + " And Convert(Datetime,AttnDet.FromDate,103)=Convert(Datetime,'" + fromdate + "',103)";
        //    query = query + " And Convert(Datetime,AttnDet.ToDate,103)=Convert(Datetime,'" + ToDate + "',103)";
        //}
        
        
        //query = query + " and EmpDet.IsActive='Yes' Order by EmpDet.ExistingCode Asc";
        query = query + " Order by EmpDet.ExistingCode Asc";

        DT_Out = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Out.Rows.Count != 0)
        {
            string str_month = ddlMonths.SelectedItem.Text.ToString();
            string str_yr = ddlFinance.SelectedValue.ToString();
            int YR = 0;
            if (str_month == "January")
            {
                YR = Convert.ToInt32(str_yr);
                YR = YR + 1;
            }
            else if (str_month == "February")
            {
                YR = Convert.ToInt32(str_yr);
                YR = YR + 1;
            }
            else if (str_month == "March")
            {
                YR = Convert.ToInt32(str_yr);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(str_yr);
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(DT_Out);
            ReportDocument report_Payslip = new ReportDocument();

            DataTable dtdCompanyAddress = new DataTable();
            query = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(query);
            string Comps_name_Str = "";
            string Cmpaddress = "";
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                Comps_name_Str = dtdCompanyAddress.Rows[0]["Cname"].ToString() + " - " + SessionLcode;
                Cmpaddress = (dtdCompanyAddress.Rows[0]["Address1"].ToString() + ", " + dtdCompanyAddress.Rows[0]["Address2"].ToString() + ", " + dtdCompanyAddress.Rows[0]["location"].ToString() + "-" + dtdCompanyAddress.Rows[0]["Pincode"].ToString());
            }            
            report_Payslip.Load(Server.MapPath("~/HR_Payslip/Coral_Payslip_Cover_Mail.rpt"));

            report_Payslip.DataDefinition.FormulaFields["CompanyName"].Text = "'" + Comps_name_Str + "'";
            report_Payslip.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            report_Payslip.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
            report_Payslip.DataDefinition.FormulaFields["Month"].Text = "'" + ddlMonths.SelectedItem.Text.ToUpper().ToString() + "'";
            report_Payslip.DataDefinition.FormulaFields["Year"].Text = "'" + YR.ToString() + "'";
            report_Payslip.DataDefinition.FormulaFields["From_Date"].Text = "'" + txtfrom.Text.ToString() + "'";
            report_Payslip.DataDefinition.FormulaFields["To_Date"].Text = "'" + txtTo.Text.ToString() + "'";
            report_Payslip.DataDefinition.FormulaFields["Employee_Type"].Text = "'" + txtEmployeeType.SelectedItem.Text.ToString() + "'";


            report_Payslip.Database.Tables[0].SetDataSource(ds.Tables[0]);

            Stream stream1 = report_Payslip.ExportToStream(ExportFormatType.PortableDocFormat);

            //prepare the "bytes" from "stream"               
            All_Module_Report_files_Add.Add(PrepareBytes(stream1));
            //finally the result added to LIST
        }
    }

    //prepare the report bytes
    private byte[] PrepareBytes(Stream stream)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            byte[] buffer = new byte[stream.Length];
            int read;
            while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            return ms.ToArray();
        }
    }

    //Merge the reports 
    private MemoryStream MergeReports(List<byte[]> files)
    {
        if (files.Count > 1)
        {
            PdfReader pdfFile;
            Document doc;
            PdfWriter pCopy;
            MemoryStream msOutput = new MemoryStream();

            pdfFile = new PdfReader(files[0]);

            doc = new Document();
            pCopy = new PdfSmartCopy(doc, msOutput);

            doc.Open();

            for (int k = 0; k < files.Count; k++)
            {
                pdfFile = new PdfReader(files[k]);
                for (int i = 1; i < pdfFile.NumberOfPages + 1; i++)
                {
                    ((PdfSmartCopy)pCopy).AddPage(pCopy.GetImportedPage(pdfFile, i));
                }
                pCopy.FreeReader(pdfFile);
            }

            pdfFile.Close();
            pCopy.Close();
            doc.Close();

            //Response.Clear();
            //Response.Buffer = false;
            //Response.AppendHeader("Content-Type", "application/pdf");
            //Response.AppendHeader("Content-Transfer-Encoding", "binary");
            //Response.AppendHeader("Content-Disposition", "attachment; filename=test.pdf");
            ////Response.Write("C:\\" + pdfFile);
            //Response.End();
            return msOutput;
        }
        else if (files.Count == 1)
        {
            return new MemoryStream(files[0]);
        }

        return null;
    }

    private void PaySlip_Mail_Sendting_Single_Employee(string Token_No_Str)
    {
        
        string str_month = ddlMonths.SelectedItem.Text.ToString();
        string str_yr = ddlFinance.SelectedValue.ToString();
        int YR = 0;
        if (str_month == "January")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "February")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "March")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(str_yr);
        }

        Payslip_Generate(Token_No_Str);

        //mail code start
        //Get Mail Address
        string query = "";
        string To_Address = "";
        string CC_Address = "";
        string BCC_Address = "";
        string Mail_Subject = "Payslip for the Month of " + ddlMonths.SelectedItem.Text.ToUpper() + " " + YR.ToString();
        DataTable DT_Mail = new DataTable();
        query = "Select Mail_ID from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And ExistingCode='" + Token_No_Str + "'";
        DT_Mail = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Mail.Rows.Count != 0)
        {
            To_Address = DT_Mail.Rows[0]["Mail_ID"].ToString();
            
        }
        if (To_Address != "")
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

            //message.To.Add(To_Address);
            message.To.Add(To_Address);
            //if (CC_Address != "") { message.CC.Add(CC_Address); }
            //message.Bcc.Add(BCC_Address);
            message.Subject = Mail_Subject;

            message.From = new System.Net.Mail.MailAddress(str_month + " PaySlip Report<hr@coralmanufacturing.com>", "PAYSLIP MAIL REPORT");
            message.Body = "Find the attachment for " + Mail_Subject + ". This mail Auto-Generate Don't Reply...";
            //message.Attachments.Add(new Attachment("test.pdf", "application/pdf"));
            Attach_File_Name = "PaySlip_Reports_" + str_month.ToString() + "_" + YR.ToString();
            Attach_File_Name = Attach_File_Name + "_" + Token_No_Str.ToString() + ".pdf";

            message.Attachments.Add(new Attachment(new MemoryStream(MergeReports(All_Module_Report_files_Add).ToArray()), Attach_File_Name));

            message.IsBodyHtml = false;

            //SmtpClient client = new SmtpClient();

            //client.EnableSsl = true;
            //client.UseDefaultCredentials = false;
            //client.Credentials = new NetworkCredential("aatm2005@gmail.com", "Altius@2005");
            //client.Host = "smtp.gmail.com";
            //client.Port = 587;
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //client.Send(message);

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";   // We use gmail as our smtp client
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = new System.Net.NetworkCredential("hr@coralmanufacturing.com", "CMW@1234");

            smtp.Send(message);

            message.Attachments.Dispose();


            //Mail Code End

        }

        //Response.End();

    }
    
}
