﻿<%@ Page Title="Attendance Report Details" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HR_RptPayAttendance.aspx.cs" Inherits="HR_Reports_HR_RptPayAttendance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- begin #content -->
    <div id="content" class="content-wrapper">
        <section class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">Report</a></li>
                <li class="active">Attendance</li>
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Attendance</h1>
            <!-- end page-header -->

            <!-- begin row -->
            <div class="row">
                <!-- begin col-12 -->
                <div class="col-md-12">
                    <!-- begin panel -->
                    <div class="panel panel-inverse">

                        <div class="panel-body">
                            <div class="form-group">

                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2"
                                                Style="width: 100%;" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged">
                                                <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
                                                <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Employee Type</label>
                                            <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" Style="width: 100%;">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Division</label>
                                            <asp:DropDownList runat="server" ID="txtDivision" class="form-control select2" Style="width: 100%;">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                </div>
                                <!-- end row -->


                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-4 -->
                                    <div class="col-md-5" runat="server" id="IF_Attn_Rpt_Type">
                                        <div class="form-group">

                                            <asp:RadioButtonList ID="rdbAttnDaysType" runat="server" RepeatColumns="3" class="form-control">
                                                <asp:ListItem Selected="true" Text="Over All Attendance" style="padding-right: 30px" Value="0"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="Statutory Attendance" Value="1"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->

                                </div>
                                <!-- end row -->
                                <!-- begin row -->
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <br />
                                            <asp:Button runat="server" ID="btnAttnReport" Text="Report" class="btn btn-success" OnClick="btnAttnReport_Click" />

                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <div class="col-md-4"></div>
                                </div>
                                <!-- end row -->
                            </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </section>
    </div>
    <!-- end #content -->
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>

</asp:Content>

