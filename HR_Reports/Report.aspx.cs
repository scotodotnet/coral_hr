﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Drawing;

public partial class HR_Reports_Report : System.Web.UI.Page
{
    System.Web.UI.WebControls.DataGrid grid =
                   new System.Web.UI.WebControls.DataGrid();
    String CurrentYear1;
    static int CurrentYear;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode; string SessionUser;
    string SessionDivision; string SessionRights;
    BALDataAccess objdata = new BALDataAccess();
    //string SSQL = "";
    string ss1 = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;
    string Date1_str;
    string Date2_str;
    string Wages_Type;
    bool Errflag = false;
    bool Errflag1 = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionRights = "CORAL_ERP";//Session["Rights"].ToString();
            SessionUser = Session["Usernmdisplay"].ToString();

            con = new SqlConnection(constr);
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");

                // Drop_EmpCode();
                Drop_Shift();
                DropDown_Division();
                DropDown_WagesType();
                DropDown_Department();
                Drop_EmpCode();

                Financial_Year();
                Load_Route();
                btnAttendanceDetails.Visible = false;
                //if (SessionDivision != "")
                //{
                //    ddlDivision.SelectedItem.Text = SessionDivision;
                //    ddlDivision.Enabled = false;
                //    empdetails();

                //}
                // ListRptName_SelectedIndexChanged(sender, e);  
            }
        }
    }

    public void DropDown_WagesType()
    {
        DataTable dt = new DataTable();

        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }

    public void DropDown_Division()
    {
        DataTable dt = new DataTable();

        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDivision.Items.Clear();
        query = "Select *from Division_Master where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDivision.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Division"] = "0";
        dr["Division"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDivision.DataTextField = "Division";
        ddlDivision.DataValueField = "Division";
        ddlDivision.DataBind();


    }

    private void Load_Route()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlRouteNo.Items.Clear();
        query = "Select Distinct RouteName from RouteMst";
        if (ddlVehicleType.SelectedItem.Text != "ALL")
        {
            query = query + " where Type='" + ddlVehicleType.SelectedItem.Text + "'";
        }
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlRouteNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["RouteName"] = "ALL";
        dr["RouteName"] = "ALL";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlRouteNo.DataTextField = "RouteName";
        ddlRouteNo.DataValueField = "RouteName";
        ddlRouteNo.DataBind();
    }

    public void DropDown_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();


    }

    public void Drop_EmpCode()
    {
        DataTable dt = new DataTable();

        string SSQL;

        SSQL = "";
        SSQL = "Select ExistingCode +'->'+CONVERT(varchar(10), MachineID) +'->'+ FirstName As EmpName From Employee_Mst";
        SSQL = SSQL + " Where CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " and LocCode='" + SessionLcode + "' And IsActive='Yes' ";
        if (ddlDivision.SelectedItem.Text != "-Select-")
        {
            SSQL = SSQL + " and Division='" + ddlDivision.SelectedItem.Text + "' ";
        }

        //If UCase(mvarUserType) = UCase("IF User") Then
        //    SSQL = SSQL & " And PFNo <> '' And PFNo IS Not Null And PFNo <> '0' And PFNo <> 'o' And PFNo <> 'A'"
        //    SSQL = SSQL & " And PFNo <> 'NULL'"
        //End If

        //If mUserLocation <> "" Then
        //if (SessionUserType == "2")
        //{
        //    SSQL = SSQL + " And IsNonAdmin='1'";
        //}

        // End If

        SSQL = SSQL + " Order By EmpNo";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ddlEmpName.Items.Clear();
            ddlEmpName.Items.Add("- select -");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlEmpName.Items.Add(dt.Rows[i]["EmpName"].ToString());
            }
        }
    }
    public void empdetails()
    {
        DataTable dt = new DataTable();
        string Division = "";
        Division = ddlDivision.SelectedItem.Text;


        // ddlDivision.SelectedItem.Text = SessionDivision;



        string SSQL;

        SSQL = "";
        SSQL = "Select ExistingCode +'->'+CONVERT(varchar(10), MachineID) +'->'+ FirstName As EmpName From Employee_Mst";
        SSQL = SSQL + " Where CompCode='" + SessionCcode + "' and Division='" + ddlDivision.SelectedItem.Text + "'";
        SSQL = SSQL + " and LocCode='" + SessionLcode + "' And IsActive='Yes'";


        SSQL = SSQL + " Order By EmpNo";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            ddlEmpName.Items.Clear();
            ddlEmpName.Items.Add("- select -");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlEmpName.Items.Add(dt.Rows[i]["EmpName"].ToString());
            }
        }
    }

    protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();

        string Division = "";
        Division = ddlDivision.SelectedItem.Text;

        if (Division != "-Select-")
        {

            Drop_EmpCode();

        }


    }
    public void Financial_Year()
    {

        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        //if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        //{
        //    CurrentYear = CurrentYear - 1;
        //}
        for (int i = 0; i < 11; i++)
        {

            string tt = (CurrentYear1);
            ddlYear.Items.Add(tt.ToString());
            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;
        }
    }



    public void Drop_Shift()
    {
        string SSQL;
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "select ShiftDesc,StartTime,EndTime from Shift_Mst Where Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ddlShift.Items.Clear();

            ddlShift.Items.Add("ALL");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlShift.Items.Add(dt.Rows[i]["ShiftDesc"].ToString());
            }
            // ddlShift.Items.Add("NO SHIFT");
        }

    }

    protected void ddlRptName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (SessionUserType != "1" && SessionUserType != "4")
        {
            if (ddlRptName.SelectedItem.Text == "ALL")
            {

                string query = "";
                DataTable DT = new DataTable();
                query = "Select *from [" + SessionRights + "]..Report_User_Rights_HR where CompCode='" + SessionCcode + "'";
                query = query + " And LocCode='" + SessionLcode + "' And ReportType='ALL' And AddRights='1' And UserName='" + SessionUser + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                ListRptName.Items.Clear();
                if (DT.Rows.Count != 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        ListRptName.Items.Add(DT.Rows[i]["ReportName"].ToString());
                    }
                }

                //ListRptName.Items.Add("DAY ATTENDANCE WITH LUNCH - DAY WISE");
                //ListRptName.Items.Add("DAY ATTENDANCE - BETWEEN DATES");
                //ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE");
                //ListRptName.Items.Add("DAY ATTENDANCE TOKEN NO - DAY WISE");
                //ListRptName.Items.Add("LUNCH REPORT-DAYWISE");
                //// ListRptName.Items.Add("ABSENT REPORT - BETWEEN DATES");

                //// ListRptName.Items.Add("OT REPORT - BETWEEN DATES");
                //// ListRptName.Items.Add("PAYROLL ATTENDANCE");
                //ListRptName.Items.Add("PAYROLL ATTENDANCE");
                ////ListRptName.Items.Add("NEW PAYROLL ATTENDANCE");
                //ListRptName.Items.Add("MANUAL ATTENDANCE - BETWEEN DATES");
                //ListRptName.Items.Add("ALL EMPLOYEE PROFILE");


                //ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE :: BELOW HOURS");
                //ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE :: ABOVE HOURS");
                //ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE :: BETWEEN HOURS");
                //ListRptName.Items.Add("EMPLOYEE ID CARD FORMAT");
                ////ListRptName.Items.Add("BELOW FOUR HOURS");
                ////ListRptName.Items.Add("BELOW EIGHT HOURS");
                ////ListRptName.Items.Add("ABOVE FOURTEEN HOURS");
                ////ListRptName.Items.Add("ABSENT REPORT DAY WISE");

                ////ListRptName.Items.Add("LATE IN");
                ////ListRptName.Items.Add("LATE IN NEW");
                ////ListRptName.Items.Add("EARLY OUT");
                ////ListRptName.Items.Add("EARLY OUT NEW");
                ////ListRptName.Items.Add("EMPLOYEE LONG ABSENT REPORT");
                //ListRptName.Items.Add("MANUAL ATTENDANCE DAY WISE");
                // ListRptName.Items.Add("PAYROLL OT HOURS");
                ////ListRptName.Items.Add("DAY ATTENDANCE WITH OT HOURS");
                ////ListRptName.Items.Add("EMPLOYEE WISE MUSTER REPORT");
                ////ListRptName.Items.Add("LUNCH TIME REPORT");
                ////ListRptName.Items.Add("DEPARTMENT SALARY CONSOLDIDATE REPORT");
                ////  ListRptName.Items.Add("GRACE TIME DEDUCTION");
                ////ListRptName.Items.Add("OT EMPLOYEE LIST");
                // ListRptName.Items.Add("SALARY CONSOLDIDATE REPORT");
                //ListRptName.Items.Add("TOTAL DAYS SUMMARY REPORT");
                ////ListRptName.Items.Add("WEEKLY OT SLIP");
            }
            if (ddlRptName.SelectedItem.Text == "SPECIFIC")
            {
                string query = "";
                DataTable DT = new DataTable();
                query = "Select *from [" + SessionRights + "]..Report_User_Rights_HR where CompCode='" + SessionCcode + "'";
                query = query + " And LocCode='" + SessionLcode + "' And ReportType='SPECIFIC' And AddRights='1' And UserName='" + SessionUser + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                ListRptName.Items.Clear();
                if (DT.Rows.Count != 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        ListRptName.Items.Add(DT.Rows[i]["ReportName"].ToString());
                    }
                }
                //ListRptName.Items.Clear();
                //ListRptName.Items.Add("EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES");
                //ListRptName.Items.Add("EMPLOYEE FULL PROFILE");
                ////ListRptName.Items.Add("DEPARTMENT WISE EMPLOYEE DETAILS");
                ////ListRptName.Items.Add("DEPARTMENT SALARY CONSOLDIDATE REPORT");
                //// ListRptName.Items.Add("TOTAL DAYS SUMMARY REPORT");

            }

            if (ddlRptName.SelectedItem.Text == "ABSTRACT")
            {
                string query = "";
                DataTable DT = new DataTable();
                query = "Select *from [" + SessionRights + "]..Report_User_Rights_HR where CompCode='" + SessionCcode + "'";
                query = query + " And LocCode='" + SessionLcode + "' And ReportType='ABSTRACT' And AddRights='1' And UserName='" + SessionUser + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                ListRptName.Items.Clear();
                if (DT.Rows.Count != 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        ListRptName.Items.Add(DT.Rows[i]["ReportName"].ToString());
                    }
                }

                //ListRptName.Items.Clear();
                //ListRptName.Items.Add("DAY EMPLOYEE SUMMARY");
                //ListRptName.Items.Add("DAY ATTENDANCE SUMMARY");
                //ListRptName.Items.Add("MISMATCH SHIFT REPORT - DAY WISE");
                //ListRptName.Items.Add("EMPLOYEE NEW JOINING");
                //ListRptName.Items.Add("EMPLOYEE RESIGN");
                ////ListRptName.Items.Add("MALE FEMALE COUNT REPORT");
                //ListRptName.Items.Add("PRESENT ABSENT STRENGTH REPORT");
                //ListRptName.Items.Add("SALARY COVER REPORT");              
            }
            if (ddlRptName.SelectedItem.Text == "OTHERS")
            {
                string query = "";
                DataTable DT = new DataTable();
                query = "Select *from [" + SessionRights + "]..Report_User_Rights_HR where CompCode='" + SessionCcode + "'";
                query = query + " And LocCode='" + SessionLcode + "' And ReportType='OTHERS' And AddRights='1' And UserName='" + SessionUser + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                ListRptName.Items.Clear();
                if (DT.Rows.Count != 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        ListRptName.Items.Add(DT.Rows[i]["ReportName"].ToString());
                    }
                }

                //ListRptName.Items.Clear();

                //ListRptName.Items.Add("EMPLOYEE MASTER");
                //// ListRptName.Items.Add("BREAK TIME");
                //ListRptName.Items.Add("LEAVE DETAILS DAY ATTENDANCE - BETWEEN DATES");
                //ListRptName.Items.Add("GRACE TIME DAYWISE");
                //ListRptName.Items.Add("GRACE TIME DAY ATTENDANCE - BETWEEN DATES");
                //ListRptName.Items.Add("SHIFT REPORT");
                //ListRptName.Items.Add("UNMATCH-SHIFT REPORT");
                //ListRptName.Items.Add("WEEK-OFF REPORT");
                //ListRptName.Items.Add("FORM6-NFH");
                //ListRptName.Items.Add("NIGHT SHIFT REPORTS");

                //ListRptName.Items.Add("DEACTIVATED EMPLOYEE REPORT");
                //ListRptName.Items.Add("HOSTEL MALE FEMALE REPORT");
                //ListRptName.Items.Add("MACHINE MANIPULATION REPORT");
                //ListRptName.Items.Add("DAY ATTENDANCE CHART");
                //ListRptName.Items.Add("MALE FEMALE CHAT");
                //ListRptName.Items.Add("TEST1");
                //ListRptName.Items.Add("TEST2");
            }


        }
        else
        {
            if (ddlRptName.SelectedItem.Text == "ALL")
            {
                ListRptName.Items.Clear();
                ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE");
                //ListRptName.Items.Add("DAY ATTENDANCE - SHIFT WISE");
                //ListRptName.Items.Add("DAY ATTENDANCE TOKEN NO - DAY WISE");
                //ListRptName.Items.Add("DAY ATTENDANCE WITH LUNCH - DAY WISE");
                //ListRptName.Items.Add("LUNCH REPORT-DAYWISE");
                ListRptName.Items.Add("DAY ATTENDANCE - BETWEEN DATES");

                //ListRptName.Items.Add("ABSENT REPORT - BETWEEN DATES");

                //ListRptName.Items.Add("WEEKLY OT HOURS");

                ListRptName.Items.Add("PAYROLL ATTENDANCE");

                ListRptName.Items.Add("MANUAL ATTENDANCE - DAY DATES");
                //ListRptName.Items.Add("MANUAL ATTENDANCE - BETWEEN DATES");
                //ListRptName.Items.Add("ALL EMPLOYEE PROFILE");

                ListRptName.Items.Add("EMPLOYEE PROFILE");
                //ListRptName.Items.Add("BUS ROUTE");
                //ListRptName.Items.Add("REGULAR EMPLOYEE TRANSPORT");
                //ListRptName.Items.Add("SPINNING INCENTIVE DAYS");
                //ListRptName.Items.Add("SPINNING INCENTIVE SHIFT WISE");
                //ListRptName.Items.Add("LONG LEAVE ABSENT - BETWEEN DATES");
                ListRptName.Items.Add("EMPLOYEE ID CARD FORMAT");
                ListRptName.Items.Add("PARENTS ID CARD FORMAT");
                ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE :: BELOW HOURS");
                ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE :: ABOVE HOURS");
                //ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE :: BETWEEN HOURS");
                //ListRptName.Items.Add("SCHEDULED LEAVE REPORT");
                //ListRptName.Items.Add("DAY ATTENDANCE - DAY WISE WITH WORKING HOUSE");
                ////ListRptName.Items.Add("ABOVE FOURTEEN HOURS");
                ////ListRptName.Items.Add("ABSENT REPORT DAY WISE");

                ListRptName.Items.Add("LATE COMMERS");
                //ListRptName.Items.Add("LATE IN NEW");
                ////ListRptName.Items.Add("EARLY OUT");
                ////ListRptName.Items.Add("EARLY OUT NEW");
                ////ListRptName.Items.Add("EMPLOYEE LONG ABSENT REPORT");
                //ListRptName.Items.Add("MANUAL ATTENDANCE DAY WISE");
                // ListRptName.Items.Add("PAYROLL OT HOURS");
                ////ListRptName.Items.Add("DAY ATTENDANCE WITH OT HOURS");
                ////ListRptName.Items.Add("EMPLOYEE WISE MUSTER REPORT");
                ////ListRptName.Items.Add("LUNCH TIME REPORT");
                ////ListRptName.Items.Add("DEPARTMENT SALARY CONSOLDIDATE REPORT");
                //// ListRptName.Items.Add("GRACE TIME DEDUCTION");
                ////ListRptName.Items.Add("OT EMPLOYEE LIST");
                //  ListRptName.Items.Add("SALARY CONSOLDIDATE REPORT");
                //ListRptName.Items.Add("TOTAL DAYS SUMMARY REPORT");
                ////ListRptName.Items.Add("WEEKLY OT SLIP");
            }
            if (ddlRptName.SelectedItem.Text == "SPECIFIC")
            {
                ListRptName.Items.Clear();
                ListRptName.Items.Add("EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES");
                ListRptName.Items.Add("EMPLOYEE FULL PROFILE");
                ListRptName.Items.Add("EMPLOYEE HISTORY");
                ListRptName.Items.Add("EMPLOYEE CONFIRMATION ORDER");
                ListRptName.Items.Add("EMPLOYEE RELIEVING ORDER");
                //ListRptName.Items.Add("DEPARTMENT WISE EMPLOYEE DETAILS");
                ////ListRptName.Items.Add("DEPARTMENT SALARY CONSOLDIDATE REPORT");

            }

            if (ddlRptName.SelectedItem.Text == "ABSTRACT")
            {
                ListRptName.Items.Clear();
                //ListRptName.Items.Add("LUNCH REPORT BETWEEN DATES");
                //ListRptName.Items.Add("DAY ATTN. MAN DAYS WITH PERCENTAGE");
                //ListRptName.Items.Add("MAN DAYS REPORT ABSTRACT");
                ListRptName.Items.Add("DEPARTMENT ABSTRACT REPORT");
                ListRptName.Items.Add("DAY EMPLOYEE SUMMARY");
                ListRptName.Items.Add("DAY ATTENDANCE SUMMARY");
                ListRptName.Items.Add("MISMATCH SHIFT REPORT - DAY WISE");

                ListRptName.Items.Add("FORM-I WORK MEN PERMANENT");
                ListRptName.Items.Add("FORM6-NFH");
                //ListRptName.Items.Add("FORM-12");
                //ListRptName.Items.Add("FORM-14");
                ListRptName.Items.Add("FORM-15");
                ListRptName.Items.Add("FORM-25B");
                //ListRptName.Items.Add("ADOLESCENT REPORT");
                //ListRptName.Items.Add("SALARY COVER REPORT");
                //ListRptName.Items.Add("SALARY COVER ABSTRACT");
                //ListRptName.Items.Add("BONUS COVER REPORT");
                //ListRptName.Items.Add("Department Min & Max Wages");
                //ListRptName.Items.Add("Designation Wise Worker Employed");
                ListRptName.Items.Add("MALE FEMALE COUNT REPORT");
                //ListRptName.Items.Add("MONTHLY OT PAYSLIP");
                //ListRptName.Items.Add("MONTHLY OT CHECKLIST");
                //ListRptName.Items.Add("DAY EMPLOYEE SUMMARY FOR WORKER IN");
                //ListRptName.Items.Add("DAY EMPLOYEE SUMMARY BETWEEN DATES FOR WORKER IN");
                //ListRptName.Items.Add("EMPLOYEE NEW JOINING");
                //ListRptName.Items.Add("EMPLOYEE RESIGN");

                ////ListRptName.Items.Add("MALE FEMALE COUNT REPORT");
                //ListRptName.Items.Add("PRESENT ABSENT STRENGTH REPORT");
                //ListRptName.Items.Add("SALARY COVER REPORT");   

                //ListRptName.Items.Add("UNPAID SALARY COVER REPORT");
                //ListRptName.Items.Add("COVER ABSTRACT REPORT");
            }
            if (ddlRptName.SelectedItem.Text == "OTHERS")
            {
                ListRptName.Items.Clear();
                //ListRptName.Items.Add("HOSTEL BREAK TIME");
                //ListRptName.Items.Add("RECURITMENT SUMMARY REPORTS");
                //ListRptName.Items.Add("RECURITMENT DETAIL REPORTS");
                //ListRptName.Items.Add("LUNCH LATEIN REPORT");
                //ListRptName.Items.Add("LUNCH ImproperPunch");
                //ListRptName.Items.Add("LUNCH IMPROPER AND LATEIN BETWEEN DATES");
                //ListRptName.Items.Add("CommunityWise Employee Count");
                //ListRptName.Items.Add("EMPLOYEE MASTER");
                //// ListRptName.Items.Add("BREAK TIME");
                //ListRptName.Items.Add("LEAVE DETAILS DAY ATTENDANCE - BETWEEN DATES");
                //ListRptName.Items.Add("GRACE TIME DAYWISE");
                //ListRptName.Items.Add("GRACE TIME DAY ATTENDANCE - BETWEEN DATES");
                //ListRptName.Items.Add("SHIFT REPORT");
                //ListRptName.Items.Add("UNMATCH-SHIFT REPORT");
                //ListRptName.Items.Add("WEEK-OFF REPORT");
                //ListRptName.Items.Add("FORM6-NFH");
                //ListRptName.Items.Add("NIGHT SHIFT REPORTS");
                //ListRptName.Items.Add("DEACTIVATED EMPLOYEE REPORT");
                //ListRptName.Items.Add("HOSTEL MALE FEMALE REPORT");
                //ListRptName.Items.Add("MACHINE MANIPULATION REPORT");
                //ListRptName.Items.Add("DAY ATTENDANCE CHART");
                //ListRptName.Items.Add("MALE FEMALE CHAT");
                //ListRptName.Items.Add("TEST1");
                //ListRptName.Items.Add("TEST2");


            }
        }

    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        string status;
        if (chkapproval.Checked == true)
        {
            status = "Approval";
        }
        else
        {
            status = "Pending";
        }
        if (RptLabel.Text == "APPOINTMENT LETTER")
        {

            if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            //else if (txtTodate.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            //}
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("HR_AppointmentLetter.aspx?FromDate=" + txtFrmdate.Text, "_blank", "");
            }
        }    
        if (RptLabel.Text == "EMPLOYEE RELIEVING ORDER")
        {
            if (txtTokenNo.Text == "" || ddlEmpName.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Employee Token Number');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_RptEmployee_Relieving_Order.aspx?Status=" + status + " &TokenNo=" + txtTokenNo.Text + "&Division=" + ddlDivision.SelectedItem.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "EMPLOYEE CONFIRMATION ORDER")
        {
            if (txtTokenNo.Text == "" || ddlEmpName.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Employee Token Number');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_RptEmployee_Confirm_Order.aspx?Status=" + status + " &TokenNo=" + txtTokenNo.Text + "&Division=" + ddlDivision.SelectedItem.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "EMPLOYEE PROFILE")
        {
            ResponseHelper.Redirect("HR_EmployeeProfile.aspx?Division=" + ddlDivision.SelectedItem.Text, "_blank", "");
        }

        if (RptLabel.Text == "DAY ATTN. MAN DAYS WITH PERCENTAGE")
        {
            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("DayAttnManDaysWithPer.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "FORM-I WORK MEN PERMANENT")
        {
            string Wages = "";
            string TempWages = "";
            if (ddlWagesType.SelectedItem.Text != "-Select-")
            {
                Wages = ddlWagesType.SelectedItem.Text;
            }
            if (Wages != "")
            {
                TempWages = Wages.Replace("&", "_");
            }
            ResponseHelper.Redirect("HR_FormI_WorkMenPermanent_PDF.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + TempWages, "_blank", "");

        }
        if (RptLabel.Text == "FORM-15")
        {
            if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else if (ddlYear.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Year');", true);
            }
            else
            {
                //DateTime date1;
                //DateTime date2;
                //date1 = Convert.ToDateTime(txtFrmdate.Text);
                //date2 = Convert.ToDateTime(txtTodate.Text);
                //string monthname = date1.ToString("MMMM");
                //string Year = ddlYear.SelectedItem.Text;
                //string[] Fyear = Year.Split('-');
                //string First_Year = Fyear[0];
                //string Second_Year = Fyear[1];

                //string FromDate = "01/04/" + First_Year.ToString();
                //string Todate = "31/03/" + Second_Year.ToString();

                //DateTime FDate = Convert.ToDateTime(FromDate.ToString());
                //DateTime TDate = Convert.ToDateTime(Todate.ToString());


                //if (date1 >= FDate && date2 <= TDate)
                //{
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("HR_Form_15_PDF_Rpt.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + TempWages + "&Year=" + ddlYear.SelectedItem.Text, "_blank", "");

                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check Finacial Year');", true);
                //}

                //ResponseHelper.Redirect("Form15.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + ddlWagesType.SelectedItem.Text + "&Year=" + ddlYear.SelectedItem.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "MAN DAYS REPORT ABSTRACT")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("ManDayReportAbstract.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "LUNCH REPORT BETWEEN DATES")
        {
            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                //ResponseHelper.Redirect("MusterReportBWDates.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
                ResponseHelper.Redirect("LunchReportBWDates.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages + "&Status=" + status + "&TokenNo=" + txtTokenNo.Text, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }

        if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE WITH WORKING HOUSE")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("DayAttendanceWorkingHouseReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Status=" + status + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }



        if (RptLabel.Text == "DAY EMPLOYEE SUMMARY FOR WORKER IN")
        {
            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("improperWorkerIn.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&status=" + status + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "MALE FEMALE COUNT REPORT")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_MaleFemaleCountReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_DayAttendanceDayWise.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Status=" + status + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "DEPARTMENT ABSTRACT REPORT")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_DepartmentAbstractRpt.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "LUNCH REPORT-DAYWISE")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("LunchReportDaywise.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&status=" + status + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "EMPLOYEE FULL PROFILE")
        {
            if (ddlEmpName.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select EmpName');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_EmployeeFull.aspx?Division=" + ddlDivision.SelectedItem.Text + "&status=" + status + "&Empcode=" + ddlEmpName.SelectedItem.Text, "_blank", "");
            }
        }


        if (RptLabel.Text == "EMPLOYEE HISTORY")
        {
            if (ddlEmpName.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select EmpName');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_EmployeeHistory.aspx?Division=" + ddlDivision.SelectedItem.Text + "&status=" + status + "&Empcode=" + ddlEmpName.SelectedItem.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "EMPLOYEE JOIN AND RELEAVING DETTAILS")
        {
            if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Wages Type');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_EmployeeJoinReleavingDet.aspx?Wages=" + ddlWagesType.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "MISMATCH SHIFT REPORT - DAY WISE")
        {

            //if (ddlShift.SelectedItem.Text == "ALL")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            //}else

            if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_MismatchReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&status=" + status + "&FDate=" + txtFrmdate.Text + "&TDate=" + txtTodate.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "DAY ATTENDANCE WITH LUNCH - DAY WISE")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("DayAttendancewithLunch.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&status=" + status + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "DAY ATTENDANCE TOKEN NO - DAY WISE")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("DayAttendanceTokenWise.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Status=" + status + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }


        if (RptLabel.Text == "DAY EMPLOYEE SUMMARY")
        {
            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_improper.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&status=" + status + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "WEEKLY OT HOURS")
        {

            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                ResponseHelper.Redirect("WeeklyOTHours.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }
        if (RptLabel.Text == "MONTHLY OT PAYSLIP")
        {

            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                ResponseHelper.Redirect("WeeklyOTHours_Monthly.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }
        if (RptLabel.Text == "MONTHLY OT CHECKLIST")
        {

            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                ResponseHelper.Redirect("WeeklyOTHours_Monthly_Checklist.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }
        if (RptLabel.Text == "MANUAL ATTENDANCE - DAY DATES")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                //ResponseHelper.Redirect("ManualAttendanceDayWise.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text, "_blank", "");
                ResponseHelper.Redirect("HR_ManualAttendanceDayWise.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text + "&Status=" + status, "_blank", "");
            }
        }
        if (RptLabel.Text == "EMPLOYEE ID CARD FORMAT")
        {

            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the WagesType');", true);
            }
            else if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                ResponseHelper.Redirect("HR_EmployeeIDCardFormat.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }
        if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE :: BELOW HOURS")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtbelowHrs.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Below Hour');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_DayAttendanceDayWiseBelowFourHours.aspx?Division=" + ddlDivision.SelectedItem.Text + "&Below=" + txtbelowHrs.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }


        if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE :: ABOVE HOURS")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtAboveHrs.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Above Hour');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_DayAttendanceDayWiseAboveHours.aspx?Division=" + ddlDivision.SelectedItem.Text + "&Above=" + txtAboveHrs.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }


        if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE :: BETWEEN HOURS")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("DayAttendanceDayWiseBetweenHours.aspx?Division=" + ddlDivision.SelectedItem.Text + "&Above=" + txtAboveHrs.Text + "&Below=" + txtbelowHrs.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }


        if (RptLabel.Text == "Department Min & Max Wages")
        {


            if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the WagesType');", true);
            }
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("DeptMinMaxWages.aspx?Division=" + ddlDivision.SelectedItem.Text + "&Wages=" + TempWages, "_blank", "");
            }
        }

        //if (RptLabel.Text == "SALARY COVER REPORT")
        //{

        //    if (txtFrmdate.Text == "" && txtTodate.Text == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);

        //    }

        //    else
        //    {

        //        ResponseHelper.Redirect("SalaryCoverReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
        //    }
        //}

        if (RptLabel.Text == "SALARY COVER REPORT")
        {
            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");

            if (txtFrmdate.Text == "" && txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);

            }
            else if (txtpayfromdate.Text == "" && txtpaytodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter PayFromDate and PayToDate');", true);

            }
            else if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the WagesType');", true);
            }
            else if (txtpayfromdate.Text == "" && txtpaytodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter PayFromDate and PayToDate');", true);

            }
            else if (txtLeft_Date.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Left Date');", true);

            }
            else
            {

                ResponseHelper.Redirect("SalaryCoverReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&PayFromDate=" + txtpayfromdate.Text + "&payToDate=" + txtpaytodate.Text + "&LeftDate_Str=" + txtLeft_Date.Text + "&Wages=" + TempWages, "_blank", "");
            }
        }
        if (RptLabel.Text == "SALARY COVER ABSTRACT")
        {
            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");

            if (txtFrmdate.Text == "" && txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);

            }
            else if (txtpayfromdate.Text == "" && txtpaytodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter PayFromDate and PayToDate');", true);

            }
            else if (txtLeft_Date.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select The Left Date...');", true);
            }

            else
            {

                ResponseHelper.Redirect("SalaryCoverAbstract.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&PayFromDate=" + txtpayfromdate.Text + "&LeftDate_Str=" + txtLeft_Date.Text + "&payToDate=" + txtpaytodate.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "BONUS COVER REPORT")
        {
            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (ddlYear.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Financial Year');", true);
            }
            else if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the WagesType');", true);
            }
            else if (txtpayfromdate.Text == "" && txtpaytodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter PayFromDate and PayToDate');", true);

            }
            else
            {
                ResponseHelper.Redirect("BonusCoverReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&Year=" + ddlYear.SelectedItem.Text + "&PayFromDate=" + txtpayfromdate.Text + "&payToDate=" + txtpaytodate.Text + "&Wages=" + TempWages, "_blank", "");
            }
        }

        if (RptLabel.Text == "SPINNING INCENTIVE DAYS")
        {

            if (txtFrmdate.Text == "" && txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);

            }
            else if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the WagesType');", true);
            }
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("SpinningIncentiveDays.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
            }
        }
        if (RptLabel.Text == "SPINNING INCENTIVE SHIFT WISE")
        {

            if (txtFrmdate.Text == "" && txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);

            }
            else if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the WagesType');", true);
            }
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("SpinningIncentiveShiftWise.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
            }
        }
        if (RptLabel.Text == "Designation Wise Worker Employed")
        {

            if (txtFrmdate.Text == "" && txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);

            }

            else
            {

                ResponseHelper.Redirect("DesignationWiseWorker.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "LUNCH ImproperPunch")
        {
            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("Lunchimproper.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "LUNCH LATEIN REPORT")
        {

            if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }

            else
            {
                ResponseHelper.Redirect("LunchLateinReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&FromDate=" + txtFrmdate.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "CommunityWise Employee Count")
        {
            if (txtFrmdate.Text == "" || txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and Todate ');", true);
            }
            else
            {
                ResponseHelper.Redirect("CommunitywiseEmployeeCount.aspx?Division=" + ddlDivision.SelectedItem.Text + " &FromDate=" + txtFrmdate.Text + " &ToDate=" + txtTodate.Text, "_blank", "");
            }

        }

        if (RptLabel.Text == "PARENTS ID CARD FORMAT")
        {

            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the WagesType');", true);
            }
            else if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                ResponseHelper.Redirect("HR_ParentsIDCardFormat.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }
        if (RptLabel.Text == "SCHEDULED LEAVE REPORT")
        {
            string TempWages = "";
            ResponseHelper.Redirect("ScheduledLeaveRpt.aspx?Wages=" + TempWages, "_blank", "");
        }
        if (RptLabel.Text == "UNPAID SALARY COVER REPORT")
        {

            if (txtFrmdate.Text == "" || txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("UnpaidSalaryCoverReport.aspx?RptName=" + RptLabel.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "COVER ABSTRACT REPORT")
        {

            if (txtFrmdate.Text == "" || txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("UnpaidSalaryCoverReport.aspx?RptName=" + RptLabel.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
            }
        }
    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        string status;
        if (chkapproval.Checked == true)
        {
            status = "Approval";
        }
        else
        {
            status = "Pending";
        }
        
        //LATE IN
        if (RptLabel.Text == "LATE IN")
        {
            if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("HR_ReportDisplay.aspx?Division=" + ddlDivision.SelectedItem.Text + "&RptName=" + RptLabel.Text + "&FromDate=" + txtFrmdate.Text, "_blank", "");

            }
        }
        if (RptLabel.Text == "HOSTEL BREAK TIME")
        {
            if (txtFrmdate.Text == "" && txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate And  Todate');", true);
            }
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("HostelBreakTimeReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");

            }
        }
        if (RptLabel.Text == "LONG LEAVE ABSENT - BETWEEN DATES")
        {
            if (txtLLeaveDays.Text == " ")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Leave Days');", true);
            }
            else if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }

            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("LongLeaveDaysReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&Wages=" + TempWages + "&LeaveDays=" + txtLLeaveDays.Text, "_blank", "");

            }

        }

        if (RptLabel.Text == "DAY EMPLOYEE SUMMARY BETWEEN DATES FOR WORKER IN")
        {
            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                //ResponseHelper.Redirect("MusterReportBWDates.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
                ResponseHelper.Redirect("ImproperWorkersBWDates.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages + "&Status=" + status + "&TokenNo=" + txtTokenNo.Text, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }

        if (RptLabel.Text == "BUS ROUTE")
        {

            ResponseHelper.Redirect("BusRoute.aspx?Division=" + ddlDivision.SelectedItem.Text, "_blank", "");

        }
        if (RptLabel.Text == "REGULAR EMPLOYEE TRANSPORT")
        {
            if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                ResponseHelper.Redirect("RegularEmployeeTransport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&BusNo=" + ddlRouteNo.SelectedItem.Text + "&Vehicles_Type=" + ddlVehicleType.SelectedItem.Text + "&Shift=" + ddlShift.SelectedItem.Text + "&Date=" + txtFrmdate.Text, "_blank", "");

            }

        }

        if (RptLabel.Text == "LUNCH REPORT BETWEEN DATES")
        {
            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                //ResponseHelper.Redirect("MusterReportBWDates.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
                ResponseHelper.Redirect("LunchReportBWDates.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages + "&Status=" + status + "&TokenNo=" + txtTokenNo.Text, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }

        if (RptLabel.Text == "SPINNING INCENTIVE SHIFT WISE")
        {

            if (txtFrmdate.Text == "" && txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);

            }
            else if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the WagesType');", true);
            }
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("SpinningIncentiveShiftWise.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
            }
        }

        if (RptLabel.Text == "DAY ATTENDANCE - BETWEEN DATES")
        {
            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                //ResponseHelper.Redirect("MusterReportBWDates.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
                ResponseHelper.Redirect("HR_MusterReportBWDates.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages + "&Status=" + status + "&TokenNo=" + txtTokenNo.Text, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }
        if (RptLabel.Text == "DAY ATTENDANCE SUMMARY")
        {
            if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter From Date');", true);
            }

            else
            {
                ResponseHelper.Redirect("HR_DayAttendanceSummary.aspx?Division=" + ddlDivision.SelectedItem.Text + "&Date1=" + txtFrmdate.Text, "_blank", "");
            }
        }



        if (RptLabel.Text == "MANUAL ATTENDANCE - BETWEEN DATES")
        {
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                //ResponseHelper.Redirect("ManualAttendanceReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
                ResponseHelper.Redirect("ManualAttendanceReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Status=" + status, "_blank", "");

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }


        if (RptLabel.Text == "EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES")
        {
            if (ddlEmpName.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select EmpName');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }

            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else
            {
                string s = ddlEmpName.SelectedItem.Text;
                string[] delimiters = new string[] { "->" };
                string[] items = s.Split(delimiters, StringSplitOptions.None);
                string ss = items[0];
                ss1 = items[1];

                ResponseHelper.Redirect("HR_EmployeeDaywiseAttendance.aspx?Division=" + ddlDivision.SelectedItem.Text + "&EmpCode=" + ss1 + "&status=" + status + "&Date1=" + txtFrmdate.Text + "&Date2=" + txtTodate.Text, "_blank", "");
            }

        }


        if (RptLabel.Text == "PAYROLL ATTENDANCE")
        {
            if (txtFrmdate.Text == "" && txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
            else if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else
            {

                //ResponseHelper.Redirect("HR_PayrollAttendance_New.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&Todate=" + txtTodate.Text + "&wages=" + ddlWagesType.SelectedItem.Text, "_blank", "");
                ResponseHelper.Redirect("HR_PayrollAttendance_New.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&Todate=" + txtTodate.Text + "&wages=" + ddlWagesType.SelectedValue, "_blank", "");
            }

        }
        if (RptLabel.Text == "ABSENT REPORT - BETWEEN DATES")
        {
            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                ResponseHelper.Redirect("AbsentReportBWDates.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Wages=" + TempWages, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
        }

        if (RptLabel.Text == "FORM-I WORK MEN PERMANENT")
        {
            if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("HR_FormI-WorkMenPermanent.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + TempWages, "_blank", "");
            }
        }
        if (RptLabel.Text == "FORM-12")
        {
            if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("Form12.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + TempWages, "_blank", "");
            }
        }
        if (RptLabel.Text == "FORM-14")
        {
            if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("Form14.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + TempWages, "_blank", "");
            }
        }
        if (RptLabel.Text == "FORM-15")
        {
            if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else if (ddlYear.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Year');", true);
            }
            else
            {
                //DateTime date1;
                //DateTime date2;
                //date1 = Convert.ToDateTime(txtFrmdate.Text);
                //date2 = Convert.ToDateTime(txtTodate.Text);
                //string monthname = date1.ToString("MMMM");
                //string Year = ddlYear.SelectedItem.Text;
                //string[] Fyear = Year.Split('-');
                //string First_Year = Fyear[0];
                //string Second_Year = Fyear[1];

                //string FromDate = "01/04/" + First_Year.ToString();
                //string Todate = "31/03/" + Second_Year.ToString();

                //DateTime FDate = Convert.ToDateTime(FromDate.ToString());
                //DateTime TDate = Convert.ToDateTime(Todate.ToString());


                //if (date1 >= FDate && date2 <= TDate)
                //{
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("HR_Form15.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + TempWages + "&Year=" + ddlYear.SelectedItem.Text, "_blank", "");

                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check Finacial Year');", true);
                //}

                //ResponseHelper.Redirect("Form15.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + ddlWagesType.SelectedItem.Text + "&Year=" + ddlYear.SelectedItem.Text, "_blank", "");
            }
        }

        if (RptLabel.Text == "FORM6-NFH")
        {
            if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else if (ddlYear.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Year');", true);
            }
            else
            {

                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("HR_Form6H.aspx?WagesType=" + TempWages + "&Year=" + ddlYear.SelectedItem.Text, "_blank", "");

                //ResponseHelper.Redirect("Form15.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + ddlWagesType.SelectedItem.Text + "&Year=" + ddlYear.SelectedItem.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "FORM-25B")
        {
            string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
            if (txtFrmdate.Text == "" && txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate and ToDate');", true);
            }
            else if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else
            {
                ResponseHelper.Redirect("HR_Form25B.aspx?Division=" + ddlDivision.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&TokenNo=" + txtTokenNo.Text + "&Wages=" + TempWages, "_blank", "");

            }
        }
        if (RptLabel.Text == "ADOLESCENT REPORT")
        {
            if (ddlTypeofCertificate.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the CertifiCate Type');", true);
            }
            else if (ddlWagesType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else
            {
                string TempWages = ddlWagesType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("AdolescentReport.aspx?Division=" + ddlDivision.SelectedItem.Text + "&AdolescentType=" + ddlTypeofCertificate.SelectedItem.Text + "&Wages=" + TempWages, "_blank", "");

            }

        }

        if (RptLabel.Text == "RECURITMENT SUMMARY REPORTS")
        {

            ResponseHelper.Redirect("RecuritmentSummaryReport.aspx?Division=" + ddlDivision.SelectedItem.Text, "_blank", "");

        }

        if (RptLabel.Text == "RECURITMENT DETAIL REPORTS")
        {

            ResponseHelper.Redirect("RecuritmentDetailsReport.aspx?Division=" + ddlDivision.SelectedItem.Text, "_blank", "");

        }
        if (RptLabel.Text == "LUNCH IMPROPER AND LATEIN BETWEEN DATES")
        {
            if (txtFrmdate.Text == "" && txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate And  Todate');", true);
            }
            else if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else
            {

                ResponseHelper.Redirect("LunchimproperandLateinBetweenDates.aspx?Division=" + ddlDivision.SelectedItem.Text + "&Shift=" + ddlShift.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");

            }
        }
        if (RptLabel.Text == "DAY ATTENDANCE - SHIFT WISE")
        {

            if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate ');", true);
            }
            else if (ddlShift.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift');", true);
            }
            else
            {

                ResponseHelper.Redirect("DayAttendanceShiftWise.aspx?Division=" + ddlDivision.SelectedItem.Text + "&Shift=" + ddlShift.SelectedItem.Text + "&Date=" + txtFrmdate.Text + "&Status=" + status, "_blank", "");

            }
        }


    }

    protected void deactive_Click(object sender, EventArgs e)
    {
        //  ResponseHelper.Redirect("DeactiveReport.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Division=" + ddlDivision.SelectedItem.Text, "_blank", "");
    }
    protected void Mail_Click(object sender, EventArgs e)
    {
    }
    protected void btnAttendanceDetails_Click(object sender, EventArgs e)
    {
    }
    protected void btnEmployee_Click(object sender, EventArgs e)
    {
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
    }

    protected void RdbGendarF_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void RdbGendarM_CheckedChanged(object sender, EventArgs e)
    {
    }
    protected void OTNo_CheckedChanged(object sender, EventArgs e)
    {
    }
    protected void OTYes_CheckedChanged(object sender, EventArgs e)
    {
    }

    protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {

    }



    protected void ListRptName_SelectedIndexChanged(object sender, EventArgs e)
    {
        RptLabel.Text = ListRptName.SelectedItem.Text;
        txtLeft_Date.Enabled = false;
        //EMPLOYEE RELIEVING ORDER
        if (RptLabel.Text == "APPOINTMENT LETTER")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "LATE IN")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "EMPLOYEE RELIEVING ORDER")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "EMPLOYEE CONFIRMATION ORDER")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "RECURITMENT SUMMARY REPORTS")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE WITH WORKING HOUSE")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            // Clear();
        }
        if (RptLabel.Text == "DAY EMPLOYEE SUMMARY FOR WORKER IN")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();

        }
        if (RptLabel.Text == "DAY EMPLOYEE SUMMARY BETWEEN DATES FOR WORKER IN")
        {

            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "RECURITMENT DETAIL REPORTS")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "MALE FEMALE COUNT REPORT")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
        }

        if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE" || RptLabel.Text == "MAN DAYS REPORT ABSTRACT" || RptLabel.Text == "DAY ATTN. MAN DAYS WITH PERCENTAGE" || RptLabel.Text == "DAY ATTENDANCE WITH LUNCH - DAY WISE" || RptLabel.Text == "LUNCH REPORT-DAYWISE" || RptLabel.Text == "DAY ATTENDANCE TOKEN NO - DAY WISE" || RptLabel.Text == "MANUAL ATTENDANCE - DAY DATES" || RptLabel.Text == "DEPARTMENT ABSTRACT REPORT")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            // Clear();
        }
        if (RptLabel.Text == "Designation Wise Worker Employed")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "FORM6-NFH")
        {

            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            ddlYear.Enabled = true;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "FORM-12" || RptLabel.Text == "FORM-14")
        {

            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "FORM-I WORK MEN PERMANENT")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "ADOLESCENT REPORT")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = true;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            // Clear();
        }

        if (RptLabel.Text == "FORM-15")
        {
            btnAttendanceDetails.Visible = false;
            btnReport.Enabled = true;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;
            ddlYear.Enabled = true;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "BUS ROUTE")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "REGULAR EMPLOYEE TRANSPORT")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = true;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = true;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }



        if (RptLabel.Text == "DAY ATTENDANCE - BETWEEN DATES" || RptLabel.Text == "PAYROLL ATTENDANCE" || RptLabel.Text == "ABSENT REPORT - BETWEEN DATES" || RptLabel.Text == "FORM-25B")
        {

            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "DAY ATTENDANCE SUMMARY")
        {
            btnAttendanceDetails.Visible = false;
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "EMPLOYEE PROFILE")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "LUNCH REPORT BETWEEN DATES")
        {

            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE :: ABOVE HOURS")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = true;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE :: BELOW HOURS")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = true;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "DAY ATTENDANCE - DAY WISE :: BETWEEN HOURS")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = true;
            txtbelowHrs.Enabled = true;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }


        if (RptLabel.Text == "EMPLOYEE ID CARD FORMAT")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "PARENTS ID CARD FORMAT")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "Department Min & Max Wages")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "SALARY COVER REPORT")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;
            txtLeft_Date.Enabled = true;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = true;
            txtpaytodate.Enabled = true;
            Clear();
        }
        if (RptLabel.Text == "UNPAID SALARY COVER REPORT" || RptLabel.Text == "COVER ABSTRACT REPORT")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "SALARY COVER ABSTRACT")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = false;
            txtLeft_Date.Enabled = true;
            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = true;
            txtpaytodate.Enabled = true;
            Clear();
        }

        if (RptLabel.Text == "BONUS COVER REPORT")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = true;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = true;
            txtpaytodate.Enabled = true;
            Clear();
        }

        if (RptLabel.Text == "SPINNING INCENTIVE DAYS")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "SPINNING INCENTIVE SHIFT WISE")
        {

            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "LONG LEAVE ABSENT - BETWEEN DATES")
        {

            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = true;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        //if (RptLabel.Text == "DAY ATTENDANCE SHIFT WISE")
        //{
        //    btnAttendanceDetails.Visible = false;
        //    btnReport.Enabled = false;
        //    btnExcel.Enabled = true;
        //    ddlEmpName.Enabled = false;
        //    ddlDepartment.Enabled = false;
        //    ddlWagesType.Enabled = false;

        //    ddlShift.Enabled = true;
        //    ddlYear.Enabled = false;
        //    ddlIsAct.Enabled = false;
        //    ddlCategory.Enabled = false;
        //    txtTypeOfCertificate.Enabled = false;
        //    txtleavedays.Enabled = false;
        //    txtFrmdate.Enabled = true;
        //    txtTodate.Enabled = false;
        //    OTYes.Enabled = false;
        //    OTNo.Enabled = false;
        //    RdbGendarM.Enabled = false;
        //    RdbGendarF.Enabled = false;

        //    ddlVehicleType.Enabled = false;
        //    ddlTypeofCertificate.Enabled = false;
        //    ddlShiftType.Enabled = false;
        //    ddlRouteNo.Enabled = false;
        //    txtLLeaveDays.Enabled = false;

        //    Clear();
        //}
        if (RptLabel.Text == "MANUAL ATTENDANCE - BETWEEN DATES")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }


        if (RptLabel.Text == "EMPLOYEE FULL PROFILE")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = true;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "EMPLOYEE HISTORY")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = true;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "EMPLOYEE JOIN AND RELEAVING DETTAILS")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES")
        {
            btnAttendanceDetails.Visible = false;
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = true;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "MISMATCH SHIFT REPORT - DAY WISE" || RptLabel.Text == "DAY EMPLOYEE SUMMARY")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = true;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();

        }
        if (RptLabel.Text == "WEEKLY OT HOURS")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "MONTHLY OT PAYSLIP")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "MONTHLY OT CHECKLIST")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = true;

            ddlShift.Enabled = false;

            //ddlShift.BackColor = Color.Black;

            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            //btnAttendanceDetails.Visible = true;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "HOSTEL BREAK TIME")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            // Clear();
        }
        if (RptLabel.Text == "LUNCH LATEIN REPORT" || RptLabel.Text == "LUNCH ImproperPunch")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            Clear();
        }



        if (RptLabel.Text == "LUNCH IMPROPER AND LATEIN BETWEEN DATES")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            Clear();
        }
        if (RptLabel.Text == "CommunityWise Employee Count")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            Clear();
        }

        if (RptLabel.Text == "SCHEDULED LEAVE REPORT")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;

            ddlShift.Enabled = false;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;
            btnAttendanceDetails.Visible = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
        }
        if (RptLabel.Text == "DAY ATTENDANCE - SHIFT WISE")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlEmpName.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlWagesType.Enabled = false;
            btnAttendanceDetails.Visible = false;
            ddlShift.Enabled = true;
            ddlYear.Enabled = false;
            ddlIsAct.Enabled = false;
            ddlCategory.Enabled = false;
            txtTypeOfCertificate.Enabled = false;
            txtleavedays.Enabled = false;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = false;
            OTYes.Enabled = false;
            OTNo.Enabled = false;
            RdbGendarM.Enabled = false;
            RdbGendarF.Enabled = false;

            ddlVehicleType.Enabled = false;
            ddlTypeofCertificate.Enabled = false;
            ddlShiftType.Enabled = false;
            ddlRouteNo.Enabled = false;
            txtLLeaveDays.Enabled = false;
            txtAboveHrs.Enabled = false;
            txtbelowHrs.Enabled = false;
            txtpayfromdate.Enabled = false;
            txtpaytodate.Enabled = false;
            Clear();
        }
    }
    public void Clear()
    {
        chkapproval.Checked = true;
        chkPending.Checked = false;
        ddlCategory.SelectedIndex = 0;
        ddlDepartment.SelectedIndex = 0;
        ddlEmpName.SelectedValue = "- select -";
        ddlIsAct.SelectedIndex = 0;

        ddlShift.SelectedIndex = 0;

        ddlWagesType.SelectedIndex = 0;
        ddlYear.SelectedIndex = 0;

        txtFrmdate.Text = "";
        txtleavedays.Text = "";
        txtTypeOfCertificate.Text = "";
        txtTodate.Text = "";

        RdbGendarF.Checked = false;
        RdbGendarM.Checked = false;
        OTNo.Checked = false;
        OTYes.Checked = false;

        txtAboveHrs.Text = "";
        txtbelowHrs.Text = "";
        btnAttendanceDetails.Visible = false;
        txtpayfromdate.Text = "";
        txtpayfromdate.Text = "";
    }

    protected void chkapproval_CheckedChanged(object sender, EventArgs e)
    {
        if (chkapproval.Checked == true)
        {
            chkPending.Checked = false;
        }
    }
    protected void chkPending_CheckedChanged(object sender, EventArgs e)
    {
        if (chkPending.Checked == true)
        {
            chkapproval.Checked = false;
        }
    }

    protected void txtTokenNo_TextChanged(object sender, EventArgs e)
    {
        string SSQL;
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "Select ExistingCode +'->'+CONVERT(varchar(10), MachineID) +'->'+ FirstName As EmpName From Employee_Mst";
        SSQL = SSQL + " Where CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " and LocCode='" + SessionLcode + "' And IsActive='Yes' ";
        if (ddlDivision.SelectedItem.Text != "-Select-")
        {
            SSQL = SSQL + " and Division='" + ddlDivision.SelectedItem.Text + "' ";
        }
        SSQL = SSQL + " And ExistingCode='" + txtTokenNo.Text + "'";


        SSQL = SSQL + " Order By EmpNo";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlEmpName.SelectedValue = dt.Rows[i]["EmpName"].ToString();
            }
        }

    }



}
