﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class HR_Reports_HR_EmployeeJLDet : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable sDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = ""; string status = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Wages;
    string FromDate;
    string ToDate;
    DataSet ds = new DataSet();
    string Division;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Full Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();

            Wages = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();


            GetEmpJoiningReleavingDet();

            ds.Tables.Add(AutoDataTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("/crystal_HR/EmployeeLRDet.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }
    public void GetEmpJoiningReleavingDet()
    {
        string SSQL = "";

        SSQL = "Select Sum(ActiveEmp) [ActiveEmp],Sum(NewJoining)[NewJoining],Sum(Leaving)[Leaving],";
        SSQL = SSQL + " ((Sum(ActiveEmp)+Sum(NewJoining))-Sum(Leaving))[Confirmation], ";
        SSQL = SSQL + " '" + FromDate + "'[Rem1],'" + ToDate + "'[Rem2] from ( ";

        SSQL = SSQL + " Select 0 [NewJoining],Count(*)[ActiveEmp],0 [Leaving] from Employee_Mst ";
        SSQL = SSQL + " Where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and  IsActive='Yes' And ";
        SSQL = SSQL + " Wages='"+ Wages +"' and Convert(DateTime,Doj,103)<=convert(datetime,'"+ FromDate +"',103) ";

        SSQL = SSQL + " Union All ";

        SSQL = SSQL + " Select Count(*)[NewJoining],0 [ActiveEmp],0 [Leaving] from Employee_Mst ";
        SSQL = SSQL + " Where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='Yes' And ";
        SSQL = SSQL + " Wages='" + Wages + "' And Convert(DateTime,Doj,103)>=convert(datetime,'" + FromDate + "',103) And ";
        SSQL = SSQL + " Convert(DateTime,Doj,103)<=convert(datetime,'" + ToDate + "',103) ";

        SSQL = SSQL + " Union All ";

        SSQL = SSQL + " Select 0 [NewJoining],0 [ActiveEmp],Count(*) [Leaving] from Employee_Mst ";
        SSQL = SSQL + " Where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='No' and";
        SSQL = SSQL + " Wages='" + Wages + "' And Convert(DateTime,Doj,103)>=convert(datetime,'" + FromDate + "',103) And ";
        SSQL = SSQL + " Convert(DateTime,Doj,103)<=convert(datetime,'" + ToDate + "',103) )A";

        AutoDataTable = objdata.RptEmployeeMultipleDetails(SSQL);

    }
}
