﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;

public partial class HR_Reports_HR_View_Report_New : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SesRoleCode = "";
    string SessionAdmin; string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string salaryType;
    string Emp_ESI_Code;

    string EmployeeType; string EmployeeTypeCd;
    string PayslipType;

    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    string Get_Division_Name = "";

    string Other_state = "";
    string Non_Other_state = "";
    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";
    string ExemptedStaff = "";

    string Basic_Report_To_Date = "";
    string Payslip_Folder = "HR_Payslip";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        string date_1 = "";
        date_1 = ("01".ToString() + "-" + "12".ToString() + "-" + "2018".ToString()).ToString();
        SqlConnection con = new SqlConnection(constr);

        SessionAdmin = Session["Isadmin"].ToString();
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        str_cate = Request.QueryString["Cate"].ToString();
        str_dept = Request.QueryString["Depat"].ToString();
        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        fromdate = Request.QueryString["fromdate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();
        salaryType = Request.QueryString["Salary"].ToString();
        EmployeeTypeCd = Request.QueryString["EmpTypeCd"].ToString();
        EmployeeType = Request.QueryString["EmpType"].ToString();


        if (EmployeeTypeCd == "1") { EmployeeType = "STAFF"; }
        if (EmployeeTypeCd == "2") { EmployeeType = "SUB STAFF"; }
        if (EmployeeTypeCd == "3") { EmployeeType = "CANTEEN"; }
        if (EmployeeTypeCd == "4") { EmployeeType = "DAYSCHOLAR"; }
        if (EmployeeTypeCd == "5") { EmployeeType = "DOFFING"; }
        if (EmployeeTypeCd == "6") { EmployeeType = "FAMILY QTRS"; }
        if (EmployeeTypeCd == "7") { EmployeeType = "HOSTEL - GENTS"; }
        if (EmployeeTypeCd == "8") { EmployeeType = "HOSTEL - LADIES"; }
        if (EmployeeTypeCd == "9") { EmployeeType = "MIXING"; }
        if (EmployeeTypeCd == "10") { EmployeeType = "SECURITY"; }
        if (EmployeeTypeCd == "11") { EmployeeType = "SWEEPER"; }

        Emp_ESI_Code = Request.QueryString["ESICode"].ToString();
        PFTypeGet = Request.QueryString["PFTypePost"].ToString();
        Left_Employee = Request.QueryString["Left_Emp"].ToString();
        Left_Date = Request.QueryString["Leftdate"].ToString();
        Salary_CashOrBank = Request.QueryString["CashBank"].ToString();
        salaryType = Request.QueryString["Salary"].ToString();
        Get_Report_Type = Request.QueryString["Report_Type"].ToString();
        ExemptedStaff = Request.QueryString["ExemptedStaff"].ToString();
        Other_state = Request.QueryString["OtherState"].ToString();
        Non_Other_state = Request.QueryString["NonOtherState"].ToString();
        Get_Division_Name = Request.QueryString["Division"].ToString();

        if (Emp_ESI_Code == "--Select--") { Emp_ESI_Code = ""; }

        int YR = 0;
        if (str_month == "January")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "February")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "March")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(str_yr);
        }
        DataTable DT_Q_OUT = new DataTable();
        DataTable dt = new DataTable();
        query = "Select Cname,Location,Address1,Address2,Location,Pincode from AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }
        if (Get_Report_Type == "Payslip")
        {
            string Bank_Name_Str = "";
            string Token_No_Search = "";
            PayslipType = Request.QueryString["PayslipType"].ToString();
            Bank_Name_Str = Request.QueryString["Bank_Name"].ToString();
            Token_No_Search = Request.QueryString["Token_No"].ToString();


            //SalDet.Stores,SalDet.Medical,SalDet.L_One,SalDet.L_Two,

            query = "Select EmpDet.UAN,AttnDet.Fixed_Work_Days, EmpDet.BankName,EmpDet.BranchCode,EmpDet.AccountNo,EmpDet.IFSC_Code,";
            query = query + " EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName,EmpDet.LastName as FatherName,";
            query = query + " EmpDet.DeptName,EmpDet.Designation,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,";
            query = query + " EmpDet.Basic_Sal_Part [FixBaseSal],EmpDet.DA_Sal_Part[FixDA],EmpDet.HRA_Sal_Part[FixHRA],";
            query = query + " EmpDet.WA_Sal_Part[FIXWA],EmpDet.SPL_Allow_Part[FixAllow],EmpDet.Other_Allow_Part[FixOtherAllow],";
            query = query + " EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber,AttnDet.TotalDays as Total_Month_Days,";
            query = query + " SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL,Cast(SalDet.WH_Work_Days as decimal(18,2))[WH_Work_Days],";
            query = query + " SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Conveyance,";
            query = query + " SalDet.Basic_Uniform as PF_Fixed_Sal,Cast(SalDet.Basic_Spl_Allowance as Decimal(18,2)) Basic_Spl_Allowance,";
            query = query + " Cast(SalDet.BasicRAI as decimal(8,2)) BasicRAI,Cast(SalDet.WashingAllow as Decimal(18,2))[WH],SalDet.allowances1,SalDet.allowances2,";
            query = query + " SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,";
            query = query + " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Misc,SalDet.Advance,";
            //query = query + " (Cast(SalDet.ProvidentFund as Decimal(18,2))+Cast(EmpDet.VPF as Decimal(18,2)))ProvidentFund,SalDet.ESI,";
            query = query + " SalDet.ProvidentFund,SalDet.ESI,";
            query = query + " SalDet.GrossEarnings,SalDet.TotalDeductions,SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,";
            query = query + " AttnDet.Permission_Hrs,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,";
            query = query + " cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2,SalDet.Month,SalDet.Year,SalDet.SalaryDate,";
            query = query + " AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,AttnDet.AEH,AttnDet.LBH,SalDet.SalaryThrough,";
            query = query + " SalDet.RoundOffNetPay,EmpDet.Account_Holder_Name,AttnDet.LWW_Days,SalDet.EmployeerPFone[EmpPFone],SalDet.EmployeerPFTwo[EmpPFTwo], ";
            query = query + " TDSAmt[TDS],SalDet.M_Ded1,SalDet.M_Ded2 From Employee_Mst EmpDet Inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo";
            query = query + " Inner Join AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
            query = query + " Where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and ";
            query = query + " EmpDet.CatName='" + str_cate + "' And AttnDet.Months='" + str_month + "' AND ";
            query = query + " AttnDet.FinancialYear='" + str_yr + "' and SalDet.Lcode='" + SessionLcode + "' And ";
            query = query + " AttnDet.Lcode='" + SessionLcode + "' And EmpDet.CompCode='" + SessionCcode + "' ";
            query = query + " and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

            if (EmployeeType == "MIXING")
            {
                query = query + " And Convert(Datetime,SalDet.FromDate,103)=Convert(Datetime,'" + fromdate + "',103)";
                query = query + " And Convert(Datetime,SalDet.ToDate,103)=Convert(Datetime,'" + ToDate + "',103)";

                query = query + " And Convert(Datetime,AttnDet.FromDate,103)=Convert(Datetime,'" + fromdate + "',103)";
                query = query + " And Convert(Datetime,AttnDet.ToDate,103)=Convert(Datetime,'" + ToDate + "',103)";
            }
            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            else
            {
                if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
            }
            // Other State
            if (Other_state == "Yes") { query = query + " and EmpDet.OtherState='Yes'"; }
            if (Non_Other_state == "No")
            {
                query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
            }
            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (EmpDet.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (EmpDet.Eligible_PF='2')";
                }
            }
            //Salary Through Bank Or Cash Check
            if (Salary_CashOrBank.ToString() != "0")
            {
                if (Salary_CashOrBank.ToString() == "1")
                {
                    //Cash Employee
                    query = query + " and (EmpDet.Salary_Through='1')";
                }
                else
                {
                    //Bank Employee
                    query = query + " and (EmpDet.Salary_Through='2')";
                }
            }
            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
            }
            if (ExemptedStaff != "")
            {
                query = query + " and EmpDet.ExemptedStaff='1'";
            }

            if (str_dept != "0" && str_dept != "")
            {
                query = query + " and EmpDet.DeptCode='" + str_dept + "'";
            }
            if (Bank_Name_Str != "")
            {
                query = query + " and EmpDet.BankName='" + Bank_Name_Str + "'";
            }
            if (Token_No_Search != "")
            {
                query = query + " and EmpDet.ExistingCode='" + Token_No_Search + "'";
            }
            //query = query + " and EmpDet.IsActive='Yes' Order by EmpDet.ExistingCode Asc";
            query = query + " Order by EmpDet.ExistingCode Asc";

            DT_Q_OUT = objdata.RptEmployeeMultipleDetails(query);

            if (DT_Q_OUT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(DT_Q_OUT);
                if (SesRoleCode == "Emp")
                {
                    rd.Load(Server.MapPath("~/HR_Payslip/Coral_Payslip_Cover_Mail.rpt"));
                }
                else
                if (PayslipType == "0")
                {
                    rd.Load(Server.MapPath("~/HR_Payslip/Check_List.rpt"));
                }
                if (PayslipType == "1")
                {
                    rd.Load(Server.MapPath("~/HR_Payslip/SignList.rpt"));
                }
                if (PayslipType == "2")
                {
                    rd.Load(Server.MapPath("~/HR_Payslip/Bank_Slip_New.rpt"));
                }

                if (PayslipType == "3")
                {
                    rd.Load(Server.MapPath("~/HR_Payslip/SamplePayslip.rpt"));
                }

                if (PayslipType == "4")
                {
                    rd.Load(Server.MapPath("~/HR_Payslip/Signature_Matrix_New.rpt"));
                }

                if (PayslipType == "5")
                {
                    rd.Load(Server.MapPath("~/HR_Payslip/Coral_Payslip_Cover.rpt"));
                }

                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
                rd.DataDefinition.FormulaFields["Month"].Text = "'" + str_month.ToUpper().ToString() + "'";
                rd.DataDefinition.FormulaFields["Year"].Text = "'" + YR.ToString() + "'";
                rd.DataDefinition.FormulaFields["From_Date"].Text = "'" + fromdate.ToString() + "'";
                rd.DataDefinition.FormulaFields["To_Date"].Text = "'" + ToDate.ToString() + "'";
                rd.DataDefinition.FormulaFields["Employee_Type"].Text = "'" + EmployeeType.ToString() + "'";

                if (PayslipType == "5")
                {
                    string Get_PF_Code = "";
                    DataTable DT_PFC = new DataTable();
                    query = "Select PF_Code from Location_Mst where LocCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
                    DT_PFC = objdata.RptEmployeeMultipleDetails(query);
                    if (DT_PFC.Rows.Count != 0)
                    {
                        Get_PF_Code = DT_PFC.Rows[0]["PF_Code"].ToString();
                    }
                    rd.DataDefinition.FormulaFields["PF_Code"].Text = "'" + Get_PF_Code.ToString() + "'";
                }

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
        }

        if (Get_Report_Type == "Wages_Abstract")
        {
            string Bank_Name_Str = "";
            PayslipType = Request.QueryString["PayslipType"].ToString();
            Bank_Name_Str = Request.QueryString["Bank_Name"].ToString();

            query = "Select EmpDet.Wages,sum(SalDet.WorkedDays) as WorkedDays,sum(cast(SalDet.BasicAndDANew as decimal(18,2))) as BasicAndDANew,";
            query = query + " sum(cast(SalDet.BasicHRA as decimal(18,2))) as BasicHRA,sum(cast(SalDet.Basic_Spl_Allowance as decimal(18,2))) as DA,";
            query = query + " sum(SalDet.allowances1) as allowances1,sum(SalDet.allowances2) as allowances2,sum(SalDet.allowances3) as allowances3,";
            query = query + " Sum(cast(SalDet.OTHoursNew as decimal(18,2))) as OTHoursNew,sum(cast(SalDet.OTHoursAmtNew as decimal(18,2))) as OTHoursAmtNew,";
            query = query + " sum(SalDet.DayIncentive) as DayIncentive,sum(SalDet.ProvidentFund) as PF,sum(SalDet.ESI) as ESI,sum(SalDet.Deduction1) as Deduction1,";
            query = query + " Sum(SalDet.Deduction2) as Deduction2,Sum(SalDet.Deduction3) as Deduction3,Sum(SalDet.Deduction4) as Deduction4,Sum(SalDet.Deduction5) as Deduction5,";
            query = query + " Sum(SalDet.Advance) as Advance,Sum(SalDet.Stores) as Stores,Sum(SalDet.Medical) as Medical,Sum(SalDet.L_One) as L_One,Sum(SalDet.L_Two) as L_Two,";
            query = query + " Sum(cast(SalDet.DedOthers1 as decimal(18,2))) as DedOthers1,Sum(cast(SalDet.DedOthers2 as decimal(18,2))) as DedOthers2,Sum(SalDet.GrossEarnings) as GrossEarnings,";
            query = query + " Sum(SalDet.TotalDeductions) as TotalDeductions,Sum(SalDet.RoundOffNetPay) as RoundOffNetPay from Employee_Mst EmpDet";
            query = query + " inner join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo";
            query = query + " where SalDet.Ccode='" + SessionCcode + "' And SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "'";
            query = query + " And SalDet.Lcode='" + SessionLcode + "' And EmpDet.CompCode='" + SessionCcode + "'";
            query = query + " and EmpDet.LocCode='" + SessionLcode + "'";
            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            else
            {
                if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
            }
            // Other State
            if (Other_state == "Yes") { query = query + " and EmpDet.OtherState='Yes'"; }
            if (Non_Other_state == "No")
            {
                query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
            }
            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (EmpDet.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (EmpDet.Eligible_PF='2')";
                }
            }
            //Salary Through Bank Or Cash Check
            if (Salary_CashOrBank.ToString() != "0")
            {
                if (Salary_CashOrBank.ToString() == "1")
                {
                    //Cash Employee
                    query = query + " and (EmpDet.Salary_Through='1')";
                }
                else
                {
                    //Bank Employee
                    query = query + " and (EmpDet.Salary_Through='2')";
                }
            }
            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
            }
            if (ExemptedStaff != "")
            {
                query = query + " and EmpDet.ExemptedStaff='1'";
            }

            if (str_dept != "0" && str_dept != "")
            {
                query = query + " and EmpDet.DeptCode='" + str_dept + "'";
            }
            if (Bank_Name_Str != "")
            {
                query = query + " and EmpDet.BankName='" + Bank_Name_Str + "'";
            }
            query = query + " Group by EmpDet.Wages order by EmpDet.Wages Asc";

            DT_Q_OUT = objdata.RptEmployeeMultipleDetails(query);

            if (DT_Q_OUT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(DT_Q_OUT);

                rd.Load(Server.MapPath("HR_Payslip/Wages_Abstract.rpt"));



                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
                rd.DataDefinition.FormulaFields["Month"].Text = "'" + str_month.ToUpper().ToString() + "'";
                rd.DataDefinition.FormulaFields["Year"].Text = "'" + YR.ToString() + "'";
                rd.DataDefinition.FormulaFields["From_Date"].Text = "'" + fromdate.ToString() + "'";
                rd.DataDefinition.FormulaFields["To_Date"].Text = "'" + ToDate.ToString() + "'";
                rd.DataDefinition.FormulaFields["Employee_Type"].Text = "'" + EmployeeType.ToString() + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
        }
    }
}
