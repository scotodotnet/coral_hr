﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class HR_Reports_HR_RptEmp_Exp_Report : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Emp_Cat;
    string Dept_Name;
    string Emp_Type = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Employee Experience Details";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            Emp_Cat = Request.QueryString["Cate"].ToString();
            Dept_Name = Request.QueryString["Dept"].ToString();
            Emp_Type = Request.QueryString["Emp_Type"].ToString();

            Emp_Experience_Report();
            
        }
    }

    public void Emp_Experience_Report()
    {
        DataTable DT_OutPut = new DataTable();

        SSQL = "";
        SSQL = "Select A.CompanyName,A.Designation,A.FromDate,A.ToDate,A.WorkingYear,A.Remarks,";
        SSQL = SSQL + " B.ExistingCode,B.Designation as Emp_Designation,Convert(varchar(20),B.DOJ,103) as DOJ,B.Wages,B.BaseSalary,";
        SSQL = SSQL + " convert(varchar(3),DATEDIFF(MONTH, B.DOJ, GETDATE())/12) +'.' + convert(varchar(2),DATEDIFF(MONTH, B.DOJ, GETDATE()) % 12) AS Experience,";
        SSQL = SSQL + " Convert(varchar(20),GETDATE(),103) as Curr_Date,B.FirstName";
        SSQL = SSQL + " from Employee_ExpDet_Mst A";
        SSQL = SSQL + " inner join Employee_Mst B on A.CCode=B.CompCode And A.LCode=B.LocCode And A.ExistingCode=B.ExistingCode";
        SSQL = SSQL + " where B.CompCode='" + Session["Ccode"].ToString() + "' ANd B.LocCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And A.CCode='" + Session["Ccode"].ToString() + "' ANd A.LCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And B.IsActive='Yes'";
        if (Emp_Cat != "")
        {
            SSQL = SSQL + " And B.CatName = '" + Emp_Cat + "'";
        }
        if (Dept_Name != "")
        {
            SSQL = SSQL + " And B.DeptName = '" + Dept_Name + "'";
        }
        if (Emp_Type != "")
        {
            SSQL = SSQL + " And B.Wages = '" + Emp_Type + "'";
        }
        SSQL = SSQL + " Order by B.ExistingCode Asc";
        DT_OutPut = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable dt = new DataTable();
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (DT_OutPut.Rows.Count != 0)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(DT_OutPut);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("crystal_HR/Attendance.rpt"));

            report.Load(Server.MapPath("../crystal_HR/Emp_Exp_Report.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }
}
