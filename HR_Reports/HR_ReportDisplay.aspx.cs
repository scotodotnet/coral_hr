﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;

public partial class HR_Reports_HR_ReportDisplay : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
    string SessionUserType;
    string fromdate;
    string RptName = "";
    string SSQL = "";
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SqlConnection con = new SqlConnection(constr);

        SessionAdmin = Session["Isadmin"].ToString();
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        RptName = Request.QueryString["RptName"].ToString();
        if (RptName == "LATE IN")
        {
            fromdate = Request.QueryString["FromDate"].ToString();
            Get_Late_IN_Rpt(fromdate);
        }
    }

    private void Get_Late_IN_Rpt(string fromdate)
    {
        SSQL = "";
        SSQL = " Select MachineID,ExistingCode,FirstName,DeptName,Designation,TimeIN,TimeOUT";
        SSQL = SSQL + " from Logtime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and Convert(date,Attn_Date,103)=Convert(date,'" + fromdate + "',103) and LateIn='1'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            grid.DataSource = dt;
            grid.DataBind();
            string attachment = "attachment;filename=LATE IN REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='left'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">LATE IN REPORT -" + Session["Lcode"].ToString() + "-" + fromdate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found!!!');", true);
        }
    }
}