﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;


using Altius.BusinessAccessLayer.BALDataAccess;


public partial class HR_Reports_HR_RptAttendanceView : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = "";// Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Load_TokenNo();
        }
    }

    private void Load_TokenNo()
    {
        SSQL = "";
        SSQL = "select * from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable dt_ = new DataTable();
        dt_ = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_.Rows.Count > 0)
        {
            ddlMachineID.DataSource = dt_;
            ddlMachineID.DataTextField = "MachineID";
            ddlMachineID.DataValueField = "ExistingCode";
            ddlMachineID.DataBind();
            ddlMachineID.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

            ddlTkno.DataSource = dt_;
            ddlTkno.DataTextField = "ExistingCode";
            ddlTkno.DataValueField = "MachineID";
            ddlTkno.DataBind();
            ddlTkno.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }

    protected void ddlMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Employee_Data("MachineID");
    }

    private void Load_Employee_Data(string Base)
    {
        SSQL = "";
        SSQL = "Select * from Employee_mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        if (Base == "MachineID")
        {
            SSQL = SSQL + " and MachineID='" + ddlMachineID.SelectedValue + "'";
            ddlTkno.SelectedValue = ddlMachineID.SelectedItem.Text;
        }
        if (Base == "TokenNo")
        {
            SSQL = SSQL + " and ExistingCode='" + ddlTkno.SelectedValue + "'";
            ddlMachineID.SelectedValue = ddlTkno.SelectedItem.Text;
        }

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtEmpName.Text = dt.Rows[0]["FirstName"].ToString();
        }
    }
    protected void ddlTkno_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Employee_Data("TokenNo");
    }

    protected void btnAttnReport_Click1(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        if (ddlMachineID.SelectedValue == "-Select-" || ddlTkno.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Machine ID or Token Numnber');", true);
        }
        if (txtFromdate.Text == "" || txtToDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the From Date and ToDate Properly');", true);
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Select Attn_Date_Str as Date,Shift,TimeIn ,TimeOUT ,LateIn,EarlyOUT,Total_Hrs1 as TotalHrs,OT_Hrs as OT_Hrs,CASE when Present='1' then 'Present' when Present='0.5' then 'Half Present' else 'Absent' end as Status";
            SSQL = SSQL + " , case when Wh_Present_Count='1.0' then 'WH/X' when Wh_Present_Count='0.5' then 'WH/H' else 'WH' end as WeekOff from LogTime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + ddlMachineID.SelectedItem.Text + "'";
            SSQL = SSQL + " and Convert(date,Attn_Date,103)>=Convert(Date,'" + txtFromdate.Text + "',103) and Convert(date,Attn_Date,103)<=Convert(date,'" + txtToDate.Text + "',103)";

            DataTable AttenDt = new DataTable();
            AttenDt = objdata.RptEmployeeMultipleDetails(SSQL);

            Repeater1.DataSource = AttenDt;
            Repeater1.DataBind();
        }
    }
}