﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Reports_HR_RptTraining_History : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string SSQL = "";
    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = "";// Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Load_TokenNo();
        }
    }
    private void Load_TokenNo()
    {
        SSQL = "";
        SSQL = "select * from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable dt_ = new DataTable();
        dt_ = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_.Rows.Count > 0)
        {
            ddlMachineID.DataSource = dt_;
            ddlMachineID.DataTextField = "MachineID";
            ddlMachineID.DataValueField = "ExistingCode";
            ddlMachineID.DataBind();
            ddlMachineID.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

            ddlTkno.DataSource = dt_;
            ddlTkno.DataTextField = "ExistingCode";
            ddlTkno.DataValueField = "MachineID";
            ddlTkno.DataBind();
            ddlTkno.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
    protected void ddlMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Employee_Data("MachineID");
    }
    private void Load_Employee_Data(string Base)
    {
        SSQL = "";
        SSQL = "Select * from Employee_mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        if (Base == "MachineID")
        {
            SSQL = SSQL + " and MachineID='" + ddlMachineID.SelectedValue + "'";
            ddlTkno.SelectedValue = ddlMachineID.SelectedItem.Text;
        }
        if (Base == "TokenNo")
        {
            SSQL = SSQL + " and ExistingCode='" + ddlTkno.SelectedValue + "'";
            ddlMachineID.SelectedValue = ddlTkno.SelectedItem.Text;
        }

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtEmpName.Text = dt.Rows[0]["FirstName"].ToString();
        }
    }
    protected void ddlTkno_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Employee_Data("TokenNo");
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        if (ddlMachineID.SelectedValue == "-Select-" || ddlTkno.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Machine ID or Token Numnber');", true);
        }
        if (txtFromdate.Text == "" || txtToDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the From Date and ToDate Properly');", true);
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Select ROW_NUMBER() OVER (Order by MachineID) as [S.No],MachineID,ExistingCode as TkNo,Name,DeptName,TrainingDetails,Location,TrainingAgency,Start_Date_Str as [Start Date],End_Date_Str as [End Date],Days,Status,Certification,Valid_Date_Str as Valid";
            SSQL = SSQL + " from Training_History where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            if (ddlMachineID.Text != "-Select-")
            {
                SSQL = SSQL + " and MachineID='" + ddlMachineID.SelectedItem.Text + "'";
            }
            if (txtFromdate.Text != "" && txtToDate.Text != "")
            {
                SSQL = SSQL + " and Convert(date,Start_Date,103)>=Convert(Date,'" + txtFromdate.Text + "',103) and Convert(date,End_Date,103)<=Convert(date,'" + txtToDate.Text + "',103)";
            }
            DataTable AttenDt = new DataTable();
            AttenDt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (AttenDt.Rows.Count > 0)
            {
                grid.DataSource = AttenDt;
                grid.DataBind();
                string attachment = "attachment;filename=Training History.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + AttenDt.Columns.Count + "'>");
                Response.Write("<a style=\"font-weight:bold\">CORAL MANUFACTURING WORK</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + AttenDt.Columns.Count + "'>");
                Response.Write("<a style=\"font-weight:bold\">TRAINING HISTORY REPORT -" + txtFromdate.Text + "-" + txtToDate.Text + "</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found');", true);
            }

        }
    }
}