﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class _Default : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    protected void Page_Load(object sender, EventArgs e)

    {
        Session.Remove("UserId");
        Session.Remove("RoleCode");
        Session.Remove("Isadmin");
        Session.Remove("Usernmdisplay");
        Session.Remove("Ccode");
        Session.Remove("Lcode");
        Session.Remove("CompanyName");
        Session.Remove("LocationName");
        Session.Remove("ProfileImg");
        Session.Remove("FinYearCode");
        Session.Remove("FinYear");
        if (!IsPostBack)
        {
            Load_Company();
            Load_location();

            ddlLocation.SelectedItem.Text = "UNIT I";
            ddlLocation.SelectedValue = "UNIT I";

            Load_FinYear();

            ddlFinYear.SelectedValue = "1";
            ddlFinYear.SelectedItem.Text = "2020_2021";
        }
    }

    private void Load_FinYear()
    {
        DataTable dtempty = new DataTable();
        ddlFinYear.DataSource = dtempty;
        ddlFinYear.DataBind();
        DataTable dt = new DataTable();

        string query = "";
        query = "Select YearCode,FinacialYear from MstFinacialYear where Ccode='" + ddlCompany.SelectedValue + "' And ";
        query = query + " Lcode ='" + ddlLocation.SelectedValue + "' Order by YearCode desc";
        dt = objdata.RptEmployeeMultipleDetails(query);

        ddlFinYear.DataSource = dt;
        ddlFinYear.DataTextField = "FinacialYear";
        ddlFinYear.DataValueField = "YearCode";
        ddlFinYear.DataBind();
    }

    private void Load_Company()
    {
        SSQL = "";
        SSQL = "Select (Ccode+'-'+Name) as Name,* from CompanyMst";
        ddlCompany.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlCompany.DataTextField = "Name";
        ddlCompany.DataValueField = "Ccode";
        ddlCompany.DataBind();
        //ddlCompany.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlCompany.SelectedValue != "-Select-")
        //    Load_location();
    }

    private void Load_location()
    {
        SSQL = "";
        SSQL = "select * from LocationMst where Ccode='" + ddlCompany.SelectedValue + "'";
        ddlLocation.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlLocation.DataTextField = "Lcode";
        ddlLocation.DataValueField = "Lcode";
        ddlLocation.DataBind();
        ddlLocation.Items.Insert(0, new ListItem("-Select-", "-Select-"));
    }

    protected void btnlogin_Click(object sender, EventArgs e)
    {
        UserRegistrationClass objuser = new UserRegistrationClass();
        if (ddlCompany.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the UserID ');", true);
            ErrFlg = true;
        }
        if (ddlLocation.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Pasword ');", true);
            ErrFlg = true;
        }
        if (!ErrFlg)
        {
            string _EncryptPassword = UTF8Encryption(txtPassword.Text);


            string _Password = UTF8Decryption("aHJAMTIz");

            if (ddlLoginType.SelectedValue == "System Admin")
            {
                SSQL = "";
                SSQL = "Select * from Login where Ccode='" + ddlCompany.SelectedValue + "' and Lcode='" + ddlLocation.SelectedValue + "'";
                SSQL = SSQL + " and  UserId='" + txtUserID.Text + "' and Password='" + UTF8Encryption(txtPassword.Text) + "'";
                DataTable login_dt = new DataTable();
                login_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (login_dt.Rows.Count > 0)
                {
                    bool _isLoggedIN = false;
                    _isLoggedIN = Convert.ToBoolean(login_dt.Rows[0]["isLoggedIN"]);

                    SSQL = "";
                    SSQL = "select * from UserDetails where UserID='" + txtUserID.Text + "' and  Ccode='" + ddlCompany.SelectedValue + "' and LCode='" + ddlLocation.SelectedValue + "' ";
                    SSQL = SSQL + " and Status='Approved'";
                    DataTable User_dt = new DataTable();
                    User_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (User_dt.Rows.Count > 0)
                    {
                        SSQL = "";
                        SSQL = "select * from CompanyMst where Ccode='" + ddlCompany.SelectedValue + "'";
                        DataTable Ccode_Dt = new DataTable();
                        Ccode_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (Ccode_Dt.Rows.Count > 0)
                        {
                            SSQL = "";
                            SSQL = "Select * from LocationMst where Ccode='" + ddlCompany.SelectedValue + "' and Lcode='" + ddlLocation.SelectedValue + "'";
                            DataTable Lcode_Dt = new DataTable();
                            Lcode_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (Lcode_Dt.Rows.Count > 0)
                            {
                                Session["Ccode"] = Ccode_Dt.Rows[0]["Ccode"].ToString();
                                Session["Lcode"] = Lcode_Dt.Rows[0]["Lcode"].ToString();
                                Session["CompanyName"] = Ccode_Dt.Rows[0]["Name"].ToString();
                                Session["LocationName"] = Lcode_Dt.Rows[0]["LocName"].ToString();
                                Session["UserId"] = txtUserID.Text;
                                Session["RoleCode"] = User_dt.Rows[0]["UserType"].ToString();
                                Session["Isadmin"] = User_dt.Rows[0]["UserType"].ToString();
                                Session["Usernmdisplay"] = User_dt.Rows[0]["UserName"].ToString();
                                Session["UserDesignation"] = User_dt.Rows[0]["Designation"].ToString();
                                Session["ProfileImg"] = User_dt.Rows[0]["profileImg"].ToString();

                                Session["FinYearCode"] = ddlFinYear.SelectedValue;
                                Session["FinYear"] = ddlFinYear.SelectedItem.Text.ToString();


                                Response.Redirect("MainDashboard.aspx");

                                SSQL = "";

                                SSQL = "update Login set isLoggedIN='TRUE' where Ccode='" + ddlCompany.SelectedValue + "' and ";
                                SSQL = SSQL + " Lcode ='" + ddlLocation.SelectedValue + "' and UserID='" + txtUserID.Text + "'";

                                objdata.RptEmployeeMultipleDetails(SSQL);

                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('The User is not Approved');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('The User ID or Password is Incorrect');", true);
                }
            }
            if (ddlLoginType.SelectedValue == "STAFF")
            {
                Session["UserId"] = txtUserID.Text.Trim();
                objuser.UserCode = txtUserID.Text.Trim();
                objuser.Password = UTF8Encryption(txtPassword.Text.Trim());
                string pwd = UTF8Encryption(txtPassword.Text);
                objuser.Ccode = ddlCompany.SelectedValue;
                objuser.Lcode = ddlLocation.SelectedValue;

                SSQL = "";
                SSQL = "Select FirstName,EmpNo,DeptName,Designation from Employee_Mst where CompCode='" + objuser.Ccode + "' And LocCode='" + objuser.Lcode + "'";
                SSQL = SSQL + " and Empno='" + txtUserID.Text + "' and Convert(datetime,BirthDate,103)=Convert(datetime,'" + Convert.ToDateTime(txtPassword.Text).ToString("dd/MM/yyyy") + "',103) and isActive='yes' and Catname='STAFF'";
                DataTable dt_emp = new DataTable();
                dt_emp = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_emp.Rows.Count > 0)
                {
                    //if (Session["Isadmin"].ToString() == "1")
                    //{
                    Session["RoleCode"] = "Emp";
                    Session["Isadmin"] = "Emp";
                    Session["Usernmdisplay"] = dt_emp.Rows[0]["Firstname"].ToString();
                    Session["EmpCode"] = dt_emp.Rows[0]["EmpNo"].ToString();
                    Session["EmpDept"] = dt_emp.Rows[0]["DeptName"].ToString();
                    Session["EmpDesig"] = dt_emp.Rows[0]["Designation"].ToString();
                    Session["UserDesignation"] = dt_emp.Rows[0]["Designation"].ToString();
                    Session["Ccode"] = objuser.Ccode;
                    Session["Lcode"] = objuser.Lcode;
                    Session["UserType"] = ddlLoginType.SelectedValue;
                    Session["ProfileImg"] = "";

                    Response.Redirect("/EMP_Portal/MstMyActivity.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                }
            }
        }
    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlFinYear.DataSource = dtempty;
        ddlFinYear.DataBind();
        DataTable dt = new DataTable();

        string query = "";
        query = "Select YearCode,FinacialYear from MstFinacialYear where Ccode='" + ddlCompany.SelectedValue + "' And ";
        query = query + " Lcode ='" + ddlLocation.SelectedValue + "' Order by YearCode desc";

        dt = objdata.RptEmployeeMultipleDetails(query);

        ddlFinYear.DataSource = dt;
        ddlFinYear.DataTextField = "FinacialYear";
        ddlFinYear.DataValueField = "YearCode";
        ddlFinYear.DataBind();
    }
}