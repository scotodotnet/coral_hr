﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstMyActivity.aspx.cs" Inherits="MstMyActivity" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <!-- begin #content -->
        <section class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">My Activity</a></li>
                <li class="active">My Attendance</li>
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">My Attendance</h1>
            <!-- end page-header -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div id="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab">My Leave</a></li>
                                    <li><a href="#tab_2" data-toggle="tab">Permission</a></li>
                                    <li><a href="#tab_3" data-toggle="tab">On Duty</a></li>
                                    <%--   <li><a href="#tab_4" data-toggle="tab">Shift Roster</a></li>--%>
                                </ul>
                                <!-- begin wizard step-1 -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <fieldset>
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">My Leave</label></h3>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Employee Code</label>
                                                                    <asp:TextBox ID="txtEmpCode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Employee Name</label>
                                                                    <asp:TextBox ID="txtEmpName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Employee Dept.</label>
                                                                    <asp:TextBox ID="txtEmpDept" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Employee Designation</label>
                                                                    <asp:TextBox ID="txtEmpDesig" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Leave Type</label>
                                                                    <asp:DropDownList runat="server" ID="ddlLeaveType" class="form-control select2" Style="width: 100%;" OnSelectedIndexChanged="ddlLeaveType_SelectedIndexChanged" AutoPostBack="true">
                                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                        <asp:ListItem Value="Earned Leave">Earned Leave</asp:ListItem>
                                                                        <asp:ListItem Value="Casual Leave">Casual Leave</asp:ListItem>
                                                                        <asp:ListItem Value="Compensation Leave">Compensation Leave</asp:ListItem>
                                                                        <asp:ListItem Value="Long Leave">Long Leave</asp:ListItem>
                                                                        <asp:ListItem Value="Medical Leave">Medical Leave</asp:ListItem>
                                                                        <asp:ListItem Value="Sick Leave">Sick Leave</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->

                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">From Date</label>
                                                                    <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">To Date</label>
                                                                    <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Avail.</label>
                                                                    <asp:TextBox runat="server" ID="txtAvail" class="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Taken</label>
                                                                    <asp:TextBox runat="server" ID="txtTaken" class="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Bal.</label>
                                                                    <asp:TextBox runat="server" ID="txtBal" class="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>
                                                        <div class="row">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Remark</label>
                                                                    <asp:TextBox runat="server" TextMode="MultiLine" Style="resize: none" ID="txtRemark" class="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>
                                                        <div class="row">
                                                            <br />
                                                            <div class="col-md-5"></div>
                                                            <asp:Button ID="btnLvApply" runat="server" Text="Apply" CssClass="btn btn-success" OnClick="btnLvApply_Click" />
                                                            <asp:Button ID="btnLvCancel" runat="server" Text="Clear" CssClass="btn btn-danger" OnClick="btnLvCancel_Click" />
                                                        </div>
                                                        <div class="row">
                                                            <!-- table start -->
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="true">
                                                                        <HeaderTemplate>
                                                                            <table id="example" class="display table table-bordered table-hover table-response">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>S.No</th>
                                                                                        <th>Leave Type</th>
                                                                                        <th>From Date</th>
                                                                                        <th>To Date</th>
                                                                                        <th>Taken</th>
                                                                                        <th>Status</th>
                                                                                        <th>Remarks</th>
                                                                                        <th>Mode</th>
                                                                                    </tr>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><%# Container.ItemIndex + 1 %></td>
                                                                                <td><%# Eval("LeaveType")%></td>
                                                                                <td><%# Eval("FromDate")%></td>
                                                                                <td><%# Eval("ToDate")%></td>
                                                                                <td><%# Eval("TotalDays")%></td>
                                                                                <td><%# Eval("Status")%></td>
                                                                                <td><%# Eval("LeaveDesc")%></td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                        Text="" OnCommand="btnDeleteGrid_Command" CommandArgument='<%# Eval("LeaveStatus")%>' CommandName='<%# Eval("Auto_ID")%>'
                                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Leave details?');">
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate></table></FooterTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                            <!-- table End -->
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-1 -->
                                    <!-- begin wizard step-2 -->
                                    <div class="tab-pane " id="tab_2">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <fieldset>
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">Permission</label></h3>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-3" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Permission Available Hrs</label>
                                                                    <asp:TextBox runat="server" ID="txtPermissionAvailHrs" Text="NO Limit" class="form-control" Enabled="false">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">From Date</label>
                                                                    <asp:TextBox runat="server" ID="txtPermissionfromdate" class="form-control datepicker">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">From Time</label>
                                                                    <asp:TextBox runat="server" ID="txtpersmisionFrom" Text="0" class="form-control time">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">ToDate</label>
                                                                    <asp:TextBox runat="server" ID="txtPermissiontodate" class="form-control datepicker">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">To Time</label>
                                                                    <asp:TextBox runat="server" ID="txtPermissionTo" Text="0" class="form-control time">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Total Hrs. Take</label>
                                                                    <asp:TextBox runat="server" ID="txtpermissiontotalhrstake" Text="0" class="form-control text">
                                                                    </asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtpermissiontotalhrstake" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->

                                                        </div>
                                                        <div class="row">
                                                            <br />
                                                            <div class="col-md-5"></div>
                                                            <asp:Button ID="btnApplyPermission" runat="server" Text="Apply" CssClass="btn btn-success" OnClick="btnApplyPermission_Click" />
                                                            <asp:Button ID="btnCancelPermission" runat="server" Text="Clear" CssClass="btn btn-danger" OnClick="btnCancelPermission_Click" />
                                                        </div>
                                                        <div class="row">
                                                            <!-- table start -->
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <asp:Repeater ID="Repeater2" runat="server" EnableViewState="true">
                                                                        <HeaderTemplate>
                                                                            <table id="example2" class="display table table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>S.No</th>
                                                                                        <th>Month</th>
                                                                                        <th>From Date</th>
                                                                                        <th>To Date</th>
                                                                                        <th>Status</th>
                                                                                        <th>Mode</th>
                                                                                    </tr>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><%# Container.ItemIndex + 1 %></td>
                                                                                <td><%# Eval("Month")%></td>
                                                                                <td><%# Eval("Fromdate")%></td>
                                                                                <td><%# Eval("Todate")%></td>
                                                                                <td><%# Eval("Status")%></td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="btnDeletePermission" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                        Text="" OnCommand="btnDeletePermission_Command" CommandArgument='<%#Eval("Status1") %>' CommandName='<%# Eval("Auto_ID")%>'
                                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Permission details?');">
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate></table></FooterTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                            <!-- table End -->
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-2 -->
                                    <!-- begin wizard step-3 -->
                                    <div class="tab-pane " id="tab_3">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <fieldset>
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">On Duty</label></h3>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Date From</label>
                                                                    <asp:TextBox runat="server" ID="txtoDDatefrom" class="form-control datepicker">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Date To</label>
                                                                    <asp:TextBox runat="server" ID="txtODDateTo" class="form-control datepicker">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Days</label>
                                                                    <asp:TextBox runat="server" ID="txtODdays" class="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">From Area</label>
                                                                    <asp:TextBox runat="server" ID="txtFromArea" class="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">To Area</label>
                                                                    <asp:TextBox runat="server" ID="txtToArea" class="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Reason</label>
                                                                    <asp:TextBox runat="server" ID="txtODReason" class="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <br />
                                                            <div class="col-md-5"></div>
                                                            <asp:Button ID="btnApplyOD" runat="server" Text="Apply" CssClass="btn btn-success" OnClick="btnApplyOD_Click" />
                                                            <asp:Button ID="btnCancelOD" runat="server" Text="Clear" CssClass="btn btn-danger" OnClick="btnCancelOD_Click" />
                                                        </div>
                                                        <div class="row">
                                                            <!-- table start -->
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <asp:Repeater ID="Repeater3" runat="server" EnableViewState="true">
                                                                        <HeaderTemplate>
                                                                            <table id="example3" class="display table table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>S.No</th>
                                                                                        <th>From Date</th>
                                                                                        <th>To Date</th>
                                                                                        <th>Days</th>
                                                                                        <th>FromArea</th>
                                                                                        <th>ToArea</th>
                                                                                        <th>Reason</th>
                                                                                        <th>Status</th>
                                                                                        <th>Mode</th>
                                                                                    </tr>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><%# Container.ItemIndex + 1 %></td>
                                                                                <td><%# Eval("ONDutyFromDate")%></td>
                                                                                <td><%# Eval("ONDutyToDate")%></td>
                                                                                <td><%# Eval("Days")%></td>
                                                                                <td><%# Eval("FromArea")%></td>
                                                                                <td><%# Eval("ToArea")%></td>
                                                                                <td><%# Eval("Reason")%></td>
                                                                                <td><%# Eval("status")%></td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="btnDeleteOD" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                        Text="" OnCommand="btnDeleteOD_Command" CommandArgument='<%#Eval("Status1") %>' CommandName='<%# Eval("Auto_ID")%>'
                                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this On Duty details?');">
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate></table></FooterTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                            <!-- table End -->
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-3 -->
                                    <!-- begin wizard step-4 -->
                                    <div class="tab-pane " id="tab_4">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <fieldset>
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            <label for="exampleInputName">Shift Roster</label></h3>
                                                    </div>
                                                    <!-- begin row -->
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Roster Type</label>
                                                                <asp:DropDownList ID="ddlShiftRoster" Enabled="false" CssClass="form-control select2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlShiftRoster_SelectedIndexChanged" Style="width: 100%"></asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <!-- end row -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">From Shift</label>
                                                                <asp:DropDownList ID="ddlFromShift" CssClass="form-control select2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlShiftRoster_SelectedIndexChanged" Style="width: 100%"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">To Shift</label>
                                                                <asp:DropDownList ID="ddlToShift" CssClass="form-control select2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlShiftRoster_SelectedIndexChanged" Style="width: 100%"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">New Shift Date</label>
                                                                <asp:TextBox runat="server" ID="txtNewShiftDate" class="form-control datepicker ">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br />
                                                        <div class="col-md-5"></div>
                                                        <asp:Button ID="btnApplyShiftRoster" runat="server" Text="Apply" CssClass="btn btn-success" OnClick="btnApplyShiftRoster_Click" />
                                                        <asp:Button ID="btnCancelShiftRoster" runat="server" Text="Clear" CssClass="btn btn-danger" OnClick="btnCancelShiftRoster_Click" />
                                                    </div>
                                                    <div class="row">
                                                        <!-- table start -->
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <asp:Repeater ID="Repeater4" runat="server" EnableViewState="true">
                                                                    <HeaderTemplate>
                                                                        <table id="example4" class="display table table-bordered table-hover">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>S.No</th>
                                                                                    <th>Roster</th>
                                                                                    <th>Prev Shift</th>
                                                                                    <th>New Shift</th>
                                                                                    <th>New Shiftdate</th>
                                                                                    <th>Status</th>
                                                                                    <th>Mode</th>
                                                                                </tr>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                                            <td><%# Eval("Roster")%></td>
                                                                            <td><%# Eval("PrevShift")%></td>
                                                                            <td><%# Eval("NewShift")%></td>
                                                                            <td><%# Eval("NewShifDate")%></td>
                                                                            <td><%# Eval("Status")%></td>
                                                                            <td>
                                                                                <asp:LinkButton ID="btnDeleteRoster" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                    Text="" OnCommand="btnDeleteRoster_Command" CommandArgument='<%#Eval("Status1") %>' CommandName='<%# Eval("Auto_ID")%>'
                                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Shift Roster Request details?');">
                                                                                </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate></table></FooterTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                        <!-- table End -->
                                                    </div>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-4 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
   <%-- <script type="text/javascript" src="assets/js/master_list_jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/master_list_jquery-ui.min.js"></script>--%>

<%--    <!-- jQuery 3 -->
    <script src="<%= ResolveUrl("../assets/adminlte/components/jquery/dist/jquery.min.js") %>"></script>
    <!--jqueryUI-->
    <script src="<%= ResolveUrl("../assets/lib/jquery-ui-1.12.1/jquery-ui.min.js") %>"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<%= ResolveUrl("../assets/adminlte/components/bootstrap/dist/js/bootstrap.min.js") %>"></script>
    <!-- Select2 -->
    <script src="<%= ResolveUrl("../assets/adminlte/components/select2/dist/js/select2.full.min.js") %>"></script>
    <!-- bootstrap datepicker -->
    <script src="<%= ResolveUrl("../assets/adminlte/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") %>"></script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
            $('#example2').dataTable();
            $('#example3').dataTable();
            $('#example4').dataTable();
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
            $('[id*=txtTake]').keyup(function (e) {
                var avail = $('[id*=txtAvail]').val();
                var Take = $(this).val();
                if (($('[id*=ddlLeaveType]').val() == "Earned Leave") || ($('[id*=ddlLeaveType]').val() == "Casual Leave") || ($('[id*=ddlLeaveType]').val() == "Compensation Leave")) {
                    if (Take > avail) {
                        alert('Taken is must Lesser than Available');
                        $(this).val('');
                    }
                    else {
                        $('[id*=txtBal]').val((avail - Take));
                    }
                }
            });

        });
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('#example2').dataTable();
                    $('#example3').dataTable();
                    $('#example4').dataTable();

                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                    $('[id*=txtTake]').keyup(function () {
                        var avail = $('[id*=txtAvail]').val();
                        var Take = $(this).val();
                        if (($('[id*=ddlLeaveType]').val() == "Earned Leave") || ($('[id*=ddlLeaveType]').val() == "Casual Leave") || ($('[id*=ddlLeaveType]').val() == "Compensation Leave")) {
                            if (Take > avail) {
                                alert('Taken is must Lesser than Available');
                                $(this).val('');
                            }
                            else {
                                $('[id*=txtBal]').val((avail - Take));
                            }
                        }
                    });
                }
            });
        };
    </script>
</asp:Content>

