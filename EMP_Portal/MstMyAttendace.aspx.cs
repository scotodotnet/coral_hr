﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Text;
using System.Security.Cryptography;
using System.IO;

public partial class MstMyAttendace : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionEmpDept;
    string SessionDesignation;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
       // SessionRights = Session["Rights"].ToString();
        SessionEmpDept = Session["EmpDept"].ToString();
        SessionDesignation = Session["EmpDesig"].ToString();

        if (!IsPostBack)
        {
            //txtfromDate.Text = Convert.ToString(DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy"));
            //txtTodate.Text = Convert.ToString(DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy"));
            //GetAttendance(txtfromDate.Text, txtTodate.Text);
        }    
    }

    private void GetAttendance(string from, string To)
    {
        DataTable dt = new DataTable();
        if (txtfromDate.Text != "" && txtTodate.Text != "")
        {

            SSQL = "";
            SSQL = "Select Case when Present='0.0' then 'A'  when Present='0.5' then 'H' when Present='1.0' then 'P' End as Status,Total_Hrs1 as TotalHrs, Attn_Date_Str as Date, TimeIN as [IN], TimeOUT as [OUT] from LogTime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + Session["EmpCode"].ToString() + "' ";
            SSQL = SSQL + " and Convert(datetime,Attn_Date,103)>=Convert(datetime,'" + txtfromDate.Text + "',103) and Convert(datetime,Attn_Date,103)<=Convert(datetime,'" + txtTodate.Text + "',103)";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count <= 0)
            {

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found')", true);
            }
            Repeater2.DataSource = dt;
            Repeater2.DataBind();
        }
    }

    protected void btnGetAttendace_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtfromDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the from date properly')", true);
            ErrFlag = true;
            return;

        }
        if (txtTodate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the from to properly')", true);
            ErrFlag = true;
            return;

        }
        if (!ErrFlag)
        {
            GetAttendance(txtfromDate.Text, txtTodate.Text);
        }
    }
}