﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;

public partial class HR_Interview_Candidate : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Details";

            Initial_Data_Referesh();
            Load_Data();
        }

    }
    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("LeaveType", typeof(string)));
        dt.Columns.Add(new DataColumn("FDate", typeof(string)));
        dt.Columns.Add(new DataColumn("TDate", typeof(string)));
        dt.Columns.Add(new DataColumn("NoOfDays", typeof(string)));
        dt.Columns.Add(new DataColumn("Status", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
        dt.Columns.Add(new DataColumn("EmpNo", typeof(string)));
        rptleaveDet.DataSource = dt;
        rptleaveDet.DataBind();
        ViewState["ItemTable"] = dt;

        //dt = Repeater1.DataSource;
    }


    

    public void Load_Data()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select 'LeaveType'LeaveType,'FDate'FDate,'','TDate'TDate,'NoOfDays'NoOfDays,'Status'Status,'Remarks'Remarks,'EmpNo'EmpNo ";
        //SSQL = SSQL + " from Emp_Memo_Sub where Category='" + ddlCategory.SelectedValue + "' and Employee_Type='" + ddlEmployeeType.SelectedItem.Text + "'";
        //SSQL = SSQL + " and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
}

