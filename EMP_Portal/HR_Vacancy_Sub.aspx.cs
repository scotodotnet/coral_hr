﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;

public partial class HR_Vacancy : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SessionJobCode;



    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Vacancies";

            if (Session["JobCode"] == null)
            {
                SessionJobCode = "";
            }
            else
            {
                SessionJobCode = Session["JobCode"].ToString();
                lblJobCode.Text = SessionJobCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }

    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        btnSave.Text = "Update";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;


        if (!ErrFlag)
        {
            TransactionNoGenerate TransNO = new TransactionNoGenerate();
            string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Bank Master", SessionFinYearVal, "1");
            if (Auto_Transaction_No == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
            }
            else
            {
                lblJobCode.Text = Auto_Transaction_No;
            }
        }

        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                //SSQL = "Update MstBank set BankName='" + txtBankName.Text + "',IFSCCode='" + txtIFSCCode.Text + "',Branch='" + txtBranch.Text + "',";
                //SSQL = SSQL + " Default_Bank='" + Def_Chk + "',AmtCharge='" + txtChargesAmt.Text + "',MinCharge='" + txtMinCharge.Text + "',";
                //SSQL = SSQL + " MaxCharge='" + txtMaxCharge.Text + "' Where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "'";
                //SSQL = SSQL + " And BankCode='" + txtBankCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Bank Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into Vacancy_Mst(CCode,LCode,FinYearCode,FinYearVal,JobId,JobTitleName,VacancyPasition,ReportingHead,";
                SSQL = SSQL + " JobType,Recruitment,ValidFrom,ValidTo,Status,RecruitmentSkill,UserName,CreateOn)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                SSQL = SSQL + " '"+ SessionFinYearVal +"','" + lblJobCode.Text + "','" + txtJobTitle.Text + "','" + txtPosition.Text + "',";
                SSQL = SSQL + " '" + txtReporting.Text + "','" + ddlJobType.SelectedItem.Text + "','" + txtRecruitment.Text + "',";
                SSQL = SSQL + "  '" + txtFromDate.Text + "','" + txtToDate.Text + "','" + ddlStatus.SelectedValue + "',";
                SSQL = SSQL + " '" + txtRecuritSkill.Text + "', '" + SessionUserName + "',Getdate())";
                
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Bank Details Saved Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";

            Response.Redirect("HR_Vacancy_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('UOM Details Already Saved');", true);
        }

    }

    private void Clear_All_Field()
    {
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
}

