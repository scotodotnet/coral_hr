﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Text;
using System.Security.Cryptography;
using System.IO;

public partial class MstMyActivity : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    //string SessionRights;
    string SessionEmpDept;
    string SessionDesignation;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();
        SessionEmpDept = Session["EmpDept"].ToString();
        SessionDesignation = Session["EmpDesig"].ToString();

        txtEmpCode.Text = Session["EmpCode"].ToString();
        txtEmpName.Text = SessionUserName;
        txtEmpDept.Text = SessionEmpDept;
        txtEmpDesig.Text = SessionDesignation;



        if (!IsPostBack)
        {
            Load_Data_EmpDet();
            Load_permissiondata();
            Load_OD();
            Load_ShiftRoster();
            loadddlShiftRoster();
            SearchRoster(sender, e);
        }
    }

    private void SearchRoster(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select Roster_Name from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and EmpNo='" + txtEmpCode.Text + "'";
        DataTable dt_Check = new DataTable();
        dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShiftRoster.SelectedValue = dt_Check.Rows[0]["Roster_Name"].ToString();
        //if(ddlShiftRoster.SelectedItem.Text!= "-Select-")
        //{                                     
        ddlShiftRoster_SelectedIndexChanged(sender, e);
    }

    private void loadddlShiftRoster()
    {
        SSQL = "";
        SSQL = "Select distinct Roster_name  from MstShiftRoster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        ddlShiftRoster.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShiftRoster.DataTextField = "Roster_name";
        ddlShiftRoster.DataValueField = "Roster_name";
        ddlShiftRoster.DataBind();
        ddlShiftRoster.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_ShiftRoster()
    {
        SSQL = "";
        SSQL = "Select Auto_ID,Roster,PrevShift,NewShift,NewShifDate,Status as Status1,CASE when Status='1' then '<span style=background:green;padding:5px;border-radius:3px;color:white;font-weight:bold> Approved </span>'  when Status='2' then '<span style=background:yellow;padding:5px;border-radius:3px;color:white;font-weight:bold> Pending </span>' when Status='3' then '<span style=background:red;padding:5px;border-radius:3px;color:white;font-weight:bold> Cancel </span>' end as status from MstShiftRosterRequest where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + " and EmpNo='" + txtEmpCode.Text + "'";
        DataTable dt_ShftRoster = new DataTable();
        dt_ShftRoster = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater4.DataSource = dt_ShftRoster;
        Repeater4.DataBind();
    }

    private void Load_OD()
    {
        SSQL = "";
        SSQL = "Select Auto_ID,ONDutyFromDate,ONDutyToDate,Days,fromArea,ToArea,Reason ,Status as Status1,CASE when Status='1' then '<span style=background:green;padding:5px;border-radius:3px;color:white;font-weight:bold> Approved </span>'  when Status='2' then '<span style=background:yellow;padding:5px;border-radius:3px;color:white;font-weight:bold> Pending </span>' when Status='3' then '<span style=background:red;padding:5px;border-radius:3px;color:white;font-weight:bold> Cancel </span>' end as status from OnDuty_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and TokenNo='" + txtEmpCode.Text + "'";
        DataTable dt_OD = new DataTable();
        dt_OD = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater3.DataSource = dt_OD;
        Repeater3.DataBind();
    }

    private void Load_permissiondata()
    {
        SSQL = "";
        SSQL = "Select Auto_ID,Month,Fromdate,Todate,PerMin,Status as Status1, CASE when Status='1' then '<span style=background:green;padding:5px;border-radius:3px;color:white;font-weight:bold> Approved </span>'  when Status='2' then '<span style=background:yellow;padding:5px;border-radius:3px;color:white;font-weight:bold> Pending </span>' when Status='3' then '<span style=background:red;padding:5px;border-radius:3px;color:white;font-weight:bold> Cancel </span>' end as status  from MstPermission where EmpNo='" + txtEmpCode.Text + "' and PerMin>0";
        DataTable dt_Permission = new DataTable();
        dt_Permission = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = dt_Permission;
        Repeater2.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        SSQL = "";
        SSQL = "select Auto_ID,LeaveType,FromDate,ToDate,TotalDays,LeaveStatus,CASE when LeaveStatus='1' then '<span style=background:green;padding:5px;border-radius:3px;color:white;font-weight:bold> Approved </span>'  when LeaveStatus='2' then '<span style=background:yellow;padding:5px;border-radius:3px;color:black;font-weight:bold> Pending </span>' when LeaveStatus='3' then '<span style=background:red;padding:5px;border-radius:3px;color:white;font-weight:bold> Cancel </span>' end as status,LeaveDesc";
        SSQL = SSQL + " from Leave_Register_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        // SSQL = SSQL + " and EmpNo='" + txtEmpCode.Text + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        string Status = e.CommandArgument.ToString();
        string ID = e.CommandName.ToString();
        if (Status == "2")
        {
            SSQL = "";
            SSQL = "Delete from Leave_Register_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Auto_ID='" + ID + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Data_EmpDet();
        }
        if (Status == "3")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Detail is Already Canceld')", true);
        }
        if (Status == "1")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Detail is Already Approved')", true);
        }
    }

    protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //SSQL = "";
        //DataTable Leave_dt = new DataTable();
        //if (ddlLeaveType.SelectedValue != "-Select-")
        //{
        //    if (ddlLeaveType.SelectedValue == "Earned Leave")
        //    {
        //        SSQL = "select SUM(CAST(ELAddDays as decimal(18,2))-CAST(ELMinDays as decimal(18,2))) as AvDays  from CLMst";
        //        SSQL = SSQL + " where EmpNo='" + txtEmpCode.Text + "'";
        //        SSQL = SSQL + "	group by EmpNo";

        //        Leave_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //    }
        //    else if (ddlLeaveType.SelectedValue == "Casual Leave")
        //    {
        //        SSQL = "select SUM(CAST(ELAddDays as decimal(18,2))-CAST(ELMinDays as decimal(18,2))) as AvDays  from CLMst";
        //        SSQL = SSQL + " where EmpNo='" + txtEmpCode.Text + "'";
        //        SSQL = SSQL + "	group by EmpNo";

        //        Leave_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //    }
        //    else if (ddlLeaveType.SelectedValue == "Compensation Leave")
        //    {
        //        SSQL = "Select (sum(isnull(cast(WH_Add as decimal(18,1)), '0'))  - sum(isnull(Cast(WH_Min as decimal(18,1)) , '0')) ) as AvDays";
        //        SSQL = SSQL + " from Mst_Compensation where MachineID='" + txtEmpCode.Text + "'";

        //        Leave_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //    }
        //    if (Leave_dt.Rows.Count > 0)
        //    {
        //        txtAvail.Text = Leave_dt.Rows[0]["AvDays"].ToString();
        //    }
        //    else
        //    {
        //        txtAvail.Text = "0";
        //    }
        //}
        //else
        //{
        //    txtAvail.Text = "0";
        //}
    }

    protected void btnLvApply_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        SSQL = "";
        if (ddlLeaveType.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Leave Type')", true);
            ErrFlag = true;
        }
        if (txtFromDate.Text == "" | txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Date properly')", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Select * from Leave_Register_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and Convert(date,'" + txtFromDate.Text + "',103)>=Convert(date,FromDate,103) and Convert(date,'" + txtToDate.Text + "',103)>=Convert(date,ToDate,103)";
            SSQL = SSQL + " and EmpNo='" + txtEmpCode.Text + "'";
            DataTable dt_Check = new DataTable();
            dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Check.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The leave is already applied on the date Please Select another Date')", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {

                string machinID_Encrypt = "";
                string Excode = "";
                string MachineID = "";

                SSQL = "";
                SSQL = "Select EmpNo,ExistingCode,MachineID_Encrypt,MachineID from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " and EmpNo='" + txtEmpCode.Text + "' and IsActive='Yes'";
                DataTable dt_emp = new DataTable();
                dt_emp = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_emp.Rows.Count > 0)
                {
                    machinID_Encrypt = dt_emp.Rows[0]["MachineID_Encrypt"].ToString();
                    Excode = dt_emp.Rows[0]["ExistingCode"].ToString();
                    MachineID = dt_emp.Rows[0]["MachineID"].ToString();
                }

                SSQL = "";
                SSQL = "Insert into Leave_Register_Mst (CompCode,LocCode,Empno,ExistingCode,machine_no,Machine_Encrypt,EmpName,fromDate,ToDate,TotalDays,LeaveType,LeaveDesc,LeaveStatus,From_Date_Dt,To_Date_Dt)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtEmpCode.Text + "','" + Excode + "','" + MachineID + "','" + machinID_Encrypt + "','" + txtEmpName.Text + "'";
                SSQL = SSQL + ",'" + txtFromDate.Text + "' ,'" + txtToDate.Text + "','" + txtTaken.Text + "','" + ddlLeaveType.SelectedValue + "','" + txtRemark.Text + "','2',Convert(date,'" + txtFromDate.Text + "',103),Convert(date,'" + txtToDate.Text + "',103))";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "insert into Leave_History(CompCode,LoCode,Empno,ExistingCode,machine_no,machine_Encrypt,EmpName,FromDate,ToDate,TotalDays)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtEmpCode.Text + "','" + MachineID + "','" + machinID_Encrypt + "','" + txtEmpName.Text + "','" + txtFromDate.Text + "','" + txtToDate.Text + "','" + txtTaken.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The Leave Details Applied Successfully')", true);

                Load_Data_EmpDet();
                btnLvCancel_Click(sender, e);
            }
        }
    }

    protected void btnLvCancel_Click(object sender, EventArgs e)
    {
        ddlLeaveType.ClearSelection();
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtAvail.Text = "0";
        txtTaken.Text = "0";
        txtBal.Text = "0";
    }

    protected void ddlShiftRoster_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlShiftRoster.SelectedValue != "-Select-")
        {
            SSQL = "";
            SSQL = "Select * from MstShiftRoster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Roster_Name='" + ddlShiftRoster.SelectedItem.Text + "'";
            ddlFromShift.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlFromShift.DataTextField = "Shift_order";
            ddlFromShift.DataValueField = "Shift_order";
            ddlFromShift.DataBind();
            ddlFromShift.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

            ddlToShift.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlToShift.DataTextField = "Shift_order";
            ddlToShift.DataValueField = "Shift_order";
            ddlToShift.DataBind();
            ddlToShift.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }

    protected void btnApplyPermission_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        SSQL = "";
        if (txtPermissionfromdate.Text == "" || txtPermissiontodate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Permission Date Properly')", true);
            ErrFlag = true;
        }
        if ((txtpersmisionFrom.Text == "" | txtPermissionTo.Text == "") || (txtpersmisionFrom.Text == "0" | txtPermissionTo.Text == "0"))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the permission Time Properly')", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Select * from MstPermission where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and Convert(date,'" + txtFromDate.Text + "',103)>=Convert(date,FromDate,103) and Convert(date,'" + txtToDate.Text + "',103)>=Convert(date,ToDate,103)";
            SSQL = SSQL + " and EmpNo='" + txtEmpCode.Text + "'";
            DataTable dt_Check = new DataTable();
            dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Check.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The Permission is already applied on the date Please Select another Date')", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {

                string machinID_Encrypt = "";
                string Excode = "";
                string MachineID = "";

                SSQL = "";
                SSQL = "Select EmpNo,ExistingCode,MachineID_Encrypt,MachineID from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " and EmpNo='" + txtEmpCode.Text + "' and IsActive='Yes'";
                DataTable dt_emp = new DataTable();
                dt_emp = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_emp.Rows.Count > 0)
                {
                    machinID_Encrypt = dt_emp.Rows[0]["MachineID_Encrypt"].ToString();
                    Excode = dt_emp.Rows[0]["ExistingCode"].ToString();
                    MachineID = dt_emp.Rows[0]["MachineID"].ToString();
                }

                SSQL = "";
                SSQL = "Insert into MstPermission (Ccode,Lcode,EmpNo,Name,Month,Year,PerAdd,PerMin,Fromdate,Todate,Status)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtEmpCode.Text + "','" + txtEmpName.Text + "'";
                SSQL = SSQL + ",month('" + txtPermissiontodate.Text + "'),(year('" + txtPermissiontodate.Text + "')+1),'0','" + txtpermissiontotalhrstake.Text + "','" + txtpersmisionFrom.Text + "' ,'" + txtPermissionTo.Text + "','1')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The Permission Details Applied Successfully')", true);

                Load_permissiondata();
                btnCancelPermission_Click(sender, e);
            }
        }
    }


    protected void btnCancelPermission_Click(object sender, EventArgs e)
    {

    }

    protected void btnApplyOD_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        SSQL = "";
        if (txtoDDatefrom.Text == "" || txtODDateTo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the date Properly')", true);
            ErrFlag = true;
        }
        if (txtFromArea.Text == "" || txtToArea.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Area Details')", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Select * from OnDuty_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and Convert(date,'" + txtFromDate.Text + "',103)>=Convert(date,ONDutyFromDate,103) and Convert(date,'" + txtToDate.Text + "',103)>=Convert(date,ONDutyToDate,103)";
            SSQL = SSQL + " and TokenNo='" + txtEmpCode.Text + "'";
            DataTable dt_Check = new DataTable();
            dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Check.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The OD is already applied on the date Please Select another Date')", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {

                string machinID_Encrypt = "";
                string Excode = "";
                string MachineID = "";


                SSQL = "";
                SSQL = "Select EmpNo,ExistingCode,MachineID_Encrypt,MachineID from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " and EmpNo='" + txtEmpCode.Text + "' and IsActive='Yes'";
                DataTable dt_emp = new DataTable();
                dt_emp = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_emp.Rows.Count > 0)
                {
                    machinID_Encrypt = dt_emp.Rows[0]["MachineID_Encrypt"].ToString();
                    Excode = dt_emp.Rows[0]["ExistingCode"].ToString();
                    MachineID = dt_emp.Rows[0]["MachineID"].ToString();
                }

                SSQL = "";
                SSQL = "Insert into OnDuty_Mst (CompCode,LocCode,TokenNo,EmpName,ONDutyFromDate,ONDutyToDate,Days,FromArea,ToArea,Reason,Status)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtEmpCode.Text + "','" + txtEmpName.Text + "'";
                SSQL = SSQL + ",'" + txtoDDatefrom.Text + "' ,'" + txtODDateTo.Text + "','" + txtODdays.Text + "','" + txtFromArea.Text + "','" + txtToArea.Text + "','" + txtODReason.Text + "','1')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The OD Details Applied Successfully')", true);

                Load_OD();
                btnCancelOD_Click(sender, e);
            }
        }
    }

    protected void btnCancelOD_Click(object sender, EventArgs e)
    {

    }

    protected void btnApplyShiftRoster_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        SSQL = "";
        if (ddlShiftRoster.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Roster Type')", true);
            ErrFlag = true;
        }
        if (ddlFromShift.SelectedValue == "-Select-" || ddlToShift.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Shifts properly')", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Select * from MstShiftRosterRequest where CopmCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and Convert(date,'" + txtFromDate.Text + "',103)>=Convert(date,FromDate,103) and Convert(date,'" + txtToDate.Text + "',103)>=Convert(date,ToDate,103)";
            SSQL = SSQL + " and EmpNo='" + txtEmpCode.Text + "'";
            DataTable dt_Check = new DataTable();
            dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Check.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The leave is already applied on the date Please Select another Date')", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {

                string machinID_Encrypt = "";
                string Excode = "";
                string MachineID = "";

                SSQL = "";
                SSQL = "Select EmpNo,ExistingCode,MachineID_Encrypt,MachineID from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " and EmpNo='" + txtEmpCode.Text + "' and IsActive='Yes'";
                DataTable dt_emp = new DataTable();
                dt_emp = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_emp.Rows.Count > 0)
                {
                    machinID_Encrypt = dt_emp.Rows[0]["MachineID_Encrypt"].ToString();
                    Excode = dt_emp.Rows[0]["ExistingCode"].ToString();
                    MachineID = dt_emp.Rows[0]["MachineID"].ToString();
                }

                SSQL = "";
                SSQL = "Insert into MstShiftRosterRequest (Ccode,Lcode,Empno,Roster,PrevShift,NewShift,NewShifDate,Status)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtEmpCode.Text + "','" + ddlShiftRoster.SelectedValue + "','" + ddlFromShift.SelectedValue + "','" + ddlToShift.SelectedValue + "','" + txtNewShiftDate.Text + "','1')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The Shift Roster Change Request Details Applied Successfully')", true);

                Load_Data_EmpDet();
                btnLvCancel_Click(sender, e);
            }
        }
    }

    protected void btnCancelShiftRoster_Click(object sender, EventArgs e)
    {

    }

    protected void btnDeletePermission_Command(object sender, CommandEventArgs e)
    {
        string Status = e.CommandArgument.ToString();
        string ID = e.CommandName.ToString();
        if (Status == "2")
        {
            SSQL = "";
            SSQL = "Delete from MstPermission where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Auto_ID='" + ID + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_permissiondata();
        }
        if (Status == "3")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Detail is Already Canceld')", true);
        }
        if (Status == "1")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Detail is Already Approved')", true);
        }
    }

    protected void btnDeleteOD_Command(object sender, CommandEventArgs e)
    {
        string Status = e.CommandArgument.ToString();
        string ID = e.CommandName.ToString();
        if (Status == "2")
        {
            SSQL = "";
            SSQL = "Delete from OnDuty_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Auto_ID='" + ID + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_OD();
        }
        if (Status == "3")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Detail is Already Canceld')", true);
        }
        if (Status == "1")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Detail is Already Approved')", true);
        }
    }

    protected void btnDeleteRoster_Command(object sender, CommandEventArgs e)
    {
        string Status = e.CommandArgument.ToString();
        string ID = e.CommandName.ToString();
        if (Status == "2")
        {
            SSQL = "";
            SSQL = "Delete from MstShiftRosterRequest where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Auto_ID='" + ID + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_ShiftRoster();
        }
        if (Status == "3")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Detail is Already Canceld')", true);
        }
        if (Status == "1")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Detail is Already Approved')", true);
        }
    }
}