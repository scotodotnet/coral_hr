﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HR_Vacancy_Sub.aspx.cs" Inherits="HR_Vacancy" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upEmpPortal" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1 class="page-header">Vacancies</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            <label for="exampleInputName">Vacancy Details</label></h3>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row" style="padding-top: 1%">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="exampleInputName">Job Title</label>
                                                    <asp:TextBox ID="txtJobTitle" runat="server" class="form-control">
                                                    </asp:TextBox>
                                                    <asp:Label ID="lblJobCode" runat="server" class="form-control" Visible="false"></asp:Label> 
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="exampleInputName">Vacancy Position</label>
                                                    <asp:TextBox ID="txtPosition" runat="server" class="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="exampleInputName">Reporting Head</label>
                                                    <asp:TextBox ID="txtReporting" runat="server" class="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="exampleInputName">Job Type</label>
                                                    <asp:DropDownList ID="ddlJobType" runat="server" class="form-control select2">
                                                        <asp:ListItem Value="1">Freshers</asp:ListItem>
                                                        <asp:ListItem Value="2">Exprience</asp:ListItem>
                                                        <asp:ListItem Value="3">Both</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="exampleInputName">Recruitment</label>
                                                    <asp:TextBox ID="txtRecruitment" runat="server" class="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="exampleInputName">Valid From</label>
                                                    <asp:TextBox ID="txtFromDate" runat="server" class="form-control datepicker">
                                                    </asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="exampleInputName">Valid To</label>
                                                    <asp:TextBox ID="txtToDate" runat="server" class="form-control datepicker">
                                                    </asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="exampleInputName">Status</label>
                                                    <asp:DropDownList ID="ddlStatus" runat="server" class="form-control select2">
                                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                                        <asp:ListItem Value="2">Closed</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="exampleInputName">Recruitment Skills</label>
                                                    <asp:TextBox ID="txtRecuritSkill" runat="server" TextMode="MultiLine" Style="resize: none"
                                                        class="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <asp:Button runat="server" ID="btnSave" Text="Save" ValidationGroup="Validate_Field" class="btn btn-primary" OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger" OnClick="btnClear_Click" />
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <style>
        .well1 {
            padding: 15px;
            background: #eeeeee;
            box-shadow: none;
            -webkit-box-shadow: none
        }

        .LabelColor {
            color: #116dca
        }

        .BorderStyle {
            border: 1px solid #5f656b;
            color: #020202;
            font-weight: bold;
        }

        .select2 {
            border: 1px solid #5f656b;
            color: #020202;
            font-weight: bold;
        }
    </style>

    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <link href="../assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css" />
    <script>
        $(document).ready(function () {
            //alert('hi');
            $('#example').dataTable();
            $('#example1').dataTable();
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
        });
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
    <script type="text/javascript">
        function showimagepreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script type="text/javascript">
        function showimagepreview2(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img2').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
</asp:Content>

