﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HR_Interview_Candidate.aspx.cs" Inherits="HR_Interview_Candidate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upEmpPortal" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1 class="page-header">Employee Portal</h1>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">   
                                <div class="box-body">
                                    <div id="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1" data-toggle="tab">My Leave</a></li>
                                            <li><a href="#tab_2" data-toggle="tab">Permission</a></li>
                                            <li><a href="#tab_3" data-toggle="tab">On Duty</a></li>
                                            <li><a href="#tab_4" data-toggle="tab">Shift Roster</a></li>
                                        </ul>
                                        <!-- begin wizard step-1 -->
                                        <div class="tab-content">
                                            <div class="tab-pane active well1" id="tab_1">
                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                    <ContentTemplate>
                                                        <fieldset class="box box-primary">

                                                            <div class="box-header with-border">
                                                                <h3 class="box-title">
                                                                    <label for="exampleInputName">Leave</label></h3>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="row" style="padding-top: 1%">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName">Machine ID</label>
                                                                            <asp:TextBox ID="txtEmpCode" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">Employee Name</label>
                                                                            <asp:TextBox ID="txtEmpName" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">Employee Department</label>
                                                                            <asp:TextBox ID="txtEmpDept" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">Employee Designation</label>
                                                                            <asp:TextBox ID="txtEmpDesign" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">Leave Type</label>
                                                                            <asp:DropDownList ID="ddlleaveType" runat="server" class="form-control select2" Style="width: 100%">
                                                                                <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                                <asp:ListItem Value="1">Earned Leave</asp:ListItem>
                                                                                <asp:ListItem Value="2">Casual Leave</asp:ListItem>
                                                                                <asp:ListItem Value="3">Compensation Leave</asp:ListItem>
                                                                                <asp:ListItem Value="4">Long Leave</asp:ListItem>
                                                                                <asp:ListItem Value="5">Medical Leave</asp:ListItem>
                                                                                <asp:ListItem Value="5">Sick Leave</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">From Date</label>
                                                                            <asp:TextBox ID="txtFDate" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">To Date</label>
                                                                            <asp:TextBox ID="txtTDate" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-1">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">Avail.</label>
                                                                            <asp:TextBox ID="txtAvailLeave" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-1">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">Taken</label>
                                                                            <asp:TextBox ID="txtTakenLeave" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-1">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">Bal.</label>
                                                                            <asp:TextBox ID="txtBalLeave" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-3">
                                                                        <asp:Button runat="server" ID="Button7" Text="Save" ValidationGroup="Validate_Field" class="btn btn-primary" />
                                                                        <asp:Button runat="server" ID="Button8" Text="Clear" class="btn btn-danger" />
                                                                    </div>
                                                                </div>

                                                                <div class="row" style="padding-top: 2%">
                                                                    <div class="box box-primary">
                                                                        <div class="box-header with-border">
                                                                            <h3 class="box-title">Leave List</h3>
                                                                        </div>
                                                                        <div class="box-footer">
                                                                            <div class="form-group">
                                                                                <div class="table-responsive mailbox-messages">
                                                                                    <div class="col-md-12">
                                                                                        <div class="row">
                                                                                            <asp:Repeater ID="rptleaveDet" runat="server" EnableViewState="false">
                                                                                                <HeaderTemplate>
                                                                                                    <table class="table table-hover table-striped RowTest table-bordered">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>S.No</th>
                                                                                                                <th>Leave Type</th>
                                                                                                                <th>From Date</th>
                                                                                                                <th>To Date</th>
                                                                                                                <th>No Of Days</th>
                                                                                                                <th>Status</th>
                                                                                                                <th>Ramarks</th>
                                                                                                                <th>Mode</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <tr>
                                                                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                                                                        <td><%# Eval("LeaveType")%></td>
                                                                                                        <td><%# Eval("FDate")%></td>
                                                                                                        <td><%# Eval("TDate")%></td>
                                                                                                        <td><%# Eval("NoOfDays")%></td>
                                                                                                        <td><%# Eval("Status")%></td>
                                                                                                        <td><%# Eval("Remarks")%></td>
                                                                                                        <td><%# Eval("EmpNo")%></td>
                                                                                                        <td>
                                                                                                            <%--OnCommand="GridDeleteClick"--%>
                                                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                                                Text="" CommandName='<%# Eval("EmpNo")%>'
                                                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                                            </asp:LinkButton>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate></table></FooterTemplate>
                                                                                            </asp:Repeater>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </fieldset>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="tab-pane well1" id="tab_2">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <fieldset class="box box-primary">

                                                            <div class="box-header with-border">
                                                                <h3 class="box-title">
                                                                    <label for="exampleInputName">Permission</label></h3>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="row" style="padding-top: 1%">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName">Permission Available HR</label>
                                                                            <asp:TextBox ID="txtPerHR" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">From Date</label>
                                                                            <asp:TextBox ID="TextBox5" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-1">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">From Time</label>
                                                                            <asp:TextBox ID="txtFTime" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">To Date</label>
                                                                            <asp:TextBox ID="TextBox6" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>


                                                                     <div class="col-md-1">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">To Time</label>
                                                                            <asp:TextBox ID="txtToTime" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-1">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">Total HR</label>
                                                                            <asp:TextBox ID="TextBox7" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-3">
                                                                        <asp:Button runat="server" ID="Button9" Text="Save" ValidationGroup="Validate_Field" class="btn btn-primary" />
                                                                        <asp:Button runat="server" ID="Button10" Text="Clear" class="btn btn-danger" />
                                                                    </div>
                                                                </div>

                                                                <div class="row" style="padding-top: 2%">
                                                                    <div class="box box-primary">
                                                                        <div class="box-header with-border">
                                                                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i>Permission <span>List</span></h3>
                                                                        </div>
                                                                        <div class="box-footer">
                                                                            <div class="form-group">
                                                                                <div class="table-responsive mailbox-messages">
                                                                                    <div class="col-md-12">
                                                                                        <div class="row">
                                                                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false" Visible="false">
                                                                                                <HeaderTemplate>
                                                                                                    <table class="table table-hover table-striped RowTest">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>S.No</th>
                                                                                                                <th>Leave Type</th>
                                                                                                                <th>From Date</th>
                                                                                                                <th>To Date</th>
                                                                                                                <th>No Of Days</th>
                                                                                                                <th>Status</th>
                                                                                                                <th>Ramarks</th>
                                                                                                                <th>Mode</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <tr>
                                                                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                                                                        <td><%# Eval("LeaveType")%></td>
                                                                                                        <td><%# Eval("FDate")%></td>
                                                                                                        <td><%# Eval("TDate")%></td>
                                                                                                        <td><%# Eval("NoOfDays")%></td>
                                                                                                        <td><%# Eval("Status")%></td>
                                                                                                        <td><%# Eval("Remarks")%></td>
                                                                                                        <td><%# Eval("EmpNo")%></td>
                                                                                                        <td>
                                                                                                            <%--OnCommand="GridDeleteClick"--%>
                                                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                                                Text="" CommandName='<%# Eval("EmpNo")%>'
                                                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                                            </asp:LinkButton>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate></table></FooterTemplate>
                                                                                            </asp:Repeater>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </fieldset>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="tab-pane well1" id="tab_3">
                                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                    <ContentTemplate>
                                                        <fieldset class="box box-primary">

                                                            <div class="box-header with-border">
                                                                <h3 class="box-title">
                                                                    <label for="exampleInputName">On Duty</label></h3>
                                                            </div>

                                                            <div class="col-md-12">

                                                                <div class="row" style="padding-top: 1%">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName">From Date</label>
                                                                            <asp:TextBox ID="TextBox10" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">To Date</label>
                                                                            <asp:TextBox ID="TextBox11" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">Days</label>
                                                                            <asp:TextBox ID="TextBox12" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">From Area</label>
                                                                            <asp:TextBox ID="TextBox13" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>
</div>
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">To Area</label>
                                                                            <asp:TextBox ID="TextBox1" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">Reason</label>
                                                                            <asp:TextBox ID="TextBox2" TextMode="MultiLine" style="resize:none" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-3">
                                                                        <asp:Button runat="server" ID="Button11" Text="Save" ValidationGroup="Validate_Field" class="btn btn-primary" />
                                                                        <asp:Button runat="server" ID="Button12" Text="Clear" class="btn btn-danger" />
                                                                    </div>
                                                                </div>

                                                                <div class="row" style="padding-top: 2%">
                                                                    <div class="box box-primary">
                                                                        <div class="box-header with-border">
                                                                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i>Duty<span> List</span></h3>
                                                                        </div>
                                                                        <div class="box-footer">
                                                                            <div class="form-group">
                                                                                <div class="table-responsive mailbox-messages">
                                                                                    <div class="col-md-12">
                                                                                        <div class="row">
                                                                                            <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false" Visible="false">
                                                                                                <HeaderTemplate>
                                                                                                    <table class="table table-hover table-striped RowTest">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>S.No</th>
                                                                                                                <th>Leave Type</th>
                                                                                                                <th>From Date</th>
                                                                                                                <th>To Date</th>
                                                                                                                <th>No Of Days</th>
                                                                                                                <th>Status</th>
                                                                                                                <th>Ramarks</th>
                                                                                                                <th>Mode</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <tr>
                                                                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                                                                        <td><%# Eval("LeaveType")%></td>
                                                                                                        <td><%# Eval("FDate")%></td>
                                                                                                        <td><%# Eval("TDate")%></td>
                                                                                                        <td><%# Eval("NoOfDays")%></td>
                                                                                                        <td><%# Eval("Status")%></td>
                                                                                                        <td><%# Eval("Remarks")%></td>
                                                                                                        <td><%# Eval("EmpNo")%></td>
                                                                                                        <td>
                                                                                                            <%--OnCommand="GridDeleteClick"--%>
                                                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                                                Text="" CommandName='<%# Eval("EmpNo")%>'
                                                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                                            </asp:LinkButton>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate></table></FooterTemplate>
                                                                                            </asp:Repeater>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </fieldset>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="tab-pane well1" id="tab_4">
                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                    <ContentTemplate>
                                                        <fieldset class="box box-primary">

                                                            <div class="box-header with-border">
                                                                <h3 class="box-title">
                                                                    <label for="exampleInputName">Shift Roster</label></h3>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="row" style="padding-top: 1%">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName">Roaster Type</label>
                                                                            <asp:TextBox ID="TextBox19" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">From Shift</label>
                                                                            <asp:TextBox ID="TextBox20" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">To Shift</label>
                                                                            <asp:TextBox ID="TextBox21" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="exampleInputName">New Shift Date</label>
                                                                            <asp:TextBox ID="TextBox3" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-3">
                                                                        <asp:Button runat="server" ID="Button13" Text="Save" ValidationGroup="Validate_Field" class="btn btn-primary" />
                                                                        <asp:Button runat="server" ID="Button14" Text="Clear" class="btn btn-danger" />
                                                                    </div>
                                                                </div>

                                                                <div class="row" style="padding-top: 2%">
                                                                    <div class="box box-primary">
                                                                        <div class="box-header with-border">
                                                                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i>Shift Roster<span> List</span></h3>
                                                                        </div>
                                                                        <div class="box-footer">
                                                                            <div class="form-group">
                                                                                <div class="table-responsive mailbox-messages">
                                                                                    <div class="col-md-12">
                                                                                        <div class="row">
                                                                                            <asp:Repeater ID="Repeater3" runat="server" EnableViewState="false" Visible="false">
                                                                                                <HeaderTemplate>
                                                                                                    <table class="table table-hover table-striped RowTest">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>S.No</th>
                                                                                                                <th>Leave Type</th>
                                                                                                                <th>From Date</th>
                                                                                                                <th>To Date</th>
                                                                                                                <th>No Of Days</th>
                                                                                                                <th>Status</th>
                                                                                                                <th>Ramarks</th>
                                                                                                                <th>Mode</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <tr>
                                                                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                                                                        <td><%# Eval("LeaveType")%></td>
                                                                                                        <td><%# Eval("FDate")%></td>
                                                                                                        <td><%# Eval("TDate")%></td>
                                                                                                        <td><%# Eval("NoOfDays")%></td>
                                                                                                        <td><%# Eval("Status")%></td>
                                                                                                        <td><%# Eval("Remarks")%></td>
                                                                                                        <td><%# Eval("EmpNo")%></td>
                                                                                                        <td>
                                                                                                            <%--OnCommand="GridDeleteClick"--%>
                                                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                                                Text="" CommandName='<%# Eval("EmpNo")%>'
                                                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                                            </asp:LinkButton>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate></table></FooterTemplate>
                                                                                            </asp:Repeater>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </fieldset>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <style>
        .well1 {
            padding: 15px;
            background: #eeeeee;
            box-shadow: none;
            -webkit-box-shadow: none
        }

        .LabelColor {
            color: #116dca
        }

        .BorderStyle {
            border: 1px solid #5f656b;
            color: #020202;
            font-weight: bold;
        }

        .select2 {
            border: 1px solid #5f656b;
            color: #020202;
            font-weight: bold;
        }
    </style>

    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <link href="../assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css" />
    <script>
        $(document).ready(function () {
            //alert('hi');
            $('#example').dataTable();
            $('#example1').dataTable();
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
        });
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
    <script type="text/javascript">
        function showimagepreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script type="text/javascript">
        function showimagepreview2(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img2').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
</asp:Content>

