﻿using Altius.BusinessAccessLayer.BALDataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class HR_Vacancy_Main : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();

    string SSQL = "";
    bool ErrFlg = false;

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }

        if (!IsPostBack)
        {
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data();
    }


  

    private void Load_Data()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select Case When Status='1' Then 'Active' Else 'Close' End Status1,* from Vacancy_Mst ";
        SSQL = SSQL + " Where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        rptVacancyDet.DataSource = DT;
        rptVacancyDet.DataBind();
    }
    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "GST Master");

        if (Rights_Check == false)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit...');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Edit New Goods Received..');", true);
        }

        if (!ErrFlg)
        {
            string Enquiry_No_Str = e.CommandName.ToString();

            Session.Remove("GSTid");
            Session["GSTid"] = Enquiry_No_Str;
            Response.Redirect("/Master/GSTSub.aspx");
        }
    }

    protected void btnDeleteIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {

        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "GST Master");
        if (Rights_Check == false)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete...');", true);
        }
        //User Rights Check End

        if (!ErrFlg)
        {

            DataTable DT = new DataTable();
            SSQL = "Select * from GstMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + " GSTid='" + e.CommandName.ToString() + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                SSQL = "Update GstMaster Set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GSTid='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Customer Details Deleted Successfully');", true);
                Load_Data();
            }
        }
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        bool Rights_Check = true;

        //Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "GST Master");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add...');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Voucher..');", true);
        }
        else
        {
            Response.Redirect("HR_Vacancy_Sub.aspx");
        }
    }

    protected void rptVacancyDet_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            RepeaterItem Item = e.Item;

            Label lblStatus = Item.FindControl("lblStatus") as Label;

            if (lblStatus.Text == "Active")
            {
                lblStatus.BackColor = Color.Green;
                lblStatus.ForeColor = Color.White;

            }
            else
            {
                lblStatus.BackColor = Color.Red;
                lblStatus.ForeColor = Color.White;
            }
        }
    }
}