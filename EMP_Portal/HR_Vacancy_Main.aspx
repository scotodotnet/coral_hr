﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HR_Vacancy_Main.aspx.cs" Inherits="HR_Vacancy_Main" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upForm" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-md-1">
                            <asp:Button ID="btnAddNew" runat="server" CssClass="btn btn-primary" OnClick="btnAddNew_Click" Text="Add New" /><br />
                                <br />
                        </div>
                        <!-- /.col -->
                    </div>
                    <!--/.row-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><span>Vacancy List</span></h3>
                                    <div class="box-tools pull-right">
                                        <div class="has-feedback">
                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body no-padding">
                                    <div class="box-footer">
                                                    <div class="form-group">
                                                        <div class="table-responsive mailbox-messages">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <asp:Repeater ID="rptVacancyDet" runat="server" EnableViewState="false" OnItemDataBound="rptVacancyDet_ItemDataBound">
                                                                        <HeaderTemplate>
                                                                            <table class="table table-hover table-striped RowTest table-bordered">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>S.No</th>
                                                                                        <th>Job Title</th>
                                                                                        <th>Position</th>
                                                                                        <th>Hiring Manager</th>
                                                                                        <th>JobType</th>
                                                                                        <th>Recruitment</th>
                                                                                        <th>Valid From</th>
                                                                                        <th>Valid To</th>
                                                                                        <th>Status</th>
                                                                                        <th>Mode</th>
                                                                                    </tr>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><%# Container.ItemIndex + 1 %></td>
                                                                                <td><%# Eval("JobTitleName")%></td>
                                                                                <td><%# Eval("VacancyPasition")%></td>
                                                                                <td><%# Eval("ReportingHead")%></td>
                                                                                <td><%# Eval("JobType")%></td>
                                                                                <td><%# Eval("Recruitment")%></td>
                                                                                <td><%# Eval("ValidFrom")%></td>
                                                                                <td><%# Eval("ValidTo")%></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblStatus" runat="server" class="form-control" Text='<%# Eval("Status1") %>'></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <%--OnCommand="GridDeleteClick"--%>
                                                                                    <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                        Text="" CommandName='<%# Eval("JobId")%>'
                                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate></table></FooterTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                </div>
                              
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
   
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!-- iCheck -->
    <script src="assets/adminlte/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('.checkbox').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
</asp:Content>

