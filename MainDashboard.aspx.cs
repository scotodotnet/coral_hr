﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using System.Data;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MainDashboard : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Dashboard";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_DashBoard"));
            li.Attributes.Add("class", "droplink active open");

            string SSQL = "";

            SSQL = "Select * From Admin_User_Rights where CompCode = '" + Session["Ccode"].ToString() + "' And ";
            SSQL = SSQL + " LocCode = '" + Session["Lcode"].ToString() + "' And ";
            SSQL = SSQL + " UserName = '" + Session["Usernmdisplay"].ToString() + "' And ModuleID = '1' And MenuID = '1' ";
            SSQL = SSQL + " And FormName = 'Dash board' And ViewRights = '1'";

            DataTable DTDash = new DataTable();

            DTDash = objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Barchart();
            Load_Pie_Chart();
            if (SessionUserID.ToString() == "Scoto")
            {
                Load_Barchart();
                Load_Pie_Chart();
            }
            else
            {
                if (DTDash.Rows.Count > 0)
                {
                    Load_Barchart();
                    Load_Pie_Chart();
                }
            }
            
        }

    }

    public void Load_Barchart()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        lblTit1.Visible = true;

        SSQL = "Select CAST((Sum(Present)/DAY(DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-3,-1)))  as decimal(18,0)) as Days,DeptName from Logtime_Days";
        SSQL = SSQL + " where CompCode='"+SessionCcode+"' and LocCode='"+SessionLcode+ "' and Convert(date,Attn_Date,103)>=Convert(date,DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3,0),103)";
        SSQL = SSQL + " and Convert(date,Attn_Date,103)<=Convert(date,DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-3,-1),103) group by Deptname order by DeptName";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        chSuppWisePurDet.DataSource = DT;
        chSuppWisePurDet.Visible = false;
        chSuppWisePurDet.Series["sriSuppPurDet"].XValueMember = "Days";
        chSuppWisePurDet.Series["sriSuppPurDet"].YValueMembers = "DeptName";
        this.chSuppWisePurDet.Series[0]["PieLabelStyle"] = "Outside";
        this.chSuppWisePurDet.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.chSuppWisePurDet.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.chSuppWisePurDet.Series[0].BorderWidth = 1;
        this.chSuppWisePurDet.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.chSuppWisePurDet.Legends.Add("Legend1");
        this.chSuppWisePurDet.Legends[0].Enabled = true;
        this.chSuppWisePurDet.Legends[0].Docking = Docking.Bottom;
        this.chSuppWisePurDet.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        
        chSuppWisePurDet.DataBind();

        this.Chart1.DataSource = DT;
        this.Chart1.Series[0].ChartType = (SeriesChartType)int.Parse("10");
        this.Chart1.Legends[0].Enabled = true;
        this.Chart1.Series[0].XValueMember = "DeptName";
        this.Chart1.Series[0].YValueMembers = "Days";
        this.Chart1.DataBind();

        Chart1.DataSource = DT;
    }

    public void Load_Pie_Chart()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        lblTit2.Visible = true;

        SSQL = "Select ItemName,sum(ReceiveQty)Qty from Trans_GoodsReceipt_Sub ";
        SSQL = SSQL + " Where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "' ";
        SSQL = SSQL + " Group By ItemName ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        chItemWisePurDet.DataSource = DT;

        chItemWisePurDet.Series["sriItemPurDet"].XValueMember = "ItemName";
        chItemWisePurDet.Series["sriItemPurDet"].YValueMembers = "Qty";
        this.chItemWisePurDet.Series[0]["PieLabelStyle"] = "Outside";
        this.chItemWisePurDet.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.chItemWisePurDet.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.chItemWisePurDet.Series[0].BorderWidth = 1;
        this.chItemWisePurDet.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.chItemWisePurDet.Legends.Add("Legend1");
        this.chItemWisePurDet.Legends[0].Enabled = true;
        this.chItemWisePurDet.Legends[0].Docking = Docking.Bottom;
        this.chItemWisePurDet.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        
        chItemWisePurDet.DataBind();
    }
}