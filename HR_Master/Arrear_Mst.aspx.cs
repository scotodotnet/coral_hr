﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Master_Arrear_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Arrears Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Data_Token_No();
        }

        Load_Data_Arrear_Details();
    }

    private void Load_Data_Arrear_Details()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstArrear_HR where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    private void Load_Data_Token_No()
    {
        string query = "";
        DataTable DT = new DataTable();

        txtToken_No.Items.Clear();
        query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtToken_No.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtToken_No.DataTextField = "ExistingCode";
        txtToken_No.DataValueField = "ExistingCode";
        txtToken_No.DataBind();
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstArrear_HR where Token_No='" + e.CommandName.ToString() + "' And Salary_Month_Date='" + e.CommandArgument + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from MstArrear_HR where Token_No='" + e.CommandName.ToString() + "' And Salary_Month_Date='" + e.CommandArgument + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Arrear Details Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Arrear_Details();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstArrear_HR where Token_No='" + e.CommandName.ToString() + "' And Salary_Month_Date='" + e.CommandArgument + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            txtToken_No.SelectedValue = DT.Rows[0]["Token_No"].ToString();
            txtArrear_Amount.Text = DT.Rows[0]["Arrear_Amount"].ToString();
            txtSalary_Month_Date.Text = DT.Rows[0]["Salary_Month_Date"].ToString();
            txtArrear_Start_Period.Text = DT.Rows[0]["Salary_Process_Period"].ToString();

            txtEmp_Name.Text = DT.Rows[0]["EmpName"].ToString();
            txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            txtToken_No.SelectedIndex = 0;
            txtArrear_Amount.Text = "0.0";
            txtSalary_Month_Date.Text = "";
            txtArrear_Start_Period.Text = "";
            txtEmp_Name.Text = "";
            txtDepartment.Text = "";

            btnSave.Text = "Save";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        bool ErrFlag = false;
        DataTable DT = new DataTable();

        if (txtToken_No.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Token No...');", true);
        }
        if (txtArrear_Amount.Text == "" || txtArrear_Amount.Text == "0" || txtArrear_Amount.Text == "0.0" || txtArrear_Amount.Text == "0.00")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Arrear Amount...');", true);
        }

        if (txtArrear_Start_Period.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Arrear Starting Period...');", true);
        }

        if (txtSalary_Month_Date.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Salary Month Date...');", true);
        }

        if (!ErrFlag)
        {
            query = "Select * from MstArrear_HR where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            query = query + " And Token_No='" + txtToken_No.SelectedValue + "' And Salary_Month_Date='" + txtSalary_Month_Date.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "delete from MstArrear_HR where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                query = query + " And Token_No='" + txtToken_No.SelectedValue + "' And Salary_Month_Date='" + txtSalary_Month_Date.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //insert
            query = "Insert into MstArrear_HR(CompCode,LocCode,Token_No,Arrear_Amount,Salary_Month_Date,Salary_Process_Period,EmpName,DeptName) Values('" + SessionCcode + "','" + SessionLcode + "',";
            query = query + " '" + txtToken_No.SelectedValue + "','" + txtArrear_Amount.Text + "','" + txtSalary_Month_Date.Text + "','" + txtArrear_Start_Period.Text + "','" + txtEmp_Name.Text + "','" + txtDepartment.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Arrear Details Updated Successfully...!');", true);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Arrear Details Saved Successfully...!');", true);
            }

            txtToken_No.SelectedIndex = 0; txtArrear_Amount.Text = "0.0"; txtArrear_Start_Period.Text = "";
            txtSalary_Month_Date.Text = "";
            txtEmp_Name.Text = ""; txtDepartment.Text = "";
            Load_Data_Arrear_Details();

        }

        
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtToken_No.SelectedIndex = 0; txtArrear_Amount.Text = "0.0"; txtArrear_Start_Period.Text = "";
        txtSalary_Month_Date.Text = "";
        txtEmp_Name.Text = ""; txtDepartment.Text = "";

        btnSave.Text = "Save";
    }

    protected void txtToken_No_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        query = query + " And ExistingCode='" + txtToken_No.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtEmp_Name.Text = DT.Rows[0]["FirstName"].ToString();
            txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
        }
        else
        {
            txtEmp_Name.Text = "";
            txtDepartment.Text = "";
        }
    }
}
