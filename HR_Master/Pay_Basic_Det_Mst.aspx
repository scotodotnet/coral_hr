﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Pay_Basic_Det_Mst.aspx.cs" Inherits="HR_Master_Pay_Basic_Det_Mst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<!-- begin #content -->
<div id="content" class="content-wrapper">
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>
    <section class="content">
    <ol class="breadcrumb pull-right">
	    <li class="active">Salary Category</li>
	</ol>
	<h1 class="page-header">Salary Category</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                
                <div class="panel-body">
                    <asp:UpdatePanel ID="Basic" runat="server">
                    <ContentTemplate>
                    <div class="form-group">
                        <h5>STAFF And WORKER BASIC DETAILS</h5>
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Category</label>
								    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control select2"  AutoPostBack="true"
                                        onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								       <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
								       <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                    
                                
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Employee Type</label>
								    <asp:DropDownList runat="server" ID="ddlEmployeeType" AutoPostBack="true"
                                        class="form-control select2" style="width:100%;" 
                                        onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Basic</label>
								    <asp:TextBox runat="server" ID="txtBasic" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>DA</label>
								    <asp:TextBox runat="server" ID="txtSpl" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>HRA</label>
								    <asp:TextBox runat="server" ID="txtHRA" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>PF Salary</label>
								    <asp:TextBox runat="server" ID="txtUniform" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div id="Div1" class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Vehicle Maintenance</label>
								    <asp:TextBox runat="server" ID="txtvehicle" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div id="Div2" class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Communication</label>
								    <asp:TextBox runat="server" ID="txtCommunication" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div id="Div3" class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Journals & Periodical Paper</label>
								    <asp:TextBox runat="server" ID="txtJornal" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div id="Div4" class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Conv.Allow</label>
								    <asp:TextBox runat="server" ID="txtConv" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div id="Div5" class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Edu.Allow</label>
								    <asp:TextBox runat="server" ID="txtEdu" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                           
                            <div id="Div6" class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>RAI</label>
								    <asp:TextBox runat="server" ID="txtRAI" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div id="Div7" class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Washing Allow</label>
								    <asp:TextBox runat="server" ID="txtWash" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                            <div id="Div8" class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Incentives</label>
								    <asp:TextBox runat="server" ID="txtIncentive" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                             <div id="Div9" class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Medical</label>
								    <asp:TextBox runat="server" ID="txtMedi" Text="0.00" class="form-control" style="width:100%;" Enabled="false"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                        onclick="btnClear_Click" />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </div>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>
   

</asp:Content>

