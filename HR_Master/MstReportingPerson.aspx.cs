﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstReportingPerson : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Reporting Person Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
        }

        Load_Data_ReportingPerson();
    }

    private void Load_Data_ReportingPerson()
    {
        SSQL = "";
        SSQL = "Select * from MstReportingPerson where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();


        query = "Delete  from MstReportingPerson where Name='" + txtName.Text + "' and CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (btnSave.Text == "Update")
        {
            SaveMode = "Update";

        }
        else
        {

            SaveMode = "Insert";
        }
        query = "Insert into MstReportingPerson (Name,Ccode,Lcode)";
        query = query + "values('" + txtName.Text + "','" + SessionCcode + "','" + SessionLcode + "')";
        objdata.RptEmployeeMultipleDetails(query);


        if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Reporting Person Updated Successfully...!');", true);
        }
        else if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Reporting Person Saved Successfully...!');", true);
        }
        else if (SaveMode == "Already")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Qualification Already Exists!');", true);
        }

        Load_Data_ReportingPerson();
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtName.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstReportingPerson where Auto_ID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        

        if (DT.Rows.Count > 0)
        {
            txtName.Text = DT.Rows[0]["Name"].ToString();


            btnSave.Text = "Update";
        }
        else
        {
            txtName.Text = "";

            btnSave.Text = "Save";
        }
        Load_Data_ReportingPerson();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from MstReportingPerson where Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data_ReportingPerson();
    }
}
