﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Master_MstHoliday : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    string MonthID;
    string Month;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Holiday Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Fin_Year_Add();
        }

        Load_Data_NFH();
    }

    public void Fin_Year_Add()
    {
        int CurrentYear;


        CurrentYear = System.DateTime.Now.Year;

        ddlYear.Items.Clear();
        ddlYear.Items.Add("-Select-");
        for (int i = 0; i < 11; i++)
        {
            ddlYear.Items.Add(Convert.ToString(CurrentYear));
            CurrentYear = CurrentYear - 1;
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        string Double_Wages_Statutory = "";
        string Form25_NFH_Present = "";

        DataTable DT_NFH = new DataTable();

        if (txtDesc.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Description...');", true);
        }

        int ServerYr = Convert.ToInt32(ddlYear.SelectedItem.Text);
        int yr = Convert.ToDateTime(txtLeaveDate.Text).Year;

        if (ServerYr > yr)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select Year Properly.');", true);
        }
        else
        {
            yr = Convert.ToDateTime(txtLeaveDate.Text).Year;
            MonthID = Convert.ToDateTime(txtLeaveDate.Text).Month.ToString();
            if (MonthID == "1")
            {
                Month = "January";
            }
            else if (MonthID == "2")
            {
                Month = "February";
            }
            else if (MonthID == "3")
            {
                Month = "March";
            }
            else if (MonthID == "4")
            {
                Month = "April";
            }
            else if (MonthID == "5")
            {
                Month = "May";
            }
            else if (MonthID == "6")
            {
                Month = "June";
            }
            else if (MonthID == "7")
            {
                Month = "July";
            }
            else if (MonthID == "8")
            {
                Month = "August";
            }
            else if (MonthID == "9")
            {
                Month = "September";
            }
            else if (MonthID == "10")
            {
                Month = "October";
            }
            else if (MonthID == "11")
            {
                Month = "November";
            }
            else if (MonthID == "12")
            {
                Month = "December";
            }
        }

        if (chkDoubleWages.Checked == true)
        {
            Double_Wages_Statutory = "Yes";
        }
        else
        {
            Double_Wages_Statutory = "No";
        }

        if (chkForm25.Checked == true)
        {
            Form25_NFH_Present = "Yes";
        }
        else
        {
            Form25_NFH_Present = "No";
        }

        if (!ErrFlag)
        {
            if (btnSave.Text == "Save")
            {
                query = "Select Convert(varchar,NFHDate,105) as NFHDate from NFH_Mst where NFHDate=convert(datetime,'" + txtLeaveDate.Text + "',105)";
                DT_NFH = objdata.RptEmployeeMultipleDetails(query);
                if (DT_NFH.Rows.Count != 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Date was Already Registered');", true);
                }
                else
                {
                    query = "Insert into NFH_Mst (NFHDate,Months,Year,Description,DateStr,NFH_Type,Dbl_Wages_Statutory,Form25_NFH_Present,Form25DisplayText) ";
                    query = query + "values (convert(DateTime,'" + txtLeaveDate.Text + "',103),'" + Month + "','" + yr + "','" + txtDesc.Text + "',convert(varchar(20),'" + txtLeaveDate.Text + "'),";
                    query = query + "'Single Wages','" + Double_Wages_Statutory + "','" + Form25_NFH_Present + "','" + txtForm25Text.Text + "')";
                    objdata.RptEmployeeMultipleDetails(query);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
                }
            }
            else if (btnSave.Text == "Update")
            {
                query = "Update NFH_Mst set NFHDate=convert(datetime,'" + txtLeaveDate.Text + "',103), Months='" + Month + "',Year='" + yr + "',";
                query = query + "Description='" + txtDesc.Text + "',DateStr=convert(varchar(20),'" + txtLeaveDate.Text + "'),NFH_Type='Single Wages',";
                query = query + "Dbl_Wages_Statutory='" + Double_Wages_Statutory + "',Form25_NFH_Present='" + Form25_NFH_Present + "',Form25DisplayText='" + txtForm25Text.Text + "' where ID='" + txtID.Value + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Updated Successfully...!');", true);
            }

        }
        Load_Data_NFH();
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void ddlNFHType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlNFHType.SelectedItem.Text == "Double Wages Checklist")
        {
            chkDoubleWages.Enabled = true;
        }
        else
        {
            chkDoubleWages.Enabled = false;

        }
        chkDoubleWages.Checked = false;
    }

    private void Clear_All_Field()
    {
        ddlYear.SelectedValue = "-Select-";
        txtDesc.Text = "";
        ddlNFHType.SelectedValue = "-Select-";
        chkDoubleWages.Checked = false;
        chkDoubleWages.Enabled = false;
        chkForm25.Checked = false;
        txtForm25Text.Text = "";
        txtLeaveDate.Text = "";
        txtID.Value = "";
        btnSave.Text = "Save";
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from NFH_Mst where ID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from NFH_Mst where ID='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Holiday Details Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Holiday Details Not Found');", true);
        }
        Load_Data_NFH();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from NFH_Mst where ID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtID.Value = DT.Rows[0]["ID"].ToString();
            ddlYear.SelectedValue = DT.Rows[0]["Year"].ToString();
            txtLeaveDate.Text = DT.Rows[0]["DateStr"].ToString();
            txtDesc.Text = DT.Rows[0]["Description"].ToString();
            //ddlNFHType.SelectedValue = DT.Rows[0]["NFH_Type"].ToString();

            //if (DT.Rows[0]["Dbl_Wages_Statutory"].ToString() == "Yes")
            //{
            //    chkDoubleWages.Checked = true;
            //}
            //else
            //{
            //    chkDoubleWages.Checked = false;
            //}
            //if (DT.Rows[0]["Form25_NFH_Present"].ToString() == "Yes")
            //{
            //    chkForm25.Checked = true;
            //}
            //else
            //{
            //    chkForm25.Checked = false;
            //}
            //txtForm25Text.Text = DT.Rows[0]["Form25DisplayText"].ToString();

            //if (ddlNFHType.SelectedItem.Text == "Double Wages Checklist")
            //{
            //    chkDoubleWages.Enabled = true;
            //}
            //else
            //{
            //    chkDoubleWages.Enabled = false;

            //}

            btnSave.Text = "Update";
        }
        else
        {
            ddlYear.SelectedValue = "-Select-";
            txtDesc.Text = "";
            ddlNFHType.SelectedValue = "-Select-";
            chkDoubleWages.Checked = false;
            chkDoubleWages.Enabled = false;
            chkForm25.Checked = false;
            txtForm25Text.Text = "";
            txtID.Value = "";
            btnSave.Text = "Save";
        }
    }

    private void Load_Data_NFH()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from NFH_Mst where Year='" + System.DateTime.Now.Year + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_NFH();
    }
}
