﻿<%@ Page Title="Qualification Master" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstQualify.aspx.cs" Inherits="HR_Master_MstQualify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();

        });
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('#example').dataTable();
                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>

    <!-- begin #content -->
    <div id="content" class="content-wrapper">
        <section class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">Master</a></li>
                <li class="active">Qualification</li>
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Qualification </h1>
            <!-- end page-header -->

            <!-- begin row -->
            <div class="row">
                <!-- begin col-12 -->
                <div class="col-md-12">
                    <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <div class="panel-body">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Qualification</label>
                                                <asp:TextBox runat="server" ID="txtQualify" class="form-control">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ControlToValidate="txtQualify" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                    ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger"
                                                    OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                    </div>
                                    <!-- end row -->

                                    <!-- table start -->
                                    <div class="col-md-12">
                                        <div class="row">
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                <th>Qualification</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("Qualification")%></td>

                                                        <td>
                                                            <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("Qualification")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("Qualification")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Qualification details?');">
                                                            </asp:LinkButton>
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                    <!-- table End -->

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </section>
    </div>
    <!-- end #content -->



</asp:Content>

