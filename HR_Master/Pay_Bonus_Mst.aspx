﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Pay_Bonus_Mst.aspx.cs" Inherits="HR_Master_Pay_Bonus_Mst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="content" class="content-wrapper">
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>
    <section class="content">
    <ol class="breadcrumb pull-right">
	    <li class="active">Bonus Master Details</li>
	</ol>
	<h1 class="page-header">BONUS MASTER DETAILS</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-body">
                    <div class="form-group">
                        <h5>STAFF AND REGULAR WORKER BONUS FIXED DETAILS</h5>
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Category</label>
								    <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                        onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								        <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
								       <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
								    </asp:DropDownList>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Employee Type</label>
								    <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" style="width:100%;" AutoPostBack="true"
                                        onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Gross Salary %</label>
								    <asp:TextBox runat="server" ID="txtGrossSalPercent" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Below 1 Year Days</label>
								    <asp:TextBox runat="server" ID="txtbelowoneYear" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Above Days %</label>
								    <asp:TextBox runat="server" ID="txtAboveDays" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Below Days %</label>
								    <asp:TextBox runat="server" ID="txtBelowDays" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Above 1 Year %</label>
								    <asp:TextBox runat="server" ID="txtAboveOneYear" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Min. Amount</label>
								    <asp:TextBox runat="server" ID="txtMinAmount" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave1" Text="Save" class="btn btn-success" onclick="btnSave1_Click" />
									<asp:Button runat="server" id="btnClear1" Text="Clear" class="btn btn-danger" onclick="btnClear1_Click" />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                         <%--<div class="panel-body">
                        <h5>HOSTEL WORKER BONUS FIXED DETAILS</h5>
                        <div class="row">
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Below 1 Year Days</label>
								    <asp:TextBox runat="server" ID="TextBox7" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Above Days</label>
								    <asp:TextBox runat="server" ID="TextBox8" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Below Days</label>
								    <asp:TextBox runat="server" ID="TextBox9" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Above 1 Year And Below 2 Year</label>
								    <asp:TextBox runat="server" ID="TextBox10" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Above 2 Year And Below 3 Year</label>
								    <asp:TextBox runat="server" ID="TextBox11" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Above 3 Year</label>
								    <asp:TextBox runat="server" ID="TextBox12" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Min. Amount</label>
								    <asp:TextBox runat="server" ID="TextBox13" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="Button1" Text="Save" class="btn btn-success" />
									<asp:Button runat="server" id="Button2" Text="Clear" class="btn btn-danger" />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        </div>--%>
                        <h5>EL Percentage Fixed</h5>
                        <div class="row">
                            <div class="col-md-4">
								<div class="form-group">
								    <label>EL Year</label>
								    <asp:DropDownList runat="server" ID="txtELYear" class="form-control select2" style="width:100%;" AutoPostBack="true"
                                        onselectedindexchanged="txtELYear_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>EL Percentage</label>
								    <asp:TextBox runat="server" ID="txtEL_Percentage" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnELPercentSave" Text="Save" class="btn btn-success" onclick="btnELPercentSave_Click" />
									
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>

</asp:Content>

