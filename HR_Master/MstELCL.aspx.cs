﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

using Newtonsoft.Json;
using GeneralClass;



public partial class HR_Master_MstELAndCL : System.Web.UI.Page
{
    public static BALDataAccess objdata = new BALDataAccess();

    public static String CurrentYear1;
    public static int CurrentYear;

    public static string SessionCcode;
    public static string SessionLcode;
    public static string SessionUserID;
    public static string SessionUserName;
    public static string SessionRights;

    string MonthID;
    string Month;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Leave Master";
        }
    }
    
    public void Financial_Year()
    {
        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());

        for (int i = 0; i < 11; i++)
        {
            string tt = (CurrentYear1);

            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;
        }
    }

    [WebMethod]
    public static List<EmpList> Load_Emp_Master()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select ExistingCode[EmpCode],ExistingCode+'--->'+FirstName[EmpName] From Employee_Mst ";
        SSQL = SSQL + " where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='Yes'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            try
            {
                List<EmpList> EmpMst = new List<EmpList>();

                foreach (DataRow dr in DT.Rows)
                {
                    EmpMst.Add(new EmpList {
                        EmpCode = dr["EmpCode"].ToString(),
                        EmpName = dr["EmpName"].ToString()
                    });
                }
                return EmpMst.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        else
        {
            List<EmpList> EmpMst = new List<EmpList>();
            return EmpMst.ToList();
        }
    }

    [WebMethod]
    public static List<LoadYears> Load_Years()
    {
        DataTable DT = new DataTable();

        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());

        List<LoadYears> Year_Mst = new List<LoadYears>();

        for (int i = 0; i < 11; i++)
        {
            string tt = (CurrentYear1);

            if (i != 0)
            {
                CurrentYear = CurrentYear - 1;
            }

            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;

            Year_Mst.Add(new LoadYears
            {
                years = CurrentYear1
            });
        }
        return Year_Mst.ToList();
    }

    [WebMethod]
    public static List<EmpSelect>SelectEmp(string EmpNo)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * From Employee_Mst ";
        SSQL = SSQL + " Where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='Yes' And ";
        SSQL = SSQL + " ExistingCode='" + EmpNo + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            try
            {
                List<EmpSelect> SelEmp = new List<EmpSelect>();

                foreach (DataRow dr in DT.Rows)
                {
                    SelEmp.Add(new EmpSelect
                    {
                        SEmpName = dr["FirstName"].ToString(),
                        sEmpDept=dr["DeptName"].ToString()
                    });
                }
                return SelEmp.ToList();
            }
            catch(Exception ex)
            {
                List<EmpSelect> SelEmp = new List<EmpSelect>();
                return SelEmp.ToList();
            }
          
        }
        else
        {
            List<EmpSelect> SelEmp = new List<EmpSelect>();
            return SelEmp.ToList();
        }
    }

    [WebMethod]
    public static string SaveEmpLeaveDet(string EmpCode, string EmpName, string EmpDept, string year, string FDate, string TDate, string EL, string CL, string SL)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string result = "true";
        Boolean errflg = false;

        string Month = "";
         
        SSQL = "Select * From CLMst where EmpNo='" + EmpCode + "' and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " Years='" + year + "' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {
            errflg = true;
        }

        if (!errflg)
        {
            SSQL = "Insert Into CLMst (Ccode,Lcode,EmpNo,EmpName,DeptName,Months,Years,FromDate,ToDate,ELAddDays,ELMinDays,CLAddDays,";
            SSQL = SSQL + " CLMinDays,MinDate,SLAddDays,SLMinDays)Values('" + SessionCcode + "','" + SessionLcode + "','" + EmpCode + "',";
            SSQL = SSQL + " '" + EmpName + "','" + EmpDept + "','" + Month + "','" + year + "','" + FDate + "','" + TDate + "','" + EL + "',";
            SSQL = SSQL + " '0','" + CL + "','0','0','" + SL + "','0')";

            objdata.RptEmployeeMultipleDetails(SSQL);
        }

        return result;
    }

    [WebMethod]
    private static List<EmpLeaveDet> GetEmpLeaveMst()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from CLMst where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            try
            {
                List<EmpLeaveDet> GetEmpLeaveDet = new List<EmpLeaveDet>();
                
                foreach (DataRow dr in DT.Rows)
                {

                    GetEmpLeaveDet.Add(new EmpLeaveDet
                    {
                        EmpNo = dr["EmpNo"].ToString(),
                        EmpName = dr["EmpName"].ToString(),
                        EmpDept = dr["DeptName"].ToString(),
                        Year = dr["Years"].ToString(),
                        FDate = dr["FromDate"].ToString(),
                        TDate = dr["ToDate"].ToString(),
                        EL = dr["ELAddDays"].ToString(),
                        CL = dr["CLAddDays"].ToString(),
                        SL = dr["SLAddDays"].ToString()
                    });
                }

                return GetEmpLeaveDet.ToList();
            }

            catch (Exception ex)
            {
                return null;
            }
        }

        else
        {
            List<EmpLeaveDet> GetEmpLeaveDet = new List<EmpLeaveDet>();
            return GetEmpLeaveDet.ToList();
        }

    }

}
