﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstDeptartment.aspx.cs" Inherits="HR_Master_MstDeptartment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">




    <%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
            $('#example1').dataTable();
            $('#example2').dataTable();
            $('#example3').dataTable();
            $('#example4').dataTable();
            $('#example5').dataTable();
            $('#example6').dataTable();
            $('#example7').dataTable();

        });
    </script>

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                    $('#example1').dataTable();
                    $('#example2').dataTable();
                    $('#example3').dataTable();
                    $('#example4').dataTable();
                    $('#example5').dataTable();
                    $('#example6').dataTable();
                    $('#example7').dataTable();
                }
            });
        };
    </script>
    <%--<asp:UpdatePanel runat="server">
<ContentTemplate>--%>
    <!-- begin #content -->
    <div class="content-wrapper">
        <section class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">Master</a></li>
                <li class="active">Category Master</li>
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Category Master </h1>
            <!-- end page-header -->

            <!-- begin row -->
            <div class="row">
                <!-- begin col-12 -->
                <div class="col-md-12">
                    <!-- begin panel -->
                    <div class="panel panel-inverse">

                        <div class="panel-body">

                            <div id="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab">Department</a></li>
                                    <li><a href="#tab_2" data-toggle="tab">Designation</a></li>
                                    <li><a href="#tab_3" data-toggle="tab">Wages Type</a></li>
                                    <li><a href="#tab_4" data-toggle="tab">Division</a></li>
                                    <li><a href="#tab_5" data-toggle="tab">Taluk</a></li>
                                    <li><a href="#tab_6" data-toggle="tab">District</a></li>
                                    <li><a href="#tab_7" data-toggle="tab">State</a></li>
                                    <li><a href="#tab_8" data-toggle="tab">Increment Level</a></li>
                                    <li><a href="#tab_9" data-toggle="tab">Sub Section</a></li>
                                </ul>
                                <!-- begin wizard step-1 -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <br />
                                                <legend class="pull-left width-full">Department</legend>

                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Department Code</label>
                                                            <asp:TextBox runat="server" ID="txtDeptCode" class="form-control"></asp:TextBox>
                                                            <asp:HiddenField runat="server" ID="txtDeptCodeHide" />
                                                            <asp:RequiredFieldValidator ControlToValidate="txtDeptCode" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtDeptCode" ValidChars="0123456789">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Department Name</label>
                                                            <asp:TextBox runat="server" ID="txtDeptName" class="form-control" Style="text-transform: uppercase"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtDeptName" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <br />
                                                            <asp:LinkButton runat="server" ID="btnDeptSave" Text="Save" class="btn btn-success" ValidationGroup="ValidateDept_Field" OnClick="btnDeptSave_Click" />
                                                            <asp:LinkButton runat="server" ID="btnDeptClear" Text="Clear" class="btn btn-danger" OnClick="btnDeptClear_Click" />
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->

                                                </div>
                                                <!-- end row -->

                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>Dept Code</th>
                                                                            <th>Dept Name</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("DeptCodeSort")%></td>
                                                                    <td><%# Eval("DeptName")%></td>
                                                                    <td>
                                                                        <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                            Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("DeptCode")%>'>
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("DeptCode")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Department details?');">
                                                                        </asp:LinkButton>
                                                                    </td>

                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->



                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-1 -->
                                    <!-- begin wizard step-2 -->
                                    <div class="tab-pane" id="tab_2">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <br />
                                                <legend class="pull-left width-full">Designation</legend>

                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Department</label>
                                                            <asp:DropDownList runat="server" ID="ddlDepartment" class="form-control select2" Style="width: 100%;">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" InitialValue="0" ValidationGroup="ValidateDesign_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Designation</label>
                                                            <asp:TextBox runat="server" ID="txtDesignation" class="form-control" Style="text-transform: uppercase"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtDesignation" ValidationGroup="ValidateDesign_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <br />
                                                            <asp:Button runat="server" ID="btnDesgnSave" Text="Save" class="btn btn-success" ValidationGroup="ValidateDesign_Field" OnClick="btnDesgnSave_Click" />
                                                            <asp:Button runat="server" ID="btnDesgnClear" Text="Clear" class="btn btn-danger" OnClick="btnDesgnClear_Click" />
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->

                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example1" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>Dept Name</th>
                                                                            <th>Designation</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("DeptName")%></td>
                                                                    <td><%# Eval("DesignName")%></td>
                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteDesignClick" CommandArgument="Delete" CommandName='<%# Eval("DesignName")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Designation details?');">
                                                                        </asp:LinkButton>
                                                                    </td>

                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->



                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-2 -->
                                    <!-- begin wizard step-3 -->
                                    <div class="tab-pane" id="tab_3">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <br />
                                                <legend class="pull-left width-full">Wages Type</legend>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Category</label>
                                                            <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" Style="width: 100%">
                                                                <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                <asp:ListItem Value="1">STAFF</asp:ListItem>
                                                                <asp:ListItem Value="2">LABOUR</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ControlToValidate="ddlCategory" InitialValue="-Select-" ValidationGroup="ValidateWages_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Wages Type</label>
                                                            <asp:TextBox runat="server" ID="txxWagesType" class="form-control" Style="text-transform: uppercase"></asp:TextBox>
                                                            <asp:HiddenField ID="txtWagesID" runat="server" />
                                                            <asp:RequiredFieldValidator ControlToValidate="txxWagesType" ValidationGroup="ValidateWages_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <br />
                                                            <asp:Button runat="server" ID="btnWagesSave" Text="Save" class="btn btn-success" ValidationGroup="ValidateWages_Field" OnClick="btnWagesSave_Click" />
                                                            <asp:Button runat="server" ID="btnWagesClear" Text="Clear" class="btn btn-danger" OnClick="btnWagesClear_Click" />
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->

                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater3" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example2" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>Wages Type</th>
                                                                            <th>Category</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("EmpType")%></td>
                                                                    <td><%# Eval("EmpCategory")%></td>
                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteWagesClick" CommandArgument="Delete" CommandName='<%# Eval("EmpTypeCd")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Wages Type details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->


                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-3 -->
                                    <!-- begin wizard step-4 -->
                                    <div class="tab-pane" id="tab_4">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <br />
                                                <legend class="pull-left width-full">Division</legend>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Division</label>
                                                            <asp:TextBox runat="server" ID="txtDivision" class="form-control" Style="text-transform: uppercase"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtDivision" ValidationGroup="ValidateDiv_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <br />
                                                            <asp:Button runat="server" ID="btnDivSave" Text="Save" ValidationGroup="ValidateDiv_Field" class="btn btn-success" OnClick="btnDivSave_Click" />
                                                            <asp:Button runat="server" ID="btnDivClear" Text="Clear" class="btn btn-danger" OnClick="btnDivClear_Click" />
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->

                                                </div>
                                                <!-- end row -->
                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater4" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example3" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>Division</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("Division")%></td>
                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteDivisionClick" CommandArgument="Delete" CommandName='<%# Eval("Division")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Division details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-4 -->
                                    <!-- begin wizard step-5 -->
                                    <div class="tab-pane" id="tab_5">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <br />
                                                <legend class="pull-left width-full">Taluk</legend>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Taluk</label>
                                                            <asp:TextBox runat="server" ID="txtTaluk" class="form-control" Style="text-transform: uppercase"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtTaluk" ValidationGroup="ValidateTaluk_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <br />
                                                            <asp:Button runat="server" ID="btnTalukSave" Text="Save" class="btn btn-success" ValidationGroup="ValidateTaluk_Field" OnClick="btnTalukSave_Click" />
                                                            <asp:Button runat="server" ID="btnTalukClear" Text="Clear" class="btn btn-danger" OnClick="btnTalukClear_Click" />
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->

                                                </div>
                                                <!-- end row -->
                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater5" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example4" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>Taluk</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("Taluk")%></td>
                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteTalukClick" CommandArgument="Delete" CommandName='<%# Eval("Taluk")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Taluk details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-5 -->
                                    <!-- begin wizard step-6 -->
                                    <div class="tab-pane" id="tab_6">
                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                            <ContentTemplate>
                                                <br />
                                                <legend class="pull-left width-full">District</legend>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>District</label>
                                                            <asp:TextBox runat="server" ID="txtDistrict" class="form-control" Style="text-transform: uppercase"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtDistrict" ValidationGroup="ValidateDistrict_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <br />
                                                            <asp:Button runat="server" ID="btnDistrictSave" Text="Save" ValidationGroup="ValidateDistrict_Field" class="btn btn-success" OnClick="btnDistrictSave_Click" />
                                                            <asp:Button runat="server" ID="btnDistrictClear" Text="Clear" class="btn btn-danger" OnClick="btnDistrictClear_Click" />
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->

                                                </div>
                                                <!-- end row -->
                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater6" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example5" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>District</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("District")%></td>
                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteDistrictClick" CommandArgument="Delete" CommandName='<%# Eval("District")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this District details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-6 -->
                                    <!-- begin wizard step-7 -->
                                    <div class="tab-pane" id="tab_7">
                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                            <ContentTemplate>
                                                <br />
                                                <legend class="pull-left width-full">State</legend>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>State</label>
                                                            <asp:TextBox runat="server" ID="txtState" class="form-control" Style="text-transform: uppercase"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtState" ValidationGroup="ValidateState_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <br />
                                                            <asp:Button runat="server" ID="btnStateSave" Text="Save" class="btn btn-success" ValidationGroup="ValidateState_Field" OnClick="btnStateSave_Click" />
                                                            <asp:Button runat="server" ID="btnStateClear" Text="Clear" class="btn btn-danger" OnClick="btnStateClear_Click" />
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->

                                                </div>
                                                <!-- end row -->

                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater7" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example6" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>State</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("State")%></td>
                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteStateClick" CommandArgument="Delete" CommandName='<%# Eval("State")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this State details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->


                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-7 -->
                                    <!-- begin wizard step-8 -->
                                    <div class="tab-pane" id="tab_8">
                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                            <ContentTemplate>
                                                <br />
                                                <legend class="pull-left width-full">Increment Level</legend>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Increment Level</label>
                                                            <asp:TextBox runat="server" ID="txtLevel" class="form-control" Style="text-transform: uppercase"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtLevel" ValidationGroup="ValidateLevel_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <br />
                                                            <asp:Button runat="server" ID="btnLevelSave" Text="Save" ValidationGroup="ValidateLevel_Field" class="btn btn-success" OnClick="btnLevelSave_Click" />
                                                            <asp:Button runat="server" ID="btnLevelClear" Text="Clear" class="btn btn-danger" OnClick="btnLevelClear_Click" />
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->

                                                </div>
                                                <!-- end row -->
                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater8" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example7" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>Level</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("LevelCode")%></td>
                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteLevelClick" CommandArgument="Delete" CommandName='<%# Eval("LevelCode")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Level details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->



                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-8 -->
                                    <!-- begin wizard step-9 -->
                                    <div class="tab-pane" id="tab_9">
                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                            <ContentTemplate>
                                                <br />
                                                <legend class="pull-left width-full">Sub Section</legend>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Sub Section Name</label>
                                                            <asp:TextBox runat="server" ID="txtSubSectionName" class="form-control" Style="text-transform: uppercase"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtSubSectionName" ValidationGroup="ValidateLevel_Field_Sub_Section" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <br />
                                                            <asp:Button runat="server" ID="btnSubSectionSave" Text="Save" ValidationGroup="ValidateLevel_Field_Sub_Section" class="btn btn-success" OnClick="btnSubSectionSave_Click" />
                                                            <asp:Button runat="server" ID="btnSubSectionClr" Text="Clear" class="btn btn-danger" OnClick="btnSubSectionClr_Click" />
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->

                                                </div>
                                                <!-- end row -->
                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater9" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example7" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>Sub Section Name</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("SubSection")%></td>
                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteEnquirySubSection_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="btnDeleteEnquirySubSection_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("Auto_ID")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Sub Section details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->



                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- end wizard step-9 -->

                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </section>
    </div>
    <!-- end #content -->
    <%--</ContentTemplate>
</asp:UpdatePanel>--%>
</asp:Content>

