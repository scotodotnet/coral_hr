﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstELCL.aspx.cs" Inherits="HR_Master_MstELAndCL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:updatepanel id="upELCL" runat="server">
            <ContentTemplate>
                <section class="content-header">
                    <h1 class="page-header">Earned and Casual Leave Master</h1>
                </section>

                <section class="content">
                    <div class="row pnlLeave" >
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="exampleInputName">Employee Code</label>
                                                <select id="ddlEmpCode" class="form-control select2 ReqSel" style="width: 100%"></select>
                                                <p class="form-error"></p>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputName">Employee Name</label>
                                                <label id="txtEmpName" class="form-control"></label>
                                                <span class="form-error"></span>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputName">Department</label>
                                                <label id="txtDept" class="form-control"></label>
                                                <span class="form-error"></span>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputName">Year</label>
                                                <select id="ddlYear" class="form-control select2 ReqSel" style="width: 100%"></select>
                                                <p class="form-error"></p>
                                            </div>
                                        </div>

                                        <div class="row" style="padding-top: 1.5%">
                                            <div class="col-md-3">
                                                <label for="exampleInputName">From Date</label>
                                                <input type="text" id="txtFDate" class="form-control datepicker Req" />
                                                <span class="form-error"></span>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputName">To Date</label>
                                                <input type="text" id="txtTDate" class="form-control datepicker Req" />
                                                <span class="form-error"></span>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="exampleInputName">Earned Leaves</label>
                                                <input type="text" id="txtEL" class="form-control Req" 
                                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                                <span class="form-error"></span>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="exampleInputName">Casual Leave</label>
                                                <input type="text" id="txtCL" class="form-control Req"
                                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                                <span class="form-error"></span>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="exampleInputName">Sick Leave</label>
                                                <input type="text" id="txtSL" class="form-control Req" 
                                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                                <span class="form-error"></span>
                                            </div>
                                        </div>

                                        <div class="row" style="padding-top: 1.5%">
                                            <div class="col-md-3 col-sm-3">
                                                 <input type="button" id="btnSaveLeave" class="btn btn-primary" value="Save" />
                                            </div>
                                        </div>

                                        <div class="row" style="padding-top: 2%">
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Leave List</h3>
                                                </div>
                                                <div class="box-footer">
                                                    <div class="form-group">
                                                        <div class="table-responsive mailbox-messages">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="divTable">
                                                                        <table id="TblLeaveDet" class="display table table-bordered table-responsive">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>S.No</th>
                                                                                    <th>Employee No</th>
                                                                                    <th>Employee Name</th>
                                                                                    <th>Department</th>
                                                                                    <th>Year</th>
                                                                                    <th>From Date</th>
                                                                                    <th>To Date</th>
                                                                                    <th>Earned Leave</th>
                                                                                    <th>Casual Leave</th>
                                                                                    <th>Sick Leave</th>
                                                                                </tr>
                                                                            </thead>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:updatepanel>
    </div>
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/Scripts/HR_ELCL_Mst.js"></script>
</asp:Content>

