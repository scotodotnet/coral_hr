﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Arrear_Mst.aspx.cs" Inherits="HR_Master_Arrear_Mst" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>

<!-- begin #content -->
<div id="content" class="content-wrapper">
    <section class="content">		
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">Arrears Master</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Arrears Master</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="LabelColor">Token No</label><span class="mandatory">*</span>
                                        <asp:DropDownList ID="txtToken_No" runat="server" AutoPostBack="true"
                                            class="form-control select2" Style="width: 100%;"
                                            OnSelectedIndexChanged="txtToken_No_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Arrears Amount</label>
								  <asp:TextBox runat="server" ID="txtArrear_Amount" class="form-control" Text="0.0">
								  </asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtArrear_Amount" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtArrear_Amount" ValidChars="0123456789.">
                                  </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                               <div class="col-md-3">
								<div class="form-group">
								  <label>Salary Month Date</label>
								  <asp:TextBox runat="server" ID="txtSalary_Month_Date" class="form-control datepicker">
								  </asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtSalary_Month_Date" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtSalary_Month_Date" ValidChars="0123456789/">
                                  </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                               <div class="col-md-3">
								<div class="form-group">
								  <label>Arrear Starting Period</label>
								  <asp:TextBox runat="server" ID="txtArrear_Start_Period" class="form-control datepicker">
								  </asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtArrear_Start_Period" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtArrear_Start_Period" ValidChars="0123456789/">
                                  </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                              <!-- end col-4 -->
                             <!-- begin col-4 -->
                                <div class="col-md-2">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnSave_Click"  />
								 </div>
                               </div>
                              <!-- end col-4 -->
                          
                              </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-md-3">
								<div class="form-group">
								  <label>Employee Name</label>
								  <asp:Label runat="server" ID="txtEmp_Name" class="form-control" BackColor="LightGray"></asp:Label>
								</div>
                            </div>
                            <div class="col-md-3">
								<div class="form-group">
								  <label>Department</label>
								  <asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="LightGray"></asp:Label>
								</div>
                            </div>
                        </div>
                    
                            <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Token No</th>
                                                <th>Name</th>
                                                <th>Arrear Amount</th>
                                                <th>Salary Month</th>
                                                <th>Arrear Period</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("Token_No")%></td>
                                        <td><%# Eval("EmpName")%></td>
                                        <td><%# Eval("Arrear_Amount")%></td>
                                        <td><%# Eval("Salary_Month_Date")%></td>
                                        <td><%# Eval("Salary_Process_Period")%></td>
                                        
                                        <td>
                                                   <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument='<%# Eval("Salary_Month_Date")%>' CommandName='<%# Eval("Token_No")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("Salary_Month_Date")%>' CommandName='<%# Eval("Token_No")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Qualification details?');">
                                                    </asp:LinkButton>
                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
    </section>
</div>
<!-- end #content -->

</asp:Content>

