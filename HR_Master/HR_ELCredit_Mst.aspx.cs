﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_Master_HR_ELCredit_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    string MonthID;
    string Month;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Holiday Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Fin_Year_Add();
            Load_Data_EmpDet();
        }

        Load_Data_EL_Credit();
    }

    public void Fin_Year_Add()
    {
        int CurrentYear;


        CurrentYear = System.DateTime.Now.Year;

        ddlYear.Items.Clear();
        ddlYear.Items.Add("-Select-");
        for (int i = 0; i < 11; i++)
        {
            ddlYear.Items.Add(Convert.ToString(CurrentYear));
            CurrentYear = CurrentYear - 1;
        }

    }
    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtEmpNo.Items.Clear();
        query = "Select CONVERT(varchar(10), EmpNo) as EmpNo from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtEmpNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["EmpNo"] = "-Select-";
        dr["EmpNo"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtEmpNo.DataTextField = "EmpNo";
        txtEmpNo.DataValueField = "EmpNo";
        txtEmpNo.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;

        DataTable DT_NFH = new DataTable();

        if (ddlYear.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the EL Year...');", true);
        }

        if (txtEmpNo.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Employee No...');", true);
        }

        if (txtEL_Credit_Days.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the EL Days...');", true);
        }



        if (!ErrFlag)
        {
            if (btnSave.Text == "Save")
            {
                query = "Select * from EL_Credit_Days_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                query = query + " And EL_Year='" + ddlYear.SelectedValue + "' And EmpNo='" + txtEmpNo.SelectedValue + "'";
                DT_NFH = objdata.RptEmployeeMultipleDetails(query);
                if (DT_NFH.Rows.Count != 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee was Already Registered');", true);
                }
                else
                {
                    query = "Insert into EL_Credit_Days_Mst (CompCode,LocCode,EL_Year,EmpNo,EL_Days) ";
                    query = query + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + ddlYear.SelectedValue + "',";
                    query = query + " '" + txtEmpNo.SelectedValue + "','" + txtEL_Credit_Days.Text + "')";
                    objdata.RptEmployeeMultipleDetails(query);

                    EL_Credit_Ledger_OP_Add();

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
                }
            }
            else if (btnSave.Text == "Update")
            {
                query = "Update EL_Credit_Days_Mst set EL_Days='" + txtEL_Credit_Days.Text + "' where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                query = query + " And EL_Year='" + ddlYear.SelectedValue + "' And EmpNo='" + txtEmpNo.SelectedValue + "'";
                objdata.RptEmployeeMultipleDetails(query);

                EL_Credit_Ledger_OP_Add();

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Updated Successfully...!');", true);
            }

        }
        Load_Data_EL_Credit();
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        //ddlYear.SelectedValue = "-Select-";
        txtEmpNo.SelectedValue = "-Select-";
        txtEL_Credit_Days.Text = "";
        btnSave.Text = "Save";
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from EL_Credit_Days_Mst where EmpNo='" + e.CommandName.ToString() + "'";
        query = query + " And EL_Year='" + e.CommandArgument.ToString() + "' And CompCode='" + SessionCcode + "'";
        query = query + " And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from EL_Credit_Days_Mst where EmpNo='" + e.CommandName.ToString() + "'";
            query = query + " And EL_Year='" + e.CommandArgument.ToString() + "' And CompCode='" + SessionCcode + "'";
            query = query + " And LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('EL Credit Details Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('EL Credit Details Not Found');", true);
        }
        Load_Data_EL_Credit();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from EL_Credit_Days_Mst where EmpNo='" + e.CommandName.ToString() + "'";
        query = query + " And EL_Year='" + e.CommandArgument.ToString() + "' And CompCode='" + SessionCcode + "'";
        query = query + " And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            ddlYear.SelectedValue = DT.Rows[0]["EL_Year"].ToString();
            txtEmpNo.SelectedValue = DT.Rows[0]["EmpNo"].ToString();
            txtEL_Credit_Days.Text = DT.Rows[0]["EL_Days"].ToString();
            
            btnSave.Text = "Update";
        }
        else
        {
            ddlYear.SelectedValue = "-Select-";
            txtEmpNo.SelectedValue = "-Select-";
            txtEL_Credit_Days.Text = "";
            btnSave.Text = "Save";
        }
    }

    private void Load_Data_EL_Credit()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select EL_Year,EmpNo,EL_Days from EL_Credit_Days_Mst where EL_Year='" + ddlYear.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_EL_Credit();
    }

    private void EL_Credit_Ledger_OP_Add()
    {
        string query = "";
        DataTable DT_EL = new DataTable();
        string Trans_Date = "01/01/" + ddlYear.SelectedValue.ToString();

        query = "Select * from EL_Credit_Ledger_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And EL_Year='" + ddlYear.SelectedValue + "' And EmpNo='" + txtEmpNo.SelectedValue + "' And Trans_Type='Opening'";
        DT_EL = objdata.RptEmployeeMultipleDetails(query);
        if (DT_EL.Rows.Count != 0)
        {
            query = "Delete from EL_Credit_Ledger_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And EL_Year='" + ddlYear.SelectedValue + "' And EmpNo='" + txtEmpNo.SelectedValue + "' And Trans_Type='Opening'";
            objdata.RptEmployeeMultipleDetails(query);
        }
        //insert Ledger Trans OP
        query = "Insert Into EL_Credit_Ledger_Det(CompCode,LocCode,EL_Year,EmpNo,Trans_Type,Trans_Date,Trans_Date_Str,Add_Days,Minus_Days) Values('" + SessionCcode + "',";
        query = query + " '" + SessionLcode + "','" + ddlYear.SelectedValue + "','" + txtEmpNo.SelectedValue + "','Opening',Convert(Datetime,'" + Trans_Date + "',103),";
        query = query + " '" + Trans_Date + "','" + txtEL_Credit_Days.Text + "','0')";
        objdata.RptEmployeeMultipleDetails(query);
    }
}
