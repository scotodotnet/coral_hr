﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HR_ELCredit_Mst.aspx.cs" Inherits="HR_Master_HR_ELCredit_Mst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<%--<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>--%>
 
<!-- begin #content -->
		<div id="content" class="content-wrapper">
		    <section class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">HR Master</a></li>
				<li class="active">EL Credit</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">EL Credit Details</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        
                        <div class="panel-body">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Year</label>
								  <asp:DropDownList runat="server" ID="ddlYear" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" onselectedindexchanged="ddlYear_SelectedIndexChanged">
								  </asp:DropDownList>
								  <asp:HiddenField ID="txtID" runat="server" />
								  <asp:RequiredFieldValidator ControlToValidate="ddlYear" Display="Dynamic" InitialValue="-Select-" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                               <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>EmpNo</label>
								  <asp:DropDownList runat="server" ID="txtEmpNo" class="form-control select2"></asp:DropDownList>
								  <asp:RequiredFieldValidator ControlToValidate="txtEmpNo" Display="Dynamic" InitialValue="-Select-" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator20" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>EL Credit No.of.Days</label>
								  <asp:TextBox runat="server" ID="txtEL_Credit_Days" class="form-control"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtEL_Credit_Days" Display="Dynamic"  ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                            
                              </div>
                        <!-- end row -->
                         
                       
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="ValidateDept_Field" onclick="btnSave_Click"/>
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                        
                         <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Year</th>
                                                <th>EmpNo</th>
                                                <th>No.of.Days</th>                                                
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("EL_Year")%></td>
                                        <td><%# Eval("EmpNo")%></td>
                                        <td><%# Eval("EL_Days")%></td>
                                        <%--<td><%# Eval("NFH_Type")%></td>--%>
                                        <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument='<%# Eval("EL_Year")%>' CommandName='<%# Eval("EmpNo")%>'></asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("EL_Year")%>' CommandName='<%# Eval("EmpNo")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this EL Credit details?');">
                                                    </asp:LinkButton>
                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
            </section>
        </div>
<!-- end #content -->
<%--</ContentTemplate>
</asp:UpdatePanel>--%>

</asp:Content>

