﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_BOMIssue_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionBOMIssNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal RetQty;
    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: BOM ISSUE";
            Initial_Data_Referesh();
            Load_ItemStock();
            Load_IssueByEmp();
            Load_TakenByEmp();

            if (Session["BOMIssue_No"] == null)
            {
                SessionBOMIssNo = "";
            }
            else
            {
                SessionBOMIssNo = Session["BOMIssue_No"].ToString();
                txtBOMIssNo.Text = SessionBOMIssNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];

        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
        }

        //Auto generate Transaction Function Call

        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "BOM Issue", SessionFinYearVal,"1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtBOMIssNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                //Update Main table

                SSQL = "Update Trans_BOMIssue_Main set BOMIssue_Date='" + txtBOMIssDate.Text + "',";
                SSQL = SSQL + " IssueBy_EmpCode='" + ddlIssBy.SelectedValue + "',IssueBy_EmpName='" + ddlIssBy.SelectedItem.Text + "',";
                SSQL = SSQL + " TakenBy_EmpCode='" + ddlTakenBy.SelectedValue + "',TakenBy_EmpName='" + ddlTakenBy.SelectedItem.Text + "',";
                SSQL = SSQL + " Remarks='" + txtRemarks.Text + "',UpdateOn='" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "'";
                SSQL = SSQL + " Where BOMIssue_No='" + txtBOMIssNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                for (int i = 0; i < Repeater1.Items.Count; i++)
                {
                    Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;

                    Label UOMCode = Repeater1.Items[i].FindControl("lblUOMCode") as Label;
                    Label UOMName = Repeater1.Items[i].FindControl("lblUOMName") as Label;

                    Label DeptCode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;

                    Label WarehouseCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                    Label WarehousName = Repeater1.Items[i].FindControl("lblWarehouseName") as Label;

                    Label RackNo = Repeater1.Items[i].FindControl("lblRackNo") as Label;
                    Label StockQty = Repeater1.Items[i].FindControl("lblStockQty") as Label;

                    TextBox IssueQty = Repeater1.Items[i].FindControl("txtIssQty") as TextBox;

                    Label Rate = Repeater1.Items[i].FindControl("lblRate") as Label;

                    Label Value = Repeater1.Items[i].FindControl("lblValue") as Label;


                    SSQL = "Update Trans_BOMIssue_Sub Set IssueQty='" + IssueQty.Text.ToString() + "',Rate='" + Rate.Text.ToString() + "',";
                    SSQL = SSQL + " Value='"+ Value.Text.ToString() +"' Where ItemCode ='" + ItemCode.Text.ToString() + "' And ";
                    SSQL = SSQL + " ItemName='" + ItemName.Text.ToString() + "' And BOMIssue_No='" + txtBOMIssNo.Text + "' And ";
                    SSQL = SSQL + " Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                    SSQL = SSQL + " And FinYearVal='" + SessionFinYearVal + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);


                    //Stock Update

                    SSQL = "Update Trans_Stock_Ledger_All Set Minus_Qty='" + IssueQty.Text.ToString() + "',Minus_Value='" + Value.Text.ToString() + "' ";
                    SSQL = SSQL + " Where ItemCode ='" + ItemCode.Text.ToString() + "' And ItemName='" + ItemName.Text.ToString() + "' And ";
                    SSQL = SSQL + " Trans_No ='" + txtBOMIssNo.Text + "' And Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And  FinYearVal='" + SessionFinYearVal + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
            else
            {
                //Insert Main Table

                SSQL = "Insert Into Trans_BOMIssue_Main(Ccode,Lcode,FinYearCode,FinYearVal,BOMIssue_No,BOMIssue_Date,IssueBy_EmpCode,IssueBy_EmpName,";
                SSQL = SSQL + " TakenBy_EmpCode,TakenBy_EmpName,Remarks,IssueType,BOM_ReqNo,BOM_ReqDate,Status,Approval_Status,UserID,UserName,CreateOn)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                SSQL = SSQL + " '" + txtBOMIssNo.Text + "','" + txtBOMIssDate.Text + "','" + ddlIssBy.SelectedValue + "','" + ddlIssBy.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + ddlTakenBy.SelectedValue + "','" + ddlTakenBy.SelectedItem.Text + "','" + txtRemarks.Text + "','New','','','Add','0',";
                SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "','" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["ItemTable"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;

                    Label UOMCode = Repeater1.Items[i].FindControl("lblUOMCode") as Label;
                    Label UOMName = Repeater1.Items[i].FindControl("lblUOMName") as Label;

                    Label Deptcode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;

                    Label WarehouseCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                    Label WarehouseName = Repeater1.Items[i].FindControl("lblWarehouseName") as Label;

                    Label RackNo = Repeater1.Items[i].FindControl("lblRackNo") as Label;
                    Label StockQty = Repeater1.Items[i].FindControl("lblStockQty") as Label;

                    TextBox IssueQty = Repeater1.Items[i].FindControl("txtIssQty") as TextBox;

                    Label Rate = Repeater1.Items[i].FindControl("lblRate") as Label;

                    Label Value = Repeater1.Items[i].FindControl("lblValue") as Label;

                    SSQL = "Insert Into Trans_BOMIssue_Sub(Ccode,Lcode,FinYearCode,FinYearVal,BOMIssue_No,BOMIssue_Date,Mat_Req_No,Mat_Req_Date,";
                    SSQL = SSQL + " Mat_Type,ItemCode,ItemName,UOMCode,UOMName,DeptCode,DeptName,WarehouseCode,WarehouseName,RackNo,StockQty,";
                    SSQL = SSQL + " RequestQty,IssueQty,Rate,Value,UserId,UserName,Status)";
                    SSQL = SSQL + "  Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtBOMIssNo.Text + "','" + txtBOMIssDate.Text + "','','','','" + dt.Rows[i]["ItemCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["ItemName"].ToString() + "','" + dt.Rows[i]["UOMCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["UOMName"].ToString() + "','" + dt.Rows[i]["Deptcode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["DeptName"].ToString() + "','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + dt.Rows[i]["RackNo"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["StockQty"].ToString() + "','','" + IssueQty.Text.ToString() + "','"+ txtRate.Text +"',";
                    SSQL = SSQL + " '" + dt.Rows[i]["Value"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "','Add')";

                    objdata.RptEmployeeMultipleDetails(SSQL);


                    ////Insert Purchase Request Approval Table
                    //SSQL = "Insert Into Pur_Request_Approval(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_Request_Amend,TotalQuantity,UserID,UserName) Values('" + SessionCcode + "',";
                    //SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "','" + txtDate.Text + "','Request','" + ReqQty + "',";
                    //SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";
                    //objdata.RptEmployeeMultipleDetails(SSQL);

                    //Stock Insert 

                    DateTime transDate = Convert.ToDateTime(txtBOMIssDate.Text);

                    SSQL = "Insert into Trans_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,";
                    SSQL = SSQL + " Supp_Code,Supp_Name,ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,Add_Qty,Add_Value,Minus_Qty,";
                    SSQL = SSQL + " Minus_Value,UserID,UserName) Values(";
                    SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtBOMIssNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtBOMIssDate.Text + "','BOM ISSUE',";
                    SSQL = SSQL + " '1','Eurocon', '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["Deptcode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["WarehouseCode"].ToString() + "','" + dt.Rows[i]["WarehouseName"].ToString() + "',";
                    SSQL = SSQL + " '0.00','0.00','" + IssueQty.Text.ToString() + "','" + dt.Rows[i]["Value"].ToString() + "',";
                    SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('BOM ISSUE Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('BOM ISSUE Details Updated Successfully');", true);
            }

            //Clear_All_Field();
            Session["BOMIssue_No"] = txtBOMIssNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
            Response.Redirect("Trans_BOMIssue_Main.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Load_IssueByEmp()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select LedgerCode,LedgerName From Acc_Mst_Ledger Where LedgerGrpName='Employee' And Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " Lcode ='" + SessionLcode + "' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlIssBy.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["LedgerName"] = "-Select-";
        dr["LedgerCode"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlIssBy.DataValueField = "LedgerCode";
        ddlIssBy.DataTextField = "LedgerName";
        ddlIssBy.DataBind();
    }

    private void Load_TakenByEmp()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select LedgerCode,LedgerName From Acc_Mst_Ledger Where LedgerGrpName='Employee' And Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " Lcode ='" + SessionLcode + "' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlTakenBy.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["LedgerName"] = "-Select-";
        dr["LedgerCode"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlTakenBy.DataValueField = "LedgerCode";
        ddlTakenBy.DataTextField = "LedgerName";
        ddlTakenBy.DataBind();
    }

    private void Load_ItemStock()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select Mat_No [ItemCode],Raw_Mat_Name [ItemName] From BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlItemName.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["ItemName"] = "-Select-";
        dr["ItemCode"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataValueField = "ItemCode";
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataBind();
    }

    private void Clear_All_Field()
    {
        txtBOMIssNo.Text = ""; txtBOMIssDate.Text = "";
        // txtDeptCode.SelectedValue = "-Select-"; txtDeptName.Text = "";
        //txtCostCenter.Text = "-Select-"; txtCostElement.Items.Clear(); txtRequestedby.Value = "-Select-";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("BOMIssue_No");
        //Load_Data_Enquiry_Grid();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Trans_BOMIssue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And BOMIssue_No ='" + txtBOMIssNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtBOMIssNo.ReadOnly = true;
            txtBOMIssNo.Text = Main_DT.Rows[0]["BOMIssue_No"].ToString();
            txtBOMIssDate.Text = Main_DT.Rows[0]["BOMIssue_Date"].ToString();
            ddlIssBy.SelectedItem.Text = Main_DT.Rows[0]["IssueBy_EmpName"].ToString();
            ddlIssBy.SelectedValue = Main_DT.Rows[0]["IssueBy_EmpCode"].ToString();
            ddlTakenBy.SelectedItem.Text = Main_DT.Rows[0]["TakenBy_EmpName"].ToString();
            ddlTakenBy.SelectedValue = Main_DT.Rows[0]["TakenBy_EmpCode"].ToString();

            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

            //txtTotQty.Text = Main_DT.Rows[0]["TotalQty"].ToString();
            //txtRoundOff.Text = Main_DT.Rows[0]["RoundOff"].ToString();
            //txtTotAmt.Text = Main_DT.Rows[0]["TotalAmount"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,RackNo,UOMCode,UOMName,StockQty,IssueQty,Rate,Value ";
            SSQL = SSQL + " From Trans_BOMIssue_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode = '" + SessionFinYearCode + "' And BOMIssue_No='" + txtBOMIssNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_BOMIssue_Main.aspx");
    }
    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Old_New_Type = "1";

        //Old_New_Type = RbtOld_New.SelectedValue;
        //if (Old_New_Type == "1")
        //{
        //    txtOld_NewCount.Text = "NEW";
        //}
        //else
        //{
        //    txtOld_NewCount.Text = "OLD";
        //}

        string SSQL = "";
        DataTable DT = new DataTable();
        string IssueType = "";

        SSQL = "Select UOMTypeCode[UOMCode],UOM_Full[UOMName],DeptCode,DeptName,WarehouseCode,WarehouseName,RackSerious ";
        SSQL = SSQL + " from BOMMaster Where Raw_Mat_Name ='" + ddlItemName.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hfUOMCode.Value = DT.Rows[0]["UOMCode"].ToString();
        hfUOMName.Value = DT.Rows[0]["UOMName"].ToString();
        hfDeptCode.Value = DT.Rows[0]["DeptCode"].ToString();
        hfDeptName.Value = DT.Rows[0]["DeptName"].ToString();
        hfWarehouseCode.Value = DT.Rows[0]["WarehouseCode"].ToString();
        hfWarehouseName.Value = DT.Rows[0]["WarehouseName"].ToString();
        hfRackNo.Value = DT.Rows[0]["RackSerious"].ToString();

        if (Old_New_Type == "1")
        {
            SSQL = "Select (sum(Add_Qty)-sum(Minus_Qty)) StockQty,(sum(Add_Value)-sum(Minus_Value)) stockVal,";
            SSQL = SSQL + " ((sum(Add_Value)-sum(Minus_Value))/(sum(Add_Qty)-sum(Minus_Qty))) Rate  from Trans_Stock_Ledger_All ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And  Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And ItemCode='" + ddlItemName.SelectedValue + "' having (sum(Add_Qty)-sum(Minus_Qty))>0";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else if (Old_New_Type == "2")
        {
            SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Add_Qty as Stock_Qty  from Reuse_Stock_Ledger_All";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And ItemCode='" + ddlItemName.SelectedValue + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
        }

        if (DT.Rows.Count != 0)
        {
            txtStockQty.Text = DT.Rows[0]["StockQty"].ToString();
            txtRate.Text = DT.Rows[0]["Rate"].ToString();
        }
        else
        {
            txtStockQty.Text = "0.00";

        }

    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        //string SSQL = "";

        if (txtIssueQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Return Qty...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedItem.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemGroupName"] = "Test";
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    dr["DeptCode"] = hfDeptCode.Value;
                    dr["DeptName"] = hfDeptName.Value;
                    dr["WarehouseCode"] = hfWarehouseCode.Value;
                    dr["WarehouseName"] = hfWarehouseName.Value;
                    dr["RackNo"] = hfRackNo.Value;
                    dr["UOMCode"] = hfUOMCode.Value;
                    dr["UOMName"] = hfUOMName.Value;
                    dr["StockQty"] = txtStockQty.Text;
                    dr["IssueQty"] = txtIssueQty.Text;
                    dr["Rate"] = txtRate.Text;
                    dr["Value"] = txtValue.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                    txtStockQty.Text = "0"; txtIssueQty.Text = "0"; txtRate.Text = "0"; txtValue.Text = "0.0";
                }
            }
            else
            {
                dr = dt.NewRow();

                dr["ItemGroupName"] = "Test";
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["DeptCode"] = hfDeptCode.Value;
                dr["DeptName"] = hfDeptName.Value;
                dr["WarehouseCode"] = hfWarehouseCode.Value;
                dr["WarehouseName"] = hfWarehouseName.Value;
                dr["RackNo"] = hfRackNo.Value;
                dr["UOMCode"] = hfUOMCode.Value;
                dr["UOMName"] = hfUOMCode.Value;
                dr["StockQty"] = txtStockQty.Text;
                dr["IssueQty"] = txtIssueQty.Text;
                dr["Rate"] = txtRate.Text;
                dr["Value"] = txtValue.Text;


                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                txtStockQty.Text = "0"; txtIssueQty.Text = "0"; txtRate.Text = "0.00"; txtValue.Text = "0.0";
            }
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemGroupName", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));
        dt.Columns.Add(new DataColumn("RackNo", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMName", typeof(string)));
        dt.Columns.Add(new DataColumn("StockQty", typeof(string)));
        dt.Columns.Add(new DataColumn("IssueQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
        //dt = Repeater1.DataSource;
    }

    protected void txtIssueQty_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtRate.Text) > 0)
        {
            txtValue.Text = Math.Round((Convert.ToDecimal(txtRate.Text) * Convert.ToDecimal(txtIssueQty.Text)), 2).ToString();
        }
    }

    protected void txtIssQty_TextChanged(object sender, EventArgs e)
    {
        TextBox txtTest = ((TextBox)(sender));
        RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

        TextBox txtIssQty = (TextBox)rpi.FindControl("txtIssQty");
        Label lblRate = (Label)rpi.FindControl("lblRate");

        Label lblValue = (Label)rpi.FindControl("lblValue");

        decimal Val = Convert.ToDecimal(lblRate.Text) * Convert.ToDecimal(txtIssQty.Text);

        lblValue.Text = Math.Round(Val, 2).ToString();
    }
}