﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_BOMIssue_Main.aspx.cs" Inherits="Trans_BOMIssue_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            <div class="content-wrapper">
    <section class="content">
                <div class="row">
                    <div class="col-md-2">
                        <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom"  runat="server" Text="Add New" OnClick="btnAddNew_Click"/>

                    </div>
                    <!-- /.col -->
                    </div>
                <!--/.row-->
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>BOM Issues</span></h3>
                                <div class="box-tools pull-right">
                                    <div class="has-feedback">
                                        <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <div class="table-responsive mailbox-messages">
                                    <div class="col-md-12">
					                    <div class="row">
					                        <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
					                            <HeaderTemplate>
                                                    <table id="example" class="table table-responsive table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>BOM Issue No</th>
                                                                <th>BOM Issue Date</th>
                                                                <th>Issue By</th>
                                                                <th>Taken By</th>
                                                                <th>Issue Type</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("BOMIssue_No")%></td>
                                                        <td><%# Eval("BOMIssue_Date")%></td>
                                                        <td><%# Eval("IssueBy_EmpName")%></td>
                                                        <td><%# Eval("TakenBy_EmpName")%></td>
                                                        <td><%# Eval("IssueType")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                                Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("BOMIssue_No")%>'>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="btnPrintEnquiry_Grid" class="btn btn-primary btn-sm fa fa-print"  runat="server" 
                                                                Visible="false" Text="" OnCommand="GridPrintEnquiryClick" CommandArgument="Print" CommandName='<%# Eval("BOMIssue_No")%>'>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                                Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("BOMIssue_No")%>' 
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this BOM Issues details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>                                
					                        </asp:Repeater>
					                </div><!-- /.table -->
					            </div>
                            </div><!-- /.mail-box-messages -->
                        </div><!-- /.box-body -->
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section>
    </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

