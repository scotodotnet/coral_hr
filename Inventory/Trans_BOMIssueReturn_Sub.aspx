﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_BOMIssueReturn_Sub.aspx.cs" Inherits="Trans_BOMIssueReturn_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" src="../assets/js/Trans_Receipt_Calc.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>

     <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.js-states').select2();
                }
            });
        };
    </script>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><i class=" text-primary"></i>New BOM Issues Return </h1>
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputName">Return No</label>
                                    <asp:TextBox ID="txtBOMIssRetNo" runat="server" class="form-control" ReadOnly="true" ></asp:TextBox>
                                </div>
                            </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputName">Return Date</label>
                                    <asp:TextBox ID="txtBOMIssRetDate" runat="server" class="form-control datepicker" AutoComplete="off"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">ReturnBy</label>
                                    <asp:DropDownList ID="ddlReturnBy" runat="server" class="form-control select2"></asp:DropDownList>
                                </div>
                            </div>
                           
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">ReceiveBy</label>
                                    <asp:DropDownList ID="ddlReceiveBy" runat="server" class="form-control select2"></asp:DropDownList>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Notes</label>
                                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Remarks</label>
                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" class="form-control" ></asp:TextBox>
                                </div>
                            </div>
					    </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Item Details</span></h3>
                                        <div class="box-tools pull-right">
                                            <div class="has-feedback">
                                                <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <!-- /.box-tools -->
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="clearfix"></div>

                                    <div class="row" runat="server" style="padding-top:15px"> 
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Item Name</label>
                                                <asp:DropDownList ID="ddlItemName" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged" ></asp:DropDownList>
                                            </div>

                                            <asp:HiddenField id="hfDeptCode" runat="server"/>
                                            <asp:HiddenField id="hfDeptName" runat="server"/>
                                            <asp:HiddenField id="hfWarehouseCode" runat="server"/>
                                            <asp:HiddenField id="hfWarehouseName" runat="server"/>
                                            <asp:HiddenField id="hfRackNo" runat="server"/>
                                            <asp:HiddenField id="hfUOMCode" runat="server"/>
                                            <asp:HiddenField id="hfUOMName" runat="server"/>

                                        </div>

                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Stock Qty</label>
                                                <asp:TextBox ID="txtStockQty" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Return Qty</label>
                                                <asp:TextBox ID="txtRetQty" runat="server" class="form-control" AutoPostBack="true" AutoComplete="off" 
                                                    OnTextChanged="txtRetQty_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label for="exampleInputName">Rate</label>
                                                <asp:TextBox ID="txtRate" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server">
                                            <div class="form-group">
                                                <label for="exampleInputName">Value</label>
                                                <asp:TextBox ID="txtValue" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server" style="padding-top:25px">
                                            <div class="form-group">
                                               <asp:Button runat="server" ID="btnAddItem" class="btn btn-primary" Text="Add Items" OnClick="btnAddItem_Click"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-body no-padding">
                                        <div class="table-responsive mailbox-messages">
					                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                                    <HeaderTemplate>
                                                    <table id="tbltest"  class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>S. No</th>
                                                                <th runat="server" visible="false">Item Code</th>
                                                                <th>Item Name</th>
                                                                <th runat="server" visible="false">DeptCode</th>
                                                                <th>DeptName</th>
                                                                <th runat="server" visible="false">WareHouseCode</th>
                                                                <th>WareHouseName</th>
                                                                <th>RackNo</th>
                                                                <th runat="server" visible="false">UOMCode</th>
                                                                <th>UOM</th>
                                                                <th>Return Qty</th>
                                                                <th>Value</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblItemCode" Text='<%# Eval("ItemCode")%>' ></asp:Label> 
                                                        </td>

                                                        <td>
                                                           <asp:Label runat="server" ID="lblItemName" Text='<%# Eval("ItemName")%>'></asp:Label>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblDeptCode" Text='<%# Eval("DeptCode")%>' ></asp:Label> 
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" ID="lblDeptName" Text='<%# Eval("DeptName")%>' ></asp:Label> 
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblWarehouseCode" Text='<%# Eval("WarehouseCode")%>' ></asp:Label> 
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" ID="lblWarehouseName" Text='<%# Eval("WarehouseName")%>' ></asp:Label> 
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" ID="lblRackNo" Text='<%# Eval("RackNo")%>' ></asp:Label> 
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblUOMCode" Text='<%# Eval("UOMCode")%>' ></asp:Label> 
                                                        </td>

                                                        <td>
                                                           <asp:Label runat="server" ID="lblUOMName" Text='<%# Eval("UOMName")%>'></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:TextBox runat="server"  ID="txtReturnQty" Text='<%# Eval("ReturnQty")%>'
                                                                Width="100px" BorderStyle="None" AutoPostBack="true" OnTextChanged="txtReturnQty_TextChanged"> 
                                                            </asp:TextBox>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblRate" Text='<%# Eval("Rate")%>' ></asp:Label> 
                                                        </td>

                                                         <td>
                                                            <asp:Label runat="server" ID="lblValue" Text='<%# Eval("Value")%>'></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>' 
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>                                
			                                </asp:Repeater><!-- /.table -->
                                        </div><!-- /.mail-box-messages -->
                                    </div><!-- /.box-body -->
                                </div>
                            </div><!-- /.col -->
                        </div>

                        <div class="row" runat="server" visible="false">
                            <div class="col-md-8"></div>
                            <div class="col-md-2">
                                <label for="exampleInputName">Total Qty</label>
                                <asp:TextBox ID="txtTotQty" runat="server" class="form-control" ></asp:TextBox>
                            </div>

                            <div class="col-md-2" runat="server" visible="false">
                                <label for="exampleInputName">Round Off</label>
                                <asp:TextBox ID="txtRoundOff" runat="server" class="form-control" ></asp:TextBox>
                            </div>

                            <div class="col-md-2" runat="server" visible="false">
                                <label for="exampleInputName">Total Amount</label>
                                <asp:TextBox ID="txtTotAmt" runat="server" class="form-control" ></asp:TextBox>
                            </div>
                       </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary"  runat="server" Text="Save" ValidationGroup="Validate_Field" 
                                OnClick="btnSave_Click"/>
                            <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </div>

</asp:Content>

