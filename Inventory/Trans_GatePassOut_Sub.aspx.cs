﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_GatePassOut_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGPOutNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal RetQty;
    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Gatepass Out";
            Initial_Data_Referesh();
            Load_ItemStock();
            Load_Supplier();

            if (Session["GPOutNo"] == null)
            {
                SessionGPOutNo = "";
            }
            else
            {
                SessionGPOutNo = Session["GPOutNo"].ToString();
                txtGPOutNo.Text = SessionGPOutNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];

        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
        }
         
        //Auto generate Transaction Function Call

        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "GatePass Out", SessionFinYearVal,"1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtGPOutNo.Text = Auto_Transaction_No;
                }
            }
        }


        string GPType = "";
        string StockType = "";

        GPType = RdpGPoutType.SelectedItem.Text;
        StockType = RdpStkType.SelectedItem.Text;    

        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                //Update Main table

                SSQL = "Update Trans_GPOut_Main set GPOutDate='" + txtGPOutDate.Text + "',SuppCode='" + ddlSupplier.SelectedValue + "',";
                SSQL = SSQL + " SuppName ='" + ddlSupplier.SelectedItem.Text + "',SuppDet='" + txtSuppDet.Text + "',GPType='" + GPType + "',";
                SSQL = SSQL + " StockType='" + StockType + "',Remarks='" + txtRemarks.Text + "',DeliveryDate='" + txtDeliDate.Text + "'";
                SSQL = SSQL + " Where GPOutNo='" + txtGPOutNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                for (int i = 0; i < Repeater1.Items.Count; i++)
                {
                    Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;

                    Label UOMCode = Repeater1.Items[i].FindControl("lblUOMCode") as Label;
                    Label UOMName = Repeater1.Items[i].FindControl("lblUOMName") as Label;

                    Label DeptCode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;

                    Label WarehouseCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                    Label WarehousName = Repeater1.Items[i].FindControl("lblWarehouseName") as Label;

                    Label RackName = Repeater1.Items[i].FindControl("lblRackName") as Label;

                    TextBox OutQty = Repeater1.Items[i].FindControl("txtOutQty") as TextBox;
                    Label Rate = Repeater1.Items[i].FindControl("lblRate") as Label;
                    Label Value = Repeater1.Items[i].FindControl("lblValue") as Label;
                    Label ItemRem = Repeater1.Items[i].FindControl("lblItemRem") as Label;


                    SSQL = "Update Trans_GPOut_Sub Set OutQty='" + OutQty.Text.ToString() + "',Rate='" + Rate.Text.ToString() + "',";
                    SSQL = SSQL + " Value='"+ Value.Text.ToString() +"' Where ItemCode ='" + ItemCode.Text.ToString() + "' And ";
                    SSQL = SSQL + " ItemName='" + ItemName.Text.ToString() + "' And GP_Out_No='" + txtGPOutNo.Text + "' And ";
                    SSQL = SSQL + " Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                    SSQL = SSQL + " And FinYearVal='" + SessionFinYearVal + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);


                    ////Stock Update

                    //SSQL = "Update Trans_Stock_Ledger_All Set Minus_Qty='" + GPOutQty.Text.ToString() + "',Minus_Value='" + Value.Text.ToString() + "' ";
                    //SSQL = SSQL + " Where ItemCode ='" + ItemCode.Text.ToString() + "' And ItemName='" + ItemName.Text.ToString() + "' And ";
                    //SSQL = SSQL + " Trans_No ='" + txtGPOutNo.Text + "' And Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                    //SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And  FinYearVal='" + SessionFinYearVal + "'";

                    //objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
            else
            {
                //Insert Main Table

                //Ccode,Lcode,FinYearCode,FinYearVal,GPOutNo,GPOutDate,GPType,SuppCode,SuppName,SuppDet,DeliveryDate,Remarks,StockType,Status,UserID,
                //UserName,CreateOn,Approval_Status


                SSQL = "Insert Into Trans_GPOut_Main(Ccode,Lcode,FinYearCode,FinYearVal,GPOutNo,GPOutDate,GPType,SuppCode,SuppName,SuppDet,";
                SSQL = SSQL + " DeliveryDate,Remarks,StockType,Status,UserID,UserName,CreateOn)Values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtGPOutNo.Text + "','" + txtGPOutDate.Text + "',";
                SSQL = SSQL + " '" + GPType + "','" + ddlSupplier.SelectedValue + "','" + ddlSupplier.SelectedItem.Text + "','" + txtSuppDet.Text + "',";
                SSQL = SSQL + " '" + txtDeliDate.Text + "','" + txtRemarks.Text + "','" + StockType + "','Add','" + SessionUserID + "',";
                SSQL = SSQL + " '" + SessionUserName + "','" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["ItemTable"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;

                    Label UOMCode = Repeater1.Items[i].FindControl("lblUOMCode") as Label;
                    Label UOMName = Repeater1.Items[i].FindControl("lblUOMName") as Label;

                    Label Deptcode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;

                    Label WarehouseCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                    Label WarehouseName = Repeater1.Items[i].FindControl("lblWarehouseName") as Label;

                    Label RackName = Repeater1.Items[i].FindControl("lblRackName") as Label;
                    Label StockQty = Repeater1.Items[i].FindControl("lblStockQty") as Label;

                    TextBox GPOutQty = Repeater1.Items[i].FindControl("txtOutQty") as TextBox;

                    Label Rate = Repeater1.Items[i].FindControl("lblRate") as Label;

                    Label Value = Repeater1.Items[i].FindControl("lblValue") as Label;
                    Label ItemRem = Repeater1.Items[i].FindControl("lblItemRem") as Label;

                    SSQL = "Insert Into Trans_GPOut_Sub(Ccode,Lcode,FinYearCode,FinYearVal,GP_Out_No,GP_Out_Date,";
                    SSQL = SSQL + " ItemCode,ItemName,UOMCode,UOMName,DeptCode,DeptName,WarehouseCode,WarehouseName,RackName,";
                    SSQL = SSQL + " OutQty,Rate,Value,Remarks,Status,UserId,UserName,CreateOn)";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtGPOutNo.Text + "','" + txtGPOutDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["ItemName"].ToString() + "','" + dt.Rows[i]["UOMCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["UOMName"].ToString() + "','" + dt.Rows[i]["Deptcode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["DeptName"].ToString() + "','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + dt.Rows[i]["RackName"].ToString() + "',";
                    SSQL = SSQL + " '" + GPOutQty.Text.ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["ItemRem"].ToString() + "','Add','" + SessionUserID + "','" + SessionUserName + "',";
                    SSQL = SSQL + "'" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "')";

                    objdata.RptEmployeeMultipleDetails(SSQL);


                    ////Insert Purchase Request Approval Table
                    //SSQL = "Insert Into Pur_Request_Approval(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_Request_Amend,TotalQuantity,UserID,UserName) Values('" + SessionCcode + "',";
                    //SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "','" + txtDate.Text + "','Request','" + ReqQty + "',";
                    //SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";
                    //objdata.RptEmployeeMultipleDetails(SSQL);

                    //Stock Insert 

                    //DateTime transDate = Convert.ToDateTime(txtGPOutDate.Text);

                    //SSQL = "Insert into Trans_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,";
                    //SSQL = SSQL + " Supp_Code,Supp_Name,ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,Add_Qty,Add_Value,Minus_Qty,";
                    //SSQL = SSQL + " Minus_Value,UserID,UserName) Values(";
                    //SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    //SSQL = SSQL + " '" + txtGPOutNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtGPOutDate.Text + "','BOM ISSUE',";
                    //SSQL = SSQL + " '1','Eurocon', '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                    //SSQL = SSQL + " '" + dt.Rows[i]["Deptcode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "',";
                    //SSQL = SSQL + " '" + dt.Rows[i]["WarehouseCode"].ToString() + "','" + dt.Rows[i]["WarehouseName"].ToString() + "',";
                    //SSQL = SSQL + " '0.00','0.00','" + GPOutQty.Text.ToString() + "','" + dt.Rows[i]["Value"].ToString() + "',";
                    //SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";

                    //objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass Out Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('GatePass Out Details Updated Successfully');", true);
            }

            //Clear_All_Field();
            Session["GPOutNo"] = txtGPOutNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
            Response.Redirect("Trans_GatePassOut_Main.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Load_Supplier()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select LedgerCode,LedgerName From Acc_Mst_Ledger where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And Status='Add' And LedgerGrpName='Supplier' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlSupplier.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlSupplier.DataValueField = "LedgerCode";
        ddlSupplier.DataTextField = "LedgerName";
        ddlSupplier.DataBind();

    }

    private void Load_ItemStock()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select Mat_No [ItemCode],Raw_Mat_Name [ItemName] From BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlItemName.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["ItemName"] = "-Select-";
        dr["ItemCode"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataValueField = "ItemCode";
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataBind();
    }

    private void Clear_All_Field()
    {
        txtGPOutNo.Text = ""; txtGPOutDate.Text = "";
        // txtDeptCode.SelectedValue = "-Select-"; txtDeptName.Text = "";
        //txtCostCenter.Text = "-Select-"; txtCostElement.Items.Clear(); txtRequestedby.Value = "-Select-";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("GPOutNo");
        //Load_Data_Enquiry_Grid();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Trans_GPOut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And GPOutNo ='" + txtGPOutNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtGPOutNo.ReadOnly = true;
            txtGPOutNo.Text = Main_DT.Rows[0]["GPOutNo"].ToString();
            txtGPOutDate.Text = Main_DT.Rows[0]["GPOutDate"].ToString();

            ddlSupplier.SelectedItem.Text = Main_DT.Rows[0]["SuppName"].ToString();
            ddlSupplier.SelectedValue = Main_DT.Rows[0]["SuppCode"].ToString();
            txtSuppDet.Text = Main_DT.Rows[0]["SuppDet"].ToString();
            txtDeliDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();

            if (Main_DT.Rows[0]["GPType"].ToString() == "Returnable")
            {
                RdpGPoutType.SelectedValue = "1";
            }
            else
            {
                RdpGPoutType.SelectedValue = "2";
            }


            if (Main_DT.Rows[0]["StockType"].ToString() == "Stock")
            {
                RdpStkType.SelectedValue = "1";
            }
            else
            {
                RdpStkType.SelectedValue = "2";
            }

            //txtTotQty.Text = Main_DT.Rows[0]["TotalQty"].ToString();
            //txtRoundOff.Text = Main_DT.Rows[0]["RoundOff"].ToString();
            //txtTotAmt.Text = Main_DT.Rows[0]["TotalAmount"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,RackName,UOMCode,UOMName,OutQty,Rate,Value,Remarks[ItemRem] ";
            SSQL = SSQL + " From Trans_GPOut_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode = '" + SessionFinYearCode + "' And GP_Out_No='" + txtGPOutNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_GatePassOut_Main.aspx");
    }
    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Old_New_Type = "1";

        //Old_New_Type = RbtOld_New.SelectedValue;
        //if (Old_New_Type == "1")
        //{
        //    txtOld_NewCount.Text = "NEW";
        //}
        //else
        //{
        //    txtOld_NewCount.Text = "OLD";
        //}

        string SSQL = "";
        DataTable DT = new DataTable();
        string IssueType = "";

        SSQL = "Select UOMTypeCode[UOMCode],UOM_Full[UOMName],DeptCode,DeptName,WarehouseCode,WarehouseName,RackSerious ";
        SSQL = SSQL + " from BOMMaster Where Raw_Mat_Name ='" + ddlItemName.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hfUOMCode.Value = DT.Rows[0]["UOMCode"].ToString();
        hfUOMName.Value = DT.Rows[0]["UOMName"].ToString();
        hfDeptCode.Value = DT.Rows[0]["DeptCode"].ToString();
        hfDeptName.Value = DT.Rows[0]["DeptName"].ToString();
        hfWarehouseCode.Value = DT.Rows[0]["WarehouseCode"].ToString();
        hfWarehouseName.Value = DT.Rows[0]["WarehouseName"].ToString();
        hfRackNo.Value = DT.Rows[0]["RackSerious"].ToString();

        if (Old_New_Type == "1")
        {
            SSQL = "Select (sum(Add_Qty)-sum(Minus_Qty)) StockQty,(sum(Add_Value)-sum(Minus_Value)) stockVal,";
            SSQL = SSQL + " ((sum(Add_Value)-sum(Minus_Value))/(sum(Add_Qty)-sum(Minus_Qty))) Rate  from Trans_Stock_Ledger_All ";
            SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And  Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And ItemCode='" + ddlItemName.SelectedValue + "' having (sum(Add_Qty)-sum(Minus_Qty))>0";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else if (Old_New_Type == "2")
        {
            SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Add_Qty as Stock_Qty  from Reuse_Stock_Ledger_All";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + " And ItemCode='" + ddlItemName.SelectedValue + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
        }

        if (DT.Rows.Count != 0)
        {

            txtRate.Text = DT.Rows[0]["Rate"].ToString();
        }
        else
        {


        }

    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        //string SSQL = "";

        if (txtGPOutQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Return Qty...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedItem.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemGroupName"] = "Test";
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    dr["DeptCode"] = hfDeptCode.Value;
                    dr["DeptName"] = hfDeptName.Value;
                    dr["WarehouseCode"] = hfWarehouseCode.Value;
                    dr["WarehouseName"] = hfWarehouseName.Value;
                    dr["RackName"] = hfRackNo.Value;
                    dr["UOMCode"] = hfUOMCode.Value;
                    dr["UOMName"] = hfUOMName.Value;

                    dr["OutQty"] = txtGPOutQty.Text;
                    dr["Rate"] = txtRate.Text;
                    dr["Value"] = txtValue.Text;

                    dr["ItemRem"] = txtItemNotes.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                    txtGPOutQty.Text = "0"; txtRate.Text = "0"; txtValue.Text = "0.0"; txtItemNotes.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();

                dr["ItemGroupName"] = "Test";
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["DeptCode"] = hfDeptCode.Value;
                dr["DeptName"] = hfDeptName.Value;
                dr["WarehouseCode"] = hfWarehouseCode.Value;
                dr["WarehouseName"] = hfWarehouseName.Value;
                dr["RackName"] = hfRackNo.Value;
                dr["UOMCode"] = hfUOMCode.Value;
                dr["UOMName"] = hfUOMCode.Value;
                dr["OutQty"] = txtGPOutQty.Text;
                dr["Rate"] = txtRate.Text;
                dr["Value"] = txtValue.Text;
                dr["ItemRem"] = txtItemNotes.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                txtGPOutQty.Text = "0"; txtRate.Text = "0.00"; txtValue.Text = "0.0"; txtItemNotes.Text = "";
            }
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemGroupName", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));
        dt.Columns.Add(new DataColumn("RackName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMName", typeof(string)));
        dt.Columns.Add(new DataColumn("OutQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRem", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
        //dt = Repeater1.DataSource;
    }

    protected void txtGPOutQty_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtRate.Text) > 0)
        {
            txtValue.Text = Math.Round((Convert.ToDecimal(txtRate.Text) * Convert.ToDecimal(txtGPOutQty.Text)), 2).ToString();
        }
    }

    protected void txtOutQty_TextChanged(object sender, EventArgs e)
    {
        TextBox txtTest = ((TextBox)(sender));
        RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

        TextBox txtGPOut = (TextBox)rpi.FindControl("txtOutQty");
        Label lblRate = (Label)rpi.FindControl("lblRate");

        Label lblValue = (Label)rpi.FindControl("lblValue");

        decimal Val = Convert.ToDecimal(lblRate.Text) * Convert.ToDecimal(txtGPOut.Text);

        lblValue.Text = Math.Round(Val, 2).ToString();
    }

    protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "select HouseNo+','+Add1+','+Add2+','+ City+'-'+PinCode [Address] from Acc_Mst_Ledger where LedgerGrpName='Supplier' And ";
        SSQL = SSQL + " LedgerCode ='" + ddlSupplier.SelectedValue + "' And Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if(DT.Rows.Count>0)
        {
            txtSuppDet.Text = DT.Rows[0]["Address"].ToString();
        }
    }
}