﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstDeptSpareFix_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearVal;
    string Dept_Code_Delete = "";
    string SessionDeptFixCode = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Department SpareFix";
            Load_Data_Empty_Department();

            if (Session["DeptSprFixCode"] == null)
            {
                SessionDeptFixCode = "";
            }
            else
            {
                SessionDeptFixCode = Session["DeptSprFixCode"].ToString();
                txtSpareFixCode.Text = SessionDeptFixCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }

    private void Load_Data_Empty_Department()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status <> 'Delete'";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlDeptName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        ddlDeptName.DataTextField = "DeptName";
        ddlDeptName.DataValueField = "DeptCode";
        ddlDeptName.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string HostName = getIPAndName.GetName();

        if (btnSave.Text == "Save")
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Dept Spare Fix");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add...');", true);
            }
        }

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from MstDeptSpareFix Where SpareFixName='" + txtSpareFixName.Text + "' And Ccode ='" + SessionCcode + "' And ";
            SSQL = SSQL + " Lcode ='" + SessionLcode + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Department Spare Fix Name Already Exsit');", true);
            }
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Dept Spare Fix", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtSpareFixCode.Text = Auto_Transaction_No;
                }
            }
        }
        
        if (!ErrFlag)
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstDeptSpareFix set SpareFixName='" + txtSpareFixName.Text + "',DeptName='" + ddlDeptName.SelectedItem.Text + "',";
                SSQL = SSQL + " DeptCode='" + ddlDeptName.SelectedValue + "' Where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' ";
                SSQL = SSQL + " And SpareFixCode='" + txtSpareFixCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Designation Details Updated Successfully');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Department Spare Fix Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstDeptSpareFix(Ccode,Lcode,SpareFixCode,SpareFixName,DeptCode,DeptName,Status,UserID,UserName,CreateOn,";
                SSQL = SSQL + " Host_IP,Host_Name) Values( '" + SessionCcode + "','" + SessionLcode + "','" + txtSpareFixCode.Text + "',";
                SSQL = SSQL + " '" + txtSpareFixName.Text + "','" + ddlDeptName.SelectedValue + "','" + ddlDeptName.SelectedItem.Text + "',";
                SSQL = SSQL + " 'Add','" + SessionUserID + "','" + SessionUserName + "',Getdate(),'" + IP + "',";
                SSQL = SSQL + " '" + HostName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Department Spare Fix Details Saved Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";

            Response.Redirect("MstDeptSpareFix_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('DepartMent SpareFix Details Already Saved');", true);
        }
    }
    private void Clear_All_Field()
    {
        //txtSpareFixCode.ReadOnly = false;
        txtSpareFixCode.Text = "";
        txtSpareFixName.Text = "";

        ddlDeptName.SelectedItem.Text = "-Select-";
        ddlDeptName.SelectedValue = "-Select-";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from MstDeptSpareFix where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status <> 'Delete' And ";
        SSQL = SSQL + " SpareFixCode='" + txtSpareFixCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            //txtSpareFixCode.ReadOnly = true;
            txtSpareFixName.Text = DT.Rows[0]["SpareFixName"].ToString();
            ddlDeptName.SelectedItem.Text = DT.Rows[0]["DeptName"].ToString();
            ddlDeptName.SelectedValue= DT.Rows[0]["DeptCode"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("MstDeptSpareFix_Main.aspx");
    }
}
