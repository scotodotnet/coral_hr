﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class DeptBOMRequiredMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionFinYearCode = Session["FinYearCode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();
            Load_Data();
        }
    }
    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from Dept_BOM_Require where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status ='Add'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {

        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Department Required BOM");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit...');", true);
        }

        if (!ErrFlag)
        {
            string Enquiry_No_Str = e.CommandName.ToString();
            Session.Remove("Mat_No");
            Session["Mat_No"] = Enquiry_No_Str;
            Response.Redirect("/Master/DeptBOMRequiredSub.aspx");
        }
    }

    protected void btnDeleteIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Department Required BOM");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete...');", true);
        }

        if (!ErrFlag)
        {
            SSQL = "Update Dept_BOM_Require set Status='Delete' where Mat_No='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Data();
        }
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {

        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Department Required BOM");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add...');", true);
        }
        else
        {
            Session.Remove("Mat_No");
            Response.Redirect("/Master/DeptBOMRequiredSub.aspx");
        }
    }
}