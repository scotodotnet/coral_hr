﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class ProductsMin : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();

    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;


    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionFinYearCode = Session["FinYearCode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();

            Load_Data();
            
        }
    }
    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and  Status ='ADD'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Production Items");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit Item...');", true);
        }

        if (!ErrFlag)
        {
            string Enquiry_No_Str = e.CommandName.ToString();
            Session.Remove("ProductNo");
            Session["ProductNo"] = Enquiry_No_Str;
            Response.Redirect("/Master/ProductsSub.aspx");
        }
    }

    protected void btnDeleteIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {

        SSQL = "";

        //Right Checking 

        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Production Items");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Delete...');", true);
        }

        if (!ErrFlag)
        {
            SSQL = "Update ProductModel set Status='Delete' where ProductNo='" + e.CommandName.ToString() + "' and ";
            SSQL = SSQL + " Ccode ='" + SessionCcode + "'";
            SSQL = SSQL + " and Lcode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Data();
        }
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Production Items");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Add...');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Item..');", true);
        }
        else
        {
            Session.Remove("ProdID");
            Response.Redirect("/Master/ProductsSub.aspx");
        }
    }
}