﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstCustomer_Demo.aspx.cs" Inherits="MstCustomer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Customer Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="col-md-12">
					        <div class="row">
					            <div class="form-group col-md-3">
					                <label for="exampleInputName">Customer Code<span class="mandatory">*</span></label>
					                <asp:TextBox ID="txtCustomerCode" MaxLength="30" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>

                                    <asp:RequiredFieldValidator ControlToValidate="txtCustomerCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1"
                                        runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>					            
					            </div>
					       					        
					            <div class="form-group col-md-3">
					                <label for="exampleInputName">Customer Name <span class="mandatory">*</span></label>
							        <asp:TextBox ID="txtCustomerName" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtCustomerName" ValidationGroup="Validate_Field" class="form_error" 
                                        ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
					            </div>

					            <div class="form-group col-md-3">
					                <label for="exampleInputName">Address 1 <span class="mandatory">*</span></label>
					                <asp:TextBox ID="txtAddress1" class="form-control" runat="server"></asp:TextBox>
	                                <asp:RequiredFieldValidator ControlToValidate="txtAddress1" ValidationGroup="Validate_Field" class="form_error" 
                                        ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
					            </div>

					            <div class="form-group col-md-3">
					            <label for="exampleInputName">Address 2</label>
					            <asp:TextBox ID="txtAddress2" class="form-control" runat="server"></asp:TextBox>
					        </div>
                            </div>
					    
                            <div class="row">
					            <div class="form-group col-md-3">
					                <label for="exampleInputName">City <span class="mandatory">*</span></label>
                                    <asp:TextBox ID="txtCity" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtCity" ValidationGroup="Validate_Field" class="form_error" 
                                        ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
					            </div>

					            <div class="form-group col-md-3">
					                <label for="exampleInputName">Pincode <span class="mandatory">*</span></label>
					                <asp:TextBox ID="txtPincode"  MaxLength="6" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtPincode" ValidationGroup="Validate_Field" class="form_error" 
                                        ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegExValPin" ValidationExpression="[0-9]{6}" runat="server" class="form_error" 
                                        ControlToValidate="txtPincode" ValidationGroup="Validate_Field" ErrorMessage="Pincode length 6">
                                    </asp:RegularExpressionValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtPincode" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
					            </div>

					            <div class="form-group col-md-3">
					                <label for="exampleInputName">State <span class="mandatory">*</span></label>							
                                    <asp:DropDownList ID="txtState" runat="server" class="form-control">
                                    <asp:ListItem Value="1" Text="Tamil Nadu"></asp:ListItem>
                                    </asp:DropDownList>
					            </div>

					            <div class="form-group col-md-3">
					            <label for="exampleInputName">Country <span class="mandatory">*</span></label>
                                <asp:DropDownList ID="txtCountry" runat="server" class="form-control">
                                <asp:ListItem Value="1" Text="India"></asp:ListItem>
                                </asp:DropDownList>
					        </div>					        
					        </div>
                       
					        <div class="row">
					            <div class="form-group col-md-3">
					                <label for="exampleInputName">Tel No.</label>
					                <div class="form-group row">
					                    <div class="col-sm-4">
							                <asp:TextBox ID="txtStdCode" MaxLength="5" class="form-control" runat="server"></asp:TextBox>
							                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtStdCode" ValidChars="0123456789">
                                            </cc1:FilteredTextBoxExtender>
							            </div>
							            <div class="col-sm-8">
					                        <asp:TextBox ID="txtTel_no" MaxLength="7" class="form-control" runat="server"></asp:TextBox>
					                        <asp:RegularExpressionValidator ID="RegExpValdTelNo" ValidationExpression="[0-9]{7}" runat="server" class="form_error" 
                                                ControlToValidate="txtTel_no" ValidationGroup="Validate_Field" ErrorMessage="Tel.No length 7">
                                            </asp:RegularExpressionValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtTel_no" ValidChars="0123456789">
                                            </cc1:FilteredTextBoxExtender>
					                    </div>
					                </div>
					            </div>

					            <div class="form-group col-md-3">
					                <label for="exampleInputName">Mobile No <span class="mandatory">*</span></label>					            
					                <div class="form-group row">
					                    <div class="col-sm-4">
							                <asp:TextBox ID="txtMblCode" Text="+91" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
							            </div>
							            <div class="col-sm-8">
                                            <asp:TextBox ID="txtMobile_no" MaxLength="10" class="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtMobile_no" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="[0-9]{10}" runat="server" class="form_error" 
                                                ControlToValidate="txtMobile_no" ValidationGroup="Validate_Field" ErrorMessage="Mobile.No length 10">
                                            </asp:RegularExpressionValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtMobile_no" ValidChars="0123456789">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                    </div>
					            </div>

					            <div class="form-group col-md-3">
					                <label for="exampleInputName">PAN No </label>
					                <asp:TextBox ID="txttin_no" MaxLength="11" class="form-control"  runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegExp1" runat="server" ErrorMessage="TIN NO length 11" class="form_error" 
                                        ControlToValidate="txttin_no" ValidationExpression="[0-9]{11}" ValidationGroup="Validate_Field"/>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server" FilterMode="ValidChars"
                                        FilterType="Custom,Numbers" TargetControlID="txttin_no" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
					            </div>	

                                <div class="form-group col-md-3">
					                <label for="exampleInputName">GST No <span class="mandatory">*</span></label>
                                    <asp:TextBox ID="txtcst_no" class="form-control"  runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtcst_no" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
					            </div>
					        </div>
					    					    
					        <div class="row">
					            <div class="form-group col-md-3">
					                <label for="exampleInputName">Mail ID </label>
                                    <asp:TextBox ID="txtMail_Id" class="form-control" runat="server"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ControlToValidate="txtMail_Id" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>--%>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" class="form_error" runat="server" ControlToValidate="txtMail_Id" 
                                        Display ="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                        ValidationGroup="Validate_Field" ErrorMessage="enter valid email address. Eg. Something@domain.com">
                                    </asp:RegularExpressionValidator>
					            </div>

                                <div class="form-group col-md-3" runat="server" visible="false">
					                <label for="exampleInputName">Ref No.</label>
                                    <asp:TextBox ID="txtRefno" runat="server" class="col-sm-12 form-control"></asp:TextBox>
					            </div>

					            <div class="form-group col-md-3">
					                <label for="exampleInputName">Description</label>
                                    <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" Height="60" class="col-sm-12 form-control"></asp:TextBox>
					            </div>
                            
					        </div>
					    </div>
                        
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-2">
                                <div class="form-group" style="align-content:center">
                                    <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                </div>
                            </div>
                            <div class="col-md-5"></div>
                        </div>
                            
                        <div class="box-body no-padding">
                            <div class="table-responsive mailbox-messages">
                                <asp:Repeater ID="GrdDepartment" runat="server">
			                        <HeaderTemplate>
                                        <table  class="table table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>S. No</th>
                                                    <th>Customer</th>
                                                    <th>Address</th>
                                                    <th>City</th>
                                                    <th>Mobile</th>
                                                    <th>Mail</th>
                                                    <th>Mode</th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Container.ItemIndex + 1 %></td>
                                            <td><%# Eval("CustName")%></td>
                                            <td><%# Eval("Address1")%></td>
                                            <td><%# Eval("City")%></td>
                                            <td><%# Eval("MobileNo")%></td>
                                            <td><%# Eval("MailId")%></td>
                                            <td>
                                                <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                    Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("CustCode")%>'>
                                                </asp:LinkButton>

                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                    Text="" CommandArgument="Delete" OnCommand="GridDeleteClick" CommandName='<%# Eval("CustCode")%>' 
                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>                                
			                    </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

