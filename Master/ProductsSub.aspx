﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProductsSub.aspx.cs" Inherits="ProductsSub" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <asp:UpdatePanel ID="upPrdSub" runat="server">
        <ContentTemplate>
             <section class="content-header">
        <h1>
            <i class="fa fa-cog text-primary"></i> Products
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Generator Model</label>
                                    <asp:DropDownList ID="ddlGenerators" class="form-control select2" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4" runat="server" visible="false">
                                <div class="form-group">
                                    <label class="control-label">Product No <span class="text-danger">*</span></label>
                                    <asp:TextBox ID="txtProductNo" class="form-control" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtProductNo" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Product Name <span class="text-danger">*</span></label>
                                    <asp:TextBox ID="txtProductName" class="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtProductName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Discription</label>
                                    <asp:TextBox ID="txtDescription" Rows="3" Columns="30" Style="resize: none" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click" ValidationGroup="Validate_Field" runat="server" Text="Save" />
                            <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                        </div>
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-3" hidden>
                <div class="callout callout-info" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-info"></i>Info:</h4>
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </section>
        </ContentTemplate>
    </asp:UpdatePanel>
   
        </div>
</asp:Content>

