﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstSupplier_Sub.aspx.cs" Inherits="MstSupplier_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">

        <asp:UpdatePanel ID="upSuppsub" runat="server">
            <ContentTemplate>
                  <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Suppler Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier Code<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtLedgerCode" runat="server" class="form-control"/>
                                    <span id="DeptCode" class="text-danger"></span>
                                    <asp:RequiredFieldValidator ControlToValidate="txtLedgerCode" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier Name<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtLedgerName" runat="server" class="form-control" />
                                    <span id="ShortName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtLedgerName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                             <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Ledger Group<span class="text-danger">*</span> </label>
                                    <asp:DropDownList  id="ddlLedgerGrp" runat="server" class="form-control select2" >
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Account Type<span class="text-danger">*</span> </label>
                                    <asp:DropDownList  id="ddlAccType" runat="server" class="form-control select2">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Apartment No/Street<span class="text-danger">*</span> </label>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtHouseNumber" runat="server" Class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtAddr1" runat="server" Class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Address2<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtAddr2" runat="server" Class="form-control"></asp:TextBox>
                                </div>
                            </div>
                         </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">City/Postal Code<span class="text-danger">*</span> </label>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtCity" runat="server" Class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtPincode" runat="server" Class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">State<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtState" runat="server" Class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Country<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtCountry" runat="server" Class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        
                             <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Mobile Code / Mobile No<span class="text-danger">*</span></label>
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txtMobiCode" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtMobile" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Std Code / Phone No<span class="text-danger">*</span></label>
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txtStdCode" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtPhone" runat="server" Class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Mail Id<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtMail" runat="server" Class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Web Site<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtWebSite" runat="server" Class="form-control"></asp:TextBox>
                                </div>
                            </div>
                       
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Account No<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtAccountNo" runat="server" Class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Bank Name<span class="text-danger">*</span> </label>
                                    <asp:DropDownList ID="ddlBankName" runat="server" Class="form-control select2"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Branch Name<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtBranchName" runat="server" Class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">IFSC Code<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtIFSC" runat="server" Class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Transport / Zone<span class="text-danger">*</span></label>
                                    <div class="form-group row">
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtTransport" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-5">
                                            <asp:TextBox ID="txtZone" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">VAT / GST Number<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtGstin" runat="server" Class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Notes<span class="text-danger">*</span> </label>
                                    <asp:TextBox ID="txtNotes" runat="server" Class="form-control" TextMode="MultiLine" style="resize:none"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <div class="form-group" style="align-content:center">
                                <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <asp:Button ID="btnClear" class="btn btn-primary"  runat="server"  Text="Clear" OnClick="btnClear_Click" />
                                <asp:Button ID="btnBack" class="btn btn-default"  runat="server"  Text="Back To List" OnClick="btnBack_Click"/>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
      
    </div>
</asp:Content>

