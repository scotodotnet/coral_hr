﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class BOMMaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();

    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionUserID;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string SessionFinYearVal = "";
    string SessionUserName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();

            Load_Data();
        }
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status='Add'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnDeleteIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "BOM Master");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete...');", true);
        }
        else
        {

            SSQL = "";
            SSQL = "Update BOMMaster set Status='Delete' where ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Bomid='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Data();
        }
    }

    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "BOM Master");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit...');", true);
        }
        else
        {
            Session["BOMID"] = e.CommandName.ToString();
            Response.Redirect("/Master/BOMMasterSub.aspx");
        }
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {

        bool ErrFlag = false;
        bool Rights_Check = false;


        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "BOM Master");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add...');", true);
        }
        else
        {
            Session.Remove("BOMID");
            Response.Redirect("/Master/BOMMasterSub.aspx");
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Master/BOMUpload.aspx");
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Reports/RptBOM.aspx");
    }
}