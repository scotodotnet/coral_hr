﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class BOMMasterSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearVal;
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;

    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();

            if (!IsPostBack)
            {
                Load_Department();
                Load_Genrator();
                Load_Product();
                Load_Supplier();
                Load_UOM();
                Load_Warehouse();
                InitialData();

                if ((string)Session["BOMID"] != null)
                {
                    Search((string)Session["BOMID"].ToString());
                }
            }
        }
    }

    private void Load_Warehouse()
    {
        SSQL = "";
        SSQL = "Select (WarehouseCode +'~>'+ WarehouseName) as Text,WarehouseCode as Value from MstWarehouse where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
        DataTable dt_Warehouse = new DataTable();
        dt_Warehouse = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlWarehouse.DataSource = dt_Warehouse;
        ddlWarehouse.DataTextField = "Text";
        ddlWarehouse.DataValueField = "Value";
        ddlWarehouse.DataBind();
        ddlWarehouse.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Product()
    {
        SSQL = "";
        SSQL = "Select * from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        //DataTable dt = new DataTable();
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (dt.Rows.Count > 0)
        //{
        ddlProduct.DataSource = objdata.RptEmployeeMultipleDetails(SSQL); ;
        ddlProduct.DataTextField = "ProductName";
        ddlProduct.DataValueField = "ProductNo";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        //}
    }

    private void Search(string id)
    {
        SSQL = "";
        SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and BOMiD='" + id + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtMatNo.ReadOnly = true;
            txtMatNo.Text = dt.Rows[0]["Mat_No"].ToString();
            txtMatName.Text = dt.Rows[0]["Raw_mat_name"].ToString();
            ddlUOM.SelectedValue = dt.Rows[0]["UOMTypeCode"].ToString();
            ddlUOM.SelectedItem.Text= dt.Rows[0]["UOM_Full"].ToString();
            ddlDepartment.SelectedValue = dt.Rows[0]["deptCode"].ToString();
            txtPartNo.Text = dt.Rows[0]["Part_No"].ToString();
            txtPlant.Text = dt.Rows[0]["Plant"].ToString();
            txtSapNo.Text = dt.Rows[0]["Sap_No"].ToString();
            txtCatelogNo.Text = dt.Rows[0]["Catlog_No"].ToString();
            txtDrawingNo.Text = dt.Rows[0]["Drawing_No"].ToString();
            txtValidFrom.Text = dt.Rows[0]["ValFrom_str"].ToString();
            txtValidTo.Text = dt.Rows[0]["ValidTo_Str"].ToString();
            txtMinQty.Text = dt.Rows[0]["Min_Qty"].ToString();
            txtMaxQty.Text = dt.Rows[0]["Max_Qty"].ToString();
            txtRequiredQty.Text = dt.Rows[0]["RequiredQty"].ToString();
            txtBarCode.Text = dt.Rows[0]["BarCode"].ToString();
            rbtCheckImg.SelectedValue = dt.Rows[0]["ChkIMgVal"].ToString();
            txtAmount.Text = dt.Rows[0]["Amount"].ToString();
            txtCGSTP.Text = dt.Rows[0]["CGSTP"].ToString();
            txtSGSTP.Text = dt.Rows[0]["SGSTP"].ToString();
            txtIGSTP.Text = dt.Rows[0]["IGSTP"].ToString();
            txtComment.Text = dt.Rows[0]["Comment"].ToString();
            ddlGenerator.SelectedValue = dt.Rows[0]["GenModelno"].ToString();
            ddlProduct.SelectedValue = dt.Rows[0]["ProductionPartNo"].ToString();
            ddlSupplier.SelectedValue = dt.Rows[0]["Supp_Code"].ToString();
            txtVAT.Text= dt.Rows[0]["VATP"].ToString();
            txtRevision.Text = dt.Rows[0]["Revision"].ToString();
            txtAmtOfStation.Text= dt.Rows[0]["Amt_of_stations"].ToString();
            txtAmtProStation.Text= dt.Rows[0]["Amt_pro_stations"].ToString();
            txtPossibleCapacity.Text= dt.Rows[0]["Possible_Capacity"].ToString();
            txtProSteps.Text= dt.Rows[0]["Production_Steps"].ToString();
            txtProType.Text= dt.Rows[0]["Production_Type"].ToString();
            ddlStage.SelectedValue= dt.Rows[0]["Stage"].ToString();
            txtBezeichnung.Text= dt.Rows[0]["Bezeichnung"].ToString();
            txtCostOffer.Text= dt.Rows[0]["OfferCost"].ToString();
            ddlWarehouse.SelectedValue = dt.Rows[0]["WarehouseCode"].ToString();
            ddlWarehouse.SelectedItem.Text = dt.Rows[0]["WarehouseName"].ToString();

            if (ddlWarehouse.SelectedItem.Text != "-Select-")
            {
                SSQL = "Select RackSerious from MstWareHouseRackMain where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
                DataTable dt_RackSerious = new DataTable();
                dt_RackSerious = objdata.RptEmployeeMultipleDetails(SSQL);
                ddlRackSerious.DataSource = dt_RackSerious;
                ddlRackSerious.DataTextField = "RackSerious";
                ddlRackSerious.DataValueField = "RackSerious";
                ddlRackSerious.DataBind();

                ddlRackSerious.SelectedValue = dt.Rows[0]["RackSerious"].ToString();
            }

            SSQL = "";
            SSQL = "select * from BOMDocument where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and BOMid='" + id + "'";
            DataTable dt_Doc = new DataTable();
            dt_Doc = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Doc.Rows.Count > 0)
            {
                ViewState["doc_dt"] = dt_Doc;
                RepBOMDoc.DataSource = dt_Doc;
                RepBOMDoc.DataBind();
            }

            btnSave.Text = "Update";
        }
    }
    private void Load_UOM()
    {
        SSQL = "";
        SSQL = "Select * from MstUOM where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        //DataTable dt = new DataTable();
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (dt.Rows.Count > 0)
        //{
        ddlUOM.DataSource = ddlProduct.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlUOM.DataTextField = "UOMFullName";
        ddlUOM.DataValueField = "UOMCode";
        ddlUOM.DataBind();
        ddlUOM.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        // }
    }

    private void Load_Supplier()
    {
        SSQL = "";
        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger where LedgerGrpName='Supplier' and Ccode='" + SessionCcode + "' and ";
        SSQL=SSQL+" Lcode ='"+ SessionLcode +"' and Status='Add'";

        //DataTable dt = new DataTable();
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (dt.Rows.Count > 0)
        //{
        ddlSupplier.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlSupplier.DataTextField = "LedgerName";
        ddlSupplier.DataValueField = "LedgerCode";
        ddlSupplier.DataBind();
        ddlSupplier.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        // }
    }

    private void Load_Genrator()
    {
        SSQL = "";
        SSQL = "Select * from GeneratorModels where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        //DataTable dt = new DataTable();
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (dt.Rows.Count > 0)
        //{
        ddlGenerator.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlGenerator.DataTextField = "GenModelName";
        ddlGenerator.DataValueField = "GenModelNo";
        ddlGenerator.DataBind();
        ddlGenerator.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        // }
    }

    private void Load_Department()
    {
        SSQL = "";
        SSQL = "Select * from MstDepartment where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlDepartment.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDepartment.DataTextField = "deptname";
        ddlDepartment.DataValueField = "deptCode";
        ddlDepartment.DataBind();
        ddlDepartment.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void ddlGenerator_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void InitialData()
    {
        DataTable initial_Table = new DataTable();
        initial_Table.Columns.Add("Doc_Discription");
        initial_Table.Columns.Add("Doc_Name");
        initial_Table.Columns.Add("Doc_Type");
        initial_Table.Columns.Add("Doc_Size");
        initial_Table.Columns.Add("Mat_No");
        initial_Table.Columns.Add("DocPath_Get");
        initial_Table.Columns.Add("DocPath_Set");
        initial_Table.Columns.Add("Doc_NewName");
        RepBOMDoc.DataSource = initial_Table;
        RepBOMDoc.DataBind();
        ViewState["doc_dt"] = RepBOMDoc.DataSource;

    }
    protected void FileUploadComplete(object sender, EventArgs e)
    {
        bool fileErr = false;
        DataRow dr = null;
        // FileInfo file = new FileInfo(Fileupload1.FileName);
        if (!Fileupload1.HasFile)
        {
            fileErr = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Choose the File First')", true); ;
        }
        if (!fileErr)
        {
            string _pathGet = "";
            SSQL = "";
            SSQL = "select Path from FilePaths where Type='DocBOM'";
            DataTable path_dt = new DataTable();
            path_dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (path_dt.Rows.Count > 0)
            {
                _pathGet = path_dt.Rows[0]["path"].ToString();
                _pathGet = _pathGet + txtMatNo.Text + @"\";
            }
            if (!Directory.Exists(_pathGet))
            {
                Directory.CreateDirectory(_pathGet);
            }

            string _fileExtension = "";

            _fileExtension = Path.GetExtension(Fileupload1.PostedFile.FileName);

            string _filePath = _pathGet + txtDocName.Text + _fileExtension;
            string path_to_Save = Server.MapPath(_filePath);
            Fileupload1.PostedFile.SaveAs(path_to_Save);
            DataTable dt = (DataTable)ViewState["doc_dt"];
            dr = dt.NewRow();
            dr["Doc_Discription"] = txtDocDescription.Text;
            dr["Doc_Name"] = txtDocName.Text + _fileExtension;
            dr["Doc_Type"] = _fileExtension;
            dr["DocPath_Get"] = _filePath;
            dr["Doc_Size"] = Fileupload1.FileBytes;
            dt.Rows.Add(dr);
            //RepBOMDoc.DataSource = dt;
            //RepBOMDoc.DataBind();
            ViewState["doc_dt"] = dt;
        }
    }
    protected void btnAddDoc_Click(object sender, EventArgs e)
    {
        
        RepBOMDoc.DataSource = (DataTable)ViewState["doc_dt"];
        RepBOMDoc.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlGenerator.SelectedItem.Text != "-Select-")
        {
            if (ddlProduct.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Choose the Products Againsed Generator')", true); ;
                ErrFlg = true;
            }
        }
        //if (txtMatNo.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Material Number')", true); ;
        //    ErrFlg = true;
        //}
        if (txtMatName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Material Number')", true);
            ErrFlg = true;
        }
        if (ddlUOM.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Choose the Unit Of Measurement')", true); ;
            ErrFlg = true;
        }
        if (ddlDepartment.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Choose the Department')", true); ;
            ErrFlg = true;
        }
        if (txtCatelogNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Drawing No')", true); ;
            ErrFlg = true;
        }
        if (txtDrawingNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Catelog No')", true); ;
            ErrFlg = true;
        }


        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        if (btnSave.Text == "Save")
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "BOM Master");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add...');", true);
            }
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "BOM Master", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtMatNo.Text = Auto_Transaction_No;
                }
            }
        }

        DataTable DT = new DataTable();

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from BOMMaster Where Ccode ='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And ";
            SSQL = SSQL + " GenModelName='" + ddlGenerator.SelectedItem.Text + "' and ProductionPartName='" + ddlProduct.SelectedItem.Text + "' ";
            SSQL = SSQL + " And Raw_Mat_Name='" + txtMatName.Text + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Designation Name Already Exsit');", true);
            }
        }


        if (!ErrFlg)
        {

            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Mat_No='" + txtMatNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Mat_No='" + txtMatNo.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('The Material Number Already taken')", true); ;

                    ErrFlg = true;
                    return;
                }
            }
            if (!ErrFlg)
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();
                string imgpath = "/assets/images/NoImage.png";

                if (hidd_imgPath.Value != imgpath)
                {
                    imgpath = hidd_imgPath.Value.ToString();
                }

                DataTable doc_dt = new DataTable();
                doc_dt = (DataTable)ViewState["doc_dt"];
                DocCount = doc_dt.Rows.Count.ToString();

                SSQL = "";
                SSQL = "Insert into BOMMaster(Ccode,Lcode,Mat_No,Raw_Mat_Name,RequiredQty,DeptCode,DeptName,GenModelNo,GenModelName,";
                SSQL = SSQL + "ProductionPartNo,ProductionPartName,Supp_Code,Supp_Name,Plant,UOMTypeCode,UOM_Full,Sap_No,Drawing_No,Catlog_No,";
                SSQL = SSQL + "Part_No,ValFrom1,ValFrom_str,ValidTo1,ValidTo_Str,Min_Qty,Max_Qty,Amt_of_stations,Amt_pro_stations,Possible_Capacity,";
                SSQL = SSQL + "Production_Steps,Production_Type,Revision,Stage,Bezeichnung,OfferCost,BarCode,ChkIMgVal,ImgPath_Get,Amount,CGSTP,";
                SSQL = SSQL + "SGSTP,IGSTP,VATP,Documnet,WarehouseCode,WarehouseName,RackSerious,CreatedOn,Host_IP,Host_Name,UserName,Status,";
                SSQL = SSQL + "Comment)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "' ,'" + txtMatNo.Text + "','" + txtMatName.Text + "',";
                SSQL = SSQL + "'" + txtRequiredQty.Text + "','" + ddlDepartment.SelectedValue + "','" + ddlDepartment.SelectedItem.Text + "',";
                SSQL = SSQL + "'" + ddlGenerator.SelectedValue + "','" + ddlGenerator.SelectedItem.Text + "','" + ddlProduct.SelectedValue + "',";
                SSQL = SSQL + "'" + ddlProduct.SelectedItem.Text + "','" + ddlSupplier.SelectedValue + "','" + ddlSupplier.SelectedItem.Text + "',";
                SSQL = SSQL + "'" + txtPlant.Text + "','" + ddlUOM.SelectedValue + "','" + ddlUOM.SelectedItem.Text + "','" + txtSapNo.Text + "',";
                SSQL = SSQL + "'" + txtDrawingNo.Text + "','" + txtCatelogNo.Text + "','" + txtPartNo.Text + "','" + Convert.ToDateTime(txtValidFrom.Text) + "',";
                SSQL = SSQL + "'" + txtValidFrom.Text + "','" + Convert.ToDateTime(txtValidTo.Text) + "','" + txtValidTo.Text + "','" + txtMinQty.Text + "',";
                SSQL = SSQL + "'" + txtMaxQty.Text + "','" + txtAmtOfStation.Text + "','" + txtAmtProStation.Text + "','" + txtPossibleCapacity.Text + "',";
                SSQL = SSQL + "'" + txtProSteps.Text + "','" + txtProType.Text + "','" + txtRevision.Text + "','" + ddlStage.SelectedValue + "',";
                SSQL = SSQL + "'" + txtBezeichnung.Text + "','" + txtCostOffer.Text + "','" + txtBarCode.Text + "','" + rbtCheckImg.SelectedValue + "',";
                SSQL = SSQL + "'" + imgpath.ToString() + "','" + txtAmount.Text + "','" + txtCGSTP.Text + "','" + txtSGSTP.Text + "',";
                SSQL = SSQL + "'" + txtIGSTP.Text + "','" + txtVAT.Text + "','" + DocCount + "','" + ddlWarehouse.SelectedValue + "',";
                SSQL = SSQL + "'" + ddlWarehouse.SelectedItem.Text.Split('>').LastOrDefault() + "','" + ddlRackSerious.SelectedValue + "',";
                SSQL = SSQL + "'" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "',";
                SSQL = SSQL + "'" + Session["UserID"] + "','Add','"+ txtComment.Text +"')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ErrFlg = false;


                if (!ErrFlg)    
                {
                    string bomid = "";
                    SSQL = "";
                    SSQL = "Select BOMID from BOMMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and mat_no='" + txtMatNo.Text + "'";
                    DataTable dt_bomid = new DataTable();
                    dt_bomid = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_bomid.Rows.Count > 0)
                    {
                        bomid = dt_bomid.Rows[0]["BOMID"].ToString();
                    }
                    if (doc_dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < doc_dt.Rows.Count; i++)
                        {
                            SSQL = "";
                            SSQL = "Insert into BOMDocument (Ccode,Lcode,BOMid,Doc_Discription,Doc_Name,Doc_Size,Doc_Type,Mat_No,DocPath_Get,DocPath_Set,Doc_NewName,CreatedOn,Host_IP,Host_Name,UserName,UserRole)";
                            SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + bomid + "','" + doc_dt.Rows[i]["Doc_Discription"].ToString() + "','" + doc_dt.Rows[i]["Doc_Name"].ToString() + "',";
                            SSQL = SSQL + " '" + doc_dt.Rows[i]["Doc_Size"].ToString() + "','" + doc_dt.Rows[i]["Doc_Type"].ToString() + "','" + txtMatNo.Text + "','" + doc_dt.Rows[i]["DocPath_Get"].ToString() + "',";
                            SSQL = SSQL + "'" + doc_dt.Rows[i]["DocPath_Set"].ToString() + "','" + doc_dt.Rows[i]["Doc_NewName"].ToString() + "',";
                            SSQL = SSQL + "'" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "','" + Session["UserID"].ToString() + "','Add')";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                    }
                }
                btnClear_Click(sender, e);
                Response.Redirect("/Master/BOMMasterMain.aspx");
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlGenerator.ClearSelection();
        ddlProduct.ClearSelection();
        ddlSupplier.ClearSelection();
        ddlWarehouse.ClearSelection();
        ddlRackSerious.ClearSelection();

        txtMatNo.Text = "";
        txtMatName.Text = "";
        txtPlant.Text = "";
        ddlUOM.ClearSelection();
        ddlDepartment.ClearSelection();
        txtSapNo.Text = "";
        txtDrawingNo.Text = "";
        txtCatelogNo.Text = "";
        txtPartNo.Text = "";
        txtValidFrom.Text = "";
        txtValidTo.Text = "";
        txtMinQty.Text = "0.00";
        txtMaxQty.Text = "0.00";
        txtRequiredQty.Text = "0.00";
        txtBarCode.Text = "";
        rbtCheckImg.ClearSelection();
        txtAmount.Text = "0.00";
        txtCGSTP.Text = "0.00";
        txtSGSTP.Text = "0.00";
        txtIGSTP.Text = "0.00";
        InitialData();
        txtPossibleCapacity.Text = "";
        txtProType.Text = "";
        txtProSteps.Text = "";
        txtAmtOfStation.Text = "";
        txtAmtProStation.Text = "";
        txtRevision.Text = "";
        ddlStage.ClearSelection();
        txtBezeichnung.Text = "";
        txtCostOffer.Text = "0.00";
        txtComment.Text = "";

        btnSave.Text = "Save";

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("BOMID");
        Response.Redirect("/Master/BOMMasterMain.aspx");
    }

    protected void btnDeleteIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {

    }

    [WebMethod]
    public static string MatnoCheck(string id)
    {
        BALDataAccess objdata = new BALDataAccess();
        string SSQL = "";
        bool Status = false;
        SSQL = "Select * from BOMMaster where Mat_No='" + id + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            Status = true;
        }
        string myJsonString = (new JavaScriptSerializer()).Serialize(Status);
        return myJsonString;
    }

    protected void ddlWarehouse_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!ddlWarehouse.SelectedValue.Contains("-Select-"))
        {
            SSQL = "";
            SSQL = "Select RackSerious from MstWareHouseRackMain where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Status!='Delete'";
            DataTable dt_RackSerious = new DataTable();
            dt_RackSerious = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlRackSerious.DataSource = dt_RackSerious;
            ddlRackSerious.DataTextField = "RackSerious";
            ddlRackSerious.DataValueField = "RackSerious";
            ddlRackSerious.DataBind();
            ddlRackSerious.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
}