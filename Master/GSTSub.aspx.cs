﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GSTSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            if (!IsPostBack)
            {
               
                if ((string)Session["GSTid"] != null)
                {
                    Search(Session["GSTid"].ToString());
                }
            }
        }
    }
    private void Search(string id)
    {
        SSQL = "";
        SSQL = "Select * from GSTMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and GSTid='" + id + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtGstType.Enabled = false;
            txtGstType.Text = dt.Rows[0]["GST_Type"].ToString();
            txtCGST_Per.Text = dt.Rows[0]["CGST_Per"].ToString();
            txtSGST_Per.Text = dt.Rows[0]["SGST_Per"].ToString();
            txtIGST_Per.Text = dt.Rows[0]["IGST_Per"].ToString();

            btnSave.Text = "Update";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtGstType.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the GST Type')", true); ;
            ErrFlg = true;
        }
        if (txtCGST_Per.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the CGST %')", true); ;
            ErrFlg = true;
        }
        if (txtSGST_Per.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the SGST %')", true); ;
            ErrFlg = true;
        }
        if (txtIGST_Per.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the IGST %')", true); ;
            ErrFlg = true;
        }
        if (!ErrFlg)
        {

            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete from GSTMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and GST_Type='" + txtGstType.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from GSTMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and GST_Type='" + txtGstType.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('The GST Type Name Already Present')", true); ;

                    ErrFlg = true;
                    return;
                }
            }
            if (!ErrFlg)
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();

                SSQL = "";
                SSQL = "Insert into GSTMaster(Ccode,Lcode,GST_Type,CGST_Per,SGST_Per,IGST_Per,CreatedOn,Host_IP,Host_Name,UserName,Status,isDefault)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','"+ txtGstType.Text +"' ,'" + txtCGST_Per.Text + "',";
                SSQL = SSQL + " '" + txtSGST_Per.Text + "','" + txtIGST_Per.Text + "','" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "',";
                SSQL = SSQL + " '" + _IP + "','" + _HostName + "','" + Session["UserID"] + "','Add','1')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                btnClear_Click(sender, e);

                Response.Redirect("/Master/GSTMain.aspx");
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtIGST_Per.Text = "";
        txtCGST_Per.Text = "";
        txtSGST_Per.Text = "";
        txtGstType.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("GSTid");
        Response.Redirect("/Master/GSTMain.aspx");
    }
}