﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DeptBOMRequiredSub.aspx.cs" Inherits="DeptBOMRequiredSub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <asp:UpdatePanel ID="upBomDeptSub" runat="server">
        <ContentTemplate>
             <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-hand-o-up text-primary"></i> Department Required BOM
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Deparment Name <span class="text-danger">*</span></label>
                                    <asp:DropDownList ID="ddlDepartment" class="form-control select2" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                     <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Material Name <span class="text-danger">*</span></label>
                                    <asp:DropDownList ID="ddlMaterials" class="form-control select2" runat="server"></asp:DropDownList>
                                     <asp:RequiredFieldValidator ControlToValidate="ddlMaterials" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Required Qty <span class="text-danger">*</span></label>
                                    <asp:TextBox ID="txtRequiredQty" class="form-control " runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtRequiredQty" Display="Dynamic"  ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtRequiredQty" ValidChars="0123456789.">
                                     </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click"  ValidationGroup="Validate_Field" runat="server" Text="Save" />
                            <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                        </div>
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-3" hidden>
                <div class="callout callout-info" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-info"></i>Info:</h4>
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </section>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- /.content -->
        </div>
     <script src="assets/js/Master/Dept_BOM_Required.js" type="text/javascript"></script>
</asp:Content>

