﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstBank_Sub.aspx.cs" Inherits="MstBank_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upBOMSub" runat="server">
            <ContentTemplate>
                <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Bank Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">

                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label>Bank Code</label>
                                    <asp:TextBox runat="server" ID="txtBankCode" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtBankCode" Display="Dynamic" ValidationGroup="ValidateDept_Field" 
                                        class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Bank Name</label>
                                    <asp:TextBox runat="server" ID="txtBankName" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtBankName" Display="Dynamic" ValidationGroup="ValidateDept_Field" 
                                        class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                                    
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>IFSC Code</label>
                                    <asp:TextBox runat="server" ID="txtIFSCCode" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtIFSCCode" Display="Dynamic" ValidationGroup="ValidateDept_Field" 
                                        class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Branch</label>
                                    <asp:TextBox runat="server" ID="txtBranch" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtBranch" Display="Dynamic" ValidationGroup="ValidateDept_Field" 
                                        class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Charges Amount</label>
                                    <asp:TextBox runat="server" ID="txtChargesAmt" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtChargesAmt" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Max. Charges</label>
                                    <asp:TextBox runat="server" ID="txtMaxCharge" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtMaxCharge" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Min. Charges</label>
                                    <asp:TextBox runat="server" ID="txtMinCharge" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtMinCharge" Display="Dynamic" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label class="checkbox-inline">
                                    <asp:CheckBox ID="chkDefault" runat="server" />
                                    Default
                                </label>
                            </div>
                        </div>

                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <asp:Button ID="btnClear" class="btn btn-primary" runat="server"  Text="Clear" OnClick="btnClear_Click"/>
                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                    </div>
                </div>
            </div>
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
</asp:Content>

