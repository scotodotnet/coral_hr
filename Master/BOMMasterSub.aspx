﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BOMMasterSub.aspx.cs" Inherits="BOMMasterSub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy'});
                    $('.select2').select2();
                }
            });
        };
    </script>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      
                 <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-cubes text-primary"></i>BOM Materials
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- Default box -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab">BOM Item Details</a></li>
                                    <li><a href="#tab_2" data-toggle="tab">Documents</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!--BOM Item Details-->
                                    <div class="tab-pane active" id="tab_1">
                                        <input type="hidden" id="DynamicInc" value="1" />
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <div class="content-heading-anchor">
                                                    <h4>Production Items</h4>
                                                    <div class="fc-content-skeleton">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Generator Model</label>
                                                                    <asp:DropDownList ID="ddlGenerator" AutoPostBack="true" OnSelectedIndexChanged="ddlGenerator_SelectedIndexChanged" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Production Part Name</label>
                                                                    <asp:DropDownList ID="ddlProduct" CssClass="form-control select2" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Supplier Name</label>
                                                                    <asp:DropDownList ID="ddlSupplier" CssClass="form-control select2" OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Stage</label>
                                                                    <asp:DropDownList ID="ddlStage"
                                                                        CssClass="form-control select2" runat="server">
                                                                        <asp:ListItem Selected="True" Value="-Select-" Text="-Select-"></asp:ListItem>
                                                                        <asp:ListItem Value="Stage1" Text="Stage1"></asp:ListItem>
                                                                        <asp:ListItem Value="Stage2" Text="Stage2"></asp:ListItem>
                                                                        <asp:ListItem Value="Stage3" Text="Stage3"></asp:ListItem>
                                                                        <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="content-heading-anchor">
                                                    <h4>BOM Items</h4>
                                                    <div class="fc-content-skeleton">
                                                        <div class="row">
                                                            <div class="col-md-3" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label class="control-label">Material Number <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtMatNo" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                    <span id="Mat_noError" class="text-danger"></span>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtMatNo" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Bezeichnung</label>
                                                                    <asp:TextBox ID="txtBezeichnung" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Material Name <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtMatName" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtMatName" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">UOM <span class="text-danger">*</span></label>
                                                                    <asp:DropDownList ID="ddlUOM" class="form-control select2" runat="server"></asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtMatName" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Department <span class="text-danger">*</span></label>
                                                                    <asp:DropDownList ID="ddlDepartment" class="form-control select2" runat="server"></asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Plant</label>
                                                                    <asp:TextBox ID="txtPlant" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">SAP Number <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtSapNo" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtSapNo" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Drawing Number</label>
                                                                    <asp:TextBox ID="txtDrawingNo" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtDrawingNo" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Catelog Number</label>
                                                                    <asp:TextBox ID="txtCatelogNo" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="control-label">Part Number</label>
                                                                    <asp:TextBox ID="txtPartNo" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="control-label">Valid From <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtValidFrom" autocomplete="off" class="form-control datepicker" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtValidFrom" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtValidFrom" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="control-label">Valid To <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtValidTo" autocomplete="off" class="form-control datepicker" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtValidTo" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtValidTo" ValidChars="0123456789/">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="control-label">Min Qty. <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtMinQty" autocomplete="off" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtMinQty" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtMinQty" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="control-label">Max Qty. <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtMaxQty" autocomplete="off" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtMaxQty" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtMaxQty" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Required Qty <span class="text-danger">*</span></label>
                                                                        <asp:TextBox ID="txtRequiredQty" autocomplete="off" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtRequiredQty" ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                        <asp:RequiredFieldValidator ControlToValidate="txtRequiredQty" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                        </asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Amount of stations</label>
                                                                        <asp:TextBox ID="txtAmtOfStation" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtAmtOfStation" ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Amount pro stations</label>
                                                                        <asp:TextBox ID="txtAmtProStation" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtAmtProStation" ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Possible capacity</label>
                                                                        <asp:TextBox ID="txtPossibleCapacity" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Production Step</label>
                                                                        <asp:TextBox ID="txtProSteps" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Production Type</label>
                                                                        <asp:TextBox ID="txtProType" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Revision</label>
                                                                        <asp:TextBox ID="txtRevision" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Barcode</label>
                                                                        <br />
                                                                        <div class="input-group ">
                                                                            <div class="input-group-addon">
                                                                                <i class="fa fa-barcode"></i>
                                                                            </div>
                                                                            <asp:TextBox ID="txtBarCode" autocomplete="off" class="form-control" runat="server"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Have Image ?</label>
                                                                        <br />
                                                                        <div class="form-control">
                                                                            <asp:RadioButtonList ID="rbtCheckImg" RepeatColumns="2" runat="server">
                                                                                <asp:ListItem Selected="True" Text="No" Value="No" style="padding: 20px"></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Photo</label>
                                                                        <br />
                                                                        <img src="/assets/images/NoImage.png" style="width: 110px; height: 60px; cursor: pointer;" class="img-thumbnail" id="MaterialImg" />
                                                                        <input style="visibility:hidden" type="file" name="Img" class="form-control" onchange='sendFile(this);' id="f_UploadImage" />
                                                                        <asp:HiddenField ID="hidd_imgPath" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                      <label class="control-label">Ware House <span class="text-danger">*</span></label>
                                                                      <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlWarehouse_SelectedIndexChanged"></asp:DropDownList>
                                                                         <asp:RequiredFieldValidator ControlToValidate="ddlWarehouse" InitialValue="-Select-"  Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator19" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                        </asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                      <label class="control-label">Ware House Rack Serious <span class="text-danger">*</span></label>
                                                                      <asp:DropDownList ID="ddlRackSerious" runat="server" CssClass="form-control select2"></asp:DropDownList>
                                                                         <asp:RequiredFieldValidator ControlToValidate="ddlRackSerious" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator20" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                        </asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Comment</label>
                                                                        <asp:TextBox ID="txtComment" runat="server" Rows="3" cols="30" Style="resize: none" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <br />
                                                <div class="content-heading-anchor">
                                                    <h4>Amount Calculation</h4>
                                                    <div class="fc-content-skeleton">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Cost per last known offer <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtCostOffer" autocomplete="off" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtCostOffer" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtCostOffer" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator18" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Amount <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtAmount" autocomplete="off" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtAmount" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtAmount" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label class="control-label">CGST % <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtCGSTP" autocomplete="off" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtCGSTP" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtCGSTP" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label class="control-label">SGST % <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtSGSTP" autocomplete="off" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtSGSTP" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtSGSTP" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label class="control-label">IGST % <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtIGSTP" autocomplete="off" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtIGSTP" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtIGSTP" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label class="control-label">VAT % <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtVAT" autocomplete="off" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers" TargetControlID="txtVAT" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtVAT" Display="Dynamic" ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!--BOM Document Details-->
                                    <div class="tab-pane" id="tab_2">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" EnableViewState="true">
                                            <ContentTemplate>
                                                <div class="content-heading-anchor">
                                                    <h4>BOM Document</h4>
                                                    <div class="fc-content-skeleton">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Document Name <span class="text-danger">*</span></label>
                                                                    <asp:TextBox ID="txtDocName" runat="server" autocomplete="off" class="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ControlToValidate="txtDocName" Display="Dynamic" ValidationGroup="Doc_Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                    </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">Document Discription</label>
                                                                    <asp:TextBox ID="txtDocDescription" runat="server" Style="resize: none" autocomplete="off" class="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Files</label>
                                                                    <cc1:AsyncFileUpload runat="server" ID="Fileupload1"
                                                                        UploaderStyle="Traditional" CompleteBackColor="White" UploadingBackColor="#CCFFFF"
                                                                        ThrobberID="imgLoader" CssClass="btn btn-default fileinput-button" OnUploadedComplete="FileUploadComplete" />
                                                                    <%--<asp:Image ID="imgLoader" runat="server" ImageUrl="~/assets/images/NoImage.png" />--%>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <br />
                                                                    <asp:Button ID="btnAddDoc" OnClick="btnAddDoc_Click" ValidationGroup="Doc_Validate_Field" class="btn btn-primary addbtn" runat="server" Text="Add" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="table-responsive mailbox-messages">
                                                                    <asp:Repeater ID="RepBOMDoc" runat="server" EnableViewState="false">
                                                                        <HeaderTemplate>
                                                                            <table class="table table-responsive table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Name</th>
                                                                                        <th>Discription</th>
                                                                                        <th>Mode</th>
                                                                                    </tr>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><%# Eval("Doc_Name")%></td>
                                                                                <td><%# Eval("Doc_Discription")%></td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="btnDeleteIssueEntry_Grid" class="btn btn-danger btn-xs fa fa-trash" runat="server"
                                                                                        Text="" OnCommand="btnDeleteIssueEntry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("Mat_No")%>' OnClientClick="retun Confirm('Are you sure want to Delete this Item ?')">
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate></table></FooterTemplate>
                                                                    </asp:Repeater>
                                                                    <!-- /.table -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="form-group" align="center">
                                <asp:Button ID="btnSave" class="btn btn-primary" ValidationGroup="Validate_Field" OnClick="btnSave_Click" runat="server" Text="Save" />
                                <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear" />
                                <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col-md-12-->
                <div class="col-md-3" hidden>
                    <div class="callout callout-info" style="margin-bottom: 0!important;">
                        <h4><i class="fa fa-info"></i>Info:</h4>
                        <p>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
           
       
    </div>

    <!-- jQuery 3 -->
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>

    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="/assets/js/Master/BOMMasters.js"></script>


</asp:Content>

