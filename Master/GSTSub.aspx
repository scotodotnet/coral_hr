﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GSTSub.aspx.cs" Inherits="GSTSub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <asp:UpdatePanel ID="upGSTSub" runat="server">
        <ContentTemplate>
             <section class="content-header">
        <h1>
            <i class="fa fa-rupee text-primary"></i>GST
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">GST Type</label>
                                    <asp:TextBox ID="txtGstType" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">CGST %</label>
                                    <asp:TextBox ID="txtCGST_Per" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtCGST_Per" Display="Dynamic"  
                                         ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator3" runat="server" 
                                         EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtCGST_Per" ValidChars="0123456789.">
                                     </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">SGST %</label>
                                    <asp:TextBox ID="txtSGST_Per" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtSGST_Per" Display="Dynamic"  
                                         ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator1" runat="server" 
                                         EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtSGST_Per" ValidChars="0123456789.">
                                     </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">IGST %</label>
                                    <asp:TextBox ID="txtIGST_Per" class="form-control" Text="0.00" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtIGST_Per" Display="Dynamic"  
                                         ValidationGroup="Validate_Field" class="text-danger" ID="RequiredFieldValidator2" runat="server" 
                                         EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtIGST_Per" ValidChars="0123456789.">
                                     </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click" runat="server" Text="Save" />
                            <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                        </div>
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-3" hidden>
                <div class="callout callout-info" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-info"></i>Info:</h4>
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
        </ContentTemplate>
    </asp:UpdatePanel>
   
        </div>
    <!-- iCheck -->
    <script src="assets/adminlte/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('.checkbox').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
</asp:Content>

