﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DeptBOMRequiredMain.aspx.cs" Inherits="DeptBOMRequiredMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <asp:UpdatePanel ID="upForm" runat="server">
        <ContentTemplate>   
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-1">
                           <%-- <a href="/Master/DeptBOMRequiredSub.aspx" class="btn btn-primary btn-block margin-bottom"><i class="fa fa-plus"></i><span>Add</span></a>--%>
                            <asp:Button ID="btnAddNew" runat="server" CssClass="btn btn-primary" OnClick="btnAddNew_Click" Text="Add New" /><br />
                                <br />
                        </div>
                        <!-- /.col -->
                    </div>
                    <!--/.row-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-hand-pointer-o text-primary"></i><span> Department Required BOM</span></h3>
                                   <%-- <div class="box-tools pull-right">
                                        <div class="has-feedback">
                                            <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                        </div>
                                    </div>--%>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                            <HeaderTemplate>
                                                <table id="example" class="table table-responsive table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Material No</th>
                                                            <th>Material Name</th>
                                                            <th>Department Name</th>
                                                            <th>Mode</th>
                                                        </tr>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tbody>
                                                    <tr>
                                                        <td><%# Eval("Mat_No")%></td>
                                                        <td><%# Eval("Raw_Mat_Name")%></td>
                                                        <td><%# Eval("DeptName")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("Mat_No")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteIssueEntry_Grid" class="btn btn-danger btn-sm fa fa-trash" runat="server"
                                                                Text="" OnCommand="btnDeleteIssueEntry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("Mat_No")%>' OnClientClick="retun Confirm('Are you sure want to Delete this Item ?')">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </ItemTemplate>
                                            <FooterTemplate></table></FooterTemplate>
                                        </asp:Repeater>
                                        <!-- /.table -->
                                    </div>
                                    <!-- /.mail-box-messages -->

                                    <div class="modal modal-primary fade" id="modalDefault">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer no-padding">
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

