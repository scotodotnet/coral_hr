﻿using System;
using System.Collections.Generic;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;
using WebFormsTest;

public partial class Master_BOMUpload : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string DocCount = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();

            if (!IsPostBack)
            {
                Load_Department();
                Load_Genrator();
                Load_Product();
                Load_Supplier();
            }
        }
    }
    private void Load_Product()
    {
        SSQL = "";
        SSQL = "Select  (ProductNo+' ~> '+ProductName) as Text,ProductNo as Value from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlProduct.DataSource = objdata.RptEmployeeMultipleDetails(SSQL); ;
        ddlProduct.DataTextField = "Text";
        ddlProduct.DataValueField = "Value";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    private void Load_Supplier()
    {
        SSQL = "";
        SSQL = "Select * from SupplierMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        //DataTable dt = new DataTable();
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (dt.Rows.Count > 0)
        //{
        ddlSupplier.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlSupplier.DataTextField = "Supp_Name";
        ddlSupplier.DataValueField = "Supp_Code";
        ddlSupplier.DataBind();
        ddlSupplier.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        // }
    }

    private void Load_Genrator()
    {
        SSQL = "";
        SSQL = "Select (GenModelNo+' ~> '+GenmodelName) as Text,GenmodelNo as Value from GeneratorModels where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        //DataTable dt = new DataTable();
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (dt.Rows.Count > 0)
        //{
        ddlGenerator.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlGenerator.DataTextField = "Text";
        ddlGenerator.DataValueField = "Value";
        ddlGenerator.DataBind();
        ddlGenerator.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        // }
    }
    private void Load_Department()
    {
        SSQL = "";
        SSQL = "Select * from MstDepartment where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlDepartment.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDepartment.DataTextField = "deptname";
        ddlDepartment.DataValueField = "deptCode";
        ddlDepartment.DataBind();
        ddlDepartment.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void ddlGenerator_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlGenerator.SelectedValue != "Select-")
        {


            SSQL = "";
            SSQL = "Select  (ProductNo+' ~> '+ProductName) as Text,ProductNo as Value from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status!='Delete'";
            SSQL = SSQL + " and GenModelno='" + ddlGenerator.SelectedValue + "'";
            ddlProduct.DataSource = objdata.RptEmployeeMultipleDetails(SSQL); ;
            ddlProduct.DataTextField = "Text";
            ddlProduct.DataValueField = "Value";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
        else
        {
            Load_Product();
        }
    }

    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("/master/BOMMasterMain.aspx");
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        string _pathToSave = "uploads/BOMUpload/";
        if (ddlGenerator.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the Generator Model')", true);
            ErrFlg = true;
        }
        if (ddlProduct.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the Product Name')", true);
            ErrFlg = true;
        }
        if (ddlSupplier.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the Supplier')", true);
            ErrFlg = true;
        }
        if (ddlDepartment.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the Department')", true);
            ErrFlg = true;
        }
        if (ddlStage.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the Items Stage Level')", true);
            ErrFlg = true;
        }
        if (FileUpload.HasFile)
        {
           
            if (!Directory.Exists(Server.MapPath(_pathToSave)))
            {
                Directory.CreateDirectory(Server.MapPath(_pathToSave));
            }
            FileUpload.SaveAs(Server.MapPath(_pathToSave + FileUpload.FileName));

        }
        if (!ErrFlg)
        {
            DataTable table = new DataTable();
            if (Path.GetExtension(FileUpload.FileName) == ".xlsx")
            {
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                ExcelPackage package = new ExcelPackage(FileUpload.FileContent);

                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
               
                foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
                {
                    table.Columns.Add(firstRowCell.Text);
                }
                for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
                {
                    var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                    var newRow = table.NewRow();
                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }
                    table.Rows.Add(newRow);
                }
            }

            if (table.Rows.Count > 0)
            {
                DateTime ValidFrom = DateTime.Now, ValidTo = DateTime.Now;
                string Bezeichnung = "";
                string Name = "";
                string SAP_no = "";
                string Matno = "";
                string Draw_No = "";
                string Revision = "";
                string Amt_Station = "";
                string Amt_ProStaion = "";
                string Possible_Capacity = "";
                string Production_Step = "";
                string Type = "";
                string OfferCost = "";
                string Amt = "";
                string CGSTP = "";
                string SGSTP = "";
                string IGSTP = "";
                string VATP = "";
                string ValidFromstr = "";
                string ValidTostr = "";
                string MinQty = "0.00";
                string MaxQty = "0.00";
                string RequiredQty = "0.00";
                string PartNo = "";
                string CatlogNo = "";
                string Plant = "";
                string WarehouseCode = "";
                string WarehouseName = "";
                string RackSerious = "";
                string UOMId = "";
                string UOMName = "";

                string _pathGet = "", _photoName = "";

                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();

                SSQL = "";
                SSQL = "select path from FilePaths where Type='PhotoBOM'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    _pathGet = dt.Rows[0]["Path"].ToString();
                }
                if (!Directory.Exists(Server.MapPath (_pathGet)))
                {
                    Directory.CreateDirectory(Server.MapPath(_pathGet));
                }
                string GenModelNo = ddlGenerator.SelectedValue;
                string GenModelName=ddlGenerator.SelectedItem.Text.Split('>').LastOrDefault().ToString();
                string ProductionPartno = ddlProduct.SelectedValue;
                string productionname=ddlProduct.SelectedItem.Text.Split('>').LastOrDefault().ToString();
                string SupplierCode = ddlSupplier.SelectedValue;
                string SupplierName = ddlSupplier.SelectedItem.Text;
                string stage = ddlStage.SelectedValue;
                string DeptCode = ddlDepartment.SelectedValue;
                string DeptName=ddlDepartment.SelectedItem.Text;

                for (int iRow = 0; iRow < table.Rows.Count; iRow++)
                {
                    Bezeichnung = table.Rows[iRow]["Bezeichnung"].ToString();
                    Name = table.Rows[iRow]["Name"].ToString();
                    SAP_no = table.Rows[iRow]["SAP or supplier number"].ToString();
                    Matno = table.Rows[iRow]["BOM Number"].ToString();
                    Draw_No = table.Rows[iRow]["Drawning Number"].ToString();
                    Revision = table.Rows[iRow]["Revision"].ToString();
                    Amt_Station = table.Rows[iRow]["Amount of stations"].ToString();
                    Amt_ProStaion = table.Rows[iRow]["Amount pro station"].ToString();
                    Possible_Capacity = table.Rows[iRow]["Possible capacity"].ToString();
                    Production_Step = table.Rows[iRow]["Production step"].ToString();
                    Type = table.Rows[iRow]["Type"].ToString();
                    OfferCost = table.Rows[iRow]["Cost per last known offer"].ToString();
                    Amt = table.Rows[iRow]["Costs summary (Amount)"].ToString();
                    CGSTP = table.Rows[iRow]["CGST %"].ToString();
                    SGSTP = table.Rows[iRow]["SGST %"].ToString();
                    IGSTP = table.Rows[iRow]["IGST %"].ToString();
                    VATP = table.Rows[iRow]["VAT %"].ToString();
                    ValidFromstr = table.Rows[iRow]["Valid From"].ToString();
                    ValidTostr = table.Rows[iRow]["Valid To"].ToString();
                    MinQty = table.Rows[iRow]["Min Qty"].ToString();
                    MaxQty = table.Rows[iRow]["Max Qty"].ToString();
                    RequiredQty = table.Rows[iRow]["Required Qty"].ToString();
                    PartNo = table.Rows[iRow]["PartNo"].ToString();
                    CatlogNo = table.Rows[iRow]["CatelogNo"].ToString();
                    Plant = table.Rows[iRow]["Plant"].ToString();
                    WarehouseCode = table.Rows[iRow]["WarehouseCode"].ToString();
                    WarehouseName = table.Rows[iRow]["WarehouseName"].ToString();
                    RackSerious = table.Rows[iRow]["RackSerious"].ToString();
                    UOMId = table.Rows[iRow]["UOMType Code"].ToString();
                    UOMName = table.Rows[iRow]["UOM Name"].ToString();

                    
                    if (ValidFromstr != "")
                    {
                        ValidFrom = Convert.ToDateTime(ValidFromstr);
                    }
                    if (ValidTostr != "")
                    {
                        ValidTo = Convert.ToDateTime(ValidTostr);
                    }

                    if (!Directory.Exists(Server.MapPath(_pathGet)))
                    {
                        Directory.CreateDirectory(Server.MapPath(_pathGet));
                    }
                    _photoName = _pathGet + Matno + "_" + Name + ".jpg";

                    SSQL = "";
                    SSQL = "Insert into BOMMaster(Ccode,Lcode,Mat_No,Raw_Mat_Name,RequiredQty,DeptCode,DeptName,GenModelNo,GenModelName,ProductionPartNo,ProductionPartName,";
                    SSQL = SSQL + "Supp_Code,Supp_Name,Plant,Sap_No,Drawing_No,Catlog_No,Part_No,ValFrom,ValFrom_str,ValidTo,ValidTo_Str,Min_Qty,Max_Qty,";
                    SSQL = SSQL + "Amt_of_stations,Amt_pro_stations,Possible_Capacity,Production_Steps,Production_Type,Revision,Stage,Bezeichnung,OfferCost,";
                    SSQL = SSQL + "ImgPath_Get,Amount,CGSTP,SGSTP,IGSTP,VATP,Documnet,CreatedOn,Host_IP,Host_Name,UserName,Status,UOM_Full,UOMTypeCode)";
                    SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "' ,'" + Matno + "','" + Name + "','" + RequiredQty + "',";
                    SSQL = SSQL + "'" + DeptCode + "','" + DeptName + "','" + GenModelNo + "','" + GenModelName + "','" + ProductionPartno + "',";
                    SSQL = SSQL + "'" + productionname + "','" + SupplierCode + "','" + SupplierName + "','" + Plant + "','" + SAP_no + "','" + Draw_No + "','" + CatlogNo + "',";
                    SSQL = SSQL + "'" + PartNo + "',Convert(date,'" + ValidFrom + "',103),'" + ValidFromstr + "',Convert(date,'" + ValidTo + "',103),'" + ValidTostr + "',";
                    SSQL = SSQL + "'" + MinQty + "','" + MaxQty + "','" + Amt_Station + "','" + Amt_ProStaion + "','" + Possible_Capacity + "','" + Production_Step + "',";
                    SSQL = SSQL + "'" + Type + "','" + Revision + "','" + stage + "','" + Bezeichnung + "','" + OfferCost + "','" + _photoName + "',";
                    SSQL = SSQL + "'" + Amt + "','" + CGSTP + "','" + SGSTP + "','" + IGSTP + "','" + VATP + "','" + DocCount + "',";
                    SSQL = SSQL + "'" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "',";
                    SSQL = SSQL + "'" + Session["UserID"] + "','Upload','" + UOMId + "','" + UOMName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        FileUpload.Dispose();
        ddlStage.ClearSelection();
        ddlGenerator.ClearSelection();
        ddlProduct.ClearSelection();
        ddlSupplier.ClearSelection();
        ddlDepartment.ClearSelection();
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        
    //    var students = new[]
    //{
    //    new {
    //        Id = "101", Name = "Vivek", Address = "Hyderabad"
    //    },
    //    new {
    //        Id = "102", Name = "Ranjeet", Address = "Hyderabad"
    //    },
    //    new {
    //        Id = "103", Name = "Sharath", Address = "Hyderabad"
    //    },
    //    new {
    //        Id = "104", Name = "Ganesh", Address = "Hyderabad"
    //    },
    //    new {
    //        Id = "105", Name = "Gajanan", Address = "Hyderabad"
    //    },
    //    new {
    //        Id = "106", Name = "Ashish", Address = "Hyderabad"
    //    }
    //};
        ExcelPackage.LicenseContext = LicenseContext.Commercial;
        ExcelPackage excel = new ExcelPackage();
        var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
        workSheet.TabColor = System.Drawing.Color.Black;
        workSheet.DefaultRowHeight = 12;
        //Header of table  
        //  
        workSheet.Row(1).Height = 20;
        workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        workSheet.Row(1).Style.Font.Bold = true;
        workSheet.Cells[1, 1].Value = "Bezeichnung";
        workSheet.Cells[1, 2].Value = "Name";
        workSheet.Cells[1, 3].Value = "SAP or supplier number";
        workSheet.Cells[1, 4].Value = "BOM Number";
        workSheet.Cells[1, 5].Value = "Drawning Number";
        workSheet.Cells[1, 6].Value = "Revision";
        workSheet.Cells[1, 7].Value = "Amount of stations";
        workSheet.Cells[1, 8].Value = "Amount pro station";
        workSheet.Cells[1, 9].Value = "Possible capacity";
        workSheet.Cells[1, 10].Value = "Production step";
        workSheet.Cells[1, 11].Value = "Type";
        workSheet.Cells[1, 12].Value = "Cost per last known offer";
        workSheet.Cells[1, 13].Value = "Costs summary (Amount)";
        workSheet.Cells[1, 14].Value = "CGST %";
        workSheet.Cells[1, 15].Value = "SGST %";
        workSheet.Cells[1, 16].Value = "IGST %";
        workSheet.Cells[1, 17].Value = "VAT %";
        workSheet.Cells[1, 18].Value = "Valid From";
        workSheet.Cells[1, 19].Value = "Valid To";
        workSheet.Cells[1, 20].Value = "Min Qty";
        workSheet.Cells[1, 21].Value = "Max Qty";
        workSheet.Cells[1, 22].Value = "Required Qty";
        workSheet.Cells[1, 23].Value = "PartNo";
        workSheet.Cells[1, 24].Value = "CatelogNo";
        workSheet.Cells[1, 25].Value = "Plant";
        workSheet.Cells[1, 26].Value = "WarehouseCode";
        workSheet.Cells[1, 27].Value = "WarehouseName";
        workSheet.Cells[1, 28].Value = "RackSerious";
        workSheet.Cells[1, 29].Value = "UOMType Code";
        workSheet.Cells[1, 30].Value = "UOM Name";
        //Body of table  
        //  
        //int recordIndex = 2;
        //foreach (var student in students)
        //{
        //    workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
        //    workSheet.Cells[recordIndex, 2].Value = student.Id;
        //    workSheet.Cells[recordIndex, 3].Value = student.Name;
        //    workSheet.Cells[recordIndex, 4].Value = student.Address;
        //    recordIndex++;
        //}
        workSheet.Column(1).AutoFit();
        workSheet.Column(2).AutoFit();
        workSheet.Column(3).AutoFit();
        workSheet.Column(4).AutoFit();
        workSheet.Column(5).AutoFit();
        workSheet.Column(6).AutoFit();
        workSheet.Column(7).AutoFit();
        workSheet.Column(8).AutoFit();
        workSheet.Column(9).AutoFit();
        workSheet.Column(10).AutoFit();
        workSheet.Column(11).AutoFit();
        workSheet.Column(12).AutoFit();
        workSheet.Column(13).AutoFit();
        workSheet.Column(14).AutoFit();
        workSheet.Column(15).AutoFit();
        workSheet.Column(16).AutoFit();
        workSheet.Column(17).AutoFit();
        workSheet.Column(18).AutoFit();
        workSheet.Column(19).AutoFit();
        workSheet.Column(20).AutoFit();
        workSheet.Column(21).AutoFit();
        workSheet.Column(22).AutoFit();

        string excelName = "BOM Materials";
        using (var memoryStream = new MemoryStream())
        {
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
            excel.SaveAs(memoryStream);
            memoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
    }

    protected void btnWareHouseList_Click(object sender, EventArgs e)
    {
        //SSQL = "";
        //SSQL = "Select WM.WarehouseCode,WM.WarehouseName,WM.RackSerious from MstWareHouseRackMain WM ";
        //SSQL = SSQL + "inner join BOMMaster BOM on WM.RackSerious!=BOM.RackSerious";
        //SSQL = SSQL + " where WM.Ccode='" + SessionCcode + "' and WM.Lcode='" + SessionLcode + "'";
        //SSQL = SSQL + " and BOM.CCode='" + SessionCcode + "' and BOM.Lcode='" + SessionLcode + "'";
        SSQL = "";
        SSQL = "SELECT WM.WarehouseCode,WM.WarehouseName,WM.RackSerious FROM MstWareHouseRackMain WM ";
        SSQL = SSQL + "WHERE NOT EXISTS (SELECT * FROM BOMMaster BOM WHERE BOM.WarehouseCode = WM.WarehouseCode and BOM.CCode='" + SessionCcode + "' and BOM.Lcode='" + SessionLcode + "')";
        SSQL = SSQL + " and WM.Ccode='" + SessionCcode + "' and WM.Lcode='" + SessionLcode + "'";
        DataTable dt_Warehouselist = new DataTable();
        dt_Warehouselist = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_Warehouselist.Rows.Count > 0)
        {
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;
            workSheet.Row(1).Height = 20;
            //workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;

            workSheet.Cells["A1"].LoadFromDataTable(dt_Warehouselist, true);
            for (int col = 1; col <= dt_Warehouselist.Columns.Count; col++)
            {
                workSheet.Column(col).AutoFit();
            }
            string excelName = "Empty Ware house List_("+DateTime.Now.ToString("ddMMyyhhmmtt")+")";
            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }
}