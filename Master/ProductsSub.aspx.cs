﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ProductsSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionFinYearVal;
    string SessionUserName;

    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionFinYearVal = Session["FinYear"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();

            if (!IsPostBack)
            {
                Load_Generator();

                if (Session["ProductNo"] != null)
                {
                    Search(Session["ProductNo"].ToString());
                }
            }
        }
    }

    private void Search(string prodID)
    {
        SSQL = "Select * from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ";
        SSQL = SSQL + " ProductNo ='" + prodID + "' ";

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            txtProductNo.Enabled = false;
            txtProductNo.Text = dt.Rows[0]["ProductNo"].ToString();
            txtProductName.Text = dt.Rows[0]["ProductName"].ToString();
            txtDescription.Text = dt.Rows[0]["Description"].ToString();
            ddlGenerators.SelectedValue = dt.Rows[0]["GenModelNo"].ToString();
            btnSave.Text = "Update";
        }
    }

    private void Load_Generator()
    {
        SSQL = "";
        SSQL = "Select GenModelName as Text , GenModelNo from GeneratorModels where Ccode='"+SessionCcode+"'";
        SSQL = SSQL + " and Lcode='" + SessionLcode + "' and Status!='Delete'";
        ddlGenerators.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlGenerators.DataTextField = "Text";
        ddlGenerators.DataValueField = "GenModelNo";
        ddlGenerators.DataBind();
        ddlGenerators.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool Rights_Check = false;

        //if (txtProductNo.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Product Number')", true); ;
        //    ErrFlg = true;
        //}
        if (txtProductName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('Please Enter the Product Name')", true); ;
            ErrFlg = true;
        }

        //User  Rights
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Production Items");

        if (Rights_Check == false)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add New Item...');", true);
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlg)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Production Items", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtProductNo.Text = Auto_Transaction_No;
                }
            }
        }

        //Dublicate Name Checking
        if (btnSave.Text == "Save")
        {
            SSQL = "";
            SSQL = "Select * from ProductModel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and ProductName='" + txtProductName.Text + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "ShowMessageError('The Product Number Already Taken')", true); ;
                //txtProductNo.Focus();
                ErrFlg = true;
                return;
            }
        }

        if (!ErrFlg)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Update ProductModel set GenModelNo='" + ddlGenerators.SelectedValue + "',";
                SSQL = SSQL + " GenModelName ='" + ddlGenerators.SelectedItem.Text + "',ProductName='" + txtProductName.Text + "',";
                SSQL = SSQL + " Description='"+ txtDescription.Text +"'  where Ccode='" + SessionCcode + "' and ";
                SSQL = SSQL + " Lcode='" + SessionLcode + "' And ProductNo='" + txtProductNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();

                SSQL = "Insert into ProductModel(Ccode,Lcode,GenModelNo,ProductNo,ProductName,GenModelName,Description,CreatedOn,";
                SSQL = SSQL + " Host_IP,Host_Name,UserName,Status)Values('" + SessionCcode + "','" + SessionLcode + "' ,";
                SSQL = SSQL + " '" + ddlGenerators.SelectedValue + "','" + txtProductNo.Text + "','" + txtProductName.Text + "',";
                SSQL = SSQL + " '" + ddlGenerators.SelectedItem.Text + "','" + txtDescription.Text + "',";
                SSQL = SSQL + " '" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "',";
                SSQL = SSQL + " '" + _HostName + "','" + Session["UserID"] + "','Add')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            btnClear_Click(sender, e);
            Response.Redirect("/Master/ProductsMain.aspx");
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlGenerators.ClearSelection();
        txtDescription.Text = "";
        txtProductName.Text = "";
        txtProductNo.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("ProductNo");
        Response.Redirect("/Master/ProductsMain.aspx");
    }
}