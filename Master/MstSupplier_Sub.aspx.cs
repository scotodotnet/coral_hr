﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstSupplier_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearVal;
    string Dept_Code_Delete = "";
    string SessionLedgerCode = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Supplier";

            Load_AccountType();
            Load_Bank();

            if (Session["LedgerCode"] == null)
            {
                SessionLedgerCode = "";
            }
            else
            {
                SessionLedgerCode = Session["LedgerCode"].ToString();
                txtLedgerCode.Text = SessionLedgerCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }

    private void Load_Bank()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select BankCode,BankName from MstBank where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status = 'Add'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlBankName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["BankCode"] = "-Select-";
        dr["BankName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlBankName.DataValueField = "BankCode";
        ddlBankName.DataTextField = "BankName";
        ddlBankName.DataBind();
    }


    private void Load_AccountType()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select AccTypeCode,AccTypeName from Acc_Mst_AccountType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status='Add'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlAccType.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["AccTypeCode"] = "-Select-";
        dr["AccTypeName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlAccType.DataValueField = "AccTypeCode";
        ddlAccType.DataTextField = "AccTypeName";
        ddlAccType.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;


        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string HostName = getIPAndName.GetName();

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from Acc_Mst_Ledger Where LedgerName='" + txtLedgerName.Text + "' And Ccode='" + SessionCcode + "' And ";
            SSQL = SSQL + " Lcode ='" + SessionLcode + "' And LedgerGrpName='Supplier' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
            }
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Supplier Master", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtLedgerCode.Text = Auto_Transaction_No;
                }
            }
        }


        if (!ErrFlag)
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update Acc_Mst_Ledger set LedgerName='" + txtLedgerName.Text + "',AccountTypeCode='"+ ddlAccType.SelectedValue +"',";
                SSQL = SSQL + " AccountType='" + ddlAccType.SelectedItem.Text + "',LedgerGrpId='1',LedgerGrpName='Supplier', ";
                SSQL = SSQL + " HouseNo='" + txtHouseNumber.Text + "',Add1='" + txtAddr1.Text + "',Add2='" + txtAddr2.Text + "',city='" + txtCity.Text + "',";
                SSQL = SSQL + " PinCode ='" + txtPincode.Text + "',State='" + txtState.Text + "',Country='" + txtCountry.Text + "',Std_Code='" + txtStdCode.Text + "',";
                SSQL = SSQL + " TelNo='" + txtPhone.Text + "',Mobile_Code='" + txtMobiCode.Text + "',MobileNo='" + txtMobile.Text + "',";
                SSQL = SSQL + " MailID='" + txtMail.Text + "',TransPort='" + txtTransport.Text + "',Zone='" + txtZone.Text + "',GSTNo='" + txtGstin.Text + "',";
                SSQL = SSQL + " Description ='" + txtNotes.Text + "', AccountNo='" + txtAccountNo.Text + "',BankId='" + ddlBankName.SelectedValue + "',";
                SSQL = SSQL + " Website ='" + txtWebSite.Text + "',BankName='" + ddlBankName.SelectedItem.Text + "',";
                SSQL = SSQL + " BranchName='" + txtBranchName.Text + "',IFSCCode='" + txtIFSC.Text + "' Where Ccode='" + SessionCcode + "' And  ";
                SSQL = SSQL + " Lcode ='" + SessionLcode + "' And LedgerCode='" + txtLedgerCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Account Type Details Updated Successfully');", true);
            }
            else
            {
                SSQL = "Insert Into Acc_Mst_Ledger(Ccode,Lcode,LedgerCode,LedgerName,AccountTypeCode,AccountType,LedgerGrpId,LedgerGrpName,HouseNo,";
                SSQL = SSQL + " Add1,Add2,City,PinCode,State,Country,Std_Code,TelNo,Mobile_Code,MobileNo,MailID,TransPort,Zone,GSTNo,Description,";
                SSQL = SSQL + " AccountNo,BankId,BankName,BranchName,IFSCCode,Status,UserID,UserName,CreateOn,IP,SystemName,Website,DeptId,DeptName,";
                SSQL = SSQL + " DesignationId,DesignationName,AadharNo)Values('" + SessionCcode + "','" + SessionLcode + "','" + txtLedgerCode.Text + "',";
                SSQL = SSQL + " '" + txtLedgerName.Text + "','" + ddlAccType.SelectedValue + "','" + ddlAccType.SelectedItem.Text + "',";
                SSQL = SSQL + " '1','Supplier','" + txtHouseNumber.Text + "','" + txtAddr1.Text + "','" + txtAddr2.Text + "',";
                SSQL = SSQL + " '" + txtCity.Text + "','" + txtPincode.Text + "','" + txtState.Text + "','" + txtCountry.Text + "',";
                SSQL = SSQL + " '" + txtStdCode.Text + "','" + txtPhone.Text + "','" + txtMobiCode.Text + "','" + txtMobile.Text + "',";
                SSQL = SSQL + " '" + txtMail.Text + "','" + txtTransport.Text + "','" + txtZone.Text + "','" + txtGstin.Text + "','" + txtNotes.Text + "',";
                SSQL = SSQL + " '" + txtAccountNo.Text + "','" + ddlBankName.SelectedValue + "',";
                SSQL = SSQL + " '" + ddlBankName.SelectedItem.Text + "','" + txtBranchName.Text + "','" + txtIFSC.Text + "','Add',";
                SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "',Getdate(),'" + IP + "','" + HostName + "','" + txtWebSite.Text + "',";
                SSQL = SSQL + " '','','','','')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Account Type Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtLedgerCode.Enabled = true;

            Response.Redirect("Mstsupplier_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Ledger Details Already Saved');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtLedgerCode.ReadOnly = false;
        txtLedgerCode.Text = "";
        txtLedgerName.Text = "";
        ddlAccType.SelectedItem.Text = "-Select-";
        txtHouseNumber.Text = "";
        txtAddr1.Text = "";
        txtAddr2.Text = "";
        txtCity.Text = "";
        txtPincode.Text = "";
        txtState.Text = "";
        txtCountry.Text = "";
        txtMobiCode.Text = "";
        txtMobile.Text = "";
        txtStdCode.Text = "";
        txtPhone.Text = "";
        txtMail.Text = "";
        ddlBankName.SelectedItem.Text = "-Select-";
        txtAccountNo.Text = "";
        txtBranchName.Text = "";
        txtIFSC.Text = "";
        txtTransport.Text = "";
        txtZone.Text = "";
        txtGstin.Text = "";
        txtNotes.Text = "";
        txtWebSite.Text = "";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status = 'Add' And ";
        SSQL = SSQL + " LedgerCode='" + txtLedgerCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtLedgerCode.ReadOnly = true;
            txtLedgerName.Text = DT.Rows[0]["LedgerName"].ToString();
            ddlAccType.SelectedItem.Text = DT.Rows[0]["AccountType"].ToString();
            ddlAccType.SelectedValue = DT.Rows[0]["AccountTypeCode"].ToString();
            txtHouseNumber.Text = DT.Rows[0]["HouseNo"].ToString();
            txtAddr1.Text = DT.Rows[0]["Add1"].ToString();
            txtAddr2.Text = DT.Rows[0]["Add2"].ToString();
            txtCity.Text = DT.Rows[0]["City"].ToString();
            txtPincode.Text = DT.Rows[0]["PinCode"].ToString();
            txtState.Text = DT.Rows[0]["State"].ToString();
            txtCountry.Text = DT.Rows[0]["Country"].ToString();
            txtStdCode.Text = DT.Rows[0]["Std_Code"].ToString();
            txtPhone.Text = DT.Rows[0]["TelNo"].ToString();
            txtMobiCode.Text = DT.Rows[0]["Mobile_Code"].ToString();
            txtMobile.Text = DT.Rows[0]["MobileNo"].ToString();
            txtMail.Text = DT.Rows[0]["MailID"].ToString();
             
            txtTransport.Text = DT.Rows[0]["TransPort"].ToString();
            txtZone.Text = DT.Rows[0]["Zone"].ToString();
            txtGstin.Text = DT.Rows[0]["GSTNo"].ToString();
            txtNotes.Text = DT.Rows[0]["Description"].ToString();
             
            ddlBankName.SelectedValue = DT.Rows[0]["BankId"].ToString();
            ddlBankName.SelectedItem.Text = DT.Rows[0]["BankName"].ToString();
            txtBranchName.Text = DT.Rows[0]["BranchName"].ToString();
            txtIFSC.Text = DT.Rows[0]["IFSCCode"].ToString();
            txtAccountNo.Text = DT.Rows[0]["AccountNo"].ToString();
            txtWebSite.Text = DT.Rows[0]["WebSite"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Mstsupplier_Main.aspx");
    }
}
