﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstWasteMaterial_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionWasteMatCode = "";
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: WasteMaterial Master";

            if (Session["WasteMatCode"] == null)
            {
                SessionWasteMatCode = "";
            }
            else
            {
                SessionWasteMatCode = Session["WasteMatCode"].ToString();
                txtWasteMatCode.Text = SessionWasteMatCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
            
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string HostName = getIPAndName.GetName();

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from MstWasteMat Where WasteMatName='" + txtWasteMatName.Text + "' And WasteMatCode='" + txtWasteMatCode.Text + "' And ";
            SSQL = SSQL + " Ccode ='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Waste Material Name Already Exsit');", true);
            }
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "Waste Items Master", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtWasteMatCode.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstWasteMat set WasteMatName='" + txtWasteMatName.Text + "' ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And WasteMatCode='" + txtWasteMatCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Designation Details Updated Successfully');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Department Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstWasteMat(Ccode,Lcode,WasteMatCode,WasteMatName,Status,UserID,UserName,CreateOn,Host_IP,Host_Name) Values( ";
                SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + txtWasteMatCode.Text + "','" + txtWasteMatName.Text + "',";
                SSQL = SSQL + " 'Add','" + SessionUserID + "','" + SessionUserName + "',Getdate(),'"+ IP +"',";
                SSQL = SSQL + " '" + HostName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Waste Material Details Saved Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtWasteMatCode.Enabled = true;

            Response.Redirect("MstWasteMaterial_Main.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Waste Material Details Already Saved');", true);
        }
    }
    private void Clear_All_Field()
    {
        txtWasteMatCode.ReadOnly = false;
        txtWasteMatCode.Text = "";
        txtWasteMatName.Text = "";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from MstWasteMat where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status <> 'Delete' And ";
        SSQL = SSQL + " WasteMatCode='" + txtWasteMatCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtWasteMatCode.ReadOnly = true;
            txtWasteMatName.Text = DT.Rows[0]["WasteMatName"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("MstWasteMaterial_Main.aspx");
    }
}
