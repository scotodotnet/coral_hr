﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstDeptSpareFix_Sub.aspx.cs" Inherits="MstDeptSpareFix_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upDeptSpFixSub" runat="server">
            <ContentTemplate>
                <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Department SpareFix Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                         <div class="row">
                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Department Spare Fix Code</label>
                                    <asp:Label id="txtSpareFixCode" runat="server" class="form-control"/>
                                    <span id="DeptCode" class="text-danger"></span>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Department Spare Fix Name</label>
                                    <asp:TextBox  id="txtSpareFixName" runat="server" class="form-control" />
                                    <span id="ShortName" class="text-danger"></span>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Department Name</label>
                                    <asp:DropDownList  id="ddlDeptName" runat="server" class="form-control select2" ></asp:DropDownList>
                                    <span id="Department Name" class="text-danger"></span>
                                </div>
                            </div>

                        </div>

                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <asp:Button ID="btnClear" class="btn btn-primary" runat="server"  Text="Clear" OnClick="btnClear_Click"/>
                        <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                    </div>
                </div>
            </div>
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
</asp:Content>

