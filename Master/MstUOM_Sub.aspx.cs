﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Mst_UOM_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionUOMCode = "";
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Unit Of Measurement";

            if (Session["UOMCode"] == null)
            {
                SessionUOMCode = "";
            }
            else
            {
                SessionUOMCode = Session["UOMCode"].ToString();
                txtUOMCode.Text = SessionUOMCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
            
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string HostName = getIPAndName.GetName();

        if (btnSave.Text == "Save")
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "UOM Type Master");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Adding New UOM...');", true);
            }
        }

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from MstUOM Where UOMShortName='" + txtShortName.Text + "' And UOMCode='" + txtUOMCode.Text + "' And ";
            SSQL = SSQL + " Ccode ='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' ";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This UOM Name Already Exsit');", true);
            }
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup_Master(SessionCcode, SessionLcode, "UOM Type Master", SessionFinYearVal, "1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtUOMCode.Text = Auto_Transaction_No;
                }
            }
        }

       

        if (!ErrFlag)
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstUOM set UOMShortName='" + txtShortName.Text + "',UOMFullName='" + txtFullName.Text + "' ";
                SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And UOMCode='" + txtUOMCode.Text + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('UOM Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstUOM(Ccode,Lcode,UOMCode,UOMShortName,UOMFullName,Status,UserID,UserName,CreateOn,Host_IP,Host_Name) Values( ";
                SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + txtUOMCode.Text + "','" + txtShortName.Text + "',";
                SSQL = SSQL + " '" + txtFullName.Text + "','ADD','" + SessionUserID + "','" + SessionUserName + "',Getdate(),'"+ IP +"',";
                SSQL = SSQL + " '" + HostName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('UOM Details Saved Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtUOMCode.Enabled = true;

            Response.Redirect("MstUOM_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('UOM Details Already Saved');", true);
        }
    }
    private void Clear_All_Field()
    {
        //txtUOMCode.ReadOnly = false;
        txtUOMCode.Text = "";
        txtShortName.Text = "";
        txtFullName.Text = "";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from MstUOM where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Status <> 'Delete' And ";
        SSQL = SSQL + " UOMCode='" + txtUOMCode.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            //txtUOMCode.ReadOnly = true;
            txtShortName.Text = DT.Rows[0]["UOMShortName"].ToString();
            txtFullName.Text= DT.Rows[0]["UOMFullName"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("MstUOM_Main.aspx");
    }
}
