﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProductsMain.aspx.cs" Inherits="ProductsMin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
    <!-- Content Wrapper. Contains page content -->
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            
     <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-1">
                <asp:Button ID="btnAddNew" runat="server" CssClass="btn btn-primary" OnClick="btnAddNew_Click" Text="Add New" /><br />
                    <br />
            </div>
            <!-- /.col -->
        </div>
        <!--/.row-->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-cog text-primary"></i><span> Product Items</span></h3>
                       
                    </div>
                    <!-- /.box-header -->
                       <div class="box-body">
                    <div class="box-body no-padding">
                        <div class="table-responsive mailbox-messages">
                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                <HeaderTemplate>
                                    <table  class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Generator No</th>
                                                <th>Generator Name</th>
                                                <td>Mode</td>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tbody>
                                        <tr>
                                            <td><%# Eval("ProductName")%></td>
                                            <td runat="server" visible="false"><%# Eval("ProductNo")%></td>
                                            <td><%# Eval("GenModelNo")%></td>
                                            <td><%# Eval("GenModelName")%></td>
                                            <td>
                                                <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-sm  fa fa-pencil" runat="server"
                                                    Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("ProductNo")%>'>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnDeleteIssueEntry_Grid" class="btn btn-danger btn-sm  fa fa-trash" runat="server"
                                                    Text="" OnCommand="btnDeleteIssueEntry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("ProductNo")%>' 
                                                    CausesValidation="true" OnClientClick="retun Confirm('Are you sure want to Delete this Item ?')">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
                            </asp:Repeater>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->

                        <div class="modal modal-primary fade" id="modalDefault">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                           </div>
                    <div class="box-footer no-padding">
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
   
        </ContentTemplate>
    </asp:UpdatePanel>

         <!-- /.content -->
        </div>
</asp:Content>

