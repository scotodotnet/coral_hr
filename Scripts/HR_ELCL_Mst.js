﻿$(document).ready(function () {
    function Get_EmpMast() {
        $.ajax({
            type: "POST",
            url: "MstELCL.aspx/Load_Emp_Master",
            contentType: "application/json;charset=utf-8",
            dataType: "JSON",
            success: function (response) {
                $('#ddlEmpCode').empty();
                $('#ddlEmpCode').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $.each(response.d, function (i, value) {
                    $('#ddlEmpCode').append($('<option>').text(value.EmpName).attr('value', value.EmpCode));
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#ddlEmpCode .form-error').html(errorThrown);
                $('#ddlEmpCode .form-error').css("color", "Red");
            }
        });
    }

    function Load_Years() {
        $.ajax({
            type: "POST",
            url: "MstELCL.aspx/Load_Years",
            contentType: "application/json;charset=utf-8",
            dataType: "JSON",
            success: function (response) {
                $('#ddlYear').empty();
                $('#ddlYear').append($('<option>').text("-Select-").attr('value', '-Select-'));
                $.each(response.d, function (i, value) {
                    $('#ddlYear').append($('<option>').text(value.years).attr('value', value.years));
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#ddlYear .form-error').html(errorThrown);
                $('#ddlYear .form-error').css("color", "Red");
            }
        });
    }

    function GetEmpLeaveList() {
        $.ajax({
            type: "POST",
            url: "MstELCL.aspx/GetEmpLeaveMst",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                var tblEmpLeaveDet = $('#TblLeaveDet').DataTable();
                tblEmpLeaveDet.clear();
                $.each(response.d, function (i, value) {
                    tblEmpLeaveDet.row.add([
                        "",
                        value.EmpNo,
                        value.EmpName,
                        value.EmpDept,
                        value.Year,
                        value.FDate,
                        value.TDate,
                        value.EL,
                        value.CL,
                        value.SL
                    ]).draw(true);
                });
            }
        });
    }

    $('#ddlEmpCode').change(function (e) {
        $.ajax({
            type: "POST",
            url: "MstELCL.aspx/SelectEmp",
            data: '{EmpNo:"' + $("#ddlEmpCode").children('option:selected').val() + '"}',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                $.each(response.d, function (i, value) {
                    $('#txtEmpName').html(value.SEmpName);
                    $('#txtDept').html(value.sEmpDept);
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#txtEmpName .form-error').html(errorThrown);
                $('#txtEmpName .form-error').css("color", "Red");

                $('#txtDept .form-error').html(errorThrown);
                $('#txtDept .form-error').css("color", "Red");
            }
        });
    });

    $("#ddlYear").change(function (e) {
        var Year = $("#ddlYear").children('option:selected').text();

        $('#txtFDate').datepicker("setDate", "01/01/" + Year);
        $('#txtTDate').datepicker("setDate", "31/12/" + Year);

    });

    $("#btnSaveLeave").click(function (e) {
        var err = false;
        $(".pnlLeave .Req").each(function (index) {
            var input = $(this);
            var span = $(input).next(".form-error");

            if (input.val() == "" || input.val() == "0") {
                span.html('*This Field is Required');
                span.css('color', 'red');
                input.attr('style', 'border-color:red');
                err = true;
            }
        });

        $(".pnlLeave .ReqSel").each(function (index) {
            if ($(this).children("option:selected").text() == "-Select-") {
                err = true;
                var sn = $(this).next('p');
                sn.text("*This Field is Required");
                sn.css('color', 'red');
            }
        })

        if (!err) {
            SavaData();
        }
    });

  


    function checking() {
        
    }

    GetEmpLeaveList();
    checking();
    Get_EmpMast();
    Load_Years();
});



function SavaData() {
    var err = false;
    var EmpCode = $('#ddlEmpCode').children('option:selected').val();
    var EmpName = $('#txtEmpName').text();
    var EmpDept = $('#txtDept').text();
    var year = $('#ddlYear').children('option:selected').text();
    var FDate = $('#txtFDate').val();
    var TDate = $('#txtTDate').val();
    var EL = $('#txtEL').val();
    var CL = $('#txtCL').val();
    var SL = $('#txtSL').val();

    $.ajax({
        type: "POST",
        url: "MstELCL.aspx/SaveEmpLeaveDet",
        data: '{EmpCode:"' + EmpCode + '",EmpName:"' + EmpName + '",EmpDept:"' + EmpDept + '",year:"' + year + '",FDate:"' + FDate + '",TDate:"' + TDate + '",EL:"' + EL + '",CL:"' + CL + '",SL:"' + SL + '"}',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.d == "true") {
                alert('Details Saved Successfully');
                ClearAllData();
                if (SaveType == "2") {
                    window.close();
                }
            }
        },
        failure: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}

function ClearAllData() {
    clearMain();
}

function clearMain() {
    $('.pnlLeave .form-control').each(function (index, element) {
        if ($(element).is('select')) {
            $(element).prop('selectedIndex', 0);
            $(element).select2().trigger('change.select2');
        } else {
            $(element).val("");
            $(element).text("");
            $(element).attr('style', '');
            $(element).next('span').text();
        }
    });
}

function SaveMsgAlert(msg) {
    alert(msg);
}

var prm = Sys.WebForms.PageRequestManager.getInstance();
if (prm != null) {
    prm.add_endRequest(function (sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
            $('#example').dataTable();
        }
    });
};


