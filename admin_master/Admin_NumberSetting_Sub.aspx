﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Admin_NumberSetting_Sub.aspx.cs" Inherits="Admin_NumberSetting_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Company Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">

                            <div class="form-group col-md-3">
					            <label for="exampleInputName">Modeule Name</label>
                                <asp:DropDownList ID="ddlModule" runat="server" class="form-control select2" AutoPostBack="true" 
                                    OnSelectedIndexChanged="ddlModule_SelectedIndexChanged" >
                                </asp:DropDownList>
					        </div>

					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Form Name</label>
                                <asp:DropDownList ID="txtFormName" runat="server" class="form-control select2"
                                    AutoPostBack="True" onselectedindexchanged="txtFormName_SelectedIndexChanged">
                                </asp:DropDownList>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Prefix</label>
                                <asp:TextBox ID="txtPrefix" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtPrefix" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">NumberFrom</label>
                                <asp:TextBox ID="txtEndNo" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtEndNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtEndNo" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2" runat="server" visible="false">
					            <label for="exampleInputName">Suffix</label>
					            <asp:TextBox ID="txtSuffix" runat="server" class="form-control"></asp:TextBox>
					        </div>

					        <div class="form-group col-md-2">
					            <%--<label for="exampleInputName">StartNo</label>--%>
                                <asp:TextBox ID="txtStartNo" class="form-control" runat="server" Visible="false" Text="0"></asp:TextBox>
                               
					        </div>
					    </div>
                        
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server"  Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                            <asp:Button ID="btnClear" class="btn btn-danger" runat="server" Text="Clear" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click" />
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

