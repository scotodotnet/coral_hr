﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Company_View.aspx.cs" Inherits="Company_View" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <%--header--%>
        <section class="content-header">
            <h1><i class=" text-primary"></i>Company Master</h1>
        </section>

        <%--Body--%>
        <section class="content">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Company Code <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtCompCode" runat="server" class="form-control" ReadOnly="true" />
                                    <span id="DeptCode" class="text-danger"></span>
                                    <asp:RequiredFieldValidator ControlToValidate="txtCompCode" Display="Dynamic" ValidationGroup="Validate_Field" 
                                        class="text-danger" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" 
                                        ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Company Name <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtCompName" runat="server" class="form-control" ReadOnly="true" />
                                    <span id="ShortName" class="text-danger"></span>
                                     <asp:RequiredFieldValidator ControlToValidate="txtCompName" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Address 1 <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtAddr1" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtAddr1" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName"> Address 2 <span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtAddr2" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtAddr2" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">City<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtCity" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtCity" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">PinCode<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtPinCode" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtPinCode" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">State<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtState" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtState" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Country<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtCountry" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtCountry" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>


                         <div class="row">
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Phone Code<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtPhoneCode" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtPhoneCode" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Phone No<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtPhoneNo" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtPhoneNo" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Mobile<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtMobile" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtMobile" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">EMail<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtMail" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtMail" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            
                        </div>


                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Tin No<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtTin" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtTin" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">GstNo<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtGst" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtGst" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">PanNo<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtPan" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtPan" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Others<span class="text-danger">*</span> </label>
                                    <asp:TextBox  id="txtOthers" runat="server" class="form-control" ReadOnly="true" />
                                    
                                     <asp:RequiredFieldValidator ControlToValidate="txtOthers" Display="Dynamic" ValidationGroup="Validate_Field" 
                                         class="text-danger" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" 
                                         ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Visible="false" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                            <asp:Button ID="btnClear" class="btn btn-primary" runat="server" Visible="false"  Text="Clear" ValidationGroup="Validate_Field" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnBack" class="btn btn-primary" runat="server"  Text="Back to List" OnClick="btnBack_Click" />
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

