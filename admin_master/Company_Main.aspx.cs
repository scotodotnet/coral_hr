﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Company_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Group Of Company";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data_Enquiry_Grid();
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Goods Return");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Company..');", true);
        }
        else
        {
            //Session.Remove("Pur_RequestNo_Approval");
            //Session.Remove("Pur_Request_No_Amend_Approval");
            Session.Remove("CompCode");
            Response.Redirect("Company_Sub.aspx");
        }
        //Session.Remove("GRetNo");
        //Response.Redirect("Trans_GoodsReturn_Sub.aspx");
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Goods Return");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit Company..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Edit Company..');", true);
        }

        //DataTable dtdpurchase = new DataTable();
        //SSQL = "select Approval_Status from Trans_GoodsReturn_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And GRetNo='" + e.CommandName.ToString() + "'";
        //dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
        //string status = dtdpurchase.Rows[0]["Approval_Status"].ToString();

        if (!ErrFlag)
        {
            //if (status == "" || status == "0")
            //{
            string Enquiry_No_Str = e.CommandName.ToString();
            //Session.Remove("Pur_RequestNo_Approval");
            //Session.Remove("Pur_Request_No_Amend_Approval");
            Session.Remove("CompCode");
            Session["CompCode"] = Enquiry_No_Str;
            Response.Redirect("Company_Sub.aspx");
            //}
            //else if (status == "2")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Voucher Details Already Rejected..');", true);
            //}
            //else if (status == "3")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Voucher Details put in pending..');", true);
            //}
            //else if (status == "1")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Voucher Cant Edit..');", true);
            //}
        }
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Goods Return");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete Company..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Voucher..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        //SSQL = "Select * from Acc_Mst_AccountType where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Do not Delete  Already Approved..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete  Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable dtdpurchase = new DataTable();
            //SSQL = "select Approval_Status from Acc_Mst_AccountType where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            //SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And AccTypeCode='" + e.CommandName.ToString() + "'";
            //dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
            //string status = dtdpurchase.Rows[0]["Approval_Status"].ToString();

            //if (status == "" || status == "0")
            //{
            DataTable DT = new DataTable();
            SSQL = "Select * from MstCompany where Ccode='" + e.CommandName.ToString() + "'";
            //And Lcode='" + SessionLcode + "' And ";
            //    SSQL = SSQL + " CompCode='" + e.CommandName.ToString() + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                SSQL = "Update MstCompany Set Status='Delete' where Ccode='" + e.CommandName.ToString() + "'";
                //And Lcode='" + SessionLcode + "'";
                //    SSQL = SSQL + " And CompCode='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Deleted Successfully');", true);
                Load_Data_Enquiry_Grid();
            }
            //}
            //else if (status == "2")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Voucher Details Already Rejected..');", true);
            //}
            //else if (status == "3")
            //{
            //    ErrFlag = true;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Voucher Details put in pending..');", true);
            //}

        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select CCode,CompanyName,City,State,Country,GSTNo,Mobile,Address2 from MstCompany where Ccode = '" + SessionCcode + "' And ";
        SSQL = SSQL + " Status='' ";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    protected void GridViewEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Goods Return");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit Company..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Edit Company..');", true);
        }

        if (!ErrFlag)
        {
            //if (status == "" || status == "0")
            //{
            string Enquiry_No_Str = e.CommandName.ToString();
            //Session.Remove("Pur_RequestNo_Approval");
            //Session.Remove("Pur_Request_No_Amend_Approval");
            Session.Remove("CompCode");
            Session["CompCode"] = Enquiry_No_Str;
            Response.Redirect("Company_View.aspx");
        }
    }
}