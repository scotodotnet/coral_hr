﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class FinancialYear_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";
    string SessionFinYearCode = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Financial Year List";

            if (Session["FinancialYearCode"] == null)
            {
                SessionFinYearCode = "";
            }
            else
            {
                SessionFinYearCode = Session["FinancialYearCode"].ToString();
                txtFinCode.Text = SessionFinYearCode;
                //txtVouchNo.ReadOnly = true;
                btnSearch_Click(sender, e);
            }
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        GetIPAndName getIPAndName = new GetIPAndName();
        string IP = getIPAndName.GetIP();
        string SysName = getIPAndName.GetName();

        //if (btnSave.Text == "Save")
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}

        //duplicate Check
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from MstFinacialYear Where YearCode='" + txtFinCode.Text + "' And Ccode='" + SessionCcode + "' And ";
            SSQL = SSQL + " LCode='" + SessionLcode + "'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
            }
        }

        if (!ErrFlag)
        {

            if (btnSave.Text == "Update")
            {
                //Update Command

                SSQL = "Update MstFinacialYear set FinacialYear='" + txtFinName.Text + "',StartingPeriod='" + txtStartDate.Text + "',";
                SSQL = SSQL + " EndingPeriod='" + txtEndDate.Text + "', ActiveMode='" + rbtIsActive.SelectedValue + "' ";
                SSQL = SSQL + " Where YearCode='" + txtFinCode.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Location Details Updated Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Updated Successfully');", true);
            }
            else
            {
                //Insert Command
                SSQL = "Insert Into MstFinacialYear(Ccode,Lcode,YearCode,StartingPeriod,EndingPeriod,FinacialYear,ActiveMode,Status,IP,SystemName,";
                SSQL = SSQL + " UserID,UserName,CreateOn) Values('" + SessionCcode + "','" + SessionLcode + "','" + txtFinCode.Text + "',";
                SSQL = SSQL + " '" + txtStartDate.Text + "','" + txtEndDate.Text + "','" + txtFinName.Text + "','" + rbtIsActive.SelectedValue + "',";
                SSQL = SSQL + " '','" + IP + "','" + SysName + "','" + SessionUserID + "','" + SessionUserName + "',Getdate())";

                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Financial Year Details Saved Successfully');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
            }

            Clear_All_Field();
            btnSave.Text = "Save";
            txtFinCode.Enabled = true;

            Response.Redirect("FinancialYear_Main.aspx");

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Financial Year Details Already Saved');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Already Saved');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtFinCode.ReadOnly = false;
        txtFinCode.Text = "";txtFinName.Text = "";txtStartDate.Text = "";txtEndDate.Text = "";
        rbtIsActive.SelectedValue = "Yes";
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from MstFinacialYear where  Status <> 'Delete' And YearCode='" + txtFinCode.Text + "' and Ccode='" + SessionCcode + "' And ";
        SSQL = SSQL + " LCode='"+ SessionLcode +"' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtFinCode.ReadOnly = true;
            txtFinName.Text = DT.Rows[0]["FinacialYear"].ToString();
            txtStartDate.Text = DT.Rows[0]["StartingPeriod"].ToString();
            txtEndDate.Text = DT.Rows[0]["EndingPeriod"].ToString();
            rbtIsActive.SelectedValue = DT.Rows[0]["ActiveMode"].ToString();
           
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("FinancialYear_Main.aspx");
    }
}
