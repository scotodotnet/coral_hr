﻿using Altius.BusinessAccessLayer.BALDataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class admin_master_NewUserMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();

    string SessionCompanyName;
    string SessionUserRole;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    string SessionUserID = "";
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserID = Session["UserId"].ToString();

            Session.Remove("UserIDEdit");
            Load_Data();

        }
    }

    private void Load_Data()
    {
        SSQL = "";

        if (SessionUserID.ToString() == "Scoto")
        {
            SSQL = "Select * from UserDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status ='Approved'";
        }
        else
        {
            SSQL = "Select * from UserDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status ='Approved'";
            SSQL = SSQL + " And UserId <> 'Scoto' ";
        }
        

        Repeater1SystemAdmin.Visible = true;
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        Repeater1SystemAdmin.DataSource = dt;
        Repeater1SystemAdmin.DataBind();

        //if ((string)Session["isAdmin"].ToString()== "SuperSystemAdmin" || (string)Session["isAdmin"].ToString()== "SystemAdmin")
        //{
        //    Repeater1SystemAdmin.Visible = true;
        //    DataTable dt = new DataTable();
        //    dt = objdata.RptEmployeeMultipleDetails(SSQL);

        //    Repeater1SystemAdmin.DataSource = dt;
        //    Repeater1SystemAdmin.DataBind();
        //}
        //else if((string)Session["isAdmin"]== "SuperAdmin")
        //{
        //    Repeater1SuperAdmin.Visible = true;
        //    SSQL = SSQL + " and UserType!='SuperSystemAdmin' and UserType!='SystemAdmin'";
        //    DataTable dt = new DataTable();
        //    dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //    Repeater1SuperAdmin.DataSource = dt;
        //    Repeater1SuperAdmin.DataBind();
        //}
        //else if((string)Session["isAdmin"]=="Admin")
        //{
        //    Repeater1Admin.Visible = true;
        //    SSQL = SSQL + " and UserType!='SuperSystemAdmin' and UserType!='SystemAdmin' and UserType!='SuperAdmin'";
        //    DataTable dt = new DataTable();
        //    dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //    Repeater1Admin.DataSource = dt;
        //    Repeater1Admin.DataBind();
        //}
    }

    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {

        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "1", "New User");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Edit...');", true);
        }

        if (!ErrFlag)
        {
            Session.Remove("UserIDEdit");
            Session["UserIDEdit"] = e.CommandName.ToString();
            Response.Redirect("/admin_master/NewUserSub.aspx");
        }
    }

    protected void btnDeleteIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "1", "New Userr");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Delete...');", true);
        }

        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Update UserDetails set Status='Delete' where ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and userID='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Load_Data();
        }
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "1", "New User");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You Do Not Have Rights to Add...');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Generator Model..');", true);
        }
        else
        {
            Session.Remove("UserIDEdit");
            Response.Redirect("NewUserSub.aspx");
        }
    }
}