﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Admin_Module_MenuHead_Rights : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: User Rights";

            Module_Name_MenuName_And_UserName_Add();
            txtModuleName_SelectedIndexChanged(sender, e);
            Load_Module_Form_Details();
        }
    }

    private void Module_Name_MenuName_And_UserName_Add()
    {
        //User Name Add
        DataTable dtUser = new DataTable();
        string SSQL = "";

        if (SessionUserID == "Scoto")
        {
            SSQL = "Select * from UserDetails where CCode='" + SessionCcode + "' And LCode='" + SessionLcode + "'  ";
            SSQL = SSQL + " And Status='Approved' Order by UserID Asc";
            dtUser = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlUserName.DataSource = dtUser;
            ddlUserName.DataTextField = "UserName";
            ddlUserName.DataValueField = "UserId";
            ddlUserName.DataBind();
        }
        else
        {
            SSQL = "Select * from UserDetails where CCode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And  UserID <> 'Scoto' ";
            SSQL = SSQL + " And Status='Approved' Order by UserID Asc";
            dtUser = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlUserName.DataSource = dtUser;
            ddlUserName.DataTextField = "UserName";
            ddlUserName.DataValueField = "UserId";
            ddlUserName.DataBind();
        }
        //Module Name Add
        DataTable dtcate = new DataTable();
        SSQL = "Select * from Admin_Company_Module_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And ModuleName='HR' Order by ModuleName Asc";
        dtcate = objdata.RptEmployeeMultipleDetails(SSQL);
        txtModuleName.DataSource = dtcate;
        txtModuleName.DataTextField = "ModuleName";
        txtModuleName.DataValueField = "ModuleID";
        txtModuleName.DataBind();

        ////Menu Name Add
        //DataTable dtMenu = new DataTable();
        //query = "Select * from [" + SessionRights + "]..Company_Module_MenuHead_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' order by MenuName Asc";
        //dtMenu = objdata.RptEmployeeMultipleDetails(query);
        //txtMenuName.DataSource = dtMenu;
        //txtMenuName.DataTextField = "MenuName";
        //txtMenuName.DataValueField = "MenuID";
        //txtMenuName.DataBind();
    }
    private void Module_Name_And_MenuName_Add()
    {
        //Module Name Add
        DataTable dtcate = new DataTable();
        string SSQL = "";
        SSQL = "Select * from Admin_Company_Module_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " Order by ModuleName Asc";

        dtcate = objdata.RptEmployeeMultipleDetails(SSQL);
        txtModuleName.DataSource = dtcate;
        txtModuleName.DataTextField = "ModuleName";
        txtModuleName.DataValueField = "ModuleID";
        txtModuleName.DataBind();
    }


    private void Load_Module_Form_Details()
    {
        string SSQL = "";
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;

        string Module_ID = "";
        //Module_ID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();

        Module_ID = txtModuleName.SelectedValue.ToString();

        if (Module_ID == "") { Module_ID = "0"; }

        string Menu_ID = "";
        //Menu_ID = Decrypt(txtMenuName.SelectedValue.ToString()).ToString();
        Menu_ID = ddlMenuHead.SelectedValue.ToString();

        if (Menu_ID == "") { Menu_ID = "0"; }

        DataTable DT = new DataTable();
        SSQL = "Select FormID,FormName from Admin_Company_Module_MenuForm_Rights where ModuleID='" + Module_ID + "' And MenuID='" + Menu_ID + "'";
        SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' order by FormName Asc";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        GVModule.DataSource = DT;
        GVModule.DataBind();

        if (GVModule.Rows.Count != 0)
        {
            GVPanel.Visible = true;
        }
        else
        {
            GVPanel.Visible = false;
        }
        Load_Module_Form_Rights();

    }

    private void Load_Module_Form_Rights()
    {
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            string ModuleName = txtModuleName.SelectedItem.ToString();
            //string ModuleID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();
            string ModuleID = txtModuleName.SelectedValue.ToString();
            string ModuleID_Encrypt = txtModuleName.SelectedValue.ToString();

            string MenuName = ddlMenuHead.SelectedItem.ToString();
            //string MenuID = Decrypt(txtMenuName.SelectedValue.ToString()).ToString();
            string MenuID = ddlMenuHead.SelectedValue.ToString();
            string MenuID_Encrypt = ddlMenuHead.SelectedValue.ToString();

            string UserName = ddlUserName.SelectedItem.Text.ToString();

            string FormName = "";
            string FormID = "0";
            string FormID_Encrypt = "0";

            Label FormID_lbl = (Label)gvsal.FindControl("FormID");
            FormID = FormID_lbl.Text.ToString();
            FormName = gvsal.Cells[1].Text.ToString();
            //FormID_Encrypt = Encrypt(FormID).ToString();
            FormID_Encrypt = FormID.ToString();
            //Get Company Module Rights
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from Admin_User_Rights where ModuleID='" + ModuleID_Encrypt + "' And MenuID='" + MenuID_Encrypt + "' ";
            query = query + " And FormID='" + FormID_Encrypt + "' And UserName='" + UserName + "' ";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                //User Rights Update in Grid
                if (DT.Rows[0]["AddRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true; }
                if (DT.Rows[0]["ModifyRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkModify")).Checked = true; }
                if (DT.Rows[0]["DeleteRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkDelete")).Checked = true; }
                if (DT.Rows[0]["ViewRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkView")).Checked = true; }
                if (DT.Rows[0]["ApproveRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkApprove")).Checked = true; }
                if (DT.Rows[0]["PrintRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkPrintout")).Checked = true; }
            }
            else
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkModify")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkDelete")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkView")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkApprove")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkPrintout")).Checked = false;
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";

        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "";
        bool ErrFlag = false;

        string Module_ID_Encrypt = "0";
        string Module_ID = "0";
        string Module_Name = "0";

        string Menu_ID_Encrypt = "0";
        string Menu_ID = "0";
        string Menu_Name = "0";

        string UserName = "";

        //Module_ID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();

        Module_ID = txtModuleName.SelectedValue.ToString();

        Module_Name = txtModuleName.SelectedItem.ToString();
        Module_ID_Encrypt = txtModuleName.SelectedValue.ToString();

        //Menu_ID = Decrypt(txtMenuName.SelectedValue.ToString()).ToString();
        Menu_ID = ddlMenuHead.SelectedValue.ToString();

        Menu_Name = ddlMenuHead.SelectedItem.ToString();
        Menu_ID_Encrypt = ddlMenuHead.SelectedValue.ToString();

        UserName = ddlUserName.SelectedValue.ToString();

        SSQL = "Delete from Admin_User_Rights where ModuleID='" + Module_ID_Encrypt + "' And MenuID='" + Menu_ID_Encrypt + "' And UserName='" + UserName + "'";
        SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            SaveMode = "Insert";


            string Form_ID_Encrypt = "0";
            string Form_ID = "0";
            string Form_Name = "0";

            CheckBox ChkSelect_chk = (CheckBox)gvsal.FindControl("chkSelect");
            string AddCheck = "0";
            string ModifyCheck = "0";
            string DeleteCheck = "0";
            string ViewCheck = "0";
            string ApproveCheck = "0";
            string PrintCheck = "0";


            CheckBox ChkModify = (CheckBox)gvsal.FindControl("chkModify");
            CheckBox ChkDelete = (CheckBox)gvsal.FindControl("chkDelete");
            CheckBox ChkView = (CheckBox)gvsal.FindControl("chkView");
            CheckBox ChkApprove = (CheckBox)gvsal.FindControl("chkApprove");
            CheckBox ChkPrint = (CheckBox)gvsal.FindControl("chkPrintout");

            if (ChkSelect_chk.Checked == true) { AddCheck = "1"; }
            if (ChkModify.Checked == true) { ModifyCheck = "1"; }
            if (ChkDelete.Checked == true) { DeleteCheck = "1"; }
            if (ChkView.Checked == true) { ViewCheck = "1"; }
            if (ChkApprove.Checked == true) { ApproveCheck = "1"; }
            if (ChkPrint.Checked == true) { PrintCheck = "1"; }

            Label FormID_lbl = (Label)gvsal.FindControl("FormID");
            Form_ID = FormID_lbl.Text.ToString();
            Form_Name = gvsal.Cells[1].Text.ToString();
            Form_ID_Encrypt = Form_ID.ToString();

            if (ChkSelect_chk.Checked == true || ChkModify.Checked == true || ChkDelete.Checked == true || ChkView.Checked == true || ChkApprove.Checked == true || ChkPrint.Checked == true)
            {
                //Get Menu LIST
                string Menu_LI_ID = "";
                string Form_LI_ID = "";
                DataTable dtID = new DataTable();
                SSQL = "Select * from Admin_Company_Module_MenuHead_Rights where ModuleID='" + Module_ID_Encrypt + "' And ";
                SSQL = SSQL + " MenuID='" + Menu_ID_Encrypt + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                dtID = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtID.Rows.Count != 0)
                {
                    Menu_LI_ID = dtID.Rows[0]["Menu_LI_ID"].ToString();
                }
                //Get Form LIST ID
                SSQL = "Select * from Admin_Company_Module_MenuForm_Rights where ModuleID='" + Module_ID_Encrypt + "' And ";
                SSQL = SSQL + " MenuID ='" + Menu_ID_Encrypt + "' And FormID='" + Form_ID_Encrypt + "'";
                SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                dtID = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtID.Rows.Count != 0)
                {
                    Form_LI_ID = dtID.Rows[0]["Form_LI_ID"].ToString();
                }

                //Insert User Rights
                SSQL = "Insert Into Admin_User_Rights(CompCode,LocCode,ModuleID,ModuleName,MenuID,MenuName,Menu_LI_ID,FormID,FormName,";
                SSQL = SSQL + " Form_LI_ID,UserName,AddRights,ModifyRights,DeleteRights,ViewRights,ApproveRights,PrintRights) Values(";
                SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + Module_ID_Encrypt + "','" + Module_Name + "',";
                SSQL = SSQL + " '" + Menu_ID_Encrypt + "','" + Menu_Name + "','" + Menu_LI_ID + "','" + Form_ID_Encrypt + "',";
                SSQL = SSQL + " '" + Form_Name + "','" + Form_LI_ID + "','" + ddlUserName.SelectedItem.Text.ToString() + "',";
                SSQL = SSQL + " '" + AddCheck + "','" + ModifyCheck + "','" + DeleteCheck + "','" + ViewCheck + "',";
                SSQL = SSQL + " '" + ApproveCheck + "','" + PrintCheck + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Module User Access Rights Details Saved Successfully');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Module User Access Rights Details Not Saved Properly...');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Load_Module_Form_Details();
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        Load_Module_Form_Details();
    }

    protected void txtModuleName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable dtMenu = new DataTable();
        if (txtModuleName.SelectedValue != "")
        {
            SSQL = "Select* from Admin_Company_Module_MenuHead_Rights where CompCode = '" + SessionCcode + "' And LocCode = '" + SessionLcode + "'";
            SSQL = SSQL + " And ModuleID = '" + txtModuleName.SelectedValue + "' order by MenuName Asc";

            dtMenu = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlMenuHead.DataSource = dtMenu;
            ddlMenuHead.DataTextField = "MenuName";
            ddlMenuHead.DataValueField = "MenuID";
            ddlMenuHead.DataBind();
            btnView_Click(sender, e);
        }
    }
    protected void ddlMenuHead_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMenuHead.SelectedValue != "")
        {
            btnView_Click(sender, e);
        }
    }

    protected void ddlUserName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Module_Form_Details();
    }
    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            //User Rights Update in Grid
            if (chkAll.Checked == true)
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;
                ((CheckBox)gvsal.FindControl("chkModify")).Checked = true;
                ((CheckBox)gvsal.FindControl("chkDelete")).Checked = true;
                ((CheckBox)gvsal.FindControl("chkView")).Checked = true;
                ((CheckBox)gvsal.FindControl("chkApprove")).Checked = true;
                ((CheckBox)gvsal.FindControl("chkPrintout")).Checked = true;
            }
            else
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkModify")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkDelete")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkView")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkApprove")).Checked = false;
                ((CheckBox)gvsal.FindControl("chkPrintout")).Checked = false;
            }
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Response.Redirect();
    }
}