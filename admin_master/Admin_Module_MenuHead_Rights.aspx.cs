﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Admin_Module_MenuHead_Rights : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Module MenuHead List";
            Module_Name_Add();
            Load_Module_MenuHead_Details();
        }
        
    }

    private void Module_Name_Add()
    {
        DataTable dtcate = new DataTable();
        string SSQL = "";
        SSQL = "Select * from Admin_Company_Module_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + "Order by ModuleName Asc";

        dtcate = objdata.RptEmployeeMultipleDetails(SSQL);

        txtModuleName.DataSource = dtcate;
        txtModuleName.DataTextField = "ModuleName";
        txtModuleName.DataValueField = "ModuleID";
        txtModuleName.DataBind();
    }


    private void Load_Module_MenuHead_Details()
    {
        string query = "";
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;

        string Module_ID = "";
        //Module_ID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();

        Module_ID = txtModuleName.SelectedValue.ToString();

        DataTable DT = new DataTable();
        query = "Select MenuID,MenuName from Admin_Module_MenuHead_List where ModuleID='" + Module_ID + "' order by MenuID Asc";

        DT = objdata.RptEmployeeMultipleDetails(query);
        GVModule.DataSource = DT;
        GVModule.DataBind();

        if (GVModule.Rows.Count != 0)
        {
            GVPanel.Visible = true;
        }
        else
        {
            GVPanel.Visible = false;
        }
        Load_Module_MenuHead_Rights();

    }

    private void Load_Module_MenuHead_Rights()
    {

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            string ModuleName = txtModuleName.SelectedItem.ToString();
            //string ModuleID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();
            string ModuleID = txtModuleName.SelectedValue.ToString();
            string ModuleID_Encrypt = txtModuleName.SelectedValue.ToString();

            string MenuName = "";
            string MenuID = "0";
            string MenuID_Encrypt = "0";

            Label MenuID_lbl = (Label)gvsal.FindControl("MenuID");
            MenuID = MenuID_lbl.Text.ToString();
            MenuName = gvsal.Cells[1].Text.ToString();
            //MenuID_Encrypt = Encrypt(MenuID).ToString();
            MenuID_Encrypt = MenuID.ToString();
            //Get Company Module Rights
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from Admin_Company_Module_MenuHead_Rights where ModuleID='" + ModuleID_Encrypt + "' And MenuID='" + MenuID_Encrypt + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //User Rights Update in Grid
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "";
        bool ErrFlag = false;
        string Module_ID_Encrypt = "0";
        string Module_ID = "0";
        string Module_Name = "0";

        //Module_ID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();

        Module_ID = txtModuleName.SelectedValue.ToString();

        Module_Name = txtModuleName.SelectedItem.ToString();
        Module_ID_Encrypt = txtModuleName.SelectedValue.ToString();

        SSQL = "Delete from Admin_Company_Module_MenuHead_Rights where ModuleID='" + Module_ID_Encrypt + "'";
        SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            SaveMode = "Insert";

            string Menu_ID_Encrypt = "0";
            string Menu_ID = "0";
            string Menu_Name = "0";

            //CheckBox ChkSelect_chk = (CheckBox)gvsal.FindControl("chkSelect");

            CheckBox chkSelectMenu = (CheckBox)gvsal.FindControl("chkSelect");

            Label MenuID_lbl = (Label)gvsal.FindControl("MenuID");
            Menu_ID = MenuID_lbl.Text.ToString();
            Menu_Name = gvsal.Cells[1].Text.ToString();
            //Menu_ID_Encrypt = Encrypt(Menu_ID).ToString();
            Menu_ID_Encrypt = Menu_ID.ToString();

            if (chkSelectMenu != null &&  chkSelectMenu.Checked)
            {
                //Get Menu LI ID
                string Menu_LI_ID = "";
                DataTable dtID = new DataTable();
                SSQL = "Select * from Admin_Module_MenuHead_List where ModuleID='" + Module_ID + "' And MenuID='" + Menu_ID + "'";
                dtID = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtID.Rows.Count != 0)
                {
                    Menu_LI_ID = dtID.Rows[0]["Menu_LI_ID"].ToString();
                }

                //Insert User Rights
                SSQL = "Insert Into Admin_Company_Module_MenuHead_Rights(CompCode,LocCode,ModuleID,ModuleName,MenuID,MenuName,Menu_LI_ID) Values(";
                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + Module_ID_Encrypt + "','" + Module_Name + "',";
                SSQL = SSQL + " '" + Menu_ID_Encrypt + "','" + Menu_Name + "','" + Menu_LI_ID + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Module Access Rights Details Saved Successfully');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Module Access Rights Details Not Saved Properly...');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        Load_Module_MenuHead_Details();
    }

    protected void txtModuleName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        string Module_ID = "";
        //Module_ID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();

        //Module_ID = txtModuleName.SelectedValue.ToString();


        GVModule.DataSource = DT;

        SSQL = "Select MenuID,MenuName from Admin_Module_MenuHead_List where ModuleID='" + txtModuleName.SelectedValue + "'";
        SSQL = SSQL + " order by MenuID Asc";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        GVModule.DataSource = DT;
        GVModule.DataBind();

        //if (GVModule.Rows.Count != 0)
        //{
        //    GVPanel.Visible = true;
        //}
        //else
        //{
        //    GVPanel.Visible = false;
        //}

        Load_Module_MenuHead_Rights();

    }
}