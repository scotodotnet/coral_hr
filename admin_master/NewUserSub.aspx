﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NewUserSub.aspx.cs" Inherits="admin_master_NewUserSub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-user-plus text-primary"></i> Add New User
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-9">
                    <!-- Default box -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">User Role Type</label>
                                        <asp:DropDownList ID="ddlUserRole" CssClass="form-control select2" runat="server">
                                            <asp:ListItem Selected="True" Value="-Select-" Text="-Select-"></asp:ListItem>
                                            <asp:ListItem Value="SuperSystemAdmin" Text="SuperSystemAdmin"></asp:ListItem>
                                            <asp:ListItem Value="SystemAdmin" Text="SystemAdmin"></asp:ListItem>
                                            <asp:ListItem Value="End User" Text="End User"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">User Name</label>
                                        <asp:TextBox ID="txtNewUserName" class="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Designation</label>
                                        <asp:DropDownList ID="txtDesignation" class="form-control select2" runat="server">

                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">User ID</label>
                                        <asp:TextBox ID="txtNewUserID" class="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Password</label>
                                        <asp:TextBox ID="txtNewPassword" class="form-control" TextMode="Password" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Confirm Password</label>
                                        <asp:TextBox ID="txtConfirmPassword" class="form-control" TextMode="Password" runat="server"></asp:TextBox>
                                        <asp:CompareValidator ID="CompareValidator1" ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmPassword" ValueToCompare="txtConfirmPassword" CssClass="text-danger" runat="server" ErrorMessage="Password Does not Matched">
                                        </asp:CompareValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Active Status</label>
                                        <asp:RadioButtonList ID="rbtIsActive" runat="server" CssClass="form-control" RepeatColumns="2">
                                            <asp:ListItem Selected="True" Value="Yes" style="padding:20px" Text="Yes"></asp:ListItem>
                                            <asp:ListItem Value="No" Text="No"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="form-group">
                                <asp:Button ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click" runat="server" Text="Save" />
                                <asp:Button ID="btnClear" class="btn btn-danger" runat="server" OnClick="btnClear_Click" Text="Clear" />
                                <asp:Button ID="btnBack" class="btn btn-default" runat="server" OnClick="btnBack_Click" Text="Back to List" />
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                    <!-- /.box -->
                </div>

                <div class="col-md-3">
                    <!-- Default box -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" align="center">
                                        <label class="control-label">Profile Photo</label>
                                        <input type="file" class="upload" onchange='sendFile(this);' id="f_UploadImage" style="visibility: hidden">
                                        <img src="../assets/images/empty_profile.png" id="myUploadedImg" alt="Photo" class="img-circle profilePicture" onclick="clickup();" style="width: 50%;" /><br />
                                        <progress id="fileProgress" style="display: none"></progress>
                                        <asp:HiddenField ID="hidd_imgPath" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="form-group">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3" hidden>
                    <div class="callout callout-info" style="margin-bottom: 0!important;">
                        <h4><i class="fa fa-info"></i>Info:</h4>
                        <p>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
         <!-- jQuery 3 -->
    <script src="/assets/adminlte/components/jquery/dist/jquery.min.js"></script>
    <!--jqueryUI-->
    <script src="/assets/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
        <script type="text/javascript">
            function progressHandlingFunction(e) {
                if (e.lengthComputable) {
                    var percentage = Math.floor((e.loaded / e.total) * 100);
                    //update progressbar percent complete
                    $('#fileProgress').html(percentage + '%');
                    console.log("Value = " + e.loaded + " :: Max =" + e.total);
                }
            }

            $('[id*=txtNewUserID]').on("change", function () {
                //alert($(this).val());
                $.ajax({
                    type: "POST",
                    url: "NewUserSub.aspx/Called",
                    data: '{id: "' + $(this).val() + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                      //  console.log(response);
                        if (response.d == "true") {
                            ShowMessageError('The User ID Already Taken');
                            $(this).focus();
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Whoops something went wrong!" + errorThrown);
                    }
                });
            });
           
            function clickup() {
                $('#f_UploadImage').click();
            }
            function sendFile(file) {

                var formData = new FormData();
                formData.append('file', $('#f_UploadImage')[0].files[0]);
               // alert(formData);
                formData.append('filename', $('#<%=txtNewUserID.ClientID%>').val());
                formData.append("type", "PhotoProfile");
                $.ajax({
                    type: 'post',
                    url: '/Handler.ashx',
                    data: formData,
                    xhr: function () {  // Custom XMLHttpRequest
                        var myXhr = $.ajaxSettings.xhr();
                        if (myXhr.upload) { // Check if upload property exists
                            // update progressbar percent complete
                            // $('#fileProgress').html('0%');
                            $("progress").show();
                            // For handling the progress of the upload
                            // myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                            myXhr.upload.addEventListener("progress", function (e) {
                                if (e.lengthComputable) {
                                    $("#fileProgress").attr({
                                        value: e.loaded,
                                        max: e.total
                                    });
                                }
                            }, false);
                        }
                        return myXhr;
                    },
                    success: function (status) {
                        if (status != 'error') {
                            $("#fileProgress").hide();
                            var my_path = "/assets/uploads/CORAL/Photo/Profile/" + status;
                            $('#<%=hidd_imgPath.ClientID%>').val(my_path);
                            $("#myUploadedImg").attr("src", my_path);
                        }
                    },
                    processData: false,
                    contentType: false,
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Whoops something went wrong!" + errorThrown);
                    }
                });
            }
        </script>
    </div>
</asp:Content>

