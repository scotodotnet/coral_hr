﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Admin_Module_MenuForm_Rights.aspx.cs" Inherits="Admin_Module_MenuHead_Rights" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Module Form Settings</span></h3>
                            <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="table-responsive mailbox-messages">
                                <div class="col-md-12">
					                <div class="row">
                                        <div class="col-sd-1"></div>
                                        <div class="col-md-12" style="padding-top:25px" runat="server">
				                            <div class="row">
                                                <div class="form-group col-md-3">
				                                    <label for="exampleInputName">Module Name<span class="mandatory">*</span></label>
				                                    <asp:DropDownList ID="txtModuleName" runat="server" class="form-control select2"
				                                        AutoPostBack="true" OnSelectedIndexChanged="txtModuleName_SelectedIndexChanged" >
                                                    </asp:DropDownList>
			                                    </div>

                                                <div class="form-group col-md-3">
				                                    <label for="exampleInputName">MenuHead Name<span class="mandatory">*</span></label>
				                                    <asp:DropDownList ID="ddlMenuHead" runat="server" class="form-control select2"
				                                        AutoPostBack="true" OnSelectedIndexChanged="ddlMenuHead_SelectedIndexChanged" >
                                                    </asp:DropDownList>
			                                    </div>

					                            <div class="form-group" style="padding-top:25px">
					                                <asp:Button ID="btnView" class="btn btn-primary"  runat="server" Text="View" ValidationGroup="Item_Validate_Field"  OnClick="btnView_Click"/>
					                            </div>
				                            </div>

				                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <asp:Panel ID="GVPanel" runat="server" ScrollBars="None" Visible="true">
				                                        <asp:GridView id="GVModule" runat="server" AutoGenerateColumns="false" 
				                                       class="gvv display table">
				                                            <Columns>
				                                                <asp:TemplateField  HeaderText="FormID" Visible="false">
				                                                    <ItemTemplate>
				                                                        <asp:Label id="FormID" runat="server" Text='<%# Eval("FormID") %>'/>
				                                                    </ItemTemplate>
				                                                </asp:TemplateField>
				                                                <asp:BoundField DataField="FormName" HeaderText="Form Name" />
				                                                <asp:TemplateField  HeaderText="Select">
				                                                    <ItemTemplate>
				                                                        <asp:CheckBox id="chkSelect" runat="server"/>
				                                                    </ItemTemplate>
				                                                </asp:TemplateField>
				                                            </Columns>
				                                        </asp:GridView>
				                                    </asp:Panel>
                                                </div>
					                        </div>
					                    </div>
                                    </div>
					            </div>
                                <div class="box-footer">
                                    <div class="form-group">
                                        <asp:Button ID="btnSave" class="btn btn-primary"  runat="server" Text="Save" ValidationGroup="Validate_Field" 
                                            OnClick="btnSave_Click"/>
                                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                    </div>
                                </div>
					        </div>
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </section>
    </div>
</asp:Content>

