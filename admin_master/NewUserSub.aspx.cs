﻿using System;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class admin_master_NewUserSub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SSQL = "";
    bool ErrFlg = false;
    string SessionCcode = "";
    string SessionLcode = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("../Default.aspx");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            if (!IsPostBack)
            {
                Load_Designation();
                if ((string)Session["UserIDEdit"] != null)
                {
                    Search(Session["UserIDEdit"].ToString());
                }
            }
        }
    }

    private void Load_Designation()
    {
        SSQL = "";
        SSQL = "Select DesigCode,DesigName from MstDesignation where Ccode='" + SessionCcode + "' and ";
        SSQL = SSQL + " Lcode ='" + SessionLcode + "' and Status='Add'";
        DataTable DT = new DataTable();
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtDesignation.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["DesigCode"] = "-Select-";
        dr["DesigName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        txtDesignation.DataTextField = "DesigName";
        txtDesignation.DataValueField = "DesigCode";
        txtDesignation.DataBind();
    }
    private void Search(string id)
    {
        SSQL = "";
        SSQL = "Select * from UserDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and UserID='" + id + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtNewUserID.Enabled = false;
            ddlUserRole.SelectedValue = dt.Rows[0]["UserType"].ToString();
            txtNewUserID.Text = dt.Rows[0]["UserID"].ToString();
            txtNewUserName.Text = dt.Rows[0]["UserName"].ToString();
            txtDesignation.SelectedItem.Text = dt.Rows[0]["Designation"].ToString();
            txtDesignation.SelectedValue = dt.Rows[0]["DesigCode"].ToString();
            hidd_imgPath.Value= dt.Rows[0]["profileImg"].ToString();

            btnSave.Text = "Update";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtNewUserID.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the User ID')", true); ;
            ErrFlg = true;
        }

        if (txtDesignation.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Designation')", true); ;
            ErrFlg = true;
        }

        if (txtNewUserName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the User Name')", true); ;
            ErrFlg = true;
        }
        if (txtNewPassword.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Password')", true); ;
            ErrFlg = true;
        }
        if (txtConfirmPassword.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Confirm Password')", true); ;
            ErrFlg = true;
        }
        if (ddlUserRole.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Choose the User Role')", true); ;
            ErrFlg = true;
        }
        if (!ErrFlg)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete from UserDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and UserID='" + txtNewUserID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from UserDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and UserID='" + txtNewUserID.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The User ID Already Present')", true);
                    ErrFlg = true;
                    return;
                }
            }
            if (!ErrFlg)
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();
                string imgpath ="/assets/images/empty_profile.png";

                if (hidd_imgPath.Value != imgpath)
                {
                    imgpath = hidd_imgPath.Value.ToString();
                }

                SSQL = "";
                SSQL = "Insert into UserDetails(Ccode,Lcode,UserID,UserName,Password,UserType,Designation,isActive,profileImg,";
                SSQL = SSQL + " CreatedOn,Host_IP,Host_Name,CreatedBy,Status,DesigCode) Values ";
                SSQL = SSQL + " ('" + SessionCcode + "','" + SessionLcode + "','" + txtNewUserID.Text + "','" + txtNewUserName.Text + "',";
                SSQL = SSQL + " '" + UTF8Encryption(txtNewPassword.Text) + "','" + ddlUserRole.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + txtDesignation.SelectedItem.Text + "','" + rbtIsActive.SelectedValue + "','" + imgpath + "',";
                SSQL = SSQL + " '" + Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "',";
                SSQL = SSQL + " '" + _HostName + "','" + Session["UserID"] + "','Approved','" + txtDesignation.SelectedValue + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Insert into Login(Ccode,Lcode,UserId,Password,isLoggedIN)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtNewUserID.Text + "','" + UTF8Encryption(txtNewPassword.Text) + "','false')";
                objdata.RptEmployeeMultipleDetails(SSQL);


                btnClear_Click(sender, e);

                Session.Remove("UserIDEdit");
                Response.Redirect("NewUserMain.aspx");
            }
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtNewPassword.Text = "";
        txtConfirmPassword.Text = "";
        txtDesignation.SelectedItem.Text = "-Select-";
        ddlUserRole.ClearSelection();
        //rbtIsActive.ClearSelection();
        rbtIsActive.SelectedValue = "Yes";
        txtNewUserID.Text = "";
        txtNewUserName.Text = "";

        btnSave.Text = "Save";
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("UserIDEdit");
        Response.Redirect("NewUserMain.aspx");
    }
    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    [WebMethod]
    public static string Called(string id)
    {
        BALDataAccess objdata = new BALDataAccess();
        string SSQL = "";
        bool Status = false;
        SSQL = "Select * from UserDetails where UserID='" + id + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            Status = true;
        }
        string myJsonString = (new JavaScriptSerializer()).Serialize(Status);
        return myJsonString;
    }
}