﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NewUserMain.aspx.cs" Inherits="admin_master_NewUserMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">
    function SaveMsgAlert(msg) 
    {
        alert(msg);
    }
</script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <asp:UpdatePanel ID="upNewUser" runat="server">
            <ContentTemplate>
                <section class="content">
            <div class="row">
                <div class="col-md-1">
                   <asp:Button ID="btnAddNew" runat="server" CssClass="btn btn-primary"  Text="Add New" OnClick="btnAddNew_Click" />
                    <br />
                            <br />
                </div>
                <!-- /.col -->
            </div>
            <!--/.row-->
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i><span> Users</span></h3>
                            <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">

                            <div class="table-responsive mailbox-messages">
                                <asp:Repeater ID="Repeater1SystemAdmin" Visible="false" runat="server" EnableViewState="false">
                                    <HeaderTemplate>
                                        <table id="example" class="table table-responsive table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>User Name</th>
                                                    <th>User ID</th>
                                                    <th>Designation</th>
                                                    <th>User Role</th>
                                                    <th runat="server" visible="false">Password</th>
                                                    <th>Profile Photo</th>
                                                    <th>Edit</th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody> 
                                            <tr>
                                                <td><%# Eval("UserName")%></td>
                                                <td><%# Eval("UserID ")%></td>
                                                <td><%# Eval("Designation")%></td>
                                                <td><%# Eval("UserType")%></td>
                                                <td runat="server" visible="false"><%# Eval("Password")%></td>
                                                <td runat="server" style="width:150px"><%--<asp:Image runat="server" ID="UserProfile" ImageUrl='<%# Eval("profileImg")%>' class="user-image profilePicture" AlternateText="User Image" /> --%>
                                                    <img src='<%# Eval("profileImg")%>' id="myUploadedImg" alt="Photo" class="img-circle profilePicture" onclick="return ImgShow(this);" style="width: 15%;" />
                                                </td>

                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                        Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("UserID")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteIssueEntry_Grid" class="btn btn-danger btn-sm fa fa-trash" runat="server"
                                                        Text="" OnCommand="btnDeleteIssueEntry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("UserID")%>' OnClientClick="retun Confirm('Are you sure want to Delete this Item ?')">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>
                                </asp:Repeater>
                                <!-- /.table -->

                                <asp:Repeater ID="Repeater1SuperAdmin" Visible="false" runat="server" EnableViewState="false">
                                    <HeaderTemplate>
                                        <table id="example" class="table table-responsive table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>User Name</th>
                                                    <th>User ID</th>
                                                    <th>Designation</th>
                                                    <td>
                                                    User Role</th>
                                                <td>
                                                    Password</th>
                                                <td>
                                                    Profile Photo</th>
                                                <th>Edit</th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr>
                                                <td><%# Eval("UserName")%></td>
                                                <td><%# Eval("UserID ")%></td>
                                                <td><%# Eval("Designation")%></td>
                                                <td><%# Eval("UserType")%></td>
                                                <td><%# Eval("Password")%></td>
                                                <td>
                                                    <img src='<%# Eval("profileImg")%>' id="myUploadedImg" alt="Photo" class="img-circle profilePicture" onclick="return ImgShow(this);" style="width: 15%;" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-xs fa fa-pencil" runat="server"
                                                        Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("UserID")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteIssueEntry_Grid" class="btn btn-danger btn-xs fa fa-trash" runat="server"
                                                        Text="" OnCommand="btnDeleteIssueEntry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("UserID")%>' OnClientClick="retun Confirm('Are you sure want to Delete this Item ?')">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>
                                </asp:Repeater>
                                <!-- /.table -->

                                <asp:Repeater ID="Repeater1Admin" Visible="false" runat="server" EnableViewState="false">
                                    <HeaderTemplate>
                                        <table id="example" class="table table-responsive table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>User Name</th>
                                                    <th>User ID</th>
                                                    <th>Designation</th>
                                                    <th>User Role</th>
                                                    <th>Password</th>
                                                    <th>Profile Photo</th>
                                                    <th>Edit</th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr>
                                                <td><%# Eval("UserName")%></td>
                                                <td><%# Eval("UserID ")%></td>
                                                <td><%# Eval("Designation")%></td>
                                                <td><%# Eval("UserType")%></td>
                                                <td><%# Eval("Password")%></td>
                                                <td>
                                                    <img src='<%# Eval("profileImg")%>' id="myUploadedImg" alt="Photo" class="img-circle profilePicture" onclick="return ImgShow(this);" style="width: 15%;" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-xs fa fa-pencil" runat="server"
                                                        Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("UserID")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteIssueEntry_Grid" class="btn btn-danger btn-xs fa fa-trash" runat="server"
                                                        Text="" OnCommand="btnDeleteIssueEntry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("UserID")%>' OnClientClick="retun Confirm('Are you sure want to Delete this Item ?')">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>
                                </asp:Repeater>
                                <!-- /.table -->
                            </div>
                            <!-- /.mail-box-messages -->

                            <div class="modal modal-primary fade" id="modalDefault">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer no-padding">
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
                 <!-- /.content -->
        <!-- Creates the bootstrap modal where the image will appear -->
        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body" align="center">
                        <img src="" id="imagepreview" style="width: 450px; height: 280px;">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        
       
        <script type="text/javascript">
            //Image Click PopUp
            function ImgShow(src) {
                $('#imagepreview').attr('src', src.src); // here asign the image to the modal when the user click the enlarge link
                //var Val = src.getAttribute('data-url').trim();
                //var SpltVal = Val.split("/");
                //$('#myModalLabel').html(SpltVal[5].split(".")[0]);
                //$('#myModalLabel').attr('style', 'font-weight:bold');
                $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
            }
        </script>
    </div>
</asp:Content>

