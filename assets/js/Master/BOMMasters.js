﻿$(document).ready(function () {
    //$("#mat_no").change(function () {
    //    if ($("#mat_no").val() != "") {
    //        $.ajax({
    //            type: "GET",
    //            url: "/BOMMasters/MaterialNo_DuplicateCheck?mat_no=" + $("#mat_no").val() + "",
    //            contentType: "application/json; charset=utf-8",
    //            success: function (response) {
    //                if (response == false) {

    //                } else {
    //                    $("#Mat_noError").append("The Material Number Already Taken");
    //                    $("#mat_no").focus();
    //                }
    //            },
    //            error: function (ex) {
    //                alert('Failed.' + ex);
    //            }
    //        });
    //    }
    //});
    $('[id*=txtMatNo]').on("change", function () {
        //alert($(this).val());
        $.ajax({
            type: "POST",
            url: "BOMMasterSub.aspx/MatnoCheck",
            data: '{id: "' + $(this).val() + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //  console.log(response);
                if (response.d == "true") {
                    ShowMessageError('The Material Number Already Taken');
                    $(this).focus();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Whoops something went wrong!" + errorThrown);
            }
        });
    });


    $("#DeptID").change(function () {
        if ($("#DeptID").val() != "") {
            $('#DeptCode').val($(this).val());
            //   alert($(this).val());
            $('#DeptName').val($("#DeptID option:selected").text());
        }
    });

    $("#UOMID").change(function () {
        if ($("#UOMID").val() != "") {
            $('#UOMTypeCode').val($(this).val());
            $('#UOM_Full').val($("#UOMID option:selected").text())
        }
    });

    $('#Dept_Code').keyup(function () {
        $('#Dept_Name option').map(function () {
            if ($(this).text == $('#Dept_Code').val()) return this;
        }).attr('selected', 'selected');
    });


    // Dynamic Control
    $("#Addbutton").click(function () {
        var i = $("#DynamicInc").val();
        var div = $("<div />").attr("class", "row").attr("data-val", i);
        var value = "";
        //var label = $("<label />").attr("name", "BOMDocumentList[" + i + "].Doc_Discription").attr("class", "control-label").attr("asp-for", "BOMDocumentList[0].Doc_Discription");
        var label = $('<br/><label class="control-label">Discription</label>');
        var Discription = $("<textarea />").attr("name", "BOMDocumentList[" + i + "].Doc_Discription").attr("class", "form-control textarea").attr("rows", "3").attr("cols", "30").attr("style", "resize:none");
        var file = $("<input />").attr("type", "file").attr("name", "file").attr("class", "form-control");

        Discription.val(value);
        file.val(value);
        div.append(label);
        div.append(Discription);
        div.append(file);
        var removeButton = $("<input />").attr("type", "button").attr("value", "Remove").attr("class", "btn btn-danger").attr("style", "width:100%");
        removeButton.attr("onclick", "RemoveTextBox(this)");
        div.append(removeButton);
        $("#TextBoxContainer").append(div);
        i++;
        $("#DynamicInc").val(i);

    });


    //GST
    $("#GSTid").on("change", function () {
        var SelectText = $(this).val();
        //alert(SelectText);
        if (SelectText != "") {
            var data = { GstId:SelectText, Amount:$("#Amount").val() };
            data = $(data).serialize();
            data = JSON.stringify(data);
            $.ajax({
                type: 'GET',
                url: '/BOMMasters/GstCal?id=' + SelectText + '&Amount=' + $("#Amount").val(),
                dataType: "json",
                //traditional: true,
                contentType: 'application/json; charset=utf-8',
                //data: data,
                success: function (data) {
                    if (data.success) {
                        //ShowMessage(data.message);
                        console.log(data.data.cgstAmt);
                        $.each(data, function (key, value) {
                           // for (var i in value) {
                           //     console.log(value[0].cgstamt);
                              //  $("#CGSTAmt").val(value[i].cgstamt);
                           // }
                        });
                    } else {
                        ShowMessageError(data.message);
                    }
                }
            });
        }
    });
});


function sendFile(file) {

    var formData = new FormData();
    formData.append('file', $('#f_UploadImage')[0].files[0]);
    // alert(formData);
    formData.append('filename', $('[id*=txtMatNo]').val() + '_' + $('[id*=txtMatName]').val());
    formData.append("type", "PhotoBOM");
    $.ajax({
        type: 'post',
        url: '/Handler.ashx',
        data: formData,
        xhr: function () {  // Custom XMLHttpRequest
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) { // Check if upload property exists
                // update progressbar percent complete
                // $('#fileProgress').html('0%');
                $("progress").show();
                // For handling the progress of the upload
                // myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                myXhr.upload.addEventListener("progress", function (e) {
                    if (e.lengthComputable) {
                        $("#fileProgress").attr({
                            value: e.loaded,
                            max: e.total
                        });
                    }
                }, false);
            }
            return myXhr;
        },
        success: function (status) {
            if (status != 'error') {
                $("#fileProgress").hide();
                var my_path = "/assets/uploads/CORAL/Photo/BOM/" + status;
                $('[id*=hidd_imgPath]').val(my_path);
                $("#myUploadedImg").attr("src", my_path);
            }
        },
        processData: false,
        contentType: false,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Whoops something went wrong!" + errorThrown);
        }
    });
}

 //Dynamic Control remove
function RemoveTextBox(button) {
    
    $(button).parent().remove();
    var i = 0;
    $("#TextBoxContainer  div textarea").each(function () {
        $(this).attr("name", "BOMDocumentList[" + i + "].Doc_Discription");
        i++;
    });
    $("#DynamicInc").val(i);
   
}
   

$(function () {

    var fileupload = $("#f_UploadImage");
    var filePath = $("#ImgPath_str");
    var image = $("#MaterialImg");
    var fReader = new FileReader();
    image.click(function () {
        fileupload.click();
    });
    fileupload.change(function () {
        var fileExtension = ['jpeg', 'jpg'];
        var fileName = $(this).val();
        // filePath.html(fileName);
        var input = document.getElementById("f_UploadImage");
        var extension = fileName.replace(/^.*\./, '');
        if ($.inArray(extension, fileExtension) == -1) {
            swal("Please select only .jpg or .jpeg files.");
            return false;
        } else {
            fReader.readAsDataURL(input.files[0]);
            fReader.onloadend = function (event) {
                var img = document.getElementById("MaterialImg");
                img.src = event.target.result;
                if (img.src == null) {
                    $("#SelectPhoto").val(0);
                } else {
                    $("#SelectPhoto").val(1);
                }
            }
        }
    });
});

//Image Click PopUp
function ImgShow(src) {
    $('#imagepreview').attr('src', src.src); // here asign the image to the modal when the user click the enlarge link
    //var Val = src.getAttribute('data-url').trim();
    //var SpltVal = Val.split("/");
    //$('#myModalLabel').html(SpltVal[5].split(".")[0]);
    //$('#myModalLabel').attr('style', 'font-weight:bold');
    $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
}

//Model Popup
function ShowPopup(url) {
    //   var popup;
    var modalId = 'modalDefault';
    var modalPlaceholder = $('#' + modalId + ' .modal-dialog .modal-content');
    $.get(url)
        .done(function (response) {
            modalPlaceholder.html(response);
            popup = $('#' + modalId + '').modal({
                keyboard: false,
                backdrop: 'static'
            });
        });
}
