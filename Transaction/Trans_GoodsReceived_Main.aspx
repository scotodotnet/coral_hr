﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_GoodsReceived_Main.aspx.cs" Inherits="Trans_GoodsReceived_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
    <asp:UpdatePanel ID="upForm" runat="server">
        <ContentTemplate>
        
    <section class="content">
                <div class="row">
                    <div class="col-md-2">
                        <asp:Button ID="btnAddNew" class="btn btn-primary btn-block margin-bottom"  runat="server" Text="Add New" OnClick="btnAddNew_Click"/>
                    </div>
                    <!-- /.col -->
                    </div>
                <!--/.row-->
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Goods Received List</span></h3>
                                <div class="box-tools pull-right">
                                    <div class="has-feedback">
                                        <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <div class="table-responsive mailbox-messages">
                                    <div class="col-md-12">
					            <div class="row">
					                <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="table table-responsive table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Goods Received No</th>
                                                        <th>Goods Received Date</th>
                                                        <th>Purchase Order No</th>
                                                        <th>Purchase Order Date</th>
                                                        <th>Supplier Name</th>
                                                        <th>Qty</th>
                                                        <th>Amount</th>
                                                        <th>Mode</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("GRNo")%></td>
                                                <td><%# Eval("GRDate")%></td>
                                                <td><%# Eval("PurOrdNo")%></td>
                                                <td><%# Eval("PurOrdDate")%></td>
                                                <td><%# Eval("SuppName")%></td>
                                                <td><%# Eval("TotalQty")%></td>
                                                <td><%# Eval("TotalAmount")%></td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("GRNo")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("GRNo")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Goods Received details?');">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
					            </div>
					        </div>
                                </div>
                                <!-- /.mail-box-messages -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
        
            </ContentTemplate>
    </asp:UpdatePanel>
   </div> 
</asp:Content>

