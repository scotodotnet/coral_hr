﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class Trans_GoodsReceived_Approval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Goods Received Approval";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data_Enquiry_Grid();
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        //bool Rights_Check = false;

        //Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Goods Received");

        //if (Rights_Check == false)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Goods Received..');", true);
        //}
        //else
        //{
        //    //Session.Remove("Pur_RequestNo_Approval");
        //    //Session.Remove("Pur_Request_No_Amend_Approval");
        //    Session.Remove("Pur_Request_No");
        //    Response.Redirect("Trans_GoodsReceived_Sub.aspx");
        //}
    }

    protected void GridApprovalClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        bool Rights_Check = false;
        string Enquiry_No_Str = e.CommandArgument.ToString();

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Goods Received Approval");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit New Goods Received..');", true);
        }

        if (!ErrFlag)
        {
            SSQL = "Select Approval_Status from Trans_GoodsReceipt_Main where GRNo='" + Enquiry_No_Str + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows[0]["Approval_Status"].ToString() == "0")
            {
                
                SSQL = "";
                SSQL = "select ItemCode,isnull(ReceiveQty,'0') as ReceiveQty from Trans_GoodsReceipt_Sub where GRNo='" + Enquiry_No_Str + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string Mat_No = dt.Rows[i]["ItemCode"].ToString();

                        SSQL = "";
                        SSQL = "Select WarehouseCode,RackSerious from BOMMaster where Mat_No='" + Mat_No + "'";
                        DataTable dt_Rack = new DataTable();
                        dt_Rack = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_Rack.Rows.Count > 0)
                        {
                            string WH_Code = dt_Rack.Rows[0]["WarehouseCode"].ToString();
                            string WH_RackSerious = dt_Rack.Rows[0]["RackSerious"].ToString();
                            SSQL = "";
                            SSQL = "Select * from MstWareHouseRackSub where WarehouseCode='" + WH_Code + "' and RackSerious='" + WH_RackSerious + "' and CAST(RackQtyUse as decimal(18,2))!=CAST(RackQty as decimal(18,2)) and  CAST(RackQtyBalance as decimal(18,2)) > CAST('0.00' as decimal(18,2)) order by RackName asc";
                            DataTable dt_WHBalance_Rack = new DataTable();
                            dt_WHBalance_Rack = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (dt_WHBalance_Rack.Rows.Count > 0)
                            {
                                decimal Bal_RacksQty = 0;
                                SSQL = "";
                                SSQL = "Select sum(CAST(RackQtyBalance as decimal(18,2))) as RackQtyBalance from MstWareHouseRackSub where WarehouseCode='" + WH_Code + "' and RackSerious='" + WH_RackSerious + "' and CAST(RackQtyUse as decimal(18,2))!=CAST(RackQty as decimal(18,2)) and  CAST(RackQtyBalance as decimal(18,2)) >CAST( '0.00' as decimal(18,2)) group by RackSerious order by RackSerious asc";
                                SqlConnection cn = new SqlConnection(constr);
                                cn.Open();
                                SqlCommand cmd_dpt1 = new SqlCommand(SSQL, cn);
                                Bal_RacksQty = Convert.ToDecimal(cmd_dpt1.ExecuteScalar());
                                cn.Close();
                                decimal Item_Qty_Recv = Convert.ToDecimal(dt.Rows[i]["ReceiveQty"].ToString());
                                for (int iRck = 0; iRck < dt_WHBalance_Rack.Rows.Count; iRck++)
                                {
                                    decimal Rack_qty_Bal = Convert.ToDecimal(dt_WHBalance_Rack.Rows[iRck]["RackQtyBalance"].ToString());
                                    decimal Rack_SaveQty = 0;
                                    decimal Bal_RackQty = 0;
                                    if (Item_Qty_Recv > 0)
                                    {
                                        if (Convert.ToDecimal(Item_Qty_Recv) > Convert.ToDecimal(Rack_qty_Bal))
                                        {
                                            Rack_SaveQty = Convert.ToDecimal(Item_Qty_Recv)-(Convert.ToDecimal(Item_Qty_Recv)-Convert.ToDecimal(Rack_qty_Bal));
                                            Bal_RackQty = Convert.ToDecimal(Rack_SaveQty) - Convert.ToDecimal(Rack_qty_Bal);
                                            Item_Qty_Recv = Convert.ToDecimal(Item_Qty_Recv) - Convert.ToDecimal(Rack_SaveQty);
                                        }
                                        else if (Convert.ToDecimal(Item_Qty_Recv) == Convert.ToDecimal(Rack_qty_Bal))
                                        {
                                            Rack_SaveQty = Convert.ToDecimal(Item_Qty_Recv);
                                            Bal_RackQty = Convert.ToDecimal(Rack_SaveQty) - Convert.ToDecimal(Rack_qty_Bal);
                                            Item_Qty_Recv = Convert.ToDecimal(Item_Qty_Recv) - Convert.ToDecimal(Rack_SaveQty);
                                        }
                                        else if (Convert.ToDecimal(Item_Qty_Recv) < Convert.ToDecimal(Rack_qty_Bal))
                                        {
                                            Rack_SaveQty = Convert.ToDecimal(Item_Qty_Recv);
                                            Bal_RackQty = Convert.ToDecimal(Rack_qty_Bal) - Convert.ToDecimal(Rack_SaveQty);
                                            Item_Qty_Recv = Convert.ToDecimal(Item_Qty_Recv) - Convert.ToDecimal(Rack_SaveQty);
                                        }
                                        SSQL = "";
                                        SSQL = "update MstWareHouseRackSub set RackQtyUse=CAST(RackQtyUse as decimal(18,2))+CAST('" + Rack_SaveQty + "' as decimal(18,2)),RackQtyBalance='" + Bal_RackQty + "' where WarehouseCode='" + dt_Rack.Rows[0]["WarehouseCode"].ToString() + "' and RackSerious='" + WH_RackSerious + "' and RackName='" + dt_WHBalance_Rack.Rows[iRck]["RackName"].ToString() + "'";
                                        objdata.RptEmployeeMultipleDetails(SSQL);
                                    }
                                }
                            }
                        }
                    }
                }

                SSQL = "Update Trans_GoodsReceipt_Main Set Approval_Status='1' where GRNo='" + Enquiry_No_Str + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Goods Received Detail Already Successed..');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Goods Received Detail Already Approved..');", true);
            }
        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approve Goods Received Detail..');", true);
        }
        ddlStatus_SelectedIndexChanged(sender, e);
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Purchase Request");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Purchase Request..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Do not Delete Purchase Request Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable dtdpurchase = new DataTable();
            query = "select Status from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";
            dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
            string status = dtdpurchase.Rows[0]["Status"].ToString();

            if (status == "" || status == "0")
            {
                DataTable DT = new DataTable();
                query = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    //Delete Main Table
                    query = "Delete from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    //Delete Main Sub Table
                    query = "Delete from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Deleted Successfully');", true);
                    Load_Data_Enquiry_Grid();
                }
            }
            else if (status == "2")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details put in pending..');", true);
            }

        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select GRNo,GRDate,PurOrdNo,PurOrdDate,SuppName,TotalQty,TotalAmount from Trans_GoodsReceipt_Main where Status='' And ";
        SSQL = SSQL + "Ccode = '" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
        SSQL = SSQL + "Approval_Status='0'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        if(ddlStatus.SelectedItem.Text== "Pending")
        {
            SSQL = "Select GRNo,GRDate,PurOrdNo,PurOrdDate,SuppName,TotalQty,TotalAmount from Trans_GoodsReceipt_Main where Status='' And ";
            SSQL = SSQL + "Ccode = '" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
            SSQL = SSQL + "Approval_Status='0'";
        }
        else if (ddlStatus.SelectedItem.Text == "Approval")
        {
            SSQL = "Select GRNo,GRDate,PurOrdNo,PurOrdDate,SuppName,TotalQty,TotalAmount from Trans_GoodsReceipt_Main where Status='' And ";
            SSQL = SSQL + "Ccode = '" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
            SSQL = SSQL + "Approval_Status='1'";
        }
            
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
}