﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="purchase_request.aspx.cs" Inherits="Transaction_purchase_request" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy'});
                    $('.select2').select2();
                }
            });
        };
    </script>
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upPurReq" runat="server">
            <ContentTemplate>
                <section class="content-header">
            <h1><i class=" text-primary"></i> Create New Purchase Request</h1>
        </section>
    
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">         
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="Req_No">Request No.</label>
                                        <asp:Label ID="txtPurRequestNo" runat="server" class="form-control"></asp:Label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Date</label>
                                        <asp:TextBox ID="txtDate" MaxLength="20" class="form-control datepicker" runat="server" AutoComplete="off" ></asp:TextBox>
					                    <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Department</label>
					                    <asp:DropDownList ID="txtDeptCode" runat="server" class="form-control select2" AutoPostBack="true" 
                                            onselectedindexchanged="txtDeptCode_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtDeptCode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="col-md-3" runat="server" visible="false">
                                    <div class="form-group">
                                        <label for="exampleInputName">Department Name</label>					          
					                    <asp:TextBox ID="txtDeptName" class="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div> 

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputName">Description</label>
					                    <asp:TextBox ID="txtOthers" TextMode="MultiLine" class="form-control" style="resize:none" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-primary">

                                        <div class="box-header with-border">
                                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Purchase Request List</span></h3>
                                            <div class="box-tools pull-right">
                                                <div class="has-feedback">
                                                    <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <!-- /.box-tools -->
                                        </div>

                                        <div class="box-body no-padding">
                                            <div class="table-responsive mailbox-messages">

                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Purchase Order Type</label>
					                                        <asp:DropDownList ID="ddlPurType" runat="server" class="form-control select2" AutoPostBack="true" 
                                                                OnSelectedIndexChanged="ddlPurType_SelectedIndexChanged">
                                                                <asp:ListItem Value="1">Single Item</asp:ListItem>
                                                                <asp:ListItem Value="2">Model Based</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtGenerator_Model" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                </div>

                                                <asp:Panel ID="pnlModel" runat="server" Visible="false">
                                                    <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Generator Model</label>
					                                        <asp:DropDownList ID="txtGenerator_Model" runat="server" class="form-control select2" AutoPostBack="true" 
                                                                onselectedindexchanged="txtGenerator_Model_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtGenerator_Model" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="exampleInputName">Production PartName</label>
					                                        <asp:DropDownList ID="txtProduction_Part_Name" runat="server" class="form-control select2" AutoPostBack="true" 
                                                                onselectedindexchanged="txtProduction_Part_Name_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtProduction_Part_Name" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                </div>
                                                </asp:Panel>
                                                
                                                <asp:Panel ID="pnlSingle" runat="server" Visible="false">
                                                    <div class="row">
                                                    <div class="form-group col-md-3">
					                                    <label for="exampleInputName">Material Name</label>
                                                        <asp:DropDownList ID="txtItemNameSelect" runat="server" class="form-control select2" AutoPostBack="true" 
                                                                onselectedindexchanged="txtItemNameSelect_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID="txtItemName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID="txtItemCode" class="form-control" runat="server" Visible="false"></asp:TextBox>  
					                                </div>

                                                    <div class="form-group col-md-2">
					                                    <label for="exampleInputName">Reuired Qty</label>
                                                        <asp:TextBox ID="txtReuiredQty" class="form-control" runat="server" MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtReuiredQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator> 
                                                        <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                            TargetControlID="txtReuiredQty" ValidChars="0123456789.">
                                                        </cc1:FilteredTextBoxExtender>
					                                </div>  

                                                    <div class="form-group col-md-2">
					                                    <label for="exampleInputName">Reuired Date</label>
					                                    <asp:TextBox ID="txtReuiredDate" MaxLength="20" class="form-control datepicker" runat="server" AutoComplete="off"></asp:TextBox>
					                                    <asp:RequiredFieldValidator ControlToValidate="txtReuiredDate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
					                                </div>
                                               
                                                    <div class="form-group col-md-4" runat="server" visible="false">
					                                    <label for="exampleInputName">Item Rate</label>
					                                    <asp:TextBox ID="txtItemRate" class="form-control" runat="server" Text="0"></asp:TextBox>
					                                </div>

                                                    <div class="form-group col-md-3">
					                                    <label for="exampleInputName">Remarks</label>
					                                    <asp:TextBox ID="txtItemRemarks" TextMode="MultiLine" style="resize:none" class="form-control" runat="server"></asp:TextBox>
					                                </div>

					                                <div class="form-group col-md-2" runat="server" style="padding-top:5px" >
					                                    <br />
					                                    <asp:Button ID="btnAddItem" class="btn btn-primary"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field" OnClick="btnAddItem_Click"/>
					                                </div>

                                                </div>
                                                </asp:Panel>
                                                

					                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                                        <HeaderTemplate>
                                                        <table  class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>S. No</th>
                                                                    <th>Item Code</th>
                                                                    <th>Item Name</th>
                                                                    <th>UOM Code</th>
                                                                    <th>Reuired Qty</th>
                                                                    <th>Reuired Date</th>
                                                                    <th>Remarks</th>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                            <td><%# Eval("ItemCode")%></td>
                                                            <td><%# Eval("ItemName")%></td>
                                                            <td><%# Eval("UOMCode")%></td>
                                                            <td><%# Eval("ReuiredQty")%></td>
                                                            <td><%# Eval("ReuiredDate")%></td>
                                                            <td><%# Eval("ItemRemarks")%></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                                    Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>' 
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>                                
			                                    </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="form-group">
                                <asp:Button ID="btnSave" class="btn btn-primary"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                <asp:Button ID="btnBackEnquiry" class="btn btn-default" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>     
                                <asp:Button ID="btnBackReqApprv" class="btn btn-default" runat="server" 
                                    Text="Approve" Visible="false" onclick="btnBackReqApprv_Click"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
</asp:Content>

