﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="standard_po_approval.aspx.cs" Inherits="Transaction_standard_po_approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Purchase Order Approval</span></h3>
                        </div>
                    
                        <div class="row" runat="server" style="padding-top:25px;padding-bottom:25px">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <label class="control-label" for="Req_No">Purchase Order Status</label>
                                    <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                        onselectedindexchanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="box-body no-padding">
                            <div class="table-responsive mailbox-messages">
                                <div class="col-md-12">
					                <div class="row">
                                        <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false">
			                                <HeaderTemplate>
                                                <table id="example" class="table table-hover table-striped">
                                                    <thead>
                                        <tr>
                                            <th>PO No</th>
                                            <th>Date</th>
                                            <th>Supplier</th>
                                            <th>Pur_Request_No</th>
                                            <th>Approve</th>
                                            <th>Cancel</th>
                                            <th>View</th>
                                            </tr>
                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("Std_PO_No")%></td>
                                                    <td><%# Eval("Std_PO_Date")%></td>
                                                    <td><%# Eval("Supp_Name")%></td>
                                                    <td><%# Eval("Pur_Request_No")%></td>                                        
                                                    <td>
                                                        <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square"  runat="server" 
                                                            Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("Std_PO_No")%>' CommandName='<%# Eval("Std_PO_No")%>' 
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this PO details?');">
                                                        </asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="LinkButton1" class="btn btn-primary btn-sm fa fa-check-square"  runat="server" 
                                                            Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("Std_PO_No")%>' CommandName='<%# Eval("Std_PO_No")%>' 
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this PO details?');">
                                                        </asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-external-link-square"  
                                                            runat="server" Text="" OnCommand="GridEditPurRequestClick" 
                                                            CommandArgument='<%# Eval("Std_PO_No")%>' CommandName='<%# Eval("Std_PO_No")%>'>
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
			                        </asp:Repeater>
					            </div>
                                    <div class="row">
			            <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" Visible="false">
			                <HeaderTemplate>
                                <table id="example" class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>PO No</th>
                                            <th>Date</th>
                                            <th>Supplier</th>
                                            <th>Pur_Request_No</th>
                                               
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("Std_PO_No")%></td>
                                    <td><%# Eval("Std_PO_Date")%></td>
                                    <td><%# Eval("Supp_Name")%></td>
                                    <td><%# Eval("Pur_Request_No")%></td> 
                                        
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>                                
			            </asp:Repeater>
			        </div>
                                    <div class="row">
			            <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" Visible="false">
			                <HeaderTemplate>
                                <table id="example" class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>PO No</th>
                                            <th>Date</th>
                                            <th>Supplier</th>
                                            <th>Pur_Request_No</th>
                                               
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("Std_PO_No")%></td>
                                    <td><%# Eval("Std_PO_Date")%></td>
                                    <td><%# Eval("Supp_Name")%></td>
                                    <td><%# Eval("Pur_Request_No")%></td>
                                        
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>                                
			            </asp:Repeater>
			        </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
    
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</asp:Content>

