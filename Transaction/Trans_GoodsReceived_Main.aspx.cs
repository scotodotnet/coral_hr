﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_GoodsReceived_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Goods Received";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        Load_Data_Enquiry_Grid();
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Goods Received");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add..');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Goods Received..');", true);
        }
        else
        {
            Session.Remove("GRNo");
            Response.Redirect("Trans_GoodsReceived_Sub.aspx");
        }

    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        bool Rights_Check = false;
        bool ErrFlag = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Goods Received");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Edit New Goods Received..');", true);
        }

        DataTable dtdpurchase = new DataTable();
        SSQL = "select Approval_Status from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And GRNo='" + e.CommandName.ToString() + "'";
        dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
        string status = dtdpurchase.Rows[0]["Approval_Status"].ToString();

        if (!ErrFlag)
        {
            if (status == "" || status == "0")
            {
                string Enquiry_No_Str = e.CommandName.ToString();
                //Session.Remove("Pur_RequestNo_Approval");
                //Session.Remove("Pur_Request_No_Amend_Approval");
                Session.Remove("GRNo");
                Session["GRNo"] = Enquiry_No_Str;
                Response.Redirect("Trans_GoodsReceived_Sub.aspx");
            }
            else if (status == "2")
            {
                ErrFlag = true;

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the UserID ');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Goods Received Details Already Rejected..');", true); 
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Goods Received Details Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Goods Received Details put in pending..');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Goods Received Details put in pending..');", true);
            }
            else if (status == "1")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Goods Received Details Already Approved Cant Edit..');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Goods Received Cant Edit..');", true);
            }

        }
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Goods Received");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Delete New Goods Received..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        SSQL = "Select * from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + "FinYearCode = '" + SessionFinYearCode + "' And GRNo='" + e.CommandName.ToString() + "' And Approval_Status='1'";

        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Do not Delete Goods Received Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable dtdpurchase = new DataTable();
            SSQL = "select Approval_Status from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + "And FinYearCode ='" + SessionFinYearCode + "' And GRNo='" + e.CommandName.ToString() + "'";

            dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
            string status = dtdpurchase.Rows[0]["Approval_Status"].ToString();

            if (status == "" || status == "0")
            {
                DataTable DT = new DataTable();
                SSQL = "Select * from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And GRNo='" + e.CommandName.ToString() + "'";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT.Rows.Count != 0)
                {
                    //Delete Main Table
                    SSQL = "Update Trans_GoodsReceipt_Main set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And GRNo='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                    //Delete Main Sub Table
                    SSQL = "Update Trans_GoodsReceipt_Sub set Status='Delete' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And GRNo='" + e.CommandName.ToString() + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Goods Received Details Deleted Successfully');", true);
                    Load_Data_Enquiry_Grid();
                }
            }
            else if (status == "2")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Goods Received Details Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Goods Received Details put in pending..');", true);
            }

        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select GRNo,GRDate,PurOrdNo,PurOrdDate,SuppName,TotalQty,TotalAmount from Trans_GoodsReceipt_Main where Status='' And ";
        SSQL = SSQL + "Ccode = '" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
}