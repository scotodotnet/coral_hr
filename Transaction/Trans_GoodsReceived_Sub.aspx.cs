﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_GoodsReceived_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGRNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal ReqQty;

    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Goods Received";
            Initial_Data_Referesh();
            Load_PurOrder_No();

            if (Session["GRNo"] == null)
            {
                SessionGRNo = "";
            }
            else
            {
                SessionGRNo = Session["GRNo"].ToString();
                txtGRNo.Text = SessionGRNo;
                btnSearch_Click(sender, e);


            }
        }
        Load_OLD_data();
    }

    private void Load_PurOrder_No()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select Std_PO_No from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And PO_Status='1'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlPurOrdNo.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["Std_PO_No"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlPurOrdNo.DataTextField = "Std_PO_No";
        ddlPurOrdNo.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];

        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
        }

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Goods Received", SessionFinYearVal,"1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtGRNo.Text = Auto_Transaction_No;
                }
            }
        }

        //string Roundoff = "0.0";

        //decimal value;
        //if (decimal.TryParse(txtTotAmt.Text, out value))
        //{
        //    value = Math.Round(value);
        //    Roundoff = value.ToString();
        //    // Do something with the new text value
        //}
        //else
        //{
        //    // Tell the user their input is invalid
        //}

        decimal Total = Convert.ToDecimal(txtTotAmt.Text);

        decimal Roundoff = Total - Convert.ToInt32(Total);

        if ((double)Roundoff <= .50)
        {
            txtTotAmt.Text = Convert.ToInt32(Total).ToString();
        }
        else
        {
            txtTotAmt.Text = (Convert.ToInt32(Total) + 1).ToString();
        }
        //lblTotal.Text = Total.ToString();

        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                //Update Main table

                SSQL = "Update Trans_GoodsReceipt_Main set GRDate='" + txtGRDate.Text + "',";
                SSQL = SSQL + " PurOrdNo='" + ddlPurOrdNo.SelectedItem.Text + "',PurOrdDate='" + txtPurOrdDate.Text + "',SuppCode='" + hdSuppCode.Value + "',";
                SSQL = SSQL + " SuppName='" + txtSuppName.Text + "',PayTerms='" + txtPayTerms.Text + "',PayMode='" + txtPayMode.Text + "',";
                SSQL = SSQL + " VehicleNo ='" + txtVehiNo.Text + "',Notes='" + txtNotes.Text + "',Remarks='" + txtRemarks.Text + "',";
                SSQL = SSQL + " TotalQty='" + txtTotQty.Text + "',RoundOff='" + Roundoff + "',TotalAmount='" + txtTotAmt.Text + "'";
                SSQL = SSQL + " Where GRNo='" + txtGRNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                //Update Sub table

                DataTable dt = new DataTable();

                //Load_OLD_data();
                //dt = (DataTable)ViewState["ItemTable"];
                //dt = (DataTable)Repeater1;

                for (int i = 0; i < Repeater1.Items.Count; i++)
                //foreach (RepeaterItem Item in Repeater1.Items)
                {
                    //string ReceivedQty = item.ToString();

                    Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;
                    Label Warehouse = Repeater1.Items[i].FindControl("lblWarehouse") as Label;
                    TextBox ReceiveQty = Repeater1.Items[i].FindControl("txtReceivQty") as TextBox;
                    Label Rate = Repeater1.Items[i].FindControl("lblRate") as Label;
                    Label ItemTotal = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                    Label CGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                    Label CGSTAmount = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                    Label SGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                    Label SGSTAmount = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                    Label IGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                    Label IGSTAmount = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                    Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;

                    SSQL = "Update Trans_GoodsReceipt_Sub Set ReceiveQty='" + ReceiveQty.Text.ToString() + "',Rate ='" + Rate.Text.ToString() + "',";
                    SSQL = SSQL + " ItemTotal='" + ItemTotal.Text + "',CGSTPer='" + CGSTPer.Text + "',CGSTAmt='" + CGSTAmount.Text + "',";
                    SSQL = SSQL + " SGSTPer='" + SGSTPer.Text + "',SGSTAmt='" + SGSTAmount.Text + "',IGSTPer='" + IGSTPer.Text + "',";
                    SSQL = SSQL + " IGSTAmt='" + IGSTAmount.Text + "',LineTotal='" + LineTotal.Text + "' Where ItemCode ='" + ItemCode.Text + "' ";
                    SSQL = SSQL + " And ItemName='" + ItemName.Text + "' And GRNo='" + txtGRNo.Text + "' And Ccode='" + SessionCcode + "' ";
                    SSQL = SSQL + " And LCode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                    SSQL = SSQL + " FinYearVal='" + SessionFinYearVal + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                    //Stock Update
                    DateTime transDate = Convert.ToDateTime(txtGRDate.Text);

                    SSQL = "Update Trans_Stock_Ledger_All set Add_Qty = '" + ReceiveQty.Text.ToString() + "',";
                    SSQL = SSQL + " Add_Value='" + ItemTotal.Text.ToString() + "' Where ItemCode ='" + ItemCode.Text + "' ";
                    SSQL = SSQL + " And ItemName='" + ItemName.Text + "' And Trans_No='" + txtGRNo.Text + "' And Ccode='" + SessionCcode + "' ";
                    SSQL = SSQL + " And LCode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
                    SSQL = SSQL + " FinYearVal='" + SessionFinYearVal + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
            }
            else
            {
                //Insert Main Table
                SSQL = "Insert Into Trans_GoodsReceipt_Main(CCode,LCode,FinYearCode,FinYearVal,GRNo,GRDate,PurOrdNo,PurOrdDate,SuppCode,SuppName,";
                SSQL = SSQL + " PayTerms,PayMode,VehicleNo,Notes,Remarks,TotalQty,RoundOff,TotalAmount,UserId,UserName,Status,CreateOn) Values(";
                SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                SSQL = SSQL + " '" + txtGRNo.Text + "','" + txtGRDate.Text + "','" + ddlPurOrdNo.SelectedItem.Text + "','" + txtPurOrdDate.Text + "',";
                SSQL = SSQL + " '" + hdSuppCode.Value + "','" + txtSuppName.Text + "','" + txtPayTerms.Text + "','" + txtPayMode.Text + "',";
                SSQL = SSQL + " '" + txtVehiNo.Text + "','" + txtNotes.Text + "','" + txtRemarks.Text + "','" + txtTotQty.Text + "',";
                SSQL = SSQL + " '" + Roundoff + "','" + txtTotAmt.Text + "','" + SessionUserID + "','" + SessionUserName + "','',Getdate())";
                objdata.RptEmployeeMultipleDetails(SSQL);

                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["ItemTable"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;
                    Label DeptCode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;
                    Label WarehouseCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                    Label Warehouse = Repeater1.Items[i].FindControl("lblWarehouse") as Label;
                    TextBox ReceiveQty = Repeater1.Items[i].FindControl("txtReceivQty") as TextBox;
                    Label Rate = Repeater1.Items[i].FindControl("lblRate") as Label;
                    Label ItemTotal = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                    Label CGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                    Label CGSTAmount = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                    Label SGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                    Label SGSTAmount = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                    Label IGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                    Label IGSTAmount = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                    Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;

                    SSQL = "Insert Into Trans_GoodsReceipt_Sub(CCode,LCode,FinYearCode,FinYearVal,GRNo,GRDate,PurOrdNo,PurOrdDate,ItemCode,ItemName,";
                    SSQL = SSQL + " DeptCode,DeptName,WarehouseCode,WarehouseName,PurOrdQty,ReceiveQty,BalQty,Rate,ItemTotal,Disc,DiscAmt,";
                    SSQL = SSQL + " CGSTPer,CGSTAmt,SGSTPer,SGSTAmt,IGSTPer,IGSTAmt,LineTotal,UserId,UserName,Status,CreateOn) ";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtGRNo.Text + "','" + txtGRDate.Text + "','" + ddlPurOrdNo.SelectedItem.Text + "','" + txtPurOrdDate.Text + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["WarehouseCode"].ToString() + "','" + dt.Rows[i]["WarehouseName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["OrderQty"].ToString() + "','" + ReceiveQty.Text.ToString() + "','0.00',";
                    SSQL = SSQL + " '" + dt.Rows[i]["Rate"].ToString() + "','" + ItemTotal.Text.ToString() + "','0.00','0.00',";
                    SSQL = SSQL + " '" + dt.Rows[i]["CGSTPer"].ToString() + "','" + CGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["SGSTPer"].ToString() + "','" + SGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["IGSTPer"].ToString() + "','" + IGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + LineTotal.Text.ToString() + "','" + SessionUserID + "','" + SessionUserName + "',";
                    SSQL = SSQL + " '',Getdate()) ";
                    //SSQL = SSQL + " '" + Stock_Qty + "','" + Last_Issue_Date + "','" + Last_Purchase_Date + "','" + Last_Purchase_Qty + "','" + Prev_Purchase_Rate + "','" + dt.Rows[i]["Re_Order_Stock_Qty"].ToString() + "','" + dt.Rows[i]["Re_Order_Item_Qty"].ToString() + "','" + dt.Rows[i]["Indent_Type"].ToString() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);


                    //Stock Insert

                    DateTime transDate = Convert.ToDateTime(txtGRDate.Text);

                    SSQL = "Insert into Trans_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,";
                    SSQL = SSQL + " Supp_Code,Supp_Name,ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,Add_Qty,Add_Value,";
                    SSQL = SSQL + " Minus_Qty,Minus_Value,UserID,UserName) Values(";
                    SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtGRNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtGRDate.Text + "','Goods Received',";
                    SSQL = SSQL + " '" + hdSuppCode.Value + "','" + txtSuppName.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["ItemName"].ToString() + "','" + dt.Rows[i]["DeptCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["DeptName"].ToString() + "','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + ReceiveQty.Text.ToString() + "',";
                    SSQL = SSQL + " '" + ItemTotal.Text.ToString() + "','0.00','0.00','" + SessionUserID + "','" + SessionUserName + "')";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Pur_Request_No"] = txtGRNo.Text;
            btnSave.Text = "Update";
            Response.Redirect("Trans_GoodsReceived_Main.aspx");
        }
    }


    private void Stock_Add()
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtGRNo.Text = ""; txtGRDate.Text = "";
        // txtDeptCode.SelectedValue = "-Select-"; txtDeptName.Text = "";
        //txtCostCenter.Text = "-Select-"; txtCostElement.Items.Clear(); txtRequestedby.Value = "-Select-";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Pur_Request_No");
        //Load_Data_Enquiry_Grid();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));
        dt.Columns.Add(new DataColumn("PurQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReceivQty", typeof(string)));
        dt.Columns.Add(new DataColumn("BalQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        TotalReqQty();

    }
    public void TotalReqQty()
    {

        //ReqQty = 0;
        //DataTable dt = new DataTable();
        //dt = (DataTable)ViewState["ItemTable"];
        //for (int i = 0; i < dt.Rows.Count; i++)
        //{
        //    ReqQty = Convert.ToDecimal(ReqQty) + Convert.ToDecimal(dt.Rows[i]["ReuiredQty"]);
        //}
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And GRNo ='" + txtGRNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            //txtGRNo.ReadOnly = true;
            txtGRNo.Text = Main_DT.Rows[0]["GRNo"].ToString();
            txtGRDate.Text = Main_DT.Rows[0]["GRDate"].ToString();
            ddlPurOrdNo.SelectedItem.Text = Main_DT.Rows[0]["PurOrdNo"].ToString();
            txtPurOrdDate.Text = Main_DT.Rows[0]["PurOrdDate"].ToString();
            hdSuppCode.Value = Main_DT.Rows[0]["SuppCode"].ToString();
            txtSuppName.Text = Main_DT.Rows[0]["SuppName"].ToString();

            txtPayTerms.Text = Main_DT.Rows[0]["PayTerms"].ToString();
            txtPayMode.Text = Main_DT.Rows[0]["PayMode"].ToString();
            txtVehiNo.Text = Main_DT.Rows[0]["VehicleNo"].ToString();
            txtNotes.Text = Main_DT.Rows[0]["Notes"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();


            txtTotQty.Text = Main_DT.Rows[0]["TotalQty"].ToString();
            txtRoundOff.Text = Main_DT.Rows[0]["RoundOff"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmount"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,PurOrdQty[OrderQty],ReceiveQty,BalQty,Rate,ItemTotal,";
            SSQL = SSQL + "CGSTPer,CGSTAmt [CGSTAmount],SGSTPer,SGSTAmt[SGSTAmount],IGSTPer,IGSTAmt[IGSTAmount], ";
            SSQL = SSQL + "LineTotal from Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
            SSQL = SSQL + "FinYearCode = '" + SessionFinYearCode + "' And GRNo='" + txtGRNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            TotalReqQty();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_GoodsReceived_Main.aspx");
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }

    protected void ddlPurOrdNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And PO_Status='1' and Std_PO_No='" + ddlPurOrdNo.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtPurOrdDate.Text = DT.Rows[0]["Std_PO_Date"].ToString();
        txtSuppName.Text = DT.Rows[0]["Supp_Name"].ToString();
        hdSuppCode.Value = DT.Rows[0]["Supp_Code"].ToString();
        //txtPartyInvNo.Text = "";
        //txtPartyInvDate.Text = "";
        txtPayTerms.Text = DT.Rows[0]["PaymentTerms"].ToString();
        txtPayMode.Text = DT.Rows[0]["PaymentMode"].ToString();

        txtTotQty.Text = DT.Rows[0]["TotalQuantity"].ToString();
        txtTotAmt.Text = DT.Rows[0]["TotalAmt"].ToString();

        SSQL = "Select POS.ItemCode,POS.ItemName,BOM.DeptCode,BOM.DeptName,BOM.WarehouseCode,BOM.WarehouseName,POS.OrderQty,";
        SSQL = SSQL + " POS.OrderQty[ReceiveQty],POS.Rate,POS.ItemTotal,POS.CGSTPer,POS.CGSTAmount,";
        SSQL = SSQL + " POS.SGSTPer,POS.SGSTAmount,POS.IGSTPer,POS.IGSTAmount,POS.LineTotal from Std_Purchase_Order_Main_Sub POS";
        SSQL = SSQL + " Inner join BOMMaster BOM on BOM.Raw_Mat_Name= POS.ItemName and BOM.Mat_No= POS.ItemCode and BOM.Ccode= POS.Ccode";
        SSQL = SSQL + " where POS.Ccode='" + SessionCcode + "' and POS.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " POS.FinYearcode='" + SessionFinYearCode + "' and POS.Std_PO_No='" + ddlPurOrdNo.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

        ViewState["ItemTable"] = Repeater1.DataSource;
    }

    protected void txtReceive_TextChanged(object sender, EventArgs e)
    {
        TextBox txtTest = ((TextBox)(sender));
        RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

        Label lblOrdQty = (Label)rpi.FindControl("lblOrdQty");
        TextBox txtReceivQty = (TextBox)rpi.FindControl("txtReceivQty");
        Label lblRate = (Label)rpi.FindControl("lblRate");
        Label lblItemTol = (Label)rpi.FindControl("lblItemTot");

        Label lblCGSTPre = (Label)rpi.FindControl("lblCGSTPer");
        Label lblSGSTPre = (Label)rpi.FindControl("lblSGSTPer");
        Label lblIGSTPre = (Label)rpi.FindControl("lblIGSTPer");

        Label lblCGSTAmt = (Label)rpi.FindControl("lblCGSTAmt");
        Label lblSGSTAmt = (Label)rpi.FindControl("lblSGSTAmt");
        Label lblIGSTAmt = (Label)rpi.FindControl("lblIGSTAmt");

        Label lblLineTol = (Label)rpi.FindControl("lblLineTot");

        //Label lblBalQty = (Label)rpi.FindControl("lblBalQty");
        //decimal BalQty = Convert.ToDecimal(lblOrdQty.Text) - Convert.ToDecimal(txtReceivQty.Text);
        //lblBalQty.Text = BalQty.ToString();

        decimal ItemTotal = Convert.ToDecimal(lblRate.Text) * Convert.ToDecimal(txtReceivQty.Text);

        decimal CGSTPer = Convert.ToDecimal(lblCGSTPre.Text.ToString());
        decimal SGSTPer = Convert.ToDecimal(lblSGSTPre.Text.ToString());
        decimal IGSTPer = Convert.ToDecimal(lblIGSTPre.Text.ToString());

        decimal cgst = Convert.ToDecimal(ItemTotal) * (CGSTPer / 100);
        decimal sgst = Convert.ToDecimal(ItemTotal) * (SGSTPer / 100);
        decimal igst = Convert.ToDecimal(ItemTotal) * (IGSTPer / 100);

        LineTot = Convert.ToDecimal(ItemTotal) + Convert.ToDecimal(cgst) + Convert.ToDecimal(sgst) + Convert.ToDecimal(igst);

        //decimal TotalVal = Convert.ToDecimal(lblRate.Text) * Convert.ToDecimal(txtReceivQty.Text);

        lblItemTol.Text = ItemTotal.ToString("#.##");
        lblLineTol.Text = LineTot.ToString("#.##");
        if (Convert.ToDecimal(cgst) > Convert.ToDecimal(0.00)) { lblCGSTAmt.Text = cgst.ToString("#.##"); } else { lblCGSTAmt.Text = "0.00"; }
        if (Convert.ToDecimal(sgst) > Convert.ToDecimal(0.00)) { lblSGSTAmt.Text = sgst.ToString("#.##"); } else { lblSGSTAmt.Text = "0.00"; }
        if (Convert.ToDecimal(igst) > Convert.ToDecimal(0.00)) { lblIGSTAmt.Text = igst.ToString("#.##"); } else { lblIGSTAmt.Text = "0.00"; }

        decimal SumQty = 0;
        decimal SumLineTot = 0;

        for (int i = 0; i < Repeater1.Items.Count; i++)
        {
            TextBox ReceiveQty = Repeater1.Items[i].FindControl("txtReceivQty") as TextBox;
            Label LineTotal = Repeater1.Items[i].FindControl("lblLineTot") as Label;

            SumQty = SumQty + Convert.ToDecimal(ReceiveQty.Text);
            SumLineTot = SumLineTot + Convert.ToDecimal(LineTotal.Text);
        }

        txtTotQty.Text = SumQty.ToString("#.##");
        txtTotAmt.Text = SumLineTot.ToString("#.##");
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        TextBox txtReceiveQyt = e.Item.FindControl("txtReceivQty") as TextBox;
    }

    protected void txtRoundOff_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtRoundOff.Text) < Convert.ToDecimal(0.5))
        {
            txtTotAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) - Convert.ToDecimal(txtRoundOff.Text)).ToString("#.##");
        }
        else
        {
            txtTotAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtRoundOff.Text)).ToString("#.##");
        }
    }
}