﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Trans_GoodsReturn_Sub.aspx.cs" Inherits="Trans_GoodsReturn_Sub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" src="../assets/js/Trans_Receipt_Calc.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>

     <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
         if (prm != null) {
             //alert("DatePicker");
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.js-states').select2();
                }
            });
        };
    </script>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <asp:UpdatePanel ID="upGoodRecv" runat="server">
            <ContentTemplate>
                 <section class="content-header">
            <h1><i class=" text-primary"></i> Create New Goods Receipt</h1>
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputName">Goods Return No.</label>
                                    <asp:TextBox ID="txtGRetNo" runat="server" class="form-control" ReadOnly="true" ></asp:TextBox>
                                </div>
                            </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputName">Goods Return Date</label>
                                    <asp:TextBox ID="txtGRetDate" runat="server" class="form-control datepicker" AutoComplete="off"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtGRetDate" ValidationGroup="Validate_Field" class="form_error" 
                                         ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" 
                                        FilterType="Custom,Numbers" TargetControlID="txtGRetDate" ValidChars="0123456789./">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputName">Goods Receipt No.</label>
                                    <asp:DropDownList runat="server" ID="ddlPurOrdNo" class="form-control select2" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlPurOrdNo_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </div>
                            </div>

                             <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputName">Goods Receipt Date</label>
                                    <asp:TextBox ID="txtPurOrdDate" runat="server" class="form-control" ></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier Name</label>
                                    <asp:TextBox ID="txtSuppName" runat="server" class="form-control" ></asp:TextBox>
                                    <asp:HiddenField ID="hdSuppCode" runat="server" />
                                </div>
                            </div>

                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Party Invoice No.</label>
                                    <asp:TextBox ID="txtPartyInvNo" runat="server" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Party Invoice Date</label>
                                    <asp:TextBox ID="txtPartyInvDate" runat="server" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Payment Terms</label>
                                    <asp:TextBox ID="txtPayTerms" runat="server" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">TransPort Name</label>
                                    <asp:TextBox ID="txtTransPort" runat="server" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Vehicle No</label>
                                    <asp:TextBox ID="txtVehiNo" runat="server" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Reason For Return</label>
                                    <asp:TextBox ID="txtRetReason" runat="server" class="form-control" ></asp:TextBox>
                                </div>
                            </div>
					    </div>

                        <div class="row">
                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Notes</label>
                                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" class="form-control" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputName">Remarks</label>
                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" class="form-control" ></asp:TextBox>
                                </div>
                            </div>
					    </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Item Details</span></h3>
                                        <div class="box-tools pull-right">
                                            <div class="has-feedback">
                                                <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <!-- /.box-tools -->
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="clearfix"></div>

                                    <div class="row" runat="server" style="padding-top:15px"> 
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Item Name</label>
                                                <asp:DropDownList ID="ddlItemName" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged" ></asp:DropDownList>
                                            </div>

                                            <asp:HiddenField id="hfDeptCode" runat="server"/>
                                            <asp:HiddenField id="hfDeptName" runat="server"/>
                                            <asp:HiddenField id="hfWarehouseCode" runat="server"/>
                                            <asp:HiddenField id="hfWarehouseName" runat="server"/>

                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Receipt Qty</label>
                                                <asp:TextBox ID="txtReceiveQty" runat="server" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtReceiveQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator> 
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtReceiveQty" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Return Qty</label>
                                                <asp:TextBox ID="txtRetQty" runat="server" class="form-control" AutoPostBack="true"
                                                    OnTextChanged="txtRetQty_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtRetQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator> 
                                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtRetQty" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">Rate</label>
                                                <asp:TextBox ID="txtRate" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtRate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator> 
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtRate" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">CGST Amount</label>
                                                <asp:TextBox ID="txtCGST" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hfCGSTPre" />
                                            </div>
                                        </div>

                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">SGST Amount</label>
                                                <asp:TextBox ID="txtSGST" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hfSGSTPre" />
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputName">IGST Amount</label>
                                                <asp:TextBox ID="txtIGST" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hfIGSTPre" />
                                            </div>
                                        </div>

                                        <div class="col-md-2" runat="server" style="padding-top:25px">
                                            <div class="form-group">
                                               <asp:Button runat="server" ID="btnAddItem" class="btn btn-primary" Text="Add Items" OnClick="btnAddItem_Click"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-body no-padding">
                                        <div class="table-responsive mailbox-messages">
					                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                                    <HeaderTemplate>
                                                    <table id="tbltest"  class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>S. No</th>
                                                                <th runat="server" visible="false">Item Code</th>
                                                                <th>Item Name</th>
                                                                <th runat="server" visible="false">DeptCode</th>
                                                                <th runat="server" visible="false">DeptName</th>
                                                                <th runat="server" visible="false">WareHouseCode</th>
                                                                <th runat="server" visible="false">WareHouseName</th>
                                                                <th>Return Qty</th>
                                                                <th>Rate</th>
                                                                <th>ItemTotal</th>
                                                                <th runat="server" visible="false">CGSTPer</th>
                                                                <th>CGST</th>
                                                                <th runat="server" visible="false">SGSTPer</th>
                                                                <th>SGST</th>
                                                                <th runat="server" visible="false">IGSTPer</th>
                                                                <th>IGST</th>
                                                                <th>Line Total</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblItemCode" Text='<%# Eval("ItemCode")%>' ></asp:Label> 
                                                        </td>

                                                        <td>
                                                           <asp:Label runat="server" ID="lblItemName" Text='<%# Eval("ItemName")%>'></asp:Label>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblDeptCode" Text='<%# Eval("DeptCode")%>' ></asp:Label> 
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblDeptName" Text='<%# Eval("DeptName")%>' ></asp:Label> 
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblWarehouseCode" Text='<%# Eval("WarehouseCode")%>' ></asp:Label> 
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblWarehouseName" Text='<%# Eval("WarehouseName")%>' ></asp:Label> 
                                                        </td>

                                                        <td>
                                                            <asp:TextBox runat="server"  ID="txtReturnQty" Text='<%# Eval("RetQty")%>'
                                                                Width="100px" BorderStyle="None" AutoPostBack="true" OnTextChanged="txtReturn_TextChanged"> 
                                                            </asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtReturnQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator> 
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtReturnQty" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" ID="lblRate" Text='<%# Eval("Rate")%>'></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" ID="lblItemTot" Text='<%# Eval("ItemTotal")%>'></asp:Label>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblCGSTPer" Text='<%# Eval("CGSTPer")%>'></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" ID="lblCGSTAmt" Text='<%# Eval("CGSTAmt")%>'></asp:Label>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblSGSTPer" Text='<%# Eval("SGSTPer")%>'></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" ID="lblSGSTAmt" Text='<%# Eval("SGSTAmt")%>'></asp:Label>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <asp:Label runat="server" ID="lblIGSTPer" Text='<%# Eval("IGSTPer")%>'></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" ID="lblIGSTAmt" Text='<%# Eval("IGSTAmt")%>'></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:Label runat="server" ID="lblLineTol" Text='<%# Eval("LineTotal")%>'></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>' 
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>                                
			                                </asp:Repeater><!-- /.table -->
                                        </div><!-- /.mail-box-messages -->
                                    </div><!-- /.box-body -->
                                </div>
                            </div><!-- /.col -->
                        </div>

                        <div class="row">
                            <div class="col-md-8"></div>
                            <div class="col-md-2">
                                <label for="exampleInputName">Total Qty</label>
                                <asp:TextBox ID="txtTotQty" runat="server" class="form-control" ></asp:TextBox>
                            </div>

                            <div class="col-md-2" runat="server" visible="false">
                                <label for="exampleInputName">Round Off</label>
                                <asp:TextBox ID="txtRoundOff" runat="server" class="form-control" ></asp:TextBox>
                            </div>

                            <div class="col-md-2">
                                <label for="exampleInputName">Total Amount</label>
                                <asp:TextBox ID="txtTotAmt" runat="server" class="form-control" ></asp:TextBox>
                            </div>
                       </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary"  runat="server" Text="Save" ValidationGroup="Validate_Field" 
                                OnClick="btnSave_Click"/>
                            <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnBack" class="btn btn-default" runat="server" Text="Back To List" OnClick="btnBack_Click"/>
                        </div>
                    </div>
                                <!-- /.box-footer-->
                </div>
                            <!-- /.box -->
            </div>
            
        </div>
                   
        </section>
            </ContentTemplate>
        </asp:UpdatePanel>
       
        <!-- /.content -->
    </div>

</asp:Content>

