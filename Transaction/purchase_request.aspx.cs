﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_purchase_request : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionPurRequestNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal ReqQty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Purchase Request";
            Initial_Data_Referesh();
            Load_Data_Empty_DeptCode();
            Load_Data_Empty_Generator_Model();
            Load_Data_Empty_Production_Part_Name();
            Load_Data_Empty_ItemCode();
            ddlPurType_SelectedIndexChanged(sender, e);

            if (Session["Pur_Request_No"] == null)
            {
                SessionPurRequestNo = "";

            }
            else
            {
                btnBackReqApprv.Visible = false;
                SessionPurRequestNo = Session["Pur_Request_No"].ToString();
                txtPurRequestNo.Text = SessionPurRequestNo;
                btnSearch_Click(sender, e);


            }

            if (Session["Pur_RequestNo_Approval"] == null)
            {
                SessionPurRequestNoApproval = "";

                btnBackReqApprv.Visible = false;
            }
            else
            {
                SessionPurRequestNoApproval = Session["Pur_RequestNo_Approval"].ToString();
                txtPurRequestNo.Text = SessionPurRequestNoApproval;
                btnSearchView_Click(sender, e);
            }
        }
        Load_OLD_data();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
        if (txtDeptCode.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Department..');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Department Code...');", true);
        }
       

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Purchase Request", SessionFinYearVal,"1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin..');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtPurRequestNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (btnSave.Text == "Update")
        {
            SSQL = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "' And Status='1'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details get Approved Cannot Update it..');", true);
            }
        }



        if (!ErrFlag)
        {
            SSQL = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                SSQL = "Delete from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "Delete from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                

            }

            //Response.Write(strValue);
            //Insert Main Table
            SSQL = "Insert Into Pur_Request_Main(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Request_No,Pur_Request_Date,DeptCode,DeptName,";
            SSQL = SSQL + " Others,TotalReqQty,UserID,UserName,Generator_Model,Generator_ModelName,Prdn_Part_Code,Prdn_Part_Name)";
            SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            SSQL = SSQL + " '" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "',";
            SSQL = SSQL + " '" + txtDate.Text + "','" + txtDeptCode.SelectedValue + "','" + txtDeptName.Text + "',";
            SSQL = SSQL + " '" + txtOthers.Text + "','" + ReqQty + "','" + SessionUserID + "','" + SessionUserName + "',";
            SSQL = SSQL + " '" + txtGenerator_Model.SelectedValue + "','" + txtGenerator_Model.SelectedItem.Text + "',";
            SSQL = SSQL + " '" + txtProduction_Part_Name.SelectedValue + "','" + txtProduction_Part_Name.SelectedItem.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //Get Some Details

                string Stock_Qty = "0";
                string Last_Issue_Date = "";
                string Last_Purchase_Date = "";
                string Last_Purchase_Qty = "0";
                string Prev_Purchase_Rate = "0";
                DataTable DT_Val = new DataTable();
                ////Get Stock Qty
                //SSQL = "select * from Stock_Current_All where ItemName='" + dt.Rows[i]["ItemName"].ToString() + "' And FinYearCode='" + SessionFinYearCode + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                //DT_Val = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (DT_Val.Rows.Count != 0)
                //{
                //    Stock_Qty = DT_Val.Rows[0]["Stock_Qty"].ToString();
                //}
                ////Get Last Issue Date
                //SSQL = "select * from Meterial_Issue_Main_Sub where ItemName='" + dt.Rows[i]["ItemName"].ToString() + "' And FinYearCode='" + SessionFinYearCode + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' Order by Mat_Issue_No Desc";
                //DT_Val = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (DT_Val.Rows.Count != 0)
                //{
                //    Last_Issue_Date = DT_Val.Rows[0]["Mat_Issue_Date"].ToString();
                //}
                ////Get Last_Purchase_Date
                //SSQL = "select * from Pur_Order_Receipt_Main_Sub where ItemName='" + dt.Rows[i]["ItemName"].ToString() + "' And FinYearCode='" + SessionFinYearCode + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' Order by PO_Receipt_No Desc";
                //DT_Val = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (DT_Val.Rows.Count != 0)
                //{
                //    Last_Purchase_Date = DT_Val.Rows[0]["PO_Receipt_Date"].ToString();
                //    Last_Purchase_Qty = DT_Val.Rows[0]["ReceivedQty"].ToString();
                //    Prev_Purchase_Rate = DT_Val.Rows[0]["ItemRate"].ToString();
                //}
                //else
                //{
                //    SSQL = "select * from Unplanned_Receipt_Main_Sub where ItemName='" + dt.Rows[i]["ItemName"].ToString() + "' And FinYearCode='" + SessionFinYearCode + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' Order by UnPlan_Recp_No Desc";
                //    DT_Val = objdata.RptEmployeeMultipleDetails(SSQL);
                //    if (DT_Val.Rows.Count != 0)
                //    {
                //        Last_Purchase_Date = DT_Val.Rows[0]["UnPlan_Recp_Date"].ToString();
                //        Last_Purchase_Qty = DT_Val.Rows[0]["ReceivedQty"].ToString();
                //        Prev_Purchase_Rate = DT_Val.Rows[0]["ItemRate"].ToString();
                //    }
                //}


                
                SSQL = "Insert Into Pur_Request_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Request_No,Pur_Request_Date,ItemCode,ItemName,UOMCode,ReuiredQty,ReuiredDate,ItemRemarks,UserID,UserName,Stock_Qty,Last_Issue_Date,Last_Purchase_Date,Last_Purchase_Qty,Prev_Purchase_Rate,Item_Rate) Values('" + SessionCcode + "',";
                SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["ReuiredQty"].ToString() + "','" + dt.Rows[i]["ReuiredDate"].ToString() + "','" + dt.Rows[i]["ItemRemarks"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "',";
                SSQL = SSQL + " '" + Stock_Qty + "','" + Last_Issue_Date + "','" + Last_Purchase_Date + "','" + Last_Purchase_Qty + "','" + Prev_Purchase_Rate + "','" + dt.Rows[i]["Item_Rate"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Updated Successfully');", true);
            }
            Clear_All_Field();

            Response.Redirect("purchase_request_main.aspx");

            //Session["Pur_Request_No"] = txtPurRequestNo.Text;
            //btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtPurRequestNo.Text = ""; txtDate.Text = ""; txtDeptCode.SelectedValue = "-Select-"; txtDeptName.Text = "";
        txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";txtGenerator_Model.SelectedIndex = 0;
        txtReuiredQty.Text = "";txtItemRemarks.Text = "";
        txtItemRate.Text = "0";
        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Pur_Request_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";

        if (txtReuiredQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Reuired Qty...');", true);
        }
        //check with Item Code And Item Name 
        SSQL = "Select * from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Mat_No='" + txtItemCode.Text + "' And Raw_Mat_Name='" + txtItemName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with BOM Material Details..');", true);
        }

        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = qry_dt.Rows[0]["UOM_Full"].ToString();
            string Item_Rate_Str = qry_dt.Rows[0]["Amount"].ToString();
            txtItemRate.Text = Item_Rate_Str;

            txtGenerator_Model.SelectedItem.Text = qry_dt.Rows[0]["GenModelName"].ToString();
            txtGenerator_Model.SelectedValue = qry_dt.Rows[0]["GenModelNo"].ToString();
            //txtProduction_Part_Name.SelectedValue = qry_dt.Rows[0]["ProductionPartNo"].ToString();

            string Partval = qry_dt.Rows[0]["ProductionPartNo"].ToString();

            txtProduction_Part_Name.SelectedItem.Text = qry_dt.Rows[0]["ProductionPartName"].ToString();
             
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCode.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = txtItemCode.Text;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["ReuiredQty"] = txtReuiredQty.Text;
                    dr["ReuiredDate"] = txtReuiredDate.Text;
                    dr["ItemRemarks"] = txtItemRemarks.Text;
                    dr["Item_Rate"] = txtItemRate.Text;
                    
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    TotalReqQty();

                    txtItemCode.Text = ""; txtItemName.Text = ""; txtReuiredQty.Text = ""; txtReuiredDate.Text = "";
                    //txtDate.Text = ""; txtDeptCode.SelectedValue = "-Select-"; txtOthers.Text = "";
                    //txtGenerator_Model.SelectedValue = "-Select-"; txtProduction_Part_Name.SelectedValue = "-Select-";
                    //txtItemNameSelect.SelectedValue = "-Select-"; txtItemRemarks.Text = "";

                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = txtItemCode.Text;
                dr["ItemName"] = txtItemName.Text;
                dr["UOMCode"] = UOM_Code_Str;
                dr["ReuiredQty"] = txtReuiredQty.Text;
                dr["ReuiredDate"] = txtReuiredDate.Text;
                dr["ItemRemarks"] = txtItemRemarks.Text;
                dr["Item_Rate"] = txtItemRate.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                TotalReqQty();

                txtItemCode.Text = ""; txtItemName.Text = ""; txtReuiredQty.Text = ""; txtReuiredDate.Text = "";
                //txtDate.Text = "";txtDeptCode.SelectedItem.Text = "-Select-";txtOthers.Text = "";
                //txtGenerator_Model.SelectedItem.Text = "-Select-";txtProduction_Part_Name.SelectedItem.Text = "-Select-";
                //txtItemNameSelect.SelectedItem.Text = "Select";txtItemRemarks.Text = "";
            }
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRemarks", typeof(string)));
        dt.Columns.Add(new DataColumn("Item_Rate", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        TotalReqQty();

    }
    public void TotalReqQty()
    {

        ReqQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ReqQty = Convert.ToDecimal(ReqQty) + Convert.ToDecimal(dt.Rows[i]["ReuiredQty"]);
        }
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();
            txtDeptCode.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDeptName.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtGenerator_Model.SelectedValue = Main_DT.Rows[0]["Generator_Model"].ToString();
            Load_Data_Empty_Production_Part_Name();
            txtProduction_Part_Name.SelectedValue = Main_DT.Rows[0]["Prdn_Part_Name"].ToString();
            Load_Data_Empty_ItemCode();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();

            
            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            
            SSQL = "Select ItemCode,ItemName,UOMCode,ReuiredQty,ReuiredDate,ItemRemarks,Item_Rate from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            TotalReqQty();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("purchase_request_main.aspx");
    }

    

    protected void btnSearchView_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();
            txtDeptCode.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            //txtDeptCode_SelectedIndexChanged(sender,e);

            txtDeptName.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtGenerator_Model.SelectedValue = Main_DT.Rows[0]["Generator_Model"].ToString();
            
            txtProduction_Part_Name.SelectedValue = Main_DT.Rows[0]["Prdn_Part_Name"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select ItemCode,ItemName,UOMCode,ReuiredQty,ReuiredDate,ItemRemarks from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            TotalReqQty();
            btnSave.Visible = false;
            btnBackEnquiry.Visible = false;
            btnBackReqApprv.Visible = true;

        }
        else
        {
            Clear_All_Field();
        }

    }
    protected void btnBackReqApprv_Click(object sender, EventArgs e)
    {
        Session.Remove("Pur_RequestNo_Approval");
        Session.Remove("Pur_Request_No_Amend_Approval");
        Session.Remove("Pur_Request_No");
        Response.Redirect("purchase_request_approval.aspx");
    }


    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    private void Load_Data_Empty_DeptCode()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtDeptCode.DataSource = Main_DT;
        txtDeptCode.DataBind();

        SSQL = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        
        txtDeptCode.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDeptCode.DataTextField = "DeptName";
        txtDeptCode.DataValueField = "DeptCode";
        txtDeptCode.DataBind();

    }

    private void Load_Data_Empty_Generator_Model()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();


        SSQL = "Select GenModelNo,GenModelName from GeneratorModels where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status='Add'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtGenerator_Model.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["GenModelNo"] = "-Select-";
        dr["GenModelName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtGenerator_Model.DataTextField = "GenModelName";
        txtGenerator_Model.DataValueField = "GenModelNo";
        txtGenerator_Model.DataBind();

    }

    private void Load_Data_Empty_Production_Part_Name()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select ProductNo,ProductName from ProductModel where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + " And GenModelNo='" + txtGenerator_Model.SelectedValue + "' and Status='Add'";

        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //Single Material
        //ALL

        txtProduction_Part_Name.DataSource = Main_DT;

        DataRow dr = Main_DT.NewRow();
        dr["ProductNo"] = "-Select-";
        dr["ProductName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);

        //dr = Main_DT.NewRow();
        //dr["ProductionPartName"] = "ALL";
        //dr["ProductionPartName"] = "ALL";
        //Main_DT.Rows.InsertAt(dr, 1);

        //dr = Main_DT.NewRow();
        //dr["ProductionPartName"] = "Single Material";
        //dr["ProductionPartName"] = "Single Material";
        //Main_DT.Rows.InsertAt(dr, 2);

        txtProduction_Part_Name.DataTextField = "ProductName";
        txtProduction_Part_Name.DataValueField = "ProductNo";
        txtProduction_Part_Name.DataBind();

    }



    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }
    
    private void Load_Data_Empty_ItemCode()
    {

        string SSQL = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        SSQL = "Select Raw_Mat_Name as ItemName,Mat_No as ItemCode from BOMMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        if (txtGenerator_Model.SelectedValue != "-Select-")
        {
            SSQL = SSQL + " And GenModelNo='" + txtGenerator_Model.SelectedValue + "'";
        }
        if (txtProduction_Part_Name.SelectedValue != "-Select-" && txtProduction_Part_Name.SelectedValue != "ALL" && txtProduction_Part_Name.SelectedValue != "Single Material")
        {
            SSQL = SSQL + " And ProductionPartName='" + txtProduction_Part_Name.SelectedItem.Text + "'";
        }
        SSQL = SSQL + " Order by Raw_Mat_Name Asc";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtItemNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemNameSelect.DataTextField = "ItemName";
        txtItemNameSelect.DataValueField = "ItemCode";
        txtItemNameSelect.DataBind();       

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCode.Text = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
        
    }


    protected void txtDeptCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        //txtDeptName.Items.Clear();
        if (txtDeptCode.Text != "" && txtDeptCode.Text != "-Select-")
        {
            SSQL = "Select DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDeptCode.SelectedValue + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < Main_DT.Rows.Count; i++)
            {
                //txtDeptName.Items.Add(Main_DT.Rows[i]["DeptName"].ToString());
                txtDeptName.Text = Main_DT.Rows[i]["DeptName"].ToString();
            }
        }
    }


    

    protected void txtItemNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtItemNameSelect.SelectedValue != "-Select-")
        {
            txtItemCode.Text = txtItemNameSelect.SelectedValue;
            txtItemName.Text = txtItemNameSelect.SelectedItem.Text;
            
        }
    }

    protected void txtGenerator_Model_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_Production_Part_Name();
        Load_Data_Empty_ItemCode();
    }

    protected void txtProduction_Part_Name_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_ItemCode();
        if (txtProduction_Part_Name.SelectedValue != "Single Material")
        {
            Load_BoM_Mat_ItemTable();
        }
    }

    private void Load_BoM_Mat_ItemTable()
    {
        string SSQL = "";
        Initial_Data_Referesh();
        DataTable DT = new DataTable();

        SSQL = "Select Mat_No as ItemCode,Raw_Mat_Name as ItemName,UOM_Full as UOMCode,RequiredQty as ReuiredQty,";
        SSQL = SSQL + " '" + txtDate.Text + "' as ReuiredDate,'" + txtOthers.Text + "' as ItemRemarks,Amount as Item_Rate from BOMMaster ";
        SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " GenModelNo ='" + txtGenerator_Model.SelectedValue + "'";

        if (txtProduction_Part_Name.SelectedValue != "ALL")
        {
            SSQL = SSQL + " And ProductionPartNo='" + txtProduction_Part_Name.SelectedValue + "'";
        }
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ViewState["ItemTable"] = DT;
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
        TotalReqQty();
    }

    protected void ddlPurType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlPurType.SelectedValue.ToString()=="1")
        {
            pnlModel.Visible = false;
            pnlSingle.Visible = true;
        }
        else if (ddlPurType.SelectedValue.ToString() == "2")
        {
            pnlSingle.Visible = false;
            pnlModel.Visible = true;
        }
        else
        {
            pnlSingle.Visible = false;
            pnlModel.Visible = false;
        }
    }
}