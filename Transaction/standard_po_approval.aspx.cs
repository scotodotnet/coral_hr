﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_standard_po_approval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Purchase Order Approval";
            //Department_Code_Add();
            purchase_Request_Status_Add();
            //Department_Code_Add_Test();
        }
        Load_Data_Empty_Grid();
    }

    protected void GridApproveRequestClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Purchase Order Approval");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('You do not have Rights to Approved Purchase Request..');", true);
        }
        //User Rights Check End


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' And PO_Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Already Cancelled.. Cannot Approved it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' And PO_Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Already get Approved..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' And PO_Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table
                query = "Update Std_Purchase_Order_Main set PO_Status='1' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);


                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Approved Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order not is Avail give input correctly..');", true);
            }
        }
    }



    protected void GridCancelRequestClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Purchase Order Approval");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('You do not have Rights to Approved Purchase Request..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' And PO_Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Already Approved.. Cannot Cancelled it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' And PO_Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Already get Cancelled..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "' And PO_Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table
                query = "Update Std_Purchase_Order_Main set PO_Status='2' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);


                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order Details Cancelled Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Purchase Order not is Avail give input correctly..');", true);
            }
        }
    }



    private void Load_Data_Empty_Grid()
    {

        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();


        DT.Columns.Add("Std_PO_No");
        DT.Columns.Add("Std_PO_Date");
        DT.Columns.Add("Supp_Name");
        DT.Columns.Add("Pur_Request_No");

        if (txtRequestStatus.Text == "Pending List")
        {
            query = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (PO_Status = '0' or PO_Status is null) Order By Std_PO_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_Pending.DataSource = DT;
            Repeater_Pending.DataBind();
            Repeater_Pending.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            query = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Status = '1' Order By Std_PO_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
            Repeater_Approve.Visible = true;
            Repeater_Pending.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            query = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Status = '2' Order By Std_PO_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_Rejected.DataSource = DT;
            Repeater_Rejected.DataBind();
            Repeater_Rejected.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Pending.Visible = false;
        }
    }

    //private void Department_Code_Add()
    //{
    //    string query = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDepartment.Items.Clear();
    //    query = "Select (DeptName + '-' + DeptCode) as DpetName_Join from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptName Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(query);
    //    txtDepartment.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDepartment.Items.Add(Main_DT.Rows[i]["DpetName_Join"].ToString());
    //    }
    //}

    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }

    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

        //string[] Department_Spilit = txtDepartment.Text.Split('-');
        //string Department_Code = "";
        //string Department_Name = "";
        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        DT.Columns.Add("Std_PO_No");
        DT.Columns.Add("Std_PO_Date");
        DT.Columns.Add("Supp_Name");
        DT.Columns.Add("Pur_Request_No");



        if (txtRequestStatus.Text == "Pending List")
        {
            query = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (PO_Status = '0' or PO_Status is null) Order By Std_PO_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_Pending.DataSource = DT;
            Repeater_Pending.DataBind();
            Repeater_Pending.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            query = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Status = '1' Order By Std_PO_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
            Repeater_Approve.Visible = true;
            Repeater_Pending.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            query = "Select Std_PO_No,Std_PO_Date,Supp_Name,Pur_Request_No from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Status = '2' Order By Std_PO_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_Rejected.DataSource = DT;
            Repeater_Rejected.DataBind();
            Repeater_Rejected.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Pending.Visible = false;
        }
    }

    //protected void txtDepartment_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (txtDepartment.Text != "-Select-")
    //    {
    //        txtRequestStatus_SelectedIndexChanged(sender, e);
    //    }
    //}

    //private void Department_Code_Add_Test()
    //{
    //    string query = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDeptCode_test.Items.Clear();
    //    query = "Select DeptCode from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptCode Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(query);
    //    txtDeptCode_test.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDeptCode_test.Items.Add(Main_DT.Rows[i]["DeptCode"].ToString());
    //    }
    //}


    protected void GridEditPurRequestClick(object sender, CommandEventArgs e)
    {
        string Enquiry_No_Str = e.CommandName.ToString();
        Session.Remove("Pur_RequestNo_Approval");
        Session.Remove("Transaction_No");
        Session.Remove("Std_PO_No");
        Session["Std_PO_No"] = Enquiry_No_Str;
        Response.Redirect("standard_po.aspx");
    }
}