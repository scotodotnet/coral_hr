﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_standard_po : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionStdPOOrderNo;
    string SessionStdPOOrderNot;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    static Decimal OrderQty;
    string NameTypeRequest;
    string SessionPurRequestNoApproval;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Purchase Order";            
            Initial_Data_Referesh();
            Delivery_Mode_Add();
            Load_Data_Empty_SuppRefDocNo();
            Load_Data_Empty_Dept();
            Load_Data_GST();
            Load_TaxData();
            Load_Data_Empty_Supp1();
            

            if (Session["Std_PO_No"] == null)
            {
                SessionStdPOOrderNo = "";

                RdpPOType.SelectedValue = "2";
                Load_Data_Purchase_Request_No();
                Load_Data_Empty_ItemCode();

            }
            else
            {
                SessionStdPOOrderNo = Session["Std_PO_No"].ToString();
                txtStdOrderNo.Text = SessionStdPOOrderNo;
                Load_Data_Purchase_Request_No();
                Load_Data_Empty_ItemCode();
                btnSearch_Click(sender, e);


            }
            if (Session["Transaction_No"] == null)
            {
                SessionStdPOOrderNot = "";

                //RdpPOType.SelectedValue = "1";
            }
            else
            {
                SessionStdPOOrderNot = Session["Transaction_No"].ToString();
                btnStd_Purchase_Click(sender, e);

                RdpPOType.SelectedValue = "2";
            }

            if (Session["Pur_RequestNo_Approval"] == null)
            {
                SessionPurRequestNoApproval = "";

            }
            else
            {
                RdpPOType.SelectedValue = "3";
                SessionPurRequestNoApproval = Session["Pur_RequestNo_Approval"].ToString();
                txtStdOrderNo.Text = SessionPurRequestNoApproval;
                btnSearchView_Click(sender, e);
            }
        }
        Load_OLD_data();
    }

    private void Load_Data_GST()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtGST_Type.Items.Clear();
        SSQL = "Select *from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtGST_Type.DataSource = Main_DT;
        //DataRow dr = Main_DT.NewRow();
        //dr["GST_Type"] = "-Select-";
        //dr["GST_Type"] = "-Select-";
        //Main_DT.Rows.InsertAt(dr, 0);
        txtGST_Type.DataTextField = "GST_Type";
        txtGST_Type.DataValueField = "GST_Type";
        txtGST_Type.DataBind();

    }

    private void Load_Data_Purchase_Request_No()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtRequestNo.Items.Clear();

        DataTable da_Customer = new DataTable();
        DataTable da_Order = new DataTable();
        string Cus_Po = "";
        string Bag_PO;
        DataTable dt = new DataTable();
        dt.Columns.Add("Pur_Request_No", typeof(string));

        if (Session["Std_PO_No"] == null)
        {
            SSQL = "Select PRM.Pur_Request_No,PRS.ItemCode,PRS.ReuiredQty from Pur_Request_Main_Sub PRS inner join Pur_Request_Main PRM on PRM.Ccode=PRS.Ccode";
            SSQL = SSQL + " And PRM.Lcode=PRS.Lcode And PRM.FinYearCode=PRS.FinYearCode And PRM.Pur_Request_No=PRS.Pur_Request_No where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And PRM.Ccode='" + SessionCcode + "' And PRM.Lcode='" + SessionLcode + "' And PRM.FinYearCode='" + SessionFinYearCode + "' And PRM.Status='1'";
            SSQL = SSQL + " Order by Pur_Request_No Asc";
            da_Customer = objdata.RptEmployeeMultipleDetails(SSQL);
            if (da_Customer.Rows.Count != 0)
            {
                for (int i = 0; i < da_Customer.Rows.Count; i++)
                {
                    string Req_No = ""; string Req_Item = ""; string Req_Qty = "0";
                    Req_No = da_Customer.Rows[i]["Pur_Request_No"].ToString();
                    Req_Item = da_Customer.Rows[i]["ItemCode"].ToString();
                    Req_Qty = da_Customer.Rows[i]["ReuiredQty"].ToString();

                    SSQL = "Select isnull(Sum(OrderQty),0) as PO_Qty from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + Req_No + "'";
                    SSQL = SSQL + " And ItemCode='" + Req_Item + "'";
                    da_Order = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (da_Order.Rows.Count != 0)
                    {
                        Bag_PO = da_Order.Rows[0]["PO_Qty"].ToString();
                        if (Convert.ToDecimal(Bag_PO.ToString()) < Convert.ToDecimal(Req_Qty))
                        {
                            DataRow row = dt.NewRow();
                            row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                            dt.Rows.Add(row);
                        }
                    }
                    else
                    {
                        DataRow row = dt.NewRow();
                        row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                        dt.Rows.Add(row);
                    }
                }
            }

        }
        else
        {
            SSQL = "Select PRM.Pur_Request_No,PRS.ItemCode,PRS.ReuiredQty from Pur_Request_Main_Sub PRS inner join Pur_Request_Main PRM on PRM.Ccode=PRS.Ccode";
            SSQL = SSQL + " And PRM.Lcode=PRS.Lcode And PRM.FinYearCode=PRS.FinYearCode And PRM.Pur_Request_No=PRS.Pur_Request_No where PRS.Ccode='" + SessionCcode + "' And PRS.Lcode='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And PRM.Ccode='" + SessionCcode + "' And PRM.Lcode='" + SessionLcode + "' And PRM.FinYearCode='" + SessionFinYearCode + "' And PRM.Status='1'";
            SSQL = SSQL + " Order by Pur_Request_No Asc";
            da_Customer = objdata.RptEmployeeMultipleDetails(SSQL);
            if (da_Customer.Rows.Count != 0)
            {
                for (int i = 0; i < da_Customer.Rows.Count; i++)
                {
                    string Req_No = ""; string Req_Item = ""; string Req_Qty = "0";
                    Req_No = da_Customer.Rows[i]["Pur_Request_No"].ToString();
                    Req_Item = da_Customer.Rows[i]["ItemCode"].ToString();
                    Req_Qty = da_Customer.Rows[i]["ReuiredQty"].ToString();

                    SSQL = "Select isnull(Sum(OrderQty),0) as PO_Qty from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + Req_No + "'";
                    SSQL = SSQL + " And ItemCode='" + Req_Item + "' And Std_PO_No <> '" + txtStdOrderNo.Text + "'";
                    da_Order = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (da_Order.Rows.Count != 0)
                    {
                        Bag_PO = da_Order.Rows[0]["PO_Qty"].ToString();
                        if (Convert.ToDecimal(Bag_PO.ToString()) < Convert.ToDecimal(Req_Qty))
                        {
                            DataRow row = dt.NewRow();
                            row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                            dt.Rows.Add(row);
                        }
                    }
                    else
                    {
                        DataRow row = dt.NewRow();
                        row["Pur_Request_No"] = Req_No; // want to use form.XYZ?
                        dt.Rows.Add(row);
                    }
                }
            }
        }
        if (dt.Rows.Count !=0)
        {
            Main_DT = RemoveDuplicatesRecords(dt);
        }
        else
        {
            Main_DT = dt;
        }
        
        txtRequestNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Pur_Request_No"] = "-Select-";
        dr["Pur_Request_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtRequestNo.DataTextField = "Pur_Request_No";
        txtRequestNo.DataValueField = "Pur_Request_No";
        txtRequestNo.DataBind();

    }

    private DataTable RemoveDuplicatesRecords(DataTable dt)
    {
        //Returns just 5 unique rows
        var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
        DataTable dt2 = UniqueRows.CopyToDataTable();
        return dt2;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('You have to add atleast one Item Details..');", true);
        }
        if (txtDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('You have to Select the Date...');", true);
        }
        if (txtSupplierName_Select.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('You have to Select the Supplier Name...');", true);
        }
        if (txtDeliveryDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('You have to Select the Delivery Date...');", true);
        }
        if (txtDeliveryMode.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Select Delivery Mode...');", true);
        }
        if (txtPaymentMode.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Select Payment Mode...');", true);
        }
        if (txtDepartmentName.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Select Department Name...');", true);
        }
        if (txtRequestNo.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Select Purchase Request No...');", true);
        }

        //Check Supplier Name And Quotation No
        SSQL = "Select * from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + txtSuppRefDocNo.Text + "' And SuppCode='" + txtSuppCodehide.Value + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (qry_dt.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Check with Supplier Name And Supp.Ref.Doc.No...');", true);
        //}
        //Check Department Name
        //SSQL = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        //qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (qry_dt.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Check with Department Name...');", true);
        //}

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Purchase Order", SessionFinYearVal,"1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtStdOrderNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (btnSave.Text == "Update")
        {
            SSQL = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "' And PO_Status='1'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Standard Purchase Order Details get Approved Cannot Update it..');", true);
            }
        }


        if (!ErrFlag)
        {
            SSQL = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                SSQL = "Delete from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "Delete from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Delete from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);


            }

            //Response.Write(strValue);
            //Insert Main Table
            SSQL = "Insert Into Std_Purchase_Order_Main(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_No,Std_PO_Date,Supp_Code,Supp_Name,Supp_Qtn_No,Supp_Qtn_Date,Pur_Request_No,Pur_Request_Date,DeliveryMode,";
            SSQL = SSQL + " DeliveryDate,DeliveryAt,PaymentMode,DeptCode,DeptName,PaymentTerms,Description,Note,Others,TotalQuantity,TotalAmt,Discount,TaxPer,TaxAmount,OtherCharge,AddOrLess,NetAmount,UserID,UserName,PO_Type,PO_Status,ModeOfTransfer,ChequeNo,ChequeDate,ChequeAmt,TaxType) Values('" + SessionCcode + "',";
            SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','" + txtSuppCodehide.Value + "','" + txtSupplierName.Text + "',";
            SSQL = SSQL + " '" + txtSuppRefDocNo.Text + "','" + txtDocDate.Text + "','" + txtRequestNo.Text + "','" + txtRequestDate.Text + "','" + txtDeliveryMode.Value + "','" + txtDeliveryDate.Text + "','" + txtDeliveryAt.Text + "',";
            SSQL = SSQL + " '" + txtPaymentMode.Value + "','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtPaymentTerms.Text + "',";
            SSQL = SSQL + " '" + txtDescription.Text + "','" + txtNote.Text + "','" + txtOthers.Text + "','" + OrderQty + "','" + txtTotAmt.Text + "','0','0','0','0','" + txtAddOrLess.Text + "','" + txtNetAmt.Text + "','" + SessionUserID + "','" + SessionUserName + "','" + RdpPOType.SelectedItem.Text + "',";
            if (RdpPOType.SelectedItem.Text == "Special")
            {
                SSQL = SSQL + " '4'";
            }
            else
            {
                SSQL = SSQL + " '0'";
            }
            SSQL = SSQL + ",'" + txtModeOfTransfer.Text + "','" + txtChequeNo.Text + "','" + txtChequeDate.Text + "','" + txtChequeAmt.Text + "','" + RdpTaxType.SelectedValue + "')";

            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SSQL = "Insert Into Std_Purchase_Order_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_No,Std_PO_Date,Pur_Request_No,Pur_Request_Date,ItemCode,ItemName,UOMCode,ReuiredQty,OrderQty,ReuiredDate,Rate,Remarks,ItemTotal,Discount_Per,Discount,";
                SSQL = SSQL + "CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount,OtherCharge,LineTotal,GstPer,UserID,UserName) Values('" + SessionCcode + "',";
                SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','" + txtRequestNo.Text + "','" + txtRequestDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["ReuiredQty"].ToString() + "','" + dt.Rows[i]["OrderQty"].ToString() + "','" + dt.Rows[i]["ReuiredDate"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["Remarks"].ToString() + "','" + dt.Rows[i]["ItemTotal"].ToString() + "','" + dt.Rows[i]["Discount_Per"].ToString() + "','" + dt.Rows[i]["Discount"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["CGSTPer"].ToString() + "','" + dt.Rows[i]["CGSTAmount"].ToString() + "','" + dt.Rows[i]["SGSTPer"].ToString() + "','" + dt.Rows[i]["SGSTAmount"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["IGSTPer"].ToString() + "','" + dt.Rows[i]["IGSTAmount"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["OtherCharge"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + dt.Rows[i]["GstPer"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (RdpPOType.SelectedItem.Text == "Special")
            {
                //Insert Purchase Request Approval Table
                //SSQL = "Insert Into Pur_Request_Approval(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                //SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','Std/PO','" + txtSupplierName.Text + "','" + OrderQty + "','" + txtNetAmt.Text + "',";
                //SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";
                //objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (RdpPOType.SelectedItem.Text == "Direct" || RdpPOType.SelectedItem.Text == "Req/Amend")
            {
                //Insert Purchase Request Approval Table
                SSQL = "Insert Into Std_Purchase_Order_Approve(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','Std/PO','" + txtSupplierName.Text + "','" + OrderQty + "','" + txtNetAmt.Text + "',";
                SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }



            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Standard Purchase Order Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Standard Purchase Order Details Updated Successfully');", true);
            }


            //DataTable dtd_REqNo = new DataTable();

            ////if (!ErrFlag)
            ////{
            //SSQL = "select Pur_Request_No from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "'";
            //SSQL = SSQL + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            //SSQL = SSQL + "  And Std_PO_No='" + txtStdOrderNo.Text + "'";

            //dtd_REqNo = objdata.RptEmployeeMultipleDetails(SSQL);

            //if (dtd_REqNo.Rows.Count > 0)
            //{
            //    string Pur_Req_No = dtd_REqNo.Rows[0]["Pur_Request_No"].ToString();
            //    DataTable Std_Req_Apprv = new DataTable();

            //    DataTable Pur_Req_Apprv = new DataTable();

            //    //Check Purchase Req Item and Purchase Order Item are same code start

            //    if (Pur_Req_No != "")
            //    {

            //        SSQL = "Select *from Std_Purchase_Order_Main_Sub where Pur_Request_No='" + Pur_Req_No + "'";
            //        Std_Req_Apprv = objdata.RptEmployeeMultipleDetails(SSQL);

            //        string ss = Pur_Req_No.Substring(0, 3);

            //        if (ss == "PR/")
            //        {
            //            SSQL = "Select *from Pur_Request_Main_Sub where Pur_Request_No ='" + Pur_Req_No + "'";
            //            Pur_Req_Apprv = objdata.RptEmployeeMultipleDetails(SSQL);
            //        }
            //        else
            //        {
            //            SSQL = "Select *from Pur_Request_Main_Sub where Pur_Request_No ='" + Pur_Req_No + "'";
            //            Pur_Req_Apprv = objdata.RptEmployeeMultipleDetails(SSQL);
            //        }


            //        if (Std_Req_Apprv.Rows.Count == Pur_Req_Apprv.Rows.Count)
            //        {
            //            SSQL = "Update Pur_Request_Approval set PO_Status='1' where Ccode='" + SessionCcode + "'";
            //            SSQL = SSQL + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            //            SSQL = SSQL + "  And Transaction_No='" + Pur_Req_No + "'";
            //            objdata.RptEmployeeMultipleDetails(SSQL);
            //        }
            //        else
            //        {
            //            SSQL = "Update Pur_Request_Approval set PO_Status='0' where Ccode='" + SessionCcode + "'";
            //            SSQL = SSQL + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            //            SSQL = SSQL + "  And Transaction_No='" + Pur_Req_No + "'";
            //            objdata.RptEmployeeMultipleDetails(SSQL);
            //        }
            //    }

            //}

            //Clear_All_Field();
            Session["Std_PO_No"] = txtStdOrderNo.Text;
            btnSave.Text = "Update";
            //Session["Transaction_No"] = txtStdOrderNo.Text;
            //Load_Data_Enquiry_Grid();
            Response.Redirect("standard_po_main.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
        txtGST_Type_SelectedIndexChanged(sender, e);
    }

    private void Clear_All_Field()
    {
        txtStdOrderNo.Text = ""; txtDate.Text = ""; txtSuppCodehide.Value = ""; txtSupplierName.Text = "";
        txtSuppRefDocNo.SelectedItem.Text = "-Select-"; txtDocDate.Text = ""; txtDeliveryMode.Value = ""; txtDeliveryDate.Text = "";
        txtDeliveryAt.Text = ""; txtPaymentMode.Value = ""; txtDepartmentName.SelectedItem.Text = "-Select-";
        txtPaymentTerms.Text = ""; txtDescription.Text = ""; txtNote.Text = ""; txtOthers.Text = "";
        txtRequestNo.Text = ""; txtRequestDate.Text = ""; txtItemCodeHide.Value = ""; txtItemName.Text = "";
        txtRequiredQty.Text = ""; txtOrderQty.Text = ""; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
        txtDiscount.Text = ""; txtTax.Text = "0"; txtOtherCharge.Text = "0"; txtRemarks.Text = "";
        txtTaxAmt.Text = "0"; txtTotAmt.Text = "0.0"; txtNetAmt.Text = "0"; RdpPOType.SelectedValue = "2";
        txtModeOfTransfer.Text = "";
        txtChequeAmt.Text = "";
        txtChequeDate.Text = "";
        txtChequeNo.Text = "";

        txtAddOrLess.Text = "0";

        txtItemTotal.Text = "0.00";
        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
        txtFinal_Total_Amt.Text = "0.00";

        TotalGstPer_Hidden.Value = "0";


        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Std_PO_No");
        Session.Remove("Transaction_No");
        Session.Remove("Pur_RequestNo_Approval");
        //Load_Data_Enquiry_Grid();
        //txtDeptCodeHide.Value = "";
        if (RdpPOType.SelectedValue == "1")
        {
            txtRequestNo.Enabled = false;
            txtRequestDate.Enabled = false;
        }

    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";

        if (txtOrderQty.Text == "0.0" || txtOrderQty.Text == "0" || txtOrderQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Enter the Order Qty...');", true);
        }
        if (txtRate.Text == "0.0" || txtRate.Text == "0" || txtRate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Enter the Rate...');", true);
        }

        if (!ErrFlag)
        {

            if (RdpPOType.SelectedValue == "2")
            {
                // Get RequestType from Type_Request_Amend

                NameTypeRequest = "Request";
                // Check RequestType = Request

                if (NameTypeRequest.Trim() == "Request")
                {
                    //SSQL = "Select * from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "'";
                    //SSQL = SSQL + "And Pur_Request_No='" + txtRequestNo.Text + "'";
                    //qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    //if (qry_dt.Rows.Count != 0)
                    //{
                    //    ErrFlag = true;
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Check with Item Details Already Exists in Purchase Order..');", true);
                    //    txtItemCodeHide.Value = ""; txtItemName.Text = "";
                    //    txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                    //    txtRemarks.Text = "";

                    //}

                    SSQL = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (qry_dt.Rows.Count == 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Check with Purchase Request No..');", true);
                    }

                    //check with Item Code And Item Name

                    SSQL = "Select * from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "'";
                    SSQL = SSQL + "And Pur_Request_No='" + txtRequestNo.Text + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (qry_dt.Rows.Count == 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Check with Item Details..');", true);
                    }

                }

                // Check RequestType = Amend

                

                if (!ErrFlag)
                {
                    //UOM Code get
                    string UOM_Code_Str = qry_dt.Rows[0]["UOMCode"].ToString();
                    string Line_Total = "";
                    Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
                    Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();
                    Total_Calculate();

                    // check view state is not null  
                    if (ViewState["ItemTable"] != null)
                    {
                        //get datatable from view state   
                        dt = (DataTable)ViewState["ItemTable"];

                        //check Item Already add or not
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('This Item Already Added..');", true);
                            }
                        }
                        if (!ErrFlag)
                        {
                            dr = dt.NewRow();

                            dr["ItemCode"] = txtItemCodeHide.Value;
                            dr["ItemName"] = txtItemName.Text;
                            dr["UOMCode"] = UOM_Code_Str;
                            dr["ReuiredQty"] = txtRequiredQty.Text;
                            dr["OrderQty"] = txtOrderQty.Text;
                            dr["ReuiredDate"] = txtRequiredDate.Text;
                            dr["Rate"] = txtRate.Text;
                            dr["Remarks"] = txtRemarks.Text;

                            dr["ItemTotal"] = txtItemTotal.Text;
                            dr["Discount_Per"] = txtDiscount_Per.Text;
                            dr["Discount"] = txtDiscount_Amount.Text;
                            dr["TaxPer"] = txtVAT_Per.Text;
                            dr["TaxAmount"] = txtVAT_AMT.Text;
                            dr["CGSTPer"] = txtCGSTPer.Text;
                            dr["CGSTAmount"] = txtCGSTAmt.Text;
                            dr["SGSTPer"] = txtSGSTPer.Text;
                            dr["SGSTAmount"] = txtSGSTAmt.Text;
                            dr["IGSTPer"] = txtIGSTPer.Text;
                            dr["IGSTAmount"] = txtIGSTAmt.Text;
                            dr["GstPer"] = TotalGstPer_Hidden.Value;
                            dr["OtherCharge"] = txtOtherCharge.Text;
                            dr["LineTotal"] = txtFinal_Total_Amt.Text;

                            dt.Rows.Add(dr);
                            ViewState["ItemTable"] = dt;
                            Repeater1.DataSource = dt;
                            Repeater1.DataBind();
                            Totalsum();
                            txtItemCodeHide.Value = ""; txtItemName.Text = "";
                            txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                            txtRemarks.Text = "";
                            TotalGstPer_Hidden.Value = "0";
                            txtItemTotal.Text = "0.00";
                            txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                            txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                            txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                            txtFinal_Total_Amt.Text = "0.00";
                            txtVAT_Per.Text = "0"; txtVAT_AMT.Text = "0";
                            txtGST_Type_SelectedIndexChanged(sender, e);
                        }
                        else
                        {
                            if (NameTypeRequest.Trim() == "Request")
                            {
                                SSQL = "Select * from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (qry_dt.Rows.Count != 0)
                                {
                                    txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                    txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                    txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                                }
                            }
                            else if (NameTypeRequest.Trim() == "Amend")
                            {
                                SSQL = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (qry_dt.Rows.Count != 0)
                                {
                                    txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                    txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                    txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                                }

                            }
                        }
                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["ItemCode"] = txtItemCodeHide.Value;
                        dr["ItemName"] = txtItemName.Text;
                        dr["UOMCode"] = UOM_Code_Str;
                        dr["ReuiredQty"] = txtRequiredQty.Text;
                        dr["OrderQty"] = txtOrderQty.Text;
                        dr["ReuiredDate"] = txtRequiredDate.Text;
                        dr["Rate"] = txtRate.Text;
                        dr["Remarks"] = txtRemarks.Text;

                        dr["ItemTotal"] = txtItemTotal.Text;
                        dr["Discount_Per"] = txtDiscount_Per.Text;
                        dr["Discount"] = txtDiscount_Amount.Text;
                        dr["TaxPer"] = txtVAT_Per.Text;
                        dr["TaxAmount"] = txtVAT_AMT.Text;
                        dr["CGSTPer"] = txtCGSTPer.Text;
                        dr["CGSTAmount"] = txtCGSTAmt.Text;
                        dr["SGSTPer"] = txtSGSTPer.Text;
                        dr["SGSTAmount"] = txtSGSTAmt.Text;
                        dr["IGSTPer"] = txtIGSTPer.Text;
                        dr["IGSTAmount"] = txtIGSTAmt.Text;
                        dr["GstPer"] = TotalGstPer_Hidden.Value;
                        dr["OtherCharge"] = txtOtherCharge.Text;
                        dr["LineTotal"] = txtFinal_Total_Amt.Text;

                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();

                        Totalsum();

                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                        txtRemarks.Text = "";
                        TotalGstPer_Hidden.Value = "0";
                        txtItemTotal.Text = "0.00";
                        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                        txtFinal_Total_Amt.Text = "0.00";
                        txtVAT_Per.Text = "0"; txtVAT_AMT.Text = "0";
                        txtGST_Type_SelectedIndexChanged(sender, e);
                    }
                }
                else
                {
                    if (NameTypeRequest.Trim() == "Request")
                    {
                        SSQL = "Select * from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (qry_dt.Rows.Count != 0)
                        {
                            txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                            txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                            txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                        }
                    }
                    else if (NameTypeRequest.Trim() == "Amend")
                    {
                        SSQL = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (qry_dt.Rows.Count != 0)
                        {
                            txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                            txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                            txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                        }

                    }
                }
            }
        }
    }

    public void Totalsum()
    {
        sum = "0";
        OrderQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["LineTotal"])).ToString();
            txtTotAmt.Text = sum;
            OrderQty = Convert.ToDecimal(OrderQty) + Convert.ToDecimal(dt.Rows[i]["OrderQty"]);
        }
        Final_Total_Calculate();

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredQty", typeof(string)));
        dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));

        dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount_Per", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("OtherCharge", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("GstPer", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        Totalsum();
        txtTax.Text = "0";
        txtTaxAmt.Text = "0";
        txtDiscount.Text = "0";
        txtOthers.Text = "0";
        txtNetAmt.Text = "0";
    }

    protected void btnStd_Purchase_Click(object sender, EventArgs e)
    {
        //string SSQL = "";
        //DataTable Main_DT = new DataTable();
        //SSQL = "Select * from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + SessionStdPOOrderNot + "'";
        //Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (Main_DT.Rows.Count != 0)
        //{
        //    txtRequestNo.Text = Main_DT.Rows[0]["Transaction_No"].ToString();
        //    txtRequestDate.Text = Main_DT.Rows[0]["Date"].ToString();
        //}
        //else
        //{
        //    Clear_All_Field();
        //}
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {

            //if (Main_DT.Rows[0]["PO_Status"].ToString() == "1")
            //{
            //    lblStatus.Text = "Approved by : " + Main_DT.Rows[0]["Approvedby"].ToString();
            //}
            //else if (Main_DT.Rows[0]["PO_Status"].ToString() == "2")
            //{
            //    lblStatus.Text = "Cancelled by : " + Main_DT.Rows[0]["Approvedby"].ToString();
            //}
            //else if (Main_DT.Rows[0]["PO_Status"].ToString() == "3" || Main_DT.Rows[0]["PO_Status"].ToString() == "0" || Main_DT.Rows[0]["PO_Status"].ToString() == "5" || Main_DT.Rows[0]["PO_Status"].ToString() == "")
            //{
            //    lblStatus.Text = "Pending..";
            //}
            //else if (Main_DT.Rows[0]["PO_Status"].ToString() == "6")
            //{
            //    lblStatus.Text = "Rejected by FM..";
            //}


            txtDate.Text = Main_DT.Rows[0]["Std_PO_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSupplierName_Select.SelectedValue = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSuppRefDocNo.SelectedItem.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
            txtDeliveryMode.Value = Main_DT.Rows[0]["DeliveryMode"].ToString();
            txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
            txtPaymentMode.Value = Main_DT.Rows[0]["PaymentMode"].ToString();
            //txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            //txtDiscount.Text = Main_DT.Rows[0]["Discount"].ToString();
            txtTax.Text = Main_DT.Rows[0]["TaxPer"].ToString();
            //txtTaxAmt.Text = Main_DT.Rows[0]["TaxAmount"].ToString();
            txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            txtRequestNo.SelectedValue = Main_DT.Rows[0]["Pur_Request_No"].ToString();
            txtRequestDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();
            if (Main_DT.Rows[0]["PO_Type"].ToString() == "Special")
            {
                RdpPOType.SelectedValue = "3";
            }
            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Direct")
            {
                RdpPOType.SelectedValue = "1";
            }
            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Req/Amend")
            {
                RdpPOType.SelectedValue = "2";
            }
            txtModeOfTransfer.Text = Main_DT.Rows[0]["ModeOfTransfer"].ToString();
            txtChequeNo.Text = Main_DT.Rows[0]["ChequeNo"].ToString();
            txtChequeDate.Text = Main_DT.Rows[0]["ChequeDate"].ToString();
            txtChequeAmt.Text = Main_DT.Rows[0]["ChequeAmt"].ToString();
            RdpTaxType.SelectedValue = Main_DT.Rows[0]["TaxType"].ToString();

            //Std_Purchase_Order_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select *from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("standard_po_main.aspx");
    }

    private void Delivery_Mode_Add()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        txtDeliveryMode.Items.Clear();
        SSQL = "Select * from MstDeliveryMode where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeliveryMode Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtDeliveryMode.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtDeliveryMode.Items.Add(Main_DT.Rows[i]["DeliveryMode"].ToString());
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "1", "Purchase Order");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('You do not have Rights to Approve Std Purchase Order...');", true);
        }
        //User Rights Check End

        SSQL = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('First You have to Register this Std Purchase Order Details..');", true);
        }

        SSQL = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "' And PO_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Std Purchase Order Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            SSQL = "Update Std_Purchase_Order_Main set PO_Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + "  And Std_PO_No='" + txtStdOrderNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);


            SSQL = "Update Std_Purchase_Order_Approve set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);


            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window","alert('Std Purchase Order Details Approved Successfully..');", true);
        }
    }

    protected void txtTax_TextChanged(object sender, EventArgs e)
    {
        txtTaxAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) * (Convert.ToDecimal(txtTax.Text) / 100)).ToString();

        txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }


    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }


    protected void btnSearchView_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {


            txtDate.Text = Main_DT.Rows[0]["Std_PO_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSuppRefDocNo.SelectedItem.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
            txtDeliveryMode.Value = Main_DT.Rows[0]["DeliveryMode"].ToString();
            txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
            txtPaymentMode.Value = Main_DT.Rows[0]["PaymentMode"].ToString();
            //txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            //txtDiscount.Text = Main_DT.Rows[0]["Discount"].ToString();
            txtTax.Text = Main_DT.Rows[0]["TaxPer"].ToString();
            //txtTaxAmt.Text = Main_DT.Rows[0]["TaxAmount"].ToString();
            txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            txtRequestNo.Text = Main_DT.Rows[0]["Pur_Request_No"].ToString();
            txtRequestDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();

            if (Main_DT.Rows[0]["PO_Type"].ToString() == "Special")
            {
                RdpPOType.SelectedValue = "3";
            }
            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Direct")
            {
                RdpPOType.SelectedValue = "1";
            }
            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Req/Amend")
            {
                RdpPOType.SelectedValue = "2";
            }


            //Std_Purchase_Order_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select *from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Visible = false;
        }
        else
        {
            Clear_All_Field();
        }
    }

    
    private void Load_Data_Empty_Supp1()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        SSQL = "Select LedgerCode,LedgerName from Acc_Mst_Ledger where Ccode='" + SessionCcode + "' And Lcode ='" + SessionLcode + "' And ";
        SSQL = SSQL + " Status='Add' And LedgerGrpName='Supplier' ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtSupplierName_Select.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["LedgerCode"] = "-Select-";
        dr["LedgerName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        txtSupplierName_Select.DataTextField = "LedgerName";
        txtSupplierName_Select.DataValueField = "LedgerCode";
        txtSupplierName_Select.DataBind();
    }

    protected void txtSupplierName_Select_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtSupplierName_Select.SelectedValue != "-Select-")
        {
            txtSuppCodehide.Value = txtSupplierName_Select.SelectedValue;
            txtSupplierName.Text = txtSupplierName_Select.SelectedItem.Text;
        }
    }


    protected void btnSuppRefDocNo_Click(object sender, EventArgs e)
    {
        //modalPop_SuppRefDocNo.Show();
    }

    private void Load_Data_Empty_SuppRefDocNo()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        SSQL = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And SuppCode='" + txtSuppCodehide.Value + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtSuppRefDocNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["QuotNo"] = "-Select-";
        //dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtSuppRefDocNo.DataTextField = "QuotNo";
        //txtDeptCode.DataValueField = "DeptCode";
        txtSuppRefDocNo.DataBind();

    }

    protected void GridViewClick_SuppRefDocNo(object sender, CommandEventArgs e)
    {
        txtSuppRefDocNo.Text = Convert.ToString(e.CommandArgument);
        txtDocDate.Text = Convert.ToString(e.CommandName);
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }

    private void Load_Data_Empty_Dept()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        SSQL = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        // txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        // txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }
    
    private void Load_Data_Empty_ItemCode()
    {
        string SSQL = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        string ss = "";
        string PO_Type = "";
        string Request_No = "";

        PO_Type = RdpPOType.SelectedValue;
        Request_No = txtRequestNo.Text;

        ss = Request_No.Substring(0, 3);
        
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");
        DT.Columns.Add("ReuiredQty");
        DT.Columns.Add("ReuiredDate");

        DataTable DT1 = new DataTable();
        DT1.Columns.Add("ItemCode");
        DT1.Columns.Add("ItemName");

        SSQL = "Select ItemCode,ItemName,ReuiredQty,ReuiredDate from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And Pur_Request_No='" + Request_No + "'";
        
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtItemNameSelect.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtItemNameSelect.DataTextField = "ItemName";
        txtItemNameSelect.DataValueField = "ItemCode";
        txtItemNameSelect.DataBind();
    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
        
    }
    protected void txtItemNameSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtItemNameSelect.SelectedValue != "-Select-")
        {
            txtItemCodeHide.Value = txtItemNameSelect.SelectedValue;
            txtItemName.Text = txtItemNameSelect.SelectedItem.Text;

            string SSQL = "";
            string Comp_Code = "";
            string Loc_Code = "";
            string Fin_Year_Code = "";
            string ss = "";
            string PO_Type = "";
            string Request_No = "";
            DataTable DT = new DataTable();


            PO_Type = RdpPOType.SelectedValue;
            Request_No = txtRequestNo.SelectedValue;

            SSQL = "Select ReuiredQty,ReuiredDate,Item_Rate from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And Pur_Request_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count !=0)
            {
                txtRequiredQty.Text = DT.Rows[0]["ReuiredQty"].ToString();
                txtRequiredDate.Text = DT.Rows[0]["ReuiredDate"].ToString();
                txtOrderQty.Text = DT.Rows[0]["ReuiredQty"].ToString();
                txtRate.Text = DT.Rows[0]["Item_Rate"].ToString();
            }

            //Get TAX Percentage
            string CGST_Per = "0";
            string SGST_Per = "0";
            string IGST_Per = "0";
            string VAT_Per = "0";
            SSQL = "Select * from BOMMaster Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Raw_Mat_Name='" + txtItemNameSelect.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                CGST_Per = DT.Rows[0]["CGSTP"].ToString();
                SGST_Per = DT.Rows[0]["SGSTP"].ToString();
                IGST_Per = DT.Rows[0]["IGSTP"].ToString();
                VAT_Per = DT.Rows[0]["VATP"].ToString();
            }

            if (RdpTaxType.SelectedValue == "1") { IGST_Per = "0";VAT_Per = "0"; }
            if (RdpTaxType.SelectedValue == "2") { CGST_Per = "0"; SGST_Per = "0"; VAT_Per = "0"; }
            if (RdpTaxType.SelectedValue == "3") { CGST_Per = "0"; SGST_Per = "0"; IGST_Per = "0"; }
            if (RdpTaxType.SelectedValue == "4") { CGST_Per = "0"; SGST_Per = "0"; IGST_Per = "0"; VAT_Per = "0"; }

            txtCGSTPer.Text = CGST_Per;
            txtSGSTPer.Text = SGST_Per;
            txtIGSTPer.Text = IGST_Per;
            txtVAT_Per.Text = VAT_Per;
            TotalGstPer_Hidden.Value = (Convert.ToDecimal(txtCGSTPer.Text) + Convert.ToDecimal(txtSGSTPer.Text) + Convert.ToDecimal(txtIGSTPer.Text)).ToString();
            TotalGstPer_Hidden.Value = (Convert.ToDecimal(TotalGstPer_Hidden.Value) + Convert.ToDecimal(txtVAT_Per.Text)).ToString();
            Total_Calculate();
        }
    }
    protected void GridViewClick_ItemCode_PR(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        string ss = "";
        string PO_Type = "";
        string Request_No = "";
        DataTable DT = new DataTable();


        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
        

        PO_Type = RdpPOType.SelectedValue;
        Request_No = txtRequestNo.Text;

        if (PO_Type == "2")
        {
            ss = Request_No.Substring(0, 3);
        }

        if (PO_Type == "2")
        {
            if (ss != "PRA")
            {
                SSQL = "Select ReuiredQty,ReuiredDate from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And Pur_Request_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                SSQL = "Select ReuiredQty,ReuiredDate from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And Amend_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);
            }

        }

        txtRequiredQty.Text = DT.Rows[0]["ReuiredQty"].ToString();
        txtRequiredDate.Text = DT.Rows[0]["ReuiredDate"].ToString();
        txtOrderQty.Text = DT.Rows[0]["ReuiredQty"].ToString();

    }
    protected void RdpPOType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdpPOType.SelectedValue == "2") { txtRequestNo.Enabled = true; }
        Load_Data_Empty_ItemCode();
    }

    protected void txtSuppRefDocNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        //txtDeptName.Items.Clear();
        if (txtSuppRefDocNo.Text != "" && txtSuppRefDocNo.Text != "-Select-")
        {
            SSQL = "Select QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And QuotNo='" + txtSuppRefDocNo.Text + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < Main_DT.Rows.Count; i++)
            {
                txtDocDate.Text = Main_DT.Rows[i]["QuotDate"].ToString();
                //txtDeptName.Text = Main_DT.Rows[i]["DeptName"].ToString();
            }
        }
    }
    protected void txtDiscount_Per_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }
    protected void txtGST_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TaxData();
    }

    private void Load_TaxData()
    {
        //string SSQL = "";
        //DataTable DT = new DataTable();
        //SSQL = "Select * from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + txtGST_Type.SelectedValue + "'";
        //DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (DT.Rows.Count != 0)
        //{
        //    txtCGSTPer.Text = DT.Rows[0]["CGST_Percent"].ToString();
        //    txtSGSTPer.Text = DT.Rows[0]["SGST_Percent"].ToString();
        //    txtIGSTPer.Text = DT.Rows[0]["IGST_Percent"].ToString();
        //}
        //else
        //{
        //    txtCGSTPer.Text = "0";
        //    txtSGSTPer.Text = "0";
        //    txtIGSTPer.Text = "0";
        //}
        //TotalGstPer_Hidden.Value = (Convert.ToDecimal(txtCGSTPer.Text) + Convert.ToDecimal(txtSGSTPer.Text) + Convert.ToDecimal(txtIGSTPer.Text)).ToString();

        //Total_Calculate();
    }
    protected void txtOtherCharge_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }

    private void Total_Calculate()
    {
        string Qty_Val = "0";
        string Item_Rate = "0";
        string Item_Total = "0";
        string Discount_Percent = "0";
        string Discount_Amt = "0";
        string VAT_Per = "0";
        string VAT_Amt = "0";
        string Tax_Amt = "0";
        string CGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Per = "0";
        string SGST_Amt = "0";
        string IGST_Per = "0";
        string IGST_Amt = "0";
        string BDUTax_Per = "0";
        string BDUTax_Amt = "0";
        string Other_Charges = "0";
        string Final_Amount = "0";
        string PackingAmt = "0";
        if (txtOrderQty.Text != "") { Qty_Val = txtOrderQty.Text.ToString(); }
        if (txtRate.Text != "") { Item_Rate = txtRate.Text.ToString(); }


        Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();
        Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();
        if (Convert.ToDecimal(Item_Total) != 0)
        {
            if (Convert.ToDecimal(txtDiscount_Per.Text.ToString()) != 0) { Discount_Percent = txtDiscount_Per.Text.ToString(); }
            if (Convert.ToDecimal(txtOtherCharge.Text.ToString()) != 0) { Other_Charges = txtOtherCharge.Text.ToString(); }
            if (Convert.ToDecimal(txtCGSTPer.Text.ToString()) != 0) { CGST_Per = txtCGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtSGSTPer.Text.ToString()) != 0) { SGST_Per = txtSGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtIGSTPer.Text.ToString()) != 0) { IGST_Per = txtIGSTPer.Text.ToString(); }

            if (Convert.ToDecimal(txtVAT_Per.Text.ToString()) != 0) { VAT_Per = txtVAT_Per.Text.ToString(); }

            //Discount Amt Calculate
            if (Convert.ToDecimal(Discount_Percent) != 0)
            {
                Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
            }
            else
            {
                Discount_Amt = "0.00";
            }

            if (Convert.ToDecimal(CGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                CGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(VAT_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                VAT_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(VAT_Per)).ToString();
                VAT_Amt = (Convert.ToDecimal(VAT_Amt) / Convert.ToDecimal(100)).ToString();
                VAT_Amt = (Math.Round(Convert.ToDecimal(VAT_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
            }
            else
            {
                VAT_Amt = "0.00";
            }

            if (Convert.ToDecimal(SGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
            }
            else
            {
                SGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(IGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                IGST_Amt = "0.00";
            }

            //Other Charges
            if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

            //Final Amt
            Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
            //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(VAT_Amt)).ToString();
            Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

            txtItemTotal.Text = Item_Total;
            txtDiscount_Amount.Text = Discount_Amt;
            txtTaxAmt.Text = Tax_Amt;
            txtCGSTAmt.Text = CGST_Amt;
            txtSGSTAmt.Text = SGST_Amt;
            txtIGSTAmt.Text = IGST_Amt;
            txtVAT_AMT.Text = VAT_Amt;
            txtFinal_Total_Amt.Text = Final_Amount;
            txtNetAmt.Text = Final_Amount;
        }
    }
    private void Final_Total_Calculate()
    {

        string Final_NetAmt = "0";
        string AddorLess = "0";
        string Item_Total_Amt = txtTotAmt.Text;

        if (Convert.ToDecimal(txtAddOrLess.Text.ToString()) != 0) { AddorLess = txtAddOrLess.Text.ToString(); }
        Final_NetAmt = (Convert.ToDecimal(AddorLess) + Convert.ToDecimal(Item_Total_Amt)).ToString();
        //txtFinal_Total_Amt.Text = Final_NetAmt;
        txtNetAmt.Text = Final_NetAmt;

    }

    protected void txtAddOrLess_TextChanged(object sender, EventArgs e)
    {
        if (txtAddOrLess.Text.ToString() != "") { Final_Total_Calculate(); }

    }
    protected void txtOrderQty_TextChanged(object sender, EventArgs e)
    {
        if (txtOrderQty.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        if (txtRate.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtRequestNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtRequestNo.SelectedValue != "-Select-")
        {
            string SSQL = "";
            DataTable DT_DA = new DataTable();
            SSQL = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Pur_Request_No='" + txtRequestNo.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
            DT_DA = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_DA.Rows.Count != 0)
            {
                txtRequestDate.Text = DT_DA.Rows[0]["Pur_Request_Date"].ToString();

            }
        }
        Load_Data_Empty_ItemCode();
        All_Item_Added_DataTable(sender,e);
    }
    

    protected void btnPO_Preview_Click(object sender, EventArgs e)
    {
        string RptName = "";
        string StdPurOrdNo = "";
        string SupQtnNo = "";
        string DeptName = "";

        RptName = "Standard Purchase Order Details Invoice Format";
        StdPurOrdNo = txtStdOrderNo.Text.ToString();

        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + "" + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + "" + "&FromDate=" + "" + "&ToDate=" + "" + "&RptName=" + RptName, "_blank", "");

    }

    private void All_Item_Added_DataTable(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select ItemCode,ItemName,ReuiredQty,ReuiredDate from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And Pur_Request_No='" + txtRequestNo.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        for (int i = 0; i < DT.Rows.Count; i++)
        {
            txtItemNameSelect.SelectedValue = DT.Rows[i]["ItemCode"].ToString();
            txtItemNameSelect_SelectedIndexChanged(sender, e);
            btnAddItem_Click(sender, e);
        }
        txtItemNameSelect.SelectedIndex = 0;

    }
}