﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Trans_GoodsReturn_Sub : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGRNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal RetQty;
    static Decimal ItemTot = 0;
    static Decimal LineTot = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "CORAL ERP :: Goods Return";
            Initial_Data_Referesh();
            Load_GoodsReceived_No();

            if (Session["GRetNo"] == null)
            {
                SessionGRNo = "";
            }
            else
            {
                SessionGRNo = Session["GRetNo"].ToString();
                txtGRetNo.Text = SessionGRNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();

    }

    private void Load_GoodsReceived_No()
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And Approval_Status='1'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlPurOrdNo.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["GRNo"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlPurOrdNo.DataTextField = "GRNo";
        ddlPurOrdNo.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];

        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You have to add atleast one Item Details..');", true);
        }

        ////User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Goods Return");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Modify Goods Return Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "1", "Goods Return");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Adding New Goods Return..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call

        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Goods Return", SessionFinYearVal,"1");
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtGRetNo.Text = Auto_Transaction_No;
                }
            }
        }

        decimal Total = Convert.ToDecimal(txtTotAmt.Text);

        decimal Roundoff = Total - Convert.ToInt32(Total);

        if ((double)Roundoff <= .50)
        {
            txtTotAmt.Text = Convert.ToInt32(Total).ToString();
        }
        else
        {
            txtTotAmt.Text = (Convert.ToInt32(Total) + 1).ToString();
        }
        //lblTotal.Text = Total.ToString();

        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                //Update Main table

                SSQL = "Update Trans_GoodsReturn_Main set GRetDate='" + txtGRetDate.Text + "',";
                SSQL = SSQL + " GReceiveNo='" + ddlPurOrdNo.SelectedItem.Text + "',GReceiveDate='" + txtPurOrdDate.Text + "',SuppCode='" + hdSuppCode.Value + "',";
                SSQL = SSQL + " SuppName='" + txtSuppName.Text + "',TransPortName='" + txtTransPort.Text + "',VehicleNo ='" + txtVehiNo.Text + "',";
                SSQL = SSQL + " ReasonForRet='" + txtRetReason.Text + "',Notes='" + txtNotes.Text + "',Remarks='" + txtRemarks.Text + "',";
                SSQL = SSQL + " TotalQty='" + txtTotQty.Text + "',RoundOff='" + Roundoff + "',TotalAmount='" + txtTotAmt.Text + "'";
                SSQL = SSQL + " Where GRetNo='" + txtGRetNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                for (int i = 0; i < Repeater1.Items.Count; i++)
                {
                    Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;

                    Label DeptCode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;

                    Label WarehouseCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                    Label WarehousName = Repeater1.Items[i].FindControl("lblWarehouseName") as Label;


                    TextBox ReturnQty = Repeater1.Items[i].FindControl("txtReturnQty") as TextBox;
                    Label Rate = Repeater1.Items[i].FindControl("lblRate") as Label;
                    Label ItemTotal = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                    Label CGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                    Label CGSTAmount = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                    Label SGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                    Label SGSTAmount = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                    Label IGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                    Label IGSTAmount = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                    Label LineTotal = Repeater1.Items[i].FindControl("lblLineTol") as Label;

                    SSQL = "Update Trans_GoodsReturn_Sub Set RetQty='" + ReturnQty.Text.ToString() + "', ";
                    SSQL = SSQL + "Rate='" + Rate.Text.ToString() + "',ItemTotal='" + ItemTotal.Text.ToString() + "',";
                    SSQL = SSQL + " CGSTPer='" + CGSTPer.Text.ToString() + "',CGSTAmt='" + CGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " SGSTPer='" + SGSTPer.Text.ToString() + "',SGSTAmt='" + SGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " IGSTPer='" + IGSTPer.Text.ToString() + "',IGSTAmt='" + IGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " LineTotal='" + LineTotal.Text.ToString() + "'";
                    SSQL = SSQL + " Where ItemCode ='" + ItemCode.Text.ToString() + "' And ItemName='" + ItemName.Text.ToString() + "'";
                    SSQL = SSQL + " And GRetNo='" + txtGRetNo.Text + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);


                    //Stock Update

                    SSQL = "Update Trans_Stock_Ledger_All Set Minus_Qty='" + ReturnQty.Text.ToString() + "',Minus_Value='" + ItemTotal.Text.ToString() + "'";
                    SSQL = SSQL + " Where ItemCode ='" + ItemCode.Text.ToString() + "' And ItemName='" + ItemName.Text.ToString() + "' And ";
                    SSQL = SSQL + " Trans_No ='" + txtGRetNo.Text + "' And Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And ";
                    SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And  FinYearVal='" + SessionFinYearVal + "'";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
            else
            {
                //Insert Main Table
                //CCode,LCode,FinYearCode,FinYearVal,GRetNo,GRetDate,GReceiveNo,GReceiveDate,SuppCode,SuppName,TransPortName,VehicleNo,ReasonForRet,Notes,
                //Remarks,TotalQty,RoundOff,TotalAmount,UserId,UserName,Status,CreateOn,Approval_Status

                SSQL = "Insert Into Trans_GoodsReturn_Main(CCode,LCode,FinYearCode,FinYearVal,GRetNo,GRetDate,GReceiveNo,GReceiveDate,SuppCode,SuppName,";
                SSQL = SSQL + " TransPortName,VehicleNo,ReasonForRet,Notes,Remarks,TotalQty,RoundOff,TotalAmount,UserId,UserName,Status,Approval_Status,CreateOn)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                SSQL = SSQL + " '" + txtGRetNo.Text + "','" + txtGRetDate.Text + "','" + ddlPurOrdNo.SelectedItem.Text + "','" + txtPurOrdDate.Text + "',";
                SSQL = SSQL + " '" + hdSuppCode.Value + "','" + txtSuppName.Text + "','" + txtTransPort.Text + "','" + txtVehiNo.Text + "',";
                SSQL = SSQL + " '" + txtRetReason.Text + "','" + txtNotes.Text + "','" + txtRemarks.Text + "','" + txtTotQty.Text + "',";
                SSQL = SSQL + " '" + Roundoff + "','" + txtTotAmt.Text + "','" + SessionUserID + "','" + SessionUserName + "','','0',Getdate())";
                objdata.RptEmployeeMultipleDetails(SSQL);

                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["ItemTable"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    Label ItemCode = Repeater1.Items[i].FindControl("lblItemCode") as Label;
                    Label ItemName = Repeater1.Items[i].FindControl("lblItemName") as Label;

                    Label Deptcode = Repeater1.Items[i].FindControl("lblDeptCode") as Label;
                    Label DeptName = Repeater1.Items[i].FindControl("lblDeptName") as Label;

                    Label WarehouseCode = Repeater1.Items[i].FindControl("lblWarehouseCode") as Label;
                    Label WarehouseName = Repeater1.Items[i].FindControl("lblWarehouseName") as Label;

                    TextBox ReturnQty = Repeater1.Items[i].FindControl("txtReturnQty") as TextBox;
                    Label Rate = Repeater1.Items[i].FindControl("lblRate") as Label;
                    Label ItemTotal = Repeater1.Items[i].FindControl("lblItemTot") as Label;
                    Label CGSTPer = Repeater1.Items[i].FindControl("lblCGSTPer") as Label;
                    Label CGSTAmount = Repeater1.Items[i].FindControl("lblCGSTAmt") as Label;
                    Label SGSTPer = Repeater1.Items[i].FindControl("lblSGSTPer") as Label;
                    Label SGSTAmount = Repeater1.Items[i].FindControl("lblSGSTAmt") as Label;
                    Label IGSTPer = Repeater1.Items[i].FindControl("lblIGSTPer") as Label;
                    Label IGSTAmount = Repeater1.Items[i].FindControl("lblIGSTAmt") as Label;
                    Label LineTotal = Repeater1.Items[i].FindControl("lblLineTol") as Label;

                    SSQL = "Insert Into Trans_GoodsReturn_Sub(CCode,LCode,FinYearCode,FinYearVal,GRetNo,GRetDate,GReceiveNo,GReceiveDate,ItemCode,";
                    SSQL = SSQL + " ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,RetQty,Rate,ItemTotal,Disc,DiscAmt,CGSTPer,CGSTAmt,";
                    SSQL = SSQL + " SGSTPer,SGSTAmt,IGSTPer,IGSTAmt,LineTotal,UserId,UserName,Status,CreateOn)";
                    SSQL = SSQL + "  Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtGRetNo.Text + "','" + txtGRetDate.Text + "','" + ddlPurOrdNo.SelectedItem.Text + "',";
                    SSQL = SSQL + " '" + txtPurOrdDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["Deptcode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["WarehouseCode"].ToString() + "','" + dt.Rows[i]["WarehouseName"].ToString() + "',";
                    SSQL = SSQL + " '" + ReturnQty.Text.ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[i]["ItemTotal"].ToString() + "','0.00','0.00',";
                    SSQL = SSQL + " '" + dt.Rows[i]["CGSTPer"].ToString() + "','" + CGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["SGSTPer"].ToString() + "','" + SGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + dt.Rows[i]["IGSTPer"].ToString() + "','" + IGSTAmount.Text.ToString() + "',";
                    SSQL = SSQL + " '" + LineTotal.Text.ToString() + "','" + SessionUserID + "','" + SessionUserName + "',";
                    SSQL = SSQL + " '',Getdate()) ";
                    //SSQL = SSQL + " '" + Stock_Qty + "','" + Last_Issue_Date + "','" + Last_Purchase_Date + "','" + Last_Purchase_Qty + "','" + Prev_Purchase_Rate + "','" + dt.Rows[i]["Re_Order_Stock_Qty"].ToString() + "','" + dt.Rows[i]["Re_Order_Item_Qty"].ToString() + "','" + dt.Rows[i]["Indent_Type"].ToString() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);


                    ////Insert Purchase Request Approval Table
                    //SSQL = "Insert Into Pur_Request_Approval(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_Request_Amend,TotalQuantity,UserID,UserName) Values('" + SessionCcode + "',";
                    //SSQL = SSQL + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "','" + txtDate.Text + "','Request','" + ReqQty + "',";
                    //SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "')";
                    //objdata.RptEmployeeMultipleDetails(SSQL);

                    //Stock Insert 

                    DateTime transDate = Convert.ToDateTime(txtGRetDate.Text);

                    SSQL = "Insert into Trans_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,";
                    SSQL = SSQL + " Supp_Code,Supp_Name,ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,Add_Qty,Add_Value,Minus_Qty,";
                    SSQL = SSQL + " Minus_Value,UserID,UserName) Values(";
                    SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + txtGRetNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtGRetDate.Text + "','Goods Return',";
                    SSQL = SSQL + " '" + hdSuppCode.Value + "','" + txtSuppName.Text + "','" + ItemCode.Text.ToString() + "',";
                    SSQL = SSQL + " '" + ItemName.Text.ToString() + "','" + dt.Rows[i]["Deptcode"].ToString() + "',";
                    SSQL = SSQL + "'" + dt.Rows[i]["DeptName"].ToString() + "','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                    SSQL = SSQL + "'" + dt.Rows[i]["WarehouseName"].ToString() + "','0.00','0.00','" + ReturnQty.Text.ToString() + "',";
                    SSQL = SSQL + " '" + ItemTotal.Text.ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Purchase Request Details Updated Successfully');", true);
            }

            //Clear_All_Field();
            Session["Pur_Request_No"] = txtGRetNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
            Response.Redirect("Trans_GoodsReturn_Main.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtGRetNo.Text = ""; txtGRetDate.Text = "";
        ddlPurOrdNo.SelectedItem.Text = "-Select-"; ddlPurOrdNo.SelectedValue = "-Select-";
        txtPurOrdDate.Text = ""; txtSuppName.Text = "";txtTransPort.Text = ""; txtVehiNo.Text = "";
        txtRetReason.Text = "";txtNotes.Text = "";txtRemarks.Text = "";
        // txtDeptCode.SelectedValue = "-Select-"; txtDeptName.Text = "";
        //txtCostCenter.Text = "-Select-"; txtCostElement.Items.Clear(); txtRequestedby.Value = "-Select-";
        //txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        //txtReuiredQty.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Pur_Request_No");
        //Load_Data_Enquiry_Grid();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        TotalRetQty();
        TotalNetAmount();
    }
    public void TotalRetQty()
    {
        RetQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            RetQty = Convert.ToDecimal(RetQty) + Convert.ToDecimal(dt.Rows[i]["RetQty"]);
        }
        txtTotQty.Text = RetQty.ToString();
    }

    public void TotalNetAmount()
    {
        decimal LineTotTst = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            LineTotTst = Convert.ToDecimal(LineTotTst) + Convert.ToDecimal(dt.Rows[i]["LineTotal"]);
        }
        txtTotAmt.Text = LineTotTst.ToString();
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Trans_GoodsReturn_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And GRetNo ='" + txtGRetNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtGRetNo.ReadOnly = true;
            txtGRetNo.Text = Main_DT.Rows[0]["GRetNo"].ToString();
            txtGRetDate.Text = Main_DT.Rows[0]["GRetDate"].ToString();
            ddlPurOrdNo.SelectedItem.Text = Main_DT.Rows[0]["GReceiveNo"].ToString();
            txtPurOrdDate.Text = Main_DT.Rows[0]["GReceiveDate"].ToString();
            hdSuppCode.Value = Main_DT.Rows[0]["SuppCode"].ToString();
            txtSuppName.Text = Main_DT.Rows[0]["SuppName"].ToString();

            txtTransPort.Text = Main_DT.Rows[0]["TransPortName"].ToString();
            txtVehiNo.Text = Main_DT.Rows[0]["VehicleNo"].ToString();
            txtRetReason.Text = Main_DT.Rows[0]["ReasonForRet"].ToString();
            txtNotes.Text = Main_DT.Rows[0]["Notes"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
 
            txtTotQty.Text = Main_DT.Rows[0]["TotalQty"].ToString();
            txtRoundOff.Text = Main_DT.Rows[0]["RoundOff"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmount"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select ItemCode,ItemName,DeptCode,DeptName,WarehouseCode,WarehouseName,RetQty,Rate,ItemTotal,CGSTPer,CGSTAmt,SGSTPer,SGSTAmt,";
            SSQL = SSQL + " IGSTPer,IGSTAmt,LineTotal from Trans_GoodsReturn_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And FinYearCode = '" + SessionFinYearCode + "' And GRetNo='" + txtGRetNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            TotalRetQty();
            TotalNetAmount();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trans_GoodsReturn_Main.aspx");
    }
    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }
    protected void ddlPurOrdNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Trans_GoodsReceipt_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And Approval_Status='1' and GRNo='" + ddlPurOrdNo.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtPurOrdDate.Text = DT.Rows[0]["GRDate"].ToString();
        txtSuppName.Text = DT.Rows[0]["SuppName"].ToString();
        hdSuppCode.Value = DT.Rows[0]["SuppCode"].ToString();

        SSQL = "Select ItemCode,ItemName from Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And GRNo='" + ddlPurOrdNo.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlItemName.DataValueField = "ItemCode";
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataBind();
    }
    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();

        SSQL = "Select * from Trans_GoodsReceipt_Sub where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " FinYearcode='" + SessionFinYearCode + "' And GRNo='" + ddlPurOrdNo.SelectedItem.Text + "' ";
        SSQL = SSQL + " And ItemName='" + ddlItemName.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        txtReceiveQty.Text = DT.Rows[0]["ReceiveQty"].ToString();
        txtRate.Text = DT.Rows[0]["Rate"].ToString();
        hfCGSTPre.Value = DT.Rows[0]["CGSTPer"].ToString();
        hfSGSTPre.Value = DT.Rows[0]["SGSTPer"].ToString();
        hfIGSTPre.Value = DT.Rows[0]["IGSTPer"].ToString();

        hfDeptCode.Value= DT.Rows[0]["DeptCode"].ToString();
        hfDeptName.Value = DT.Rows[0]["DeptName"].ToString();
        hfWarehouseCode.Value = DT.Rows[0]["WarehouseCode"].ToString();
        hfWarehouseName.Value = DT.Rows[0]["WarehouseName"].ToString();
    }
    protected void txtRetQty_TextChanged(object sender, EventArgs e)
    {
        ItemTot = Convert.ToDecimal(txtRetQty.Text) * Convert.ToDecimal(txtRate.Text);

        hfCGSTPre.Value = "2.5";
        hfSGSTPre.Value = "2.5";
        hfIGSTPre.Value = "0";

        decimal CGSTPer = Convert.ToDecimal(hfCGSTPre.Value);
        decimal SGSTPer = Convert.ToDecimal(hfSGSTPre.Value);
        decimal IGSTPer = Convert.ToDecimal(hfIGSTPre.Value);

        decimal cgst = Convert.ToDecimal(ItemTot) * (CGSTPer / 100);
        decimal sgst = Convert.ToDecimal(ItemTot) * (SGSTPer / 100);
        decimal igst = Convert.ToDecimal(ItemTot) * (IGSTPer / 100);

        LineTot = Convert.ToDecimal(ItemTot) + Convert.ToDecimal(cgst) + Convert.ToDecimal(sgst) + Convert.ToDecimal(igst);

        txtCGST.Text = cgst.ToString();
        txtSGST.Text = sgst.ToString();
        txtIGST.Text = igst.ToString();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        //string SSQL = "";

        if (txtRetQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Return Qty...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedItem.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Item Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;

                    dr["DeptCode"] = hfDeptCode.Value;
                    dr["DeptName"] = hfDeptName.Value;
                    dr["WarehouseCode"] = hfWarehouseCode.Value;
                    dr["WarehouseName"] = hfWarehouseName.Value;


                    dr["RetQty"] = txtRetQty.Text;
                    dr["Rate"] = txtRate.Text;
                    dr["ItemTotal"] = ItemTot.ToString();

                    dr["CGSTPer"] = hfCGSTPre.Value;
                    dr["CGSTAmt"] = txtCGST.Text;
                    dr["SGSTPer"] = hfSGSTPre.Value;
                    dr["SGSTAmt"] = txtSGST.Text;
                    dr["IGSTPer"] = hfIGSTPre.Value;
                    dr["IGSTAmt"] = txtIGST.Text;
                    dr["LineTotal"] = LineTot;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    TotalRetQty();
                    TotalNetAmount();

                    ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                    txtReceiveQty.Text = "0"; txtRetQty.Text = "0"; txtRate.Text = "0";
                    hfCGSTPre.Value = "0"; hfSGSTPre.Value = "0"; hfIGSTPre.Value = "0";
                    txtCGST.Text = "0"; txtSGST.Text = "0"; txtIGST.Text = "0";
                    //LineTot = 0;ItemTot = 0;
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;

                dr["DeptCode"] = hfDeptCode.Value;
                dr["DeptName"] = hfDeptName.Value;
                dr["WarehouseCode"] = hfWarehouseCode.Value;
                dr["WarehouseName"] = hfWarehouseName.Value;

                dr["RetQty"] = txtRetQty.Text;
                dr["Rate"] = txtRate.Text;
                dr["ItemTotal"] = ItemTot.ToString();

                dr["CGSTPer"] = hfCGSTPre.Value;
                dr["CGSTAmt"] = txtCGST.Text;
                dr["SGSTPer"] = hfSGSTPre.Value;
                dr["SGSTAmt"] = txtSGST.Text;
                dr["IGSTPer"] = hfIGSTPre.Value;
                dr["IGSTAmt"] = txtIGST.Text;
                dr["LineTotal"] = LineTot;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                TotalRetQty();
                TotalNetAmount();

                ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                txtReceiveQty.Text = "0"; txtRetQty.Text = "0"; txtRate.Text = "0";
                hfCGSTPre.Value = "0"; hfSGSTPre.Value = "0"; hfIGSTPre.Value = "0";
                txtCGST.Text = "0"; txtSGST.Text = "0"; txtIGST.Text = "0";
                //LineTot = 0; ItemTot = 0;
            }
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));
        dt.Columns.Add(new DataColumn("RetQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
        //dt = Repeater1.DataSource;
    }
    protected void txtReturn_TextChanged(object sender, EventArgs e)
    {
        TextBox txtTest = ((TextBox)(sender));
        RepeaterItem rpi = ((RepeaterItem)(txtTest.NamingContainer));

        TextBox txtReturnQty = (TextBox)rpi.FindControl("txtReturnQty");
        Label lblRate = (Label)rpi.FindControl("lblRate");
        Label lblItemTol = (Label)rpi.FindControl("lblItemTot");
        Label lblCGSTPre = (Label)rpi.FindControl("lblCGSTPer");
        Label lblSGSTPre = (Label)rpi.FindControl("lblSGSTPer");
        Label lblIGSTPre = (Label)rpi.FindControl("lblIGSTPer");

        Label lblCGSTAmt = (Label)rpi.FindControl("lblCGSTAmt");
        Label lblSGSTAmt = (Label)rpi.FindControl("lblSGSTAmt");
        Label lblIGSTAmt = (Label)rpi.FindControl("lblIGSTAmt");

        Label lblLineTol = (Label)rpi.FindControl("lblLineTol");

        //Label lblBalQty = (Label)rpi.FindControl("lblBalQty");
        //decimal BalQty = Convert.ToDecimal(lblOrdQty.Text) - Convert.ToDecimal(txtReceivQty.Text);
        //lblBalQty.Text = BalQty.ToString();

        //hfCGSTPre.Value = "2.5";
        //hfSGSTPre.Value = "2.5";
        //hfIGSTPre.Value = "0";

        decimal TotalVal = Convert.ToDecimal(lblRate.Text) * Convert.ToDecimal(txtReturnQty.Text);

        decimal CGSTPer = Convert.ToDecimal(lblCGSTPre.Text.ToString());
        decimal SGSTPer = Convert.ToDecimal(lblSGSTPre.Text.ToString());
        decimal IGSTPer = Convert.ToDecimal(lblIGSTPre.Text.ToString());

        decimal cgst = Convert.ToDecimal(TotalVal) * (CGSTPer / 100);
        decimal sgst = Convert.ToDecimal(TotalVal) * (SGSTPer / 100);
        decimal igst = Convert.ToDecimal(TotalVal) * (IGSTPer / 100);

        LineTot = Convert.ToDecimal(TotalVal) + Convert.ToDecimal(cgst) + Convert.ToDecimal(sgst) + Convert.ToDecimal(igst);

        lblItemTol.Text = TotalVal.ToString();
        lblCGSTAmt.Text = cgst.ToString();
        lblSGSTAmt.Text = sgst.ToString();
        lblIGSTAmt.Text = igst.ToString();
        lblLineTol.Text = LineTot.ToString();

        decimal SumQty = 0;
        decimal SumLineTot = 0;

        for (int i = 0; i < Repeater1.Items.Count; i++)
        {
            TextBox RetQty = Repeater1.Items[i].FindControl("txtReturnQty") as TextBox;
            Label LineTotal = Repeater1.Items[i].FindControl("lblLineTol") as Label;

            SumQty = SumQty + Convert.ToDecimal(RetQty.Text);
            SumLineTot = SumLineTot + Convert.ToDecimal(LineTotal.Text);
        }

        txtTotQty.Text = SumQty.ToString();
        txtTotAmt.Text = SumLineTot.ToString();
    }
}