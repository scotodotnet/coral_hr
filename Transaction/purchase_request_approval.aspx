﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="purchase_request_approval.aspx.cs" Inherits="Transaction_purchase_request_approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel runat="server" ID="upMain">
            <ContentTemplate>
                <section class="content">
                    <div class="row">          
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Purchase Request List</span></h3>
                                <div class="box-tools pull-right" runat="server" visible="false">
                                    <div class="has-feedback">
                                        <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row" runat="server" style="padding-top:25px;padding-bottom:25px">
                            <div class="col-md-12">
                                <div class="row" style="padding-left:15px">
                                    <div  class="col-md-3">
                                        <label class="control-label" for="Req_No">Request Status</label>
                                        <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                            onselectedindexchanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <div class="table-responsive mailbox-messages">
                                <div class="col-md-12">
					                <div class="row">
                                        <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false">
			                                <HeaderTemplate>
                                                <table id="example" class="table table-responsive table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Trans No</th>
                                                            <th>Date</th>
                                                            <th>Tot Qty</th>
                                                            <th>Approve</th>
                                                            <th>Cancel</th>
                                                            <th>View</th>
                                                            </tr>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("Pur_Request_No")%></td>
                                                    <td><%# Eval("Pur_Request_Date")%></td>
                                                    <td><%# Eval("TotalReqQty")%></td>
                                        
                                                    <td>
                                                        <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square"  runat="server" 
                                                            Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("Pur_Request_No")%>' CommandName='<%# Eval("Pur_Request_No")%>' 
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this Purchase Request details?');">
                                                        </asp:LinkButton>
                                                        </td>
                                                        <td>
                                                        <asp:LinkButton ID="LinkButton1" class="btn btn-success btn-sm fa fa-check-square"  runat="server" 
                                                            Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("Pur_Request_No")%>' CommandName='<%# Eval("Pur_Request_No")%>' 
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this Purchase Request details?');">
                                                        </asp:LinkButton>
                                                        </td>
                                          
                                                    <td>
                                                    <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-external-link-square"  runat="server" 
                                                                    Text="" OnCommand="GridEditPurRequestClick" CommandArgument='<%# Eval("Pur_Request_No")%>' CommandName='<%# Eval("Pur_Request_No")%>'>
                                                    </asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate></table></FooterTemplate>                                
			                            </asp:Repeater>
					                </div>
					            
                                <div class="row">
			                    <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" Visible="false">
			                        <HeaderTemplate>
                                        <table id="example" class="table table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Trans No</th>
                                                    <th>Date</th>                                               
                                                    <th>Tot Qty</th>
                                                    <th>Approvedby</th>
                                               
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("Pur_Request_No")%></td>
                                            <td><%# Eval("Pur_Request_Date")%></td>
                                            <td><%# Eval("TotalReqQty")%></td>
                                            <td><%# Eval("Approvedby")%></td>
                                        
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>                                
			                    </asp:Repeater>
			                </div>
                                <div class="row">
			            <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" Visible="false">
			                <HeaderTemplate>
                                <table id="example" class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>Trans No</th>
                                            <th>Date</th>                                               
                                            <th>Tot Qty</th>
                                            <th>Approvedby</th>
                                               
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("Pur_Request_No")%></td>
                                    <td><%# Eval("Pur_Request_Date")%></td>
                                    <td><%# Eval("TotalReqQty")%></td>
                                    <td><%# Eval("Approvedby")%></td>
                                        
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>                                
			            </asp:Repeater>
			        </div>

                            </div>

                                <!-- /.table -->
                            </div>
                            <!-- /.mail-box-messages -->
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        </ContentTemplate>
    </asp:UpdatePanel>
        </div>
</asp:Content>

