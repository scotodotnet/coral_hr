﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="standard_po.aspx.cs" Inherits="Transaction_standard_po" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.select2').select2();
                }
            });
        };
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('#ItemTable_Normal_Query').dataTable();
                }
            });
        };
    </script>

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <asp:UpdatePanel ID="upform1" runat="server">
                <ContentTemplate>
        <section class="content-header">
            <h1><i class=" text-primary"></i> Create New Purchase Order</h1>
        </section>
        <!-- Main content -->
        
        <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">                            
                            <%--<div class="form-group" hidden>
                                <label class="control-label" for="Status">Status</label>
                                <input class="form-control" type="text" id="Status" name="Status" value="" />
                                <span class="text-danger field-validation-valid" data-valmsg-for="Status" data-valmsg-replace="true"></span>
                            </div>--%>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="Req_No">Order No</label>
                                    <asp:Label ID="txtStdOrderNo" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Date</label>
                                    <asp:TextBox ID="txtDate" MaxLength="20" class="form-control datepicker" runat="server" AutoComplete="off"></asp:TextBox>
					                <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDate" ValidChars="0123456789./">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Supplier Name</label>
                                    <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                    <asp:HiddenField ID="txtSuppCodehide" runat="server" />
					                <asp:DropDownList ID="txtSupplierName_Select" runat="server" class="form-control select2" AutoPostBack="true" 
                                        onselectedindexchanged="txtSupplierName_Select_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtSupplierName_Select" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            
                             <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Supp.Ref.Doc.No</label>					          
					                <asp:DropDownList ID="txtSuppRefDocNo" runat="server" class="form-control select2" AutoPostBack="true"
                                        OnSelectedIndexChanged="txtSuppRefDocNo_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Dispatch Through</label>					          
					                <select name="txtDeliveryMode" id="txtDeliveryMode" class="form-control select2" runat="server">
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                           
                            <div class="col-md-4" runat="server" visible="false">
                                <div class="form-group">
                                    <label for="exampleInputName">Doc Date</label>					          
					                <asp:TextBox ID="txtDocDate" MaxLength="20" class="form-control datepicker" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Delivery Date</label>
                                    <asp:TextBox ID="txtDeliveryDate" MaxLength="20" class="form-control datepicker" AutoComplete="off" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtDeliveryDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtDeliveryDate" ValidChars="0123456789./">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Delivery At</label>
					                <asp:TextBox ID="txtDeliveryAt" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Payment Mode</label>
                                    <select name="txtPaymentMode" id="txtPaymentMode" class="form-control select2" runat="server">
                                        <option value="- select -">- select -</option>
                                        <option value="CASH">CASH</option>
                                        <option value="CHEQUE">CHEQUE</option>
                                        <option value="NEFT">NEFT</option>
                                        <option value="RTGS">RTGS</option>
                                        <option value="DD">DD</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Department Name</label>
					                <asp:DropDownList ID="txtDepartmentName" runat="server" class="form-control select2">
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Payment Terms</label>
					                <asp:TextBox ID="txtPaymentTerms" TextMode="MultiLine" class="form-control" style="resize:none" runat="server"></asp:TextBox>
                                </div>
                            </div>

                             <div class="form-group col-md-3">
					            <label for="exampleInputName">Special Instruction</label>
					            <asp:TextBox ID="txtDescription" TextMode="MultiLine" class="form-control" style="resize:none" runat="server"></asp:TextBox>	
					        </div>

                            <div class="form-group col-md-3">
					            <label for="exampleInputName">Note</label>
					            <asp:TextBox ID="txtNote" TextMode="MultiLine" class="form-control" style="resize:none" runat="server"></asp:TextBox>	
					        </div>

                            <div class="form-group col-md-3">
                                <label for="exampleInputName">Delivery Schedules</label>
                                <asp:TextBox ID="txtOthers" class="form-control" runat="server"></asp:TextBox>
                            </div>

                        </div>

                        <div class="row">
					       
                            <div class="form-group col-md-3">
                                <label for="exampleInputName">Mode Of Transfer</label>
                                <asp:TextBox ID="txtModeOfTransfer" class="form-control" runat="server"></asp:TextBox>
                            </div>
					   
                            <div class="form-group col-md-3">
                                <label for="exampleInputName">Cheque No</label>
                                <asp:TextBox ID="txtChequeNo" class="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="exampleInputName">Cheque Date</label>
                                <asp:TextBox ID="txtChequeDate" class="form-control datepicker" runat="server"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtChequeDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="exampleInputName">Cheque Amount</label>
                                <asp:TextBox ID="txtChequeAmt" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4" runat="server" visible="false">
                                <label for="exampleInputName">Pur Order Type</label>
                                <asp:RadioButtonList ID="RdpPOType" class="form-control" runat="server"
                                    RepeatColumns="3" OnSelectedIndexChanged="RdpPOType_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="1" Text="Direct" style="padding-right: 40px"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Req/Amend"  Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Special"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>

                            <div class="form-group col-md-5">
                                <label for="exampleInputName">Select Tax Type</label>
                                <asp:RadioButtonList ID="RdpTaxType" class="form-control" runat="server"
                                    RepeatColumns="4">
                                    <asp:ListItem Value="1" Text="CGST" style="padding-right: 40px" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="IGST" style="padding-right: 40px"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="VAT" style="padding-right: 40px"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="NONE"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputName">Request No</label>
					                <asp:DropDownList ID="txtRequestNo" runat="server" class="form-control select2" 
                                        OnSelectedIndexChanged="txtRequestNo_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="exampleInputName">Request Date</label>
                                <asp:TextBox ID="txtRequestDate" class="form-control datepicker" runat="server"></asp:TextBox>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <br />

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"> <i class="fa fa-user-plus text-primary"></i> Item Details</h3>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
					                        <label for="exampleInputName">Item Name</label>
                                            <asp:DropDownList ID="txtItemNameSelect" runat="server" class="form-control select2" AutoPostBack="true" 
                                                    onselectedindexchanged="txtItemNameSelect_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtItemName" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                            <asp:HiddenField ID="txtItemCodeHide" runat="server" />
					                    </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Required Qty</label>
                                            <asp:TextBox ID="txtRequiredQty" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Order Qty</label>
                                            <asp:TextBox ID="txtOrderQty" class="form-control" runat="server" AutoPostBack="true"
                                                OnTextChanged="txtOrderQty_TextChanged"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtOrderQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtOrderQty" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Required Date</label>
                                            <asp:TextBox ID="txtRequiredDate" class="form-control date-picker" runat="server"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtRequiredDate" ValidChars="0123456789./">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Rate</label>
                                            <asp:TextBox ID="txtRate" class="form-control" runat="server" Text="0.0" AutoPostBack="true"
                                                OnTextChanged="txtRate_TextChanged"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtRate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtRate" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                    <div class="row">
                            
                                        <div class="form-group col-md-3" runat="server" visible="false">
                                            <label for="exampleInputName">Remarks</label>
                                            <asp:TextBox ID="txtRemarks" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Item Total</label>
                                            <asp:TextBox ID="txtItemTotal" runat="server" class="form-control" Text="0.0"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label for="exampleInputName">Disc. Per</label>
                                            <asp:TextBox ID="txtDiscount_Per" class="form-control" runat="server"
                                                Text="0.0" AutoPostBack="true" OnTextChanged="txtDiscount_Per_TextChanged"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtDiscount_Per" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Discount Amount</label>
                                            <asp:Label ID="txtDiscount_Amount" runat="server" class="form-control" Text="0.0"></asp:Label>
                                        </div>
                                        <div class="form-group col-md-2" runat="server" visible="false">
                                            <label for="exampleInputName">GST Type</label>
                                            <asp:DropDownList ID="txtGST_Type" runat="server" class="form-control"
                                                AutoPostBack="true" OnSelectedIndexChanged="txtGST_Type_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label for="exampleInputName">CGST %</label>
                                            <asp:Label ID="txtCGSTPer" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>                                
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">CGST Amt</label>
                                            <asp:Label ID="txtCGSTAmt" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label for="exampleInputName">SGST %</label>
                                            <asp:Label ID="txtSGSTPer" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>                                
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">SGST Amt</label>
                                            <asp:Label ID="txtSGSTAmt" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-1">
                                            <label for="exampleInputName">IGST %</label>
                                            <asp:Label ID="txtIGSTPer" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>                                
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">IGST Amt</label>
                                            <asp:Label ID="txtIGSTAmt" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label for="exampleInputName">VAT %</label>
                                            <asp:Label ID="txtVAT_Per" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>                                
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">VAT Amt</label>
                                            <asp:Label ID="txtVAT_AMT" class="form-control" runat="server" Text="0.0" BackColor="LightGray"></asp:Label>
                                        </div>
                                        <asp:HiddenField ID="TotalGstPer_Hidden" runat="server" />

                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Other Amt</label>
                                            <asp:TextBox ID="txtOtherCharge" class="form-control" runat="server" Text="0"
                                                AutoPostBack="true" OnTextChanged="txtOtherCharge_TextChanged"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtOtherCharge" ValidChars="0123456789.-">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Total Amt</label>
                                            <asp:Label ID="txtFinal_Total_Amt" runat="server" class="form-control" Text="0.0"></asp:Label>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <br />
                                            <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success" runat="server" Text="Add" ValidationGroup="Item_Validate_Field" OnClick="btnAddItem_Click" />
                                        </div>
                                    </div>
                               
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"><i class="fa fa-user-plus text-primary"></i> <span>Order Item List</span></h3>
                                            <div class="box-tools pull-right">
                                                <div class="has-feedback">
                                                    <input type="text" id="mainSearch" class="form-control input-sm" placeholder="Search...">
                                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <!-- /.box-tools -->
                                        </div><!-- /.box-header -->
                                    
					                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <%-- <th>StdPo Number</th>--%>
                                                                <th>Item Name</th>
                                                                <th>Requ. Qty</th>
                                                                <th>Order Qty</th>
                                                                <th>Requ. Date</th>
                                                                <th>Rate</th>
                                                           
                                                                <th>Remarks</th>
                                                                <th>Total</th>
                                                                  <th>Dis%</th>
                                                                  <th>GST%</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <%-- <td><%# Eval("Pur_Request_No")%></td>
                                            <td><%# Eval("Pur_Request_Date")%></td>--%>
                                                        <%--<td><%# Eval("Std_PO_No")%></td>--%>
                                                        <td><%# Eval("ItemName")%></td>
                                                        <%--<td><%# Eval("UOMCode")%></td>--%>
                                                        <td><%# Eval("ReuiredQty")%></td>
                                                        <td><%# Eval("OrderQty")%></td>
                                                        <td><%# Eval("ReuiredDate")%></td>
                                                        <td><%# Eval("Rate")%></td>
                                                        <td><%# Eval("Remarks")%></td>
                                                        <td><%# Eval("LineTotal")%></td>
                                                          <td><%# Eval("Discount_Per")%></td>
                                                          <td><%# Eval("GstPer")%></td>
                                                        <td>
                                                            <%--CommandArgument='<%# Eval("Std_PO_No")%>'--%>

                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridDeleteClick" CommandName='<%# Eval("ItemCode")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater><!-- /.table -->
                                    </div><!-- /.mail-box-messages -->
                                </div><!-- /.box-body -->
                            </div>

                            <div class="row">
                            <div class="form-group col-md-10"></div>
                            <div class="form-group col-md-2">
                                <label for="exampleInputName">Total Amt</label>
                                <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-8">
                            </div>
                            <div class="form-group col-md-1" runat="server" visible="false">
                                <label for="exampleInputName">Tax Per</label>
                                <asp:TextBox ID="txtTax" class="form-control" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
                            </div>
                            <div class="form-group col-md-2" runat="server" visible="false">
                                <label for="exampleInputName">Tax Amount</label>
                                <asp:Label ID="txtTaxAmt" runat="server" class="form-control" Text="0"></asp:Label>
                            </div>
                            <div class="form-group col-md-1" runat="server" visible="false">
                                <label for="exampleInputName">Discount</label>
                                <asp:TextBox ID="txtDiscount" class="form-control" runat="server"
                                    OnTextChanged="txtDiscount_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscount" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
                            </div>



                            <div class="form-group col-md-2">
                                <label for="exampleInputName">Add or Less</label>
                                <asp:TextBox ID="txtAddOrLess" class="form-control" runat="server"
                                    Text="0" AutoPostBack="true" OnTextChanged="txtAddOrLess_TextChanged"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtAddOrLess" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="exampleInputName">Net Amt</label>
                                <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>

                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="form-group">
                            <asp:Button ID="btnSave" class="btn btn-primary"  runat="server" Text="Save" OnClick="btnSave_Click"/>
                            <asp:Button ID="btnPO_Preview" class="btn btn-primary" runat="server" Text="PO Preview" OnClick="btnPO_Preview_Click" />
                            <asp:Button ID="btnCancel" class="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnBackEnquiry" class="btn btn-default" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>     
                            <asp:Button ID="btnApprove" class="btn btn-default" runat="server" 
                                Text="Approve" Visible="false" onclick="btnApprove_Click"/>
                        </div>
                    </div>
                                <!-- /.box-footer-->
                </div>
                            <!-- /.box -->
            </div>
            
        </div>
                   
        </section>
        <!-- /.content -->
                    </ContentTemplate>
                </asp:UpdatePanel>
    </div>

</asp:Content>

