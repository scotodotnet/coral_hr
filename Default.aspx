﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>CORAL | Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<%= ResolveUrl("assets/adminlte/components/bootstrap/dist/css/bootstrap.min.css") %>" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<%= ResolveUrl("assets/adminlte/components/font-awesome/css/font-awesome.min.css") %>" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="<%= ResolveUrl("assets/adminlte/components/Ionicons/css/ionicons.min.css") %>" />
    <!-- Theme style -->
    <link rel="stylesheet" href="<%= ResolveUrl("assets/adminlte/dist/css/AdminLTE.min.css") %>" />
    <!-- Select2 -->
    <link rel="stylesheet" href="<%= ResolveUrl("assets/adminlte/components/select2/dist/css/select2.min.css") %>" />
    <!-- iCheck -->
    <link rel="stylesheet" href="<%= ResolveUrl("assets/adminlte/plugins/iCheck/square/blue.css") %>" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<%= ResolveUrl("assets/adminlte/dist/css/skins/_all-skins.min.css") %>" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" />
</head>
<body class="hold-transition login-page" style="background-image: url('/assets/images/b2.jpg'); background-size: cover; background-attachment: fixed">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="login-box">
            <!-- /.login-logo -->
            <div class="login-box-body">
                <div class="login-logo">
                    <b>CORAL</b><br />
                    Manufacturing Works
                </div>
                <p class="login-box-msg">Sign in to start your session</p>
                <br />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <div class="form-group has-feedback" runat="server" visible="false">
                            <label>Company</label>
                            <asp:DropDownList ID="ddlCompany" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                        <div class="form-group has-feedback" runat="server" visible="false">
                            <label>Location</label>
                            <asp:DropDownList ID="ddlLocation" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" runat="server"></asp:DropDownList>
                        </div>
                        <div class="form-group has-feedback" runat="server" visible="false">
                            <label>Financial Year</label>
                            <asp:DropDownList ID="ddlFinYear" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                       <div class="form-group has-feedback" runat="server" >
                            <label>User Type</label>
                            <asp:DropDownList runat="server" ID="ddlLoginType" class="form-control">
                                <asp:ListItem Value="System Admin" Text="System Admin"></asp:ListItem>
                                <asp:ListItem Value="STAFF" Text="Employee"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group has-feedback">
                            <label>User ID</label>
                            <asp:TextBox ID="txtUserID" CssClass="form-control" AutoComplete="off" runat="server"
                                placeholder="User ID" Text=""></asp:TextBox>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" class="form_error" runat="server" ControlToValidate="txtUserID" 
                                Display ="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                ValidationGroup="Validate_Field" ErrorMessage="enter valid email address. Eg. Something@domain.com">
                            </asp:RegularExpressionValidator>--%>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Password</label>
                            <asp:TextBox ID="txtPassword" TextMode="Password" CssClass="form-control" AutoComplete="off" runat="server" Text="ScotoAdmin2020" placeholder="Password"></asp:TextBox>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <%--  <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>--%>
                                <asp:Button ID="btnlogin" class="btn btn-primary btn-block btn-flat" runat="server" Text="Login" OnClick="btnlogin_Click"></asp:Button>
                            </div>
                            <!-- /.col -->
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </div>
    </form>
    <!-- jQuery 3 -->
    <script src="<%= ResolveUrl("assets/adminlte/components/jquery/dist/jquery.min.js") %>"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<%= ResolveUrl("assets/adminlte/components/bootstrap/dist/js/bootstrap.min.js") %>"></script>
    <!-- iCheck -->
    <script src="<%= ResolveUrl("assets/adminlte/plugins/iCheck/icheck.min.js") %>"></script>
    <!-- Select2 -->
    <script src="<%= ResolveUrl("assets/adminlte/components/select2/dist/js/select2.full.min.js") %>"></script>


    <script type="text/javascript">
        $('.select2').select2({

        });

    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    //$('.datepicker').datepicker({
                    //    format: "dd/mm/yyyy",
                    //    autoclose: true
                    //});

                    $('.select2').select2();
                }
            });
        };
    </script>
</body>
</html>
