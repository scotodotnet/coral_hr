﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_OT_OT_Approval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionUserType;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        SessionUserType = Session["RoleCode"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: OT Approval";
            //Department_Code_Add();
            purchase_Request_Status_Add();
            //Department_Code_Add_Test();
        }
        Load_Data_Empty_Grid();
    }
    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }
    private void Load_Data_Empty_Grid()
    {
        try
        {
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT1 = new DataTable();
            DataTable DT2 = new DataTable();

            DT.Columns.Add("MachineID");
            DT.Columns.Add("ExistingCode");
            DT.Columns.Add("EmpName");
            DT.Columns.Add("OTDate_Str");
            DT.Columns.Add("OTHrs");

            if (txtRequestStatus.Text == "Pending List")
            {
                SSQL = "Select MachineID,ExistingCode,EmpName,OTDate_Str,OTHrs,Auto_ID From Weekly_OTHours  ";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'  ";
                //if (SessionUserType == "Manager")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '0' or Approval_Status is null) ";
                //}
                //else if (SessionUserType == "VP")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '1' ) ";
                //}
                //else if (SessionUserType == "Finance")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '2' ) ";
                //}
                //else if (SessionUserType == "JMD")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '3' ) ";
                //}
                //else
                //{
                SSQL = SSQL + " And (Approval_Status = '0' or Approval_Status is null) ";
                //}
                SSQL = SSQL + "  Order By MachineID,OTDate Asc";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Pending.DataSource = DT;
                Repeater_Pending.DataBind();
                Repeater_Pending.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Approved List")
            {
                SSQL = "Select MachineID,ExistingCode,EmpName,OTDate_Str,OTHrs,Auto_ID From Weekly_OTHours   ";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                SSQL = SSQL + " And Approval_Status = '1' ";
                //if (SessionUserType == "Manager")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
                //}
                //else if (SessionUserType == "VP")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '2' ) ";
                //}
                //else if (SessionUserType == "Finance")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '3' ) ";
                //}
                //else if (SessionUserType == "JMD")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '4' ) ";
                //}
                //else
                //{
                //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
                //}
                SSQL = SSQL + "  Order By MachineID,OTDate Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);


                Repeater_Approve.DataSource = DT;
                Repeater_Approve.DataBind();
                Repeater_Approve.Visible = true;
                Repeater_Pending.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Rejected List")
            {

                SSQL = "Select MachineID,ExistingCode,EmpName,OTDate_Str,OTHrs,Auto_ID From Weekly_OTHours   ";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                SSQL = SSQL + " And Approval_Status = '-1'  Order By MachineID,OTDate Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);


                Repeater_Rejected.DataSource = DT;
                Repeater_Rejected.DataBind();
                Repeater_Rejected.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Pending.Visible = false;
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GridApproveRequestClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "11", "3", "OT Approval");

        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved OT..');", true);
        }
        //User Rights Check End


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            SSQL = SSQL + "  And Auto_ID='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And Approval_Status = '-1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('OT Details Already Cancelled.. Cannot Approved it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            SSQL = "Select * from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            SSQL = SSQL + "  And Auto_ID='" + e.CommandArgument.ToString() + "' ";
            SSQL = SSQL + " And Approval_Status = '1'";
            //if (SessionUserType == "Manager")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
            //}
            //else if (SessionUserType == "VP")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '2' ) ";
            //}
            //else if (SessionUserType == "Finance")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '3' ) ";
            //}
            //else if (SessionUserType == "JMD")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '4' ) ";
            //}
            //else
            //{
            //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
            //}
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('OT Details Already get Approved..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            SSQL = SSQL + "  And Auto_ID='" + e.CommandArgument.ToString() + "'";
            SSQL = SSQL + " And Approval_Status = '1'";
            //if (SessionUserType == "Manager")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
            //}
            //else if (SessionUserType == "VP")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '2' ) ";
            //}
            //else if (SessionUserType == "Finance")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '3' ) ";
            //}
            //else if (SessionUserType == "JMD")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '4' ) ";
            //}
            //else
            //{
            //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
            //}
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table

                SSQL = "Update Weekly_OTHours set ";
                //if (SessionUserType == "Manager")
                //{
                //    SSQL = SSQL + "  Approval_Status = '1' ";
                //}
                //else if (SessionUserType == "VP")
                //{
                //    SSQL = SSQL + "  Approval_Status = '2'  ";
                //}
                //else if (SessionUserType == "Finance")
                //{
                //    SSQL = SSQL + "  Approval_Status = '3'  ";
                //}
                //else if (SessionUserType == "JMD")
                //{
                //    SSQL = SSQL + "  Approval_Status = '4' ";
                //}
                //else
                //{
                SSQL = SSQL + "  Approval_Status = '1' ";
                //}
                SSQL = SSQL + "where CompCode='" + SessionCcode + "' ";
                SSQL = SSQL + "  And LocCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " Auto_ID ='" + e.CommandArgument.ToString() + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                //SSQL = "";
                //SSQL = "Select * from Trans_Reservation_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
                //SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And TransNo='" + e.CommandArgument.ToString() + "' ";
                //DataTable dt_Main = new DataTable();
                //dt_Main = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (dt_Main.Rows.Count > 0)
                //{
                //    SSQL = "";
                //    SSQL = "Select Part_No,Part_QRCode from Trans_Enercon_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                //    SSQL = SSQL + " and Std_PO_No='" + dt_Main.Rows[0]["PoNo"].ToString() + "'";

                //    SSQL = SSQL + "UNION ALL";

                //    SSQL = "Select Part_No,Part_QRCode from Trans_Escrow_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                //    SSQL = SSQL + " and Std_PO_No='" + dt_Main.Rows[0]["PoNo"].ToString() + "' ";

                //    SSQL = SSQL + "UNION ALL";

                //    SSQL = "Select Part_No,Part_QRCode from Trans_Coral_PurOrd_Main where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                //    SSQL = SSQL + " and Std_PO_No='" + dt_Main.Rows[0]["PoNo"].ToString() + "' ";

                //    DataTable dt_PO = new DataTable();
                //    dt_PO = objdata.RptEmployeeMultipleDetails(SSQL);
                //    if (dt_PO.Rows.Count > 0)
                //    {
                //        SSQL = "";
                //        SSQL = "Update [CORAL_ERP_Sales]..Sales_Customer_Purchase_Order_QR_Status set Part_Status='4', Part_Description='Gate IN Completed'";
                //        SSQL = SSQL + " where Part_No='" + dt_PO.Rows[0]["Pasrt_No"].ToString() + "' and Parts_QR_Code='" + dt_PO.Rows[0]["Part_QRCode"].ToString() + "'";
                //        objdata.RptEmployeeMultipleDetails(SSQL);
                //    }
                //}

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('OT Details Approved Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('OT not is Avail give input correctly..');", true);
            }
        }
    }
    protected void GridCancelRequestClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "11", "3", "OT Approval");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Approved Request..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            SSQL = SSQL + " And Auto_ID='" + e.CommandArgument.ToString() + "' And ";
            SSQL = SSQL + " Approval_Status = '1'";
            //if (SessionUserType == "Manager")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
            //}
            //else if (SessionUserType == "VP")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '2' ) ";
            //}
            //else if (SessionUserType == "Finance")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '3' ) ";
            //}
            //else if (SessionUserType == "JMD")
            //{
            //    SSQL = SSQL + " And (Approval_Status = '4' ) ";
            //}
            //else
            // {
            //     SSQL = SSQL + " and  Approval_Status = '1' ";
            // }

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('OT Details Already Approved.. Cannot Cancelled it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();

            SSQL = "Select * from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            SSQL = SSQL + "  And Auto_ID='" + e.CommandArgument.ToString() + "' ";
            //SSQL = SSQL + " And Approval_Status = '2'";
            SSQL = SSQL + " And Approval_Status = '-1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('OT Details Already get Cancelled..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            SSQL = "Select * from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            SSQL = SSQL + " And Auto_ID='" + e.CommandArgument.ToString() + "' ";
            //SSQL = SSQL + " And Approval_Status = '2'";
            SSQL = SSQL + " And Approval_Status = '-1'";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table

                SSQL = "Update Weekly_OTHours set Approval_Status='-1' where CompCode='" + SessionCcode + "' And ";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " Auto_ID ='" + e.CommandArgument.ToString() + "'";

                SSQL = "Update Weekly_OTHours set Approval_Status='-1' where CompCode='" + SessionCcode + "' And ";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And ";
                SSQL = SSQL + " Auto_ID ='" + e.CommandArgument.ToString() + "'";


                objdata.RptEmployeeMultipleDetails(SSQL);


                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('OT Details Cancelled Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Reservation not is Avail give input correctly..');", true);
            }
        }
    }
    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //string[] Department_Spilit = txtDepartment.Text.Split('-');
            //string Department_Code = "";
            //string Department_Name = "";
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT1 = new DataTable();
            DataTable DT2 = new DataTable();

            DT.Columns.Add("MachineID");
            DT.Columns.Add("ExistingCode");
            DT.Columns.Add("EmpName");
            DT.Columns.Add("OTDate_Str");
            DT.Columns.Add("OTHrs");

            if (txtRequestStatus.Text == "Pending List")
            {
                SSQL = "Select MachineID,ExistingCode,EmpName,OTDate_Str,OTHrs,Auto_ID From Weekly_OTHours";
                SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'  ";
                SSQL = SSQL + " And (Approval_Status = '0' or Approval_Status is null) ";

                //if (SessionUserType == "Manager")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '0' or Approval_Status is null) ";
                //}
                //else if (SessionUserType == "VP")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '1' ) ";
                //}
                //else if (SessionUserType == "Finance")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '2' ) ";
                //}
                //else if (SessionUserType == "JMD")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '3' ) ";
                //}
                //else
                //{
                //    SSQL = SSQL + " And (Approval_Status = '0' or Approval_Status is null) ";
                //}
                SSQL = SSQL + " Order By MachineID,OTDate Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                Repeater_Pending.DataSource = DT;
                Repeater_Pending.DataBind();
                Repeater_Pending.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Approved List")
            {
                SSQL = "Select MachineID,ExistingCode,EmpName,OTDate_Str,OTHrs,Auto_ID From Weekly_OTHours";
                //SSQL = SSQL + " Case when ReceiveStatus='1' then 'Goods Received' else 'Goods Not Received' End Status ";
                //SSQL = SSQL + " From Trans_Coral_PurOrd_Main ";
                SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And Approval_Status = '1' By MachineID,OTDate Asc";
                //if (SessionUserType == "Manager")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
                //}
                //else if (SessionUserType == "VP")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '2' ) ";
                //}
                //else if (SessionUserType == "Finance")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '3' ) ";
                //}
                //else if (SessionUserType == "JMD")
                //{
                //    SSQL = SSQL + " And (Approval_Status = '4' ) ";
                //}
                //else
                //{
                //    SSQL = SSQL + " And (Approval_Status = '1' or Approval_Status is null) ";
                //}
                SSQL = SSQL + "  Order By MachineID,OTDate Asc";

                DT = objdata.RptEmployeeMultipleDetails(SSQL);


                Repeater_Approve.DataSource = DT;
                Repeater_Approve.DataBind();
                Repeater_Approve.Visible = true;
                Repeater_Pending.Visible = false;
                Repeater_Rejected.Visible = false;
            }
            else if (txtRequestStatus.Text == "Rejected List")
            {
                SSQL = "Select MachineID,ExistingCode,EmpName,OTDate_Str,OTHrs,Auto_ID From Weekly_OTHours";
                SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ";
                SSQL = SSQL + "  Approval_Status = '-1' Order By MachineID,OTDate Asc";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);


                Repeater_Rejected.DataSource = DT;
                Repeater_Rejected.DataBind();
                Repeater_Rejected.Visible = true;
                Repeater_Approve.Visible = false;
                Repeater_Pending.Visible = false;
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GridEditPurRequestClick(object sender, CommandEventArgs e)
    {
        string RptName = "";
        string Enquiry_No_Str = e.CommandName.ToString();
        RptName = "Reservation Request Report";
        //Session.Remove("Pur_RequestNo_Approval");
        //Session.Remove("Transaction_No");
        Session.Remove("TransNo");
        Session["TransNo"] = Enquiry_No_Str;
        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?TransNo=" + Enquiry_No_Str + "&RptName=" + RptName, "_blank", "");

    }
    protected void Repeater_Approve_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

    //protected void btnMRR_View_Command(object sender, CommandEventArgs e)
    //{
    //    string RptName = "";
    //    string Enquiry_No_Str = e.CommandName.ToString();
    //    RptName = "RFI MRR Report";
    //    //Session.Remove("Pur_RequestNo_Approval");
    //    //Session.Remove("Transaction_No");
    //    Session.Remove("TransNo");
    //    Session["TransNo"] = Enquiry_No_Str;
    //    ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?TransNo=" + Enquiry_No_Str + "&RptName=" + RptName, "_blank", "");
    //}
}