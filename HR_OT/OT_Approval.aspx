﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OT_Approval.aspx.cs" Inherits="HR_OT_OT_Approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="ffa-check-square-o text-primary"></i><span>OT Approval</span></h3>
                                </div>

                                <div class="row" runat="server" style="padding-top: 25px; padding-bottom: 25px">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="control-label" for="Req_No">OT Status</label>
                                            <asp:DropDownList ID="txtRequestStatus" runat="server" class="form-control select2"
                                                OnSelectedIndexChanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body no-padding">
                                    <div class="table-responsive mailbox-messages">
                                        <div class="col-md-12">
                                            <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>MachineID</th>
                                                                <th>ExistingCode</th>
                                                                <th>EmpName</th>
                                                                <th>Date</th>
                                                                <th>OTHrs</th>
                                                                <th>Approve</th>
                                                                <th>Cancel</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex+1%></td>
                                                        <td><%# Eval("MachineID")%></td>
                                                        <td><%# Eval("ExistingCode")%></td>
                                                        <td><%# Eval("EmpName")%></td>
                                                        <td><%# Eval("OTDate_Str")%></td>
                                                        <td><%# Eval("OTHrs")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnApproveRequest" class="btn btn-primary btn-sm fa fa-check-square" runat="server"
                                                                Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("Auto_ID")%>' CommandName='Approval'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this OT details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="LinkButton1" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("Auto_ID")%>' CommandName='Cancel'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this OT details?');">
                                                            </asp:LinkButton>
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" Visible="false" OnItemDataBound="Repeater_Approve_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>MachineID</th>
                                                                    <th>ExistingCode</th>
                                                                    <th>EmpName</th>
                                                                    <th>Date</th>
                                                                    <th>OTHrs</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1%></td>
                                                            <td><%# Eval("MachineID")%></td>
                                                            <td><%# Eval("ExistingCode")%></td>
                                                            <td><%# Eval("EmpName")%></td>
                                                            <td><%# Eval("OTDate_Str")%></td>
                                                            <td><%# Eval("OTHrs")%></td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                            <div class="col-md-12">
                                                <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" Visible="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>MachineID</th>
                                                                    <th>ExistingCode</th>
                                                                    <th>EmpName</th>
                                                                    <th>Date</th>
                                                                    <th>OTHrs</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex+1%></td>
                                                            <td><%# Eval("MachineID")%></td>
                                                            <td><%# Eval("ExistingCode")%></td>
                                                            <td><%# Eval("EmpName")%></td>
                                                            <td><%# Eval("OTDate_Str")%></td>
                                                            <td><%# Eval("OTHrs")%></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

