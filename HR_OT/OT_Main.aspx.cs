﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_OT_OT_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "CORAL HR :: OT List";
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "11", "3", "OT Hours");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Add New OT...');", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Purchase Request..');", true);
        }
        else
        {

            Session.Remove("Auto_ID");
            Response.Redirect("WeeklyOTHrs.aspx");
        }

        //Session.Remove("Pur_RequestNo_Approval");
        //Session.Remove("Pur_Request_No_Amend_Approval");
        //Session.Remove("Pur_Request_No");
        //Response.Redirect("purchase_request.aspx");
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        bool Rights_Check = false;
        bool ErrFlag = false;

        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "11", "3", "OT Hours");

        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('You do not have Rights to Edit OT...');", true);
        }
        else
        {
            DataTable dtdpurchase = new DataTable();
            query = "select Approval_Status as Status from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            query = query + "  And Auto_ID='" + e.CommandName.ToString() + "'";
            dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
            string status = dtdpurchase.Rows[0]["Status"].ToString();

            if (status == "" || status == "0")
            {
                string Enquiry_No_Str = e.CommandName.ToString();
                Session.Remove("Auto_ID");
                Session["Auto_ID"] = Enquiry_No_Str;
                EditEnqAsp.Value = Enquiry_No_Str;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "send();", true);
                Response.Redirect("WeeklyOTHrs.aspx");
            }
            else if (status == "2")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('OT Details Already Rejected..');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('OT Details put in pending..');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details put in pending..');", true);
            }
            else if (status == "1")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('OT Cant Edit..');", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Purchase Request Cant Edit..');", true);
            }
        }
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "11", "3", "OT Hours");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete OT..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
        query = query + "  And Auto_ID='" + e.CommandName.ToString() + "' And Approval_Status='1'";

        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete OT Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable dtdpurchase = new DataTable();

            query = "select Approval_Status as Status from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            query = query + " And Auto_ID='" + e.CommandName.ToString() + "'";

            dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
            string status = dtdpurchase.Rows[0]["Status"].ToString();

            if (status == "" || status == "0")
            {
                DataTable DT = new DataTable();
                query = "Select * from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                query = query + " And Auto_ID='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    //Delete Main Table
                    query = "Delete from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                    query = query + "  And Auot_ID='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    //Delete Main Sub Table
                    query = "Delete from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                    query = query + " And Auto_ID='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    //Delete Main Tools Sub Table
                    query = "Delete from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    query = query + "  And Auto_ID='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    //Delete Main Asset Sub Table
                    query = "Delete from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                    query = query + " And Auto_ID='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('OT Details Deleted Successfully');", true);
                    Load_Data_Enquiry_Grid();
                }
            }
            else if (status == "-1")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('OT Details Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('OT Details put in pending..');", true);
            }
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Auto_ID,MachineID,";
        query = query + "ExistingCode,EmpName,OTDate_Str,OTHrs ";
        query = query + " , Case When Approval_Status='1' Then 'Successfully' When Approval_Status='0' then 'Pending' when Approval_Status='-1' then 'Cancel' End Status ";
        query = query + " From Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
        query = query + " Order by MachineID,OTDate asc ";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            RepeaterItem Item = e.Item;

            Label lblStatus = Item.FindControl("lblStatus") as Label;

            if (lblStatus.Text == "Successfully")
            {
                lblStatus.BackColor = Color.Green;
                lblStatus.ForeColor = Color.White;

            }
            else
            {
                lblStatus.BackColor = Color.Red;
                lblStatus.ForeColor = Color.White;
            }
        }
    }
}