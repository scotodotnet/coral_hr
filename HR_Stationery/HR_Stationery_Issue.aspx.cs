﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

public partial class HR_Stationery_HR_Stationery_Issue : System.Web.UI.Page
{
    public static BALDataAccess objdata = new BALDataAccess();

    public static String CurrentYear1;
    public static int CurrentYear;

    public static string SessionCcode;
    public static string SessionLcode;
    public static string SessionUserID;
    public static string SessionUserName;
    public static string SessionRights;

    string MonthID;
    string Month;
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Stationer Issued Details";
            Load_EmpNo();
            txtIssuedDate.Text = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy");
            txtIssuedBy.Text = SessionUserID;
        }
        Load_Data();
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstStationery_Issued_Details where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    private void Load_EmpNo()
    {
        SSQL = "";
        SSQL = "Select * from Employee_mst where CompCode='" + SessionCcode + "' and  LocCode='" + SessionLcode + "'";
        DataTable dt_ = new DataTable();
        dt_ = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMachineID_IssueTo.DataSource = dt_;
        ddlMachineID_IssueTo.DataTextField = "MachineID";
        ddlMachineID_IssueTo.DataValueField = "ExistingCode";
        ddlMachineID_IssueTo.DataBind();
        ddlMachineID_IssueTo.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlMachineID_IssueTo.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Issued To MachineID');", true);
            ErrFlag = true;
            return;
        }
        if (txtIssuedBy.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Issued By Name');", true);
            ErrFlag = true;
            return;
        }
        if (txtIssuedDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Issued Date');", true);
            ErrFlag = true;
            return;
        }
        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete from MstStationery_Issued_Details where Ccode='" + SessionCcode + "' and lCode='" + SessionLcode + "' and Auto_ID='" + HdnAuto_ID.Value + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            SSQL = "";
            SSQL = "Insert into MstStationery_Issued_Details(Ccode,Lcode,Issued_To_MachineID,Issued_To_ExistingCode,Issued_To_Name,Issued_To_DeptName,Issued_By_Name,Item_Name,Issued_On)";
            SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlMachineID_IssueTo.SelectedItem.Text + "','" + ddlMachineID_IssueTo.SelectedValue + "','" + txtIssuedToName.Text + "','" + txtDeptName.Text + "','" + txtIssuedBy.Text + "'";
            SSQL = SSQL + ", '" + Item_Name.Text + "','" + txtIssuedDate.Text + "')";

            objdata.RptEmployeeMultipleDetails(SSQL);

            if (btnSave.Text == "Save")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Saved Successfully!!!');", true);

            }
            if (btnSave.Text == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Updated Successfully!!!');", true);
            }
            btnClear_Click(sender, e);
            Load_Data();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {

        ddlMachineID_IssueTo.ClearSelection();
        txtIssuedToName.Text = "";
        txtDeptName.Text = "";
        txtIssuedBy.Text = "";
        Item_Name.Text = "";
        txtIssuedDate.Text = "";
        btnSave.Text = "Save";
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstStationery_Issued_Details where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ddlMachineID_IssueTo.SelectedValue = dt.Rows[0]["Issued_To_ExistingCode"].ToString();
            txtIssuedToName.Text = dt.Rows[0]["Issued_To_Name"].ToString();
            txtDeptName.Text = dt.Rows[0]["Issued_To_DeptName"].ToString();
            txtIssuedBy.Text = dt.Rows[0]["Issued_By_Name"].ToString();
            Item_Name.Text = dt.Rows[0]["Item_Name"].ToString();
            txtIssuedDate.Text = dt.Rows[0]["Issued_On"].ToString();
            HdnAuto_ID.Value = e.CommandName.ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from MstStationery_Issued_Details where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Auto_ID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }

    protected void ddlMachineID_IssueTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Employee_mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and MachineID='" + ddlMachineID_IssueTo.SelectedItem.Text + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtIssuedToName.Text = dt.Rows[0]["FirstName"].ToString();
            txtDeptName.Text = dt.Rows[0]["DeptName"].ToString();
        }
    }
}