﻿using Altius.BusinessAccessLayer.BALDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode = "";
    string SessionLcode = "";
    string ProfileImg = "";
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SessionLocationName;
    string SesRoleCode = "";
    string SSQL = "";
    string SessionUser = "";
    string SessionUserId = "";
    bool ErrFlg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["UserID"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            lblUserName_side.Text = Session["Usernmdisplay"].ToString();
            lblUserName.Text = Session["Usernmdisplay"].ToString();
            lblUserEmail.Text = Session["UserId"].ToString();
            lblDesign.Text = Session["UserDesignation"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUser = Session["Usernmdisplay"].ToString();
            SessionUserId = Session["UserId"].ToString();
            SesRoleCode = Session["RoleCode"].ToString();
            ProfileImg = Session["ProfileImg"].ToString();

            ImgUserProfilePicture.ImageUrl = ProfileImg;
            ImgUserProfilePicture_head.ImageUrl = ProfileImg;
            ImgUserProfilePicture_Side.ImageUrl = ProfileImg;
        }

        if (SessionUser != "SCOTO MD")
        {
            All_Module_Disable();
            NonAdmin_User_Rights_Check();
        }
        if (SesRoleCode == "Emp")
        {
            this.FindControl("Link_MyActivity").Visible = true;
        }
      

    }

    private void All_Module_Disable()
    {
        //this.FindControl("btnStores_Link").Visible = false;
        //this.FindControl("btnMain_Lint").Visible = false;
        //this.FindControl("btnProduction").Visible = false;
        //this.FindControl("btnCotton").Visible = false;
        //this.FindControl("btneAlert_Link").Visible = false;
        //this.FindControl("btnePayroll_Link").Visible = false;
        //this.FindControl("btnQuality_Analysis_Link").Visible = false;
    }

    private void Admin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = "1";//= Encrypt("5").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from Admin_Company_Module_MenuHead_Rights ";
        query = query + " Where ModuleID='" + ModuleID + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

        DT_Head = objdata.RptEmployeeMultipleDetails(query);

        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from Admin_Company_Module_MenuForm_Rights ";
            query = query + " Where ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }
    }

    private void NonAdmin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = "3";
        //string ModuleID = Encrypt("1").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();

        //Check with Header Menu
        string SSQL = "Select Distinct Menu_LI_ID,MenuID from Admin_User_Rights where ";
        SSQL = SSQL + "ModuleID = '" + ModuleID + "' And UserName='" + SessionUser + "'";
        SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

        DT_Head = objdata.RptEmployeeMultipleDetails(SSQL);

        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            //this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            SSQL = "Select Distinct Form_LI_ID from Admin_User_Rights where ";
            SSQL = SSQL + " ModuleID = '" + ModuleID + "' And MenuID='" + MenuID + "' And UserName='" + SessionUser + "'";
            SSQL = SSQL + " And ViewRights='1' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }
        //this.FindControl("Rpt_AllCustomerBalance").Visible = false;
    }

    private void Admin_User_Form_Header_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = "3";
        //string ModuleID = Encrypt("1").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string SSQL = "";
        // SSQL = "Select Distinct Menu_LI_ID,MenuID from [" + SessionRights + "]..Company_Module_MenuHead_Rights where ModuleID='" + ModuleID + "'";
        //SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
         
        SSQL = "Select Distinct MHRL.Menu_LI_ID,MHRL.MenuID from Admin_Company_Module_MenuHead_Rights MHRL ";
        SSQL = SSQL + "Inner join Admin_User_Rights MURL on MURL.Menu_LI_ID=MHRL.Menu_LI_ID  ";
        SSQL = SSQL + " And MURL.UserName='" + SessionUser + "' and MURL.ViewRights='1' ";
        SSQL = SSQL + " Where MHRL.ModuleID='" + ModuleID + "' And  MHRL.CompCode='" + SessionCcode + "' ";
        SSQL = SSQL + " And MHRL.LocCode='" + SessionLcode + "'";

        DT_Head = objdata.RptEmployeeMultipleDetails(SSQL);

        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            SSQL = "Select Distinct Form_LI_ID from Admin_Company_Module_MenuForm_Rights Where ModuleID='" + ModuleID + "' ";
            SSQL = SSQL + " And MenuID='" + MenuID + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";

            DT_Form = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                //Modify By Kavin 27_01_2020
                //string SSQL = "";
                DataTable DT_Form_View = new DataTable();

                SSQL = "select ViewRights from Admin_User_Rights where UserName='" + SessionUser + "' and ModuleID='" + ModuleID + "'";
                SSQL = SSQL + " And MenuID ='" + MenuID + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                SSQL = SSQL + " And Form_LI_ID='" + DT_Form.Rows[J]["Form_LI_ID"].ToString() + "'";

                DT_Form_View = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT_Form_View.Rows.Count > 0)
                {
                    if (DT_Form_View.Rows[0]["ViewRights"].ToString() == "1")
                    {
                        this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
                    }
                }
            }
        }
        //this.FindControl("Rpt_AllCustomerBalance").Visible = false;
    }
    private void ALL_Menu_Header_Disable()
    {
        this.FindControl("Link_DashBoard").Visible = false;        
        this.FindControl("Link_Admin_Master").Visible = false;
        this.FindControl("Link_Master").Visible = false;
        this.FindControl("Menu_Head_HR_Master").Visible = false;
        this.FindControl("Menu_Head_HR_Bio_Metric").Visible = false;
        this.FindControl("Menu_Head_HR_Basic_Wages").Visible = false;
        this.FindControl("Menu_Head_HR_Employee_Profile").Visible = false;
        this.FindControl("Menu_Head_HR_Manual_Entry").Visible = false;
        this.FindControl("Menu_Head_HR_Salary_Process").Visible = false;
        this.FindControl("Menu_Head_HR_Bonus_Process").Visible = false;
        this.FindControl("Menu_Head_HR_Reports_ALL").Visible = false;
        this.FindControl("Mnu_Head_Portal").Visible = false;
        this.FindControl("Mnu_Head_Recruitment").Visible = false;


        this.FindControl("Link_Transaction").Visible = false;
        this.FindControl("Link_Inventory").Visible = false;
        this.FindControl("Link_Report").Visible = false;
        this.FindControl("Link_Inventory_Report").Visible = false;
        this.FindControl("Link_Stock_Report").Visible = false;
        this.FindControl("Menu_Head_HR_Training").Visible = false;
        this.FindControl("Menu_Head_HR_Stationery").Visible = false;
        this.FindControl("Menu_Head_OT").Visible = false;
        this.FindControl("Menu_Head_OT").Visible = false;
        this.FindControl("Link_MyActivity").Visible = false;

    }
    private void ALL_Menu_Header_Forms_Disable()
    {
        this.FindControl("Link_DashBoard").Visible = false;

        //Admin Master
        this.FindControl("Mnu_NewUser").Visible = false;
        this.FindControl("Mnu_Company").Visible = false;
        this.FindControl("Mnu_Location").Visible = false;
        this.FindControl("Mnu_UserRights").Visible = false;
        this.FindControl("Mnu_Company_Module_Rights").Visible = false;
        this.FindControl("Mnu_Company_Module_MenuHead_Rights").Visible = false;
        this.FindControl("Mnu_Module_Form_Rights").Visible = false;
        this.FindControl("Mnu_FinYear").Visible = false;
        this.FindControl("Mnu_NumberSetup").Visible = false;
        this.FindControl("Mnu_HR_Report_Rights").Visible = false;

        //Master
        this.FindControl("Mnu_Design").Visible = false;
        this.FindControl("Mnu_UOMType").Visible = false;
        this.FindControl("Mnu_BOMMaster").Visible = false;
        this.FindControl("Mnu_GeneratorModel").Visible = false;
        this.FindControl("Mnu_ProductItem").Visible = false;
        this.FindControl("Mnu_Department").Visible = false;
        this.FindControl("Mnu_DepartmentRequiredBOM").Visible = false;
        this.FindControl("Mnu_ManPowerRequired").Visible = false;
        this.FindControl("Mnu_DeptSpareFix").Visible = false;
        this.FindControl("Mnu_Customer").Visible = false;
        this.FindControl("Mnu_Supplier").Visible = false;
        this.FindControl("Mnu_Employee").Visible = false;
        this.FindControl("Mnu_Transport").Visible = false;
        this.FindControl("Mnu_MaintenanceSchedule").Visible = false;
        this.FindControl("Mnu_Gst").Visible = false;
        this.FindControl("Mnu_BankMaster").Visible = false;
        this.FindControl("Mnu_Machine").Visible = false;
        this.FindControl("Mnu_WasteItem").Visible = false;
        this.FindControl("Mnu_WareHouse").Visible = false;
        this.FindControl("Mnu_AccountType").Visible = false;

        //Transacion
        this.FindControl("Menu_HR_ELCL_Mst").Visible = false;
        this.FindControl("Mnu_TransPurchaseRequest").Visible = false;
        this.FindControl("Mnu_TransPRApprove").Visible = false;
        this.FindControl("Mnu_TransSupplierQuotation").Visible = false;
        this.FindControl("Mnu_TransSQApprove").Visible = false;
        this.FindControl("Mnu_TransSQRateChecking").Visible = false;
        this.FindControl("Mnu_TransPurchaseOrderStand").Visible = false;
        this.FindControl("Mnu_TransPOApproveStand").Visible = false;
        this.FindControl("Mnu_GoodsReceived").Visible = false;
        this.FindControl("Mnu_GoodsApproval").Visible = false;
        this.FindControl("Mnu_GoodsReturn").Visible = false;
         
        //Inventory
        this.FindControl("Mnu_BOMIssue").Visible = false;
        this.FindControl("Mnu_BOMIssueRet").Visible = false;
        this.FindControl("Mnu_GPIN").Visible = false;
        this.FindControl("Mnu_GPOut").Visible = false;

        //Report
        this.FindControl("Mnu_Rpt_Purchase_Request").Visible = false;
        this.FindControl("Mnu_Rpt_Supplier_Quotation").Visible = false;
        this.FindControl("Mnu_Rpt_Purchase_Order").Visible = false;
        this.FindControl("Mnu_Rpt_GoodsRceipt").Visible = false;
        this.FindControl("Mnu_Rpt_RceiptReturn").Visible = false;
       
        //Stock Report
        this.FindControl("Mnu_StkRpt_StkDet").Visible = false;

        //HR All Menus False
        //HR Master
        this.FindControl("Menu_HR_Holiday_Mst").Visible = false;
        this.FindControl("Menu_HR_Category_Mst").Visible = false;
        this.FindControl("Menu_HR_Qualification_Mst").Visible = false;
        this.FindControl("Menu_HR_DeActiveDays_Mst").Visible = false;
        this.FindControl("Menu_HR_PF_ESI_Mst").Visible = false;
        this.FindControl("Menu_HR_Basic_Salary_Category_Mst").Visible = false;
        this.FindControl("Menu_HR_Bonus_Category_Mst").Visible = false;
        
        //HR_BioMetric Download_Mst
        this.FindControl("Menu_HR_BioMetricDownload_Mst").Visible = false;
        
        //Basic Wages
        this.FindControl("Menu_HR_SalaryUpdate_Trans").Visible = false;
        this.FindControl("Menu_HR_SalaryApprove_Trans").Visible = false;
        this.FindControl("Menu_HR_SalaryCorrection_Trans").Visible = false;
        this.FindControl("Menu_HR_SalaryApprove_List_Trans").Visible = false;
        
        //Menu_Head_HR_Employee_Profile
        this.FindControl("Menu_HR_Employee_Profile").Visible = false;
        this.FindControl("Menu_HR_Employee_Approval").Visible = false;
        this.FindControl("Menu_HR_Employee_Approval_List").Visible = false;
        //Manual Entry
        this.FindControl("Menu_HR_Manual_Entry").Visible = false;
        this.FindControl("Menu_HR_Advance_Issue_Entry").Visible = false;
        this.FindControl("Menu_HR_Advance_RePayment_Entry").Visible = false;
        this.FindControl("Rpt_HR_Advance_Report").Visible = false;
        //Salary Process
        this.FindControl("Menu_HR_Allowance_Deduction").Visible = false;
        this.FindControl("Menu_HR_EPay_Transfer").Visible = false;
        this.FindControl("Menu_HR_Attendance_Upload_Process").Visible = false;
        this.FindControl("Menu_HR_Salary_Calculation_Process").Visible = false;
        //Bonus Process
        this.FindControl("Menu_HR_Bonus_Calculation_Process").Visible = false;
        //HR Reports
        this.FindControl("Rpt_HR_Atten_BioMetric_Report").Visible = false;
        this.FindControl("Rpt_HR_Employee_Details_Report").Visible = false;
        this.FindControl("Rpt_HR_Salary_History_Report").Visible = false;
        this.FindControl("Rpt_HR_Attendance_Report").Visible = false;
        this.FindControl("Rpt_HR_PF_And_ESI_Report").Visible = false;
        this.FindControl("Rpt_HR_Payslip_Report").Visible = false;

        //HR Portal

        this.FindControl("Mnu_EmpPortal").Visible = false;

        //HR Recruitment
        this.FindControl("Mnu_Vacancy").Visible = false;
        this.FindControl("Mnu_Candidate").Visible = false;



    }
    protected void btnSignOut_Click(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Update Login set isLoggedIN='FALSE' where UserID='" + Session["UserID"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        //Page.ResolveUrl("Default.aspx");
        //Response.Redirect("Default.aspx");
        Response.Redirect(@"~/Default.aspx");
    }
}
