﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for AdolscentDetailsClass
/// </summary>
public class AdolscentDetailsClass
{
	public AdolscentDetailsClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _Ccode;
    private string _Mode;

    public string Mode
    {
        get { return _Mode; }
        set { _Mode = value; }
    }


    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _ExistingNo;

    public string ExistingNo
    {
        get { return _ExistingNo; }
        set { _ExistingNo = value; }
    }

    private string _MachineID;

    public string MachineID
    {
        get { return _MachineID; }
        set { _MachineID = value; }
    }

    private string _DOB;

    public string DOB
    {
        get { return _DOB; }
        set { _DOB = value; }
    }

    private string _AdlCertificateNo;

    public string AdlCertificateNo
    {
        get { return _AdlCertificateNo; }
        set { _AdlCertificateNo = value; }
    }
    private string _AdlIssuseDate;

    public string AdlIssuseDate
    {
        get { return _AdlIssuseDate; }
        set { _AdlIssuseDate = value; }
    }
    private string _AdlCertificateType;

    public string AdlCertificateType
    {
        get { return _AdlCertificateType; }
        set { _AdlCertificateType = value; }
    }
    private string _AdlNextDueDate;

    public string AdlNextDueDate
    {
        get { return _AdlNextDueDate; }
        set { _AdlNextDueDate = value; }
    }

    private string _AdlRemarks;

    public string AdlRemarks
    {
        get { return _AdlRemarks; }
        set { _AdlRemarks = value; }
    }
}
