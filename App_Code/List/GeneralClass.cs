﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeneralClass
{
    public class EmpList
    {
        public string EmpCode;
        public string EmpName;
    }

    public class LoadYears
    {
        public string years;
    }

    public class EmpSelect
    {
        public string SEmpName;
        public string sEmpDept;

    }

    public class EmpLeaveDet
    {
        public string EmpNo;
        public string EmpName;
        public string EmpDept;
        public string Year;
        public string FDate;
        public string TDate;
        public string EL;
        public string CL;
        public string SL;
    }
}