﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Text;
using System.IO;

/// <summary>
/// Summary description for TransactionNoGenerate
/// </summary>
public class TransactionNoGenerate
{
    

	public TransactionNoGenerate()
	{
		//
		// TODO: Add constructor logic here
		//

        
	}

    public string cls_Trans_Type;
    public string cls_ItemCode;
    public string cls_ItemName;
    public string cls_Add_Qty;
    public string cls_Add_Value;
    public string cls_Minus_Qty;
    public string cls_Minus_Value;
    public string cls_DeptCode;
    public string cls_DeptName;
    public string cls_CostCenterCode;
    public string cls_CostCenterName;
    public string cls_WarehouseCode;
    public string cls_WarehouseName;
    public string cls_ZoneName;
    public string cls_BinName;
    public string cls_Supp_Code;
    public string cls_Supp_Name;
    public string cls_UserID;
    public string cls_UserName;
    public string cls_Stock_Qty;
    public string cls_Stock_Value;
    
    public string Auto_Generate_No_Numbering_Setup(string CompCode, string LocCode, string FormName, string FinYearCode,string ModuleId)
    {
        BALDataAccess objdata = new BALDataAccess();

        string Prefix = "";
        string Fin_Year_Join = "";
        string Last_No = "0";
        string query = "";
        string Final_Transaction_No = "";
        string Zero_Join = "";
        DataTable DT = new DataTable();

        string[] Fin_year_Split = FinYearCode.Split('_');
        string Fin_year = "/" + RightVal(Fin_year_Split[0], 2) + "-" + RightVal(Fin_year_Split[1], 2);

        query = "Select * from MstNumberingSetup where Ccode='" + CompCode + "' And Lcode='" + LocCode + "' And FormName='" + FormName + "'";
        //And Suffix='" + Fin_year + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            Prefix = DT.Rows[0]["Prefix"].ToString();
            Last_No = DT.Rows[0]["EndNo"].ToString();
            Fin_Year_Join = DT.Rows[0]["Suffix"].ToString();

            //Zero Add
            if (Last_No.Length < 4)
            {
                for (int i = Last_No.Length; i <= 4; i++)
                {
                    Zero_Join = Zero_Join + "0";
                }
            }

            Final_Transaction_No = Prefix + Zero_Join + Last_No + Fin_Year_Join;

            //Insert Last No Increment
            string New_Last_No = "";
            New_Last_No = (Convert.ToDecimal(Last_No) + Convert.ToDecimal(1)).ToString();
            query = "Update MstNumberingSetup Set EndNo='" + New_Last_No + "' where Ccode='" + CompCode + "' And Lcode='" + LocCode + "' And FormName='" + FormName + "' And Suffix='" + Fin_year + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }
        else
        {
            Final_Transaction_No = "";
        }
        return Final_Transaction_No;
    }

    public string Auto_Generate_No_Numbering_Setup_Master(string CompCode, string LocCode, string FormName, string FinYearCode,string ModuleId)
    {
        BALDataAccess objdata = new BALDataAccess();

        string Prefix = "";
        string Fin_Year_Join = "";
        string Last_No = "0";
        string query = "";
        string Final_Transaction_No = "";
        string Zero_Join = "";
        DataTable DT = new DataTable();

        string[] Fin_year_Split = FinYearCode.Split('_');
        string Fin_year = "/" + RightVal(Fin_year_Split[0], 2) + "-" + RightVal(Fin_year_Split[1], 2);

        query = "Select * from MstNumberingSetup where Ccode='" + CompCode + "' And Lcode='" + LocCode + "' And ";
        query = query + " FormName ='" + FormName + "' And ModuleId='" + ModuleId + "' ";

        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            Prefix = DT.Rows[0]["Prefix"].ToString();
            Last_No = DT.Rows[0]["EndNo"].ToString();
            Fin_Year_Join = DT.Rows[0]["Suffix"].ToString();

            //Zero Add
            if (Last_No.Length < 4)
            {
                for (int i = Last_No.Length; i <= 4; i++)
                {
                    Zero_Join = Zero_Join + "0";
                }
            }

            Final_Transaction_No = Prefix + Zero_Join + Last_No + Fin_Year_Join;

            //Insert Last No Increment
            string New_Last_No = "";
            New_Last_No = (Convert.ToDecimal(Last_No) + Convert.ToDecimal(1)).ToString();
            query = "Update MstNumberingSetup Set EndNo='" + New_Last_No + "' where Ccode='" + CompCode + "' And Lcode='" + LocCode + "'";
            query = query + " And FormName='" + FormName + "' And ModuleId='" + ModuleId + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }
        else
        {
            Final_Transaction_No = "";
        }
        return Final_Transaction_No;
    }

    public string Leftval(string str, int length)
    {
        string Left_Val_Pass = str.Substring(0, Math.Min(length, str.Length));

        return Left_Val_Pass;
    }

    public string RightVal(string str, int length)
    {
        return str.Substring(str.Length - length, length);
        
    }

    public bool AddRights_Check_OLD(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool Add_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from MstModuleFormUserRights where Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
        query = query + " And UserID='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And AddRights='1'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0) { Add_Rights = true; }

        return Add_Rights;
    }

    //public bool AddRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    //{
    //    BALDataAccess objdata = new BALDataAccess();
    //    bool Add_Rights = false;
    //    string query = "";
    //    DataTable dt = new DataTable();
    //    query = "Select * from TSM_Rights..Company_Module_User_Rights where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
    //    query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
    //    query = query + " And FormName='" + FormName + "' And AddRights='1'";
    //    dt = objdata.RptEmployeeMultipleDetails(query);
    //    if (dt.Rows.Count != 0) { Add_Rights = true; }

    //    return Add_Rights;
    //}

    public bool AddRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool Add_Rights = false;
        string query = "";

        DataTable dt = new DataTable();

        query = "Select * From Admin_User_Rights  where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
        //query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
        query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And AddRights='1'";

        dt = objdata.RptEmployeeMultipleDetails(query);

        if (UserID_Str == "Scoto")
        {
            Add_Rights = true;
        }
        else
        {
            if (dt.Rows.Count != 0)
            {
                Add_Rights = true;
            }
        }

        return Add_Rights;
    }

    //public bool ModifyRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    //{
    //    BALDataAccess objdata = new BALDataAccess();
    //    bool Modify_Rights = false;
    //    string query = "";
    //    DataTable dt = new DataTable();
    //    query = "Select * from TSM_Rights..Company_Module_User_Rights where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
    //    query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
    //    query = query + " And FormName='" + FormName + "' And ModifyRights='1'";
    //    dt = objdata.RptEmployeeMultipleDetails(query);
    //    if (dt.Rows.Count != 0) { Modify_Rights = true; }

    //    return Modify_Rights;
    //}

    public bool ModifyRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool Modify_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from Admin_User_Rights where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
        //query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
        query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And ModifyRights='1'";

        dt = objdata.RptEmployeeMultipleDetails(query);


        if (UserID_Str == "Scoto")
        {
            Modify_Rights = true;
        }
        else
        {
            if (dt.Rows.Count != 0)
            {
                Modify_Rights = true;
            }
        }

        return Modify_Rights;
    }

    public bool ModifyRights_Check_OLD(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool Modify_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from MstModuleFormUserRights where Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
        query = query + " And UserID='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And ModifyRights='1'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0) { Modify_Rights = true; }

        return Modify_Rights;
    }

    //public bool DeleteRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    //{
    //    BALDataAccess objdata = new BALDataAccess();
    //    bool Delete_Rights = false;
    //    string query = "";
    //    DataTable dt = new DataTable();
    //    query = "Select * from TSM_Rights..Company_Module_User_Rights where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
    //    query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
    //    query = query + " And FormName='" + FormName + "' And DeleteRights='1'";
    //    dt = objdata.RptEmployeeMultipleDetails(query);
    //    if (dt.Rows.Count != 0) { Delete_Rights = true; }

    //    return Delete_Rights;
    //}

    public bool DeleteRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool Delete_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from Admin_User_Rights where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
        //query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
        query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And DeleteRights='1'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (UserID_Str == "Scoto")
        {
            Delete_Rights = true;
        }
        else
        {
            if (dt.Rows.Count != 0)
            {
                Delete_Rights = true;
            }
        }

        //if (dt.Rows.Count != 0) { Delete_Rights = true; }

        return Delete_Rights;
    }

    public bool DeleteRights_Check_OLD(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool Delete_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from MstModuleFormUserRights where Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
        query = query + " And UserID='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And DeleteRights='1'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0) { Delete_Rights = true; }

        return Delete_Rights;
    }

    //public bool ViewRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    //{
    //    BALDataAccess objdata = new BALDataAccess();
    //    bool View_Rights = false;
    //    string query = "";
    //    DataTable dt = new DataTable();
    //    query = "Select * from TSM_Rights..Company_Module_User_Rights where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
    //    query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
    //    query = query + " And FormName='" + FormName + "' And ViewRights='1'";
    //    dt = objdata.RptEmployeeMultipleDetails(query);
    //    if (dt.Rows.Count != 0) { View_Rights = true; }

    //    return View_Rights;
    //}


    public bool ViewRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool View_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from Admin_User_Rights where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
        //query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
        query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And ViewRights='1'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (UserID_Str == "Scoto")
        {
            View_Rights = true;
        }
        else
        {
            if (dt.Rows.Count != 0)
            {
                View_Rights = true;
            }
        }

        //if (dt.Rows.Count != 0) { View_Rights = true; }

        return View_Rights;
    }


    public bool ViewRights_Check_OLD(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool View_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from MstModuleFormUserRights where Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
        query = query + " And UserID='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And ViewRights='1'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0) { View_Rights = true; }

        return View_Rights;
    }

    //public bool ApproveRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    //{
    //    BALDataAccess objdata = new BALDataAccess();
    //    bool Approve_Rights = false;
    //    string query = "";
    //    DataTable dt = new DataTable();
    //    query = "Select * from TSM_Rights..Company_Module_User_Rights where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
    //    query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
    //    query = query + " And FormName='" + FormName + "' And ApproveRights='1'";
    //    dt = objdata.RptEmployeeMultipleDetails(query);
    //    if (dt.Rows.Count != 0) { Approve_Rights = true; }

    //    return Approve_Rights;
    //}

    public bool ApproveRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool Approve_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from Admin_User_Rights where CompCode ='" + Ccode + "' And LocCode='" + Lcode + "'";
        //query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
        query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And ApproveRights='1'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (UserID_Str == "Scoto")
        {
            Approve_Rights = true;
        }
        else
        {
            if (dt.Rows.Count != 0)
            {
                Approve_Rights = true;
            }
        }

        //if (dt.Rows.Count != 0) { Approve_Rights = true; }

        return Approve_Rights;
    }

    public bool ApproveRights_Check_OLD(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool Approve_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from MstModuleFormUserRights where Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
        query = query + " And UserID='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And ApproveRights='1'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0) { Approve_Rights = true; }

        return Approve_Rights;
    }

    //public bool PrintRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    //{
    //    BALDataAccess objdata = new BALDataAccess();
    //    bool Print_Rights = false;
    //    string query = "";
    //    DataTable dt = new DataTable();
    //    query = "Select * from TSM_Rights..Company_Module_User_Rights where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
    //    query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
    //    query = query + " And FormName='" + FormName + "' And PrintRights='1'";
    //    dt = objdata.RptEmployeeMultipleDetails(query);
    //    if (dt.Rows.Count != 0) { Print_Rights = true; }

    //    return Print_Rights;
    //}


    public bool PrintRights_Check(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool Print_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from Admin_User_Rights where CompCode='" + Ccode + "' And LocCode='" + Lcode + "'";
        //query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + Encrypt(ModuleID) + "' And MenuID='" + Encrypt(MenuID) + "'";
        query = query + " And UserName='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And PrintRights='1'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (UserID_Str == "Scoto")
        {
            Print_Rights = true;
        }
        else
        {
            if (dt.Rows.Count != 0)
            {
                Print_Rights = true;
            }
        }

        //if (dt.Rows.Count != 0) { Print_Rights = true; }

        return Print_Rights;
    }

    public bool PrintRights_Check_OLD(string Ccode, string Lcode, string UserID_Str, string MenuID, string ModuleID, string FormName)
    {
        BALDataAccess objdata = new BALDataAccess();
        bool Print_Rights = false;
        string query = "";
        DataTable dt = new DataTable();
        query = "Select * from MstModuleFormUserRights where Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
        query = query + " And UserID='" + UserID_Str + "' And ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
        query = query + " And FormName='" + FormName + "' And PrintRights='1'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0) { Print_Rights = true; }

        return Print_Rights;
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }


    public void Stock_Ledger_And_Current_Stock_Add(string Ccode, string Lcode, string FinYearCode, string FinYearVal, string Trans_No, string Trans_Date)
    {
        BALDataAccess objdata = new BALDataAccess();
        string query = "";
        DataTable dt_check = new DataTable();

        query = "Select * from Stock_Ledger_All where Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
        query = query + " And FinYearCode='" + FinYearCode + "' And Trans_No='" + Trans_No + "'";
        query = query + " And ItemCode='" + cls_ItemCode + "'";
        dt_check = objdata.RptEmployeeMultipleDetails(query);
        if (dt_check.Rows.Count != 0)
        {
            query = "Delete from Stock_Ledger_All where Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
            query = query + " And FinYearCode='" + FinYearCode + "' And Trans_No='" + Trans_No + "'";
            query = query + " And ItemCode='" + cls_ItemCode + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }
        DateTime TranDate = Convert.ToDateTime(Trans_Date);

        //Insert Into Stock_Ledger_All
        query = "Insert Into Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
        query = query + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Add_Qty,Add_Value,Minus_Qty,Minus_Value,WarehouseCode,WarehouseName,";
        query = query + " ZoneName,BinName,Supp_Code,Supp_Name,UserID,UserName) Values('" + Ccode + "','" + Lcode + "',";
        query = query + " '" + FinYearCode + "','" + FinYearVal + "','" + Trans_No + "','" + TranDate.ToString("MM/dd/yyyy") + "','" + Trans_Date + "',";
        query = query + " '" + cls_Trans_Type + "','" + cls_DeptCode + "','" + cls_DeptName + "','" + cls_CostCenterCode + "','" + cls_CostCenterName + "','" + cls_ItemCode + "','" + cls_ItemName + "','" + cls_Add_Qty + "',";
        query = query + " '" + cls_Add_Value + "','" + cls_Minus_Qty + "','" + cls_Minus_Value + "','" + cls_WarehouseCode + "',";
        query = query + " '" + cls_WarehouseName + "','" + cls_ZoneName + "','" + cls_BinName + "','" + cls_Supp_Code + "',";
        query = query + " '" + cls_Supp_Name + "','" + cls_UserID + "','" + cls_UserName + "')";
        objdata.RptEmployeeMultipleDetails(query);

        //Get Stock Qty
        query = "Select isnull((sum(Add_Qty)-sum(Minus_Qty)),0) as Stock_Qty,isnull((sum(Add_Value)-sum(Minus_Value)),0) as Stock_Value";
        query = query + " from Stock_Ledger_All where Ccode='" + Ccode + "' And Lcode='" + Lcode + "' And FinYearCode='" + FinYearCode + "'";
        query = query + " And ItemCode='" + cls_ItemCode + "'";
        dt_check = objdata.RptEmployeeMultipleDetails(query);
        if (dt_check.Rows.Count != 0)
        {
            cls_Stock_Qty = dt_check.Rows[0]["Stock_Qty"].ToString();
            cls_Stock_Value = dt_check.Rows[0]["Stock_Value"].ToString();
        }
        else
        {
            cls_Stock_Qty = "0";
            cls_Stock_Value = "0";
        }

        //Check Current Stock item
        query = "Select * from Stock_Current_All where Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
        query = query + " And FinYearCode='" + FinYearCode + "' And ItemCode='" + cls_ItemCode + "'";
        dt_check = objdata.RptEmployeeMultipleDetails(query);
        if (dt_check.Rows.Count != 0)
        {
            query = "Delete from Stock_Current_All where Ccode='" + Ccode + "' And Lcode='" + Lcode + "'";
            query = query + " And FinYearCode='" + FinYearCode + "' And ItemCode='" + cls_ItemCode + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }
        //Insert Current Stock
        query = "Insert Into Stock_Current_All(Ccode,Lcode,FinYearCode,FinYearVal,ItemCode,ItemName,Stock_Qty,Stock_Value,DeptCode,DeptName,";
        query = query + " WarehouseCode,WarehouseName,ZoneName,BinName,UserID,UserName) Values('" + Ccode + "',";
        query = query + " '" + Lcode + "','" + FinYearCode + "','" + FinYearVal + "','" + cls_ItemCode + "',";
        query = query + " '" + cls_ItemName + "','" + cls_Stock_Qty + "','" + cls_Stock_Value + "',";
        query = query + " '" + cls_DeptCode + "','" + cls_DeptName + "',";
        query = query + " '" + cls_WarehouseCode + "','" + cls_WarehouseName + "',";
        query = query + " '" + cls_ZoneName + "','" + cls_BinName + "','" + cls_UserID + "','" + cls_UserName + "')";
        objdata.RptEmployeeMultipleDetails(query);
    }
}
