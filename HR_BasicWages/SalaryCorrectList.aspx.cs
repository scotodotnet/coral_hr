﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class HR_BasicWages_SalaryCorrectList : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        //SessionRights = Session["Rights"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Salary Update";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_WagesType();
        }
        Load_Data();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("ExistingCode", typeof(string)));
        dt.Columns.Add(new DataColumn("FirstName", typeof(string)));
        dt.Columns.Add(new DataColumn("DOJ", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("Payroll_EmpNo", typeof(string)));
        dt.Columns.Add(new DataColumn("OLD_Salary", typeof(string)));
        dt.Columns.Add(new DataColumn("New_Salary", typeof(string)));
        dt.Columns.Add(new DataColumn("OLD_Alllowance2", typeof(string)));
        dt.Columns.Add(new DataColumn("Alllowance2", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();

        string MachineID = e.CommandName.ToString();
        string ExstCode = e.CommandArgument.ToString();
        string[] Existing = ExstCode.Split(',');
        string ExisitingCode = Existing[1].ToString();

        SSQL = "Select * from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And (Approve_Status='Cancel' or Approve_Status='Rechecked') and Wages='" + ddlWages.SelectedItem.Text + "'";
        SSQL = SSQL + " And MachineID='" + MachineID + "' And ExistingCode='" + ExisitingCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            SSQL = "Delete from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And (Approve_Status='Cancel' or Approve_Status='Rechecked') and Wages='" + ddlWages.SelectedItem.Text + "'";
            SSQL = SSQL + " And MachineID='" + MachineID + "' And ExistingCode='" + ExisitingCode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Salary Update Details Deleted Successfully.!');", true);
            btnSearch_Click(sender, e);
        }
    }

    protected void GridCancelEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();
        string Machine_ID_Str = "";
        string Token_No_Str = "";
        string Payroll_Emp_No = "";
        string[] PayrollStr;
        PayrollStr = e.CommandArgument.ToString().Split(',');
        Payroll_Emp_No = PayrollStr[0].ToString();
        Token_No_Str = PayrollStr[1].ToString();
        Machine_ID_Str = e.CommandName.ToString();

        lblEmpNo.Text = Machine_ID_Str;
        lblTokenNo.Text = Token_No_Str;

        string Salary_Type = "";
        if (chkNewWages.Checked == true)
        {
            Salary_Type = "New";
        }
        else
        {
            Salary_Type = "Increment";
        }

        DataTable Sal_Ds = new DataTable();
        SSQL = "Select * from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And (Approve_Status='Cancel' or Approve_Status='Rechecked') and Wages='" + ddlWages.SelectedItem.Text + "'";
        SSQL = SSQL + " And Salary_Type='" + Salary_Type + "' And MachineID='" + Machine_ID_Str + "' And ExistingCode='" + Token_No_Str + "'";
        SSQL = SSQL + " Order by ExistingCode Asc";
        Sal_Ds = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Sal_Ds.Rows.Count != 0)
        {
            txtReason.Text = Sal_Ds.Rows[0]["Reason"].ToString();
            txtAllowance_Amt.Text = Sal_Ds.Rows[0]["Alllowance2"].ToString();

        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        string Salary_Type = "";
        string Approve_Status = "";

        if (chkNewWages.Checked == false && chkOldWages.Checked == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Salary Type.!');", true);
        }
        if (chkNewWages.Checked == true)
        {
            Salary_Type = "New";
        }
        else
        {
            Salary_Type = "Increment";
        }

        DataTable Sal_Ds = new DataTable();
        SSQL = "Select * from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And (Approve_Status='Cancel' or Approve_Status='Rechecked') and Wages='" + ddlWages.SelectedItem.Text + "'";
        SSQL = SSQL + " And Salary_Type='" + Salary_Type + "'";
        SSQL = SSQL + " Order by ExistingCode Asc";
        Sal_Ds = objdata.RptEmployeeMultipleDetails(SSQL);

        Repeater1.DataSource = Sal_Ds;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Sal_Ds;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string Salary_Type = "";
        DataTable DT = new DataTable();

        if (lblEmpNo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Employee to Cancel.!');", true);
        }
        if (txtReason.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the reason.!');", true);
        }
        if (txtSalary.Text == "" || txtSalary.Text == "0" || txtSalary.Text == "0.0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Salary.!');", true);
        }

        if (chkNewWages.Checked == true)
        {
            Salary_Type = "New";
        }
        else
        {
            Salary_Type = "Increment";
        }
        if (!ErrFlag)
        {
            //update Employee Check
            SSQL = "Select * from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And ExistingCode='" + lblTokenNo.Text + "' And MachineID='" + lblEmpNo.Text + "' ";
            SSQL = SSQL + " And (Approve_Status='Cancel' or Approve_Status='Rechecked')";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                //Re-Update Salary
                SSQL = "Update Salary_Update_Det set New_Salary='" + txtSalary.Text + "',Alllowance2='" + txtAllowance_Amt.Text + "',Approve_Status='Rechecked',Reason='" + txtReason.Text + "'";
                SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And ExistingCode='" + lblTokenNo.Text + "' And MachineID='" + lblEmpNo.Text + "'";
                SSQL = SSQL + " And (Approve_Status='Cancel' or Approve_Status='Rechecked')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                lblEmpNo.Text = "";
                lblTokenNo.Text = "";
                txtSalary.Text = "0";
                txtReason.Text = "";
                txtAllowance_Amt.Text = "0";
                btnSearch_Click(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Correct Employee Details below list of Rows.!');", true);
            }

        }

    }
    protected void chkNewWages_CheckedChanged(object sender, EventArgs e)
    {
        if (chkNewWages.Checked == true)
        {
            chkOldWages.Checked = false;

        }
    }
    protected void chkOldWages_CheckedChanged(object sender, EventArgs e)
    {
        if (chkOldWages.Checked == true)
        {
            chkNewWages.Checked = false;
        }
    }
}
